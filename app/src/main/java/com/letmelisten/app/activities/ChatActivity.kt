package com.letmelisten.app.activities

import android.annotation.SuppressLint
import android.app.*
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.media.RingtoneManager
import android.os.Build
import android.os.Bundle
import android.os.CountDownTimer
import android.os.Handler
import android.util.Log
import android.view.*
import android.widget.ImageView
import android.widget.RemoteViews
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.lifecycle.ViewModelProviders
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.google.gson.Gson
import com.iarcuschin.simpleratingbar.SimpleRatingBar
import com.letmelisten.app.LetMeListenApplication
import com.letmelisten.app.Model.*
import com.letmelisten.app.R
import com.letmelisten.app.adapters.ChatAdapter
import com.letmelisten.app.utils.*
import com.letmelisten.app.viewmodels.StripePaymentViewModel
import io.socket.client.Socket
import io.socket.emitter.Emitter
import kotlinx.android.synthetic.main.activity_chat.*
import kotlinx.android.synthetic.main.activity_wallet.*
import org.json.JSONException
import org.json.JSONObject
import rjsv.circularview.CircleView
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.math.roundToInt


class ChatActivity : BaseActivity() {
    @BindView(R.id.chatRV)
    lateinit var chatRV: RecyclerView
    var total_cost: String = ""
    var NOTIFICATION_ID = 1
    var expandedView: RemoteViews? = null
    var new_time: Long = 0
    var time: String = ""
    var price: String = ""
    private var receiver: BroadcastReceiver? = null
    var userName: String = ""
    var advisorName: String = ""
    var advisorId: String = ""
    var rating: String = ""
    var roomId: String = ""
    private var newTime = 0
    var planId: String = ""
    var newWalletAmount: Int = 0
    var speaker_value: Long = 0
    var perMinCost: String = ""
    var priceNum: Int = 0
    var new_value: Long = 0
    var speakerr_value: Long = 0
    var totalMoney: Int = 0
    var timer: CountDownTimer? = null
    var mPerPage: Int = 1000
    var mPageNo: Int = 1
    private var mSocket: Socket? = null
    var alertRatingDialog: Dialog? = null
    var mIsPagination = true
    lateinit var chatAdapter: ChatAdapter
    private var viewModel: StripePaymentViewModel? = null
    var timeStop: String = ""
    var currentTime: Long = 0

    var mMsgArrayList: ArrayList<DataItem?>? = ArrayList()
    var mHandler: Handler = Handler()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat)
        ButterKnife.bind(this)
        LetMeListenPreferences().writeString(
            mActivity,
            CURRENT_TIME,
            ""
        )
        viewModel = ViewModelProviders.of(this).get(StripePaymentViewModel::class.java)
        setStatusBar(mActivity)
        //  setEditTextFocused(mActivity, editChatET)

        //Disable send mssg editView
        editChatET.isEnabled = false
        showAlertDialog(mActivity, getString(R.string.chat_start_in_15_sec))
        //editChatET.isEnabled = false

        if (LetMeListenPreferences().readString(
                mActivity,
                APP_STATUS,
                ""
            ).equals("app_killed")
        ) {
            executeEndSessionRequest()
            LetMeListenPreferences().writeString(
                mActivity,
                APP_STATUS,
                ""
            )
        } else {
            getIntentData()
            setTopSwipeRefresh()
            // setReceiverMethod()
        }
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        return if (keyCode == KeyEvent.KEYCODE_BACK) {
            showRatingDialog(mActivity)
            false
        } else super.onKeyDown(keyCode, event)
    }

    private fun setReceiverMethod() {

        receiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                val call_type = intent.extras!!.getString("call_type")
                if (call_type.equals("app_killed", ignoreCase = true)) {
                    executeEndSessionRequest()
                }
            }
        }
    }


    override fun onPause() {
        super.onPause()
        if (alertRatingDialog != null && alertRatingDialog!!.isShowing()) {
            alertRatingDialog!!.dismiss();
        }
        // unregisterReceiver(receiver)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onResume() {
        super.onResume()
//        LocalBroadcastManager.getInstance(mActivity)
//            .registerReceiver(object : BroadcastReceiver() {
//                override fun onReceive(context: Context, intent: Intent) {
//                    val status = intent.getStringExtra("Status")
//                    if (status == "editImage") {
//                        showToast(mActivity, "Testttttt")
//                        LetMeListenPreferences().writeString(
//                            mActivity,
//                            APP_STATUS,
//                            "app_killed"
//                        )
//                    }
//                }
//            }, IntentFilter("changeImage"))


        var time = LetMeListenPreferences().readString(
            mActivity,
            CURRENT_TIME,
            ""
        )
        if (time != null && !time.equals("")) {
            val difference: Long = System.currentTimeMillis() - time!!.toLong()
            getDate(difference)

        }


        if (timeStop.equals("timestop")) {
            initCountDown(new_time)
        }

    }
//    private fun getDate(difference: Long) {
//        val simpleDateFormat = SimpleDateFormat("dd/MM/yyyy hh:mm:ss.SSS")
//        val dateString: String = simpleDateFormat.format(difference)
//        Log.e(TAG,"TIME::"+dateString)
//
//    }

    fun getDate(strDateTime: Long): String? {
        //convert seconds to milliseconds
        val date = Date(strDateTime * 1000)
        // format of the date
        val jdf = SimpleDateFormat("HH:mm aa")
        return jdf.format(date)
    }

    override fun onDestroy() {
        super.onDestroy()
        LetMeListenPreferences().writeString(
                            mActivity,
            TO_DELETE_ROOMID,
                            roomId
                        )
    }
    override fun onBackPressed() {
        super.onBackPressed()
        mainViewRL.isFocusableInTouchMode = true
        mainViewRL.requestFocus()
        mainViewRL.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                showRatingDialog(mActivity)
                return@OnKeyListener true
            }
            false
        })
    }

    private fun setHandlerForSec() {
        Handler().postDelayed({
            editChatET.isEnabled = true
            timeLeftRL.visibility = View.VISIBLE
            initCountDown(TimeUnit.MINUTES.toMillis(time.toLong()))
        }, 13000)
    }

    private fun initCountDown(long: Long) {
        currentTime = System.currentTimeMillis()
        LetMeListenPreferences().writeString(
            mActivity,
            CURRENT_TIME,
            currentTime.toString()
        )
        timer = object : CountDownTimer(long, 1000) {

            override fun onTick(long: Long) {
                txtTimeLeftTV.text = "-" + String.format(
                    "%02d : %02d",
                    TimeUnit.MILLISECONDS.toMinutes(long),
                    TimeUnit.MILLISECONDS.toSeconds(long) -
                            TimeUnit.MINUTES.toSeconds(
                                TimeUnit.MILLISECONDS.toMinutes(
                                    long
                                )
                            )
                )
                speakerr_value = long

                new_time = speakerr_value
            }

            override fun onFinish() {
                // txtTimeLeftTV.text = "done!"
                txtChatEndedTV.visibility = View.VISIBLE
                addmoreRL.visibility = View.VISIBLE
                timeLeftRL.visibility = View.GONE
                editChatET.isEnabled = false
                //   showRatingDialog(mActivity)
            }
        }.start()
    }


    private fun getIntentData() {
        if (intent != null) {
            time = intent.getStringExtra(CHAT_TIME).toString()
            price = intent.getStringExtra(PRICE).toString()
            advisorId = intent.getStringExtra(ADVISOR_ID).toString()
            planId = intent.getStringExtra(PLAN_ID).toString()
            roomId = intent.getStringExtra(ROOM_ID).toString()
            perMinCost = intent.getStringExtra(PER_MIN_COST).toString()
            userName = intent.getStringExtra(USER_NAME).toString()
            advisorName = intent.getStringExtra(ADVISOR_NAME).toString()
            chatUserTV.text = advisorName
            Log.e("TAG", "TIME::" + time)
            setHandlerForSec()
            // initCountDown(TimeUnit.MINUTES.toMillis(time.toLong()))
            setUpSocketData()
        }
    }

    @OnClick(
        R.id.backChatRL,
        R.id.CallRL,

        R.id.addmoreRL,
        R.id.sendRL
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            //R.id.backChatRL -> onBackPressed()
            R.id.backChatRL -> showRatingDialog(mActivity)
            R.id.CallRL -> performCallClick()
            R.id.addmoreRL -> performAddMoreTimeClick()
            R.id.sendRL -> performSendMessageClick()

        }
    }

    private fun performSendMessageClick() {

        if (isNetworkAvailable(mActivity)) {
            if (isValidate()) {
                sendRL.isEnabled = false
                executeSendMesagesRequest()
            }
        } else {
            showToast(mActivity, getString(R.string.internet_connection_error))
        }

    }

    /*
     * Set up validations for Sign In fields
     * */
    private fun isValidate(): Boolean {
        var flag = true
        if (editChatET.text.toString().trim { it <= ' ' } == "") {
            flag = false
        }
        return flag
    }


    private fun performAddMoreTimeClick() {
        if (isWalletValidate()) {
            showTimerDialog(mActivity)
        }

    }

    open fun showTimerDialog(context: Context) {
        val alertDialog = Dialog(context)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.bottomsheet_timer)
        alertDialog.setCanceledOnTouchOutside(true)
        alertDialog.setCancelable(true)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val window = alertDialog.window
        val wlp = window!!.attributes
        wlp.gravity = Gravity.BOTTOM
        val imgCancelIV: ImageView = alertDialog.findViewById(R.id.imgCancelIV)
        val circleView: CircleView = alertDialog.findViewById(R.id.circle_view)
        val txtAddTV: TextView = alertDialog.findViewById(R.id.txtAddTV)
        imgCancelIV.setOnClickListener {
            alertDialog.dismiss()
        }

        txtAddTV.setOnClickListener {
            speaker_value = TimeUnit.MINUTES.toMillis(circleView.progressValue.toLong())
            timer!!.cancel()

            //   new_time = speakerr_value + speaker_value
            txtChatEndedTV.visibility = View.GONE
            editChatET.isEnabled = true
            // priceNum = Integer.parseInt(perMinCost.substring(1))
            priceNum = Integer.parseInt(perMinCost)

            newTime = circleView.progressValue.roundToInt()
            totalMoney = priceNum * newTime
            executeAddMoreTimeApi()


            alertDialog.dismiss()
        }
        alertDialog.show()
    }


    private fun mParams(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["roomId"] = roomId
        mMap["perPage"] = "" + mPerPage
        mMap["pageNo"] = "" + mPageNo
        Log.e("params", "**PARAM**$mMap")
        return mMap
    }

    private fun executeGetAllMegesRequest() {
        mSwipeRefreshSR.isRefreshing = true
        viewModel?.getChatMessageData(mActivity, mParams(), mHeaderParam())?.observe(this,
            androidx.lifecycle.Observer<ChatMessagesModel?> { mSessionpojo ->
                if (mSessionpojo.status == 1) {
                    mSwipeRefreshSR.isRefreshing = false
                    dismissProgressDialog()
                    setUserPrefilled(mSessionpojo)
                    for (i in 0..mSessionpojo.data!!.size - 1) {
                        if (mSessionpojo.userDetails?.userId.equals(getLoggedInUserID())) {
                            mSessionpojo.data.get(i)!!.viewType = RIGHT_VIEW_HOLDER
                        } else {
                            mSessionpojo.data.get(i)!!.viewType = LEFT_VIEW_HOLDER
                        }

                    }
                    if (mSessionpojo.userDetails?.avaliableCall.equals("1")) //Call status on of advisor
                    {
                        CallRL.visibility = View.VISIBLE
                    } else {
                        CallRL.visibility = View.GONE
                    }
                    mMsgArrayList!!.addAll(0, mSessionpojo.data)
                    initRecylerView()
                    // chatAdapter.notifyDataSetChanged()
                    if (mPageNo == 1) {
                        chatRV.scrollToPosition(mMsgArrayList!!.size - 1)
                    }
                } else if (mSessionpojo?.status == 0) {
                    setUserPrefilled(mSessionpojo)
                    mSwipeRefreshSR.isRefreshing = false
                    // chatAdapter.notifyDataSetChanged()
                    initRecylerView()
                }

            })
    }

    fun setUserPrefilled(mSessionpojo: ChatMessagesModel) {
        if (mSessionpojo.userDetails?.avaliableStatus.equals("0")) {
            userStatusIV.setImageResource(R.drawable.online)
        } else {
            userStatusIV.setImageResource(R.drawable.online)
        }
//        chatUserTV.text =
//            mSessionpojo.userDetails?.firstName + " " + mSessionpojo.userDetails?.lastName

    }


    private fun mSendParams(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["roomId"] = roomId
        mMap["message"] = editChatET.text.toString().trim()
        Log.e("params", "**PARAM**$mMap")
        return mMap
    }

    private fun executeSendMesagesRequest() {
        progressBarNew.visibility = View.VISIBLE
        sendIV.visibility = View.GONE
        viewModel?.getSendMessageData(mActivity, mSendParams(), mHeaderParam())?.observe(this,
            androidx.lifecycle.Observer<SendMsgModel?> { mModel ->
                if (mModel.status == 1) {
                    sendRL.isEnabled = true
                    progressBarNew.visibility = View.GONE
                    sendIV.visibility = View.VISIBLE
                    dismissProgressDialog()
                    mModel.data!!.viewType = RIGHT_VIEW_HOLDER
                    mMsgArrayList!!.add(mModel.data)
                    chatAdapter.notifyDataSetChanged()
                    scrollToBottom()
                    // perform the sending message attempt.
                    val gson = Gson()
                    val mOjectString = gson.toJson(mModel.data)
                    Log.e(TAG, "****Msg****" + mOjectString)
                    mSocket!!.emit("newMessage", roomId, mOjectString)
                    editChatET.setText("")
                } else if (mModel?.status == 0) {
                    sendRL.isEnabled = true
                    dismissProgressDialog()
                    showToast(mActivity, mModel.message)
                }

            })
    }

    private fun scrollToBottom() {
        chatRV.scrollToPosition(chatAdapter.itemCount - 1)
    }

    private fun setTopSwipeRefresh() {
        // mSwipeRefreshSR.setColorSchemeColors(resources.getColor(R.color.colorGreen),resources.getColor(R.color.colorBlack),resources.getColor(R.color.colorRed));
        mSwipeRefreshSR.setOnRefreshListener {
            if (mIsPagination == true) {
                ++mPageNo
                executeGetAllMegesRequest()
            } else {
                mSwipeRefreshSR.isRefreshing = false
            }

        }
    }


    private fun mHeaderParam(): HashMap<String, String> {
        val headers = HashMap<String, String>()
        mActivity.let {
            LetMeListenPreferences().readString(it, TOKEN, null)?.let {
                headers.put(
                    "Token",
                    it
                )
            }
        }
        Log.e(
            TAG,
            "**Token**${mActivity.let { LetMeListenPreferences().readString(it, TOKEN, null) }}"
        )
        return headers
    }


    private fun performCallClick() {
        //   showToast(mActivity, getString(R.string.coming_soon))
//        val intent = Intent(mActivity, CallingActivity::class.java)
//        intent.putExtra("time", new_time)
//        intent.putExtra("price", price)
//        intent.putExtra("advisorId", advisorId)
//        intent.putExtra(ROOM_ID, roomId)
//        startActivity(intent)
    }

    private fun initRecylerView() {
        val layoutManager: RecyclerView.LayoutManager =
            LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false)
        chatRV.layoutManager = layoutManager
        chatAdapter = ChatAdapter(mActivity, mMsgArrayList)
        chatRV.adapter = chatAdapter

    }

    private fun setUpSocketData() {
        val app: LetMeListenApplication = application as LetMeListenApplication
        mSocket = app.getSocket()
        mSocket!!.emit("ConncetedChat", roomId)
        mSocket!!.on(Socket.EVENT_CONNECT, onConnect)
        mSocket!!.on(Socket.EVENT_DISCONNECT, onDisconnect)
        mSocket!!.on(Socket.EVENT_CONNECT_ERROR, onConnectError)
        mSocket!!.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError)
        mSocket!!.on("newMessage", onNewMessage)
        mSocket!!.on("ChatStatus", onUserJoined)
        mSocket!!.on("leaveChat", onUserLeft)
        mSocket!!.on("type", onTyping)
        mSocket!!.on("stop typing", onStopTyping)
        mSocket!!.connect()
        executeGetAllMegesRequest()
    }


    /*
    * Socket Chat Implementations:
    * */

    private val onConnect = Emitter.Listener {
        runOnUiThread(
            Runnable { })
    }
    private val onDisconnect = Emitter.Listener {
        runOnUiThread(
            Runnable { })
    }
    private val onConnectError = Emitter.Listener {
        runOnUiThread(Runnable { Log.e(TAG, "Error connecting") })
    }

    private val onNewMessage = Emitter.Listener { args ->
        runOnUiThread(Runnable {
            try {
                Log.e(TAG, "****MessageObject****" + args.get(1).toString())
                var value: String = args.get(1).toString()
                if (value.contains("{")) {
                    val mGson = Gson()
                    val mChatMessageData: DataItem =
                        mGson.fromJson(args.get(1).toString(), DataItem::class.java)
                    if (mChatMessageData.userId!!.equals(getLoggedInUserID())) {
                        mChatMessageData.viewType = RIGHT_VIEW_HOLDER
                    } else {
                        mChatMessageData.viewType = LEFT_VIEW_HOLDER
                    }
                    mMsgArrayList!!.add(mChatMessageData)
                    chatAdapter.notifyDataSetChanged()
                    editChatET.setText("")
                    scrollToBottom()
                } else if (value.equals("endSession")) {
                    finish()
                } else {
                    timer!!.cancel()
                    initCountDown(speakerr_value + TimeUnit.MINUTES.toMillis(value.toLong()))
                }


            } catch (e: Exception) {
                e.printStackTrace()
            }
        })
    }
    private val onUserJoined = Emitter.Listener { args ->
        runOnUiThread(Runnable {
            var mJsonData: JSONObject? = null
            try {
                mJsonData = JSONObject(args[1].toString())
            } catch (e: JSONException) {
                e.printStackTrace()
            }
        })
    }
    private val onUserLeft = Emitter.Listener {
        runOnUiThread(
            Runnable { })
    }
    private val onTyping =
        Emitter.Listener { args -> runOnUiThread(Runnable { val isTyping = args[1] as Boolean }) }
    private val onStopTyping = Emitter.Listener {
        runOnUiThread(
            Runnable { })
    }
    private val onTypingTimeout = Runnable { }


    override fun onStop() {
        super.onStop()
        LetMeListenPreferences().writeString(
            mActivity,
            TO_DELETE_ROOMID,
            ROOM_ID
        )
    }
    @SuppressLint("RemoteViewLayout")
    private fun sendNotification(mTitle: String, message: String) {


        var intent: Intent? = null
        var pendingIntent: PendingIntent? = null
        intent = Intent(this, HomeActivity::class.java)
        pendingIntent = PendingIntent.getActivity(
            this, 0, intent,
            PendingIntent.FLAG_ONE_SHOT
        )

        expandedView = RemoteViews(mActivity.packageName, R.layout.notification_chat)


        timer = object : CountDownTimer(speaker_value, 1000) {

            override fun onTick(long: Long) {
                expandedView!!.setTextViewText(
                    R.id.txtMessageTimeTV, "-" + String.format(
                        "%02d : %02d",
                        TimeUnit.MILLISECONDS.toMinutes(long),
                        TimeUnit.MILLISECONDS.toSeconds(long) -
                                TimeUnit.MINUTES.toSeconds(
                                    TimeUnit.MILLISECONDS.toMinutes(
                                        long
                                    )
                                )
                    )
                );
                speakerr_value = long

                new_time = speakerr_value
            }

            override fun onFinish() {
                // txtTimeLeftTV.text = "done!"
                txtChatEndedTV.visibility = View.VISIBLE
                addmoreRL.visibility = View.VISIBLE
                timeLeftRL.visibility = View.GONE
                editChatET.isEnabled = false
                //   showRatingDialog(mActivity)
            }
        }.start()


        val channelId = getString(R.string.default_notification_channel_id)
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder: NotificationCompat.Builder =
            NotificationCompat.Builder(this, channelId)
                .setSmallIcon(R.drawable.ic_notify_app)
                .setColor(getResources().getColor(R.color.colorGreen))
                .setContentTitle(mTitle)
                .setContentText(initCountDown(speaker_value).toString())
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setCustomContentView(expandedView)
                .setCustomBigContentView(expandedView)
                .setContentIntent(pendingIntent)
        val notificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            if (notificationManager != null) {
                val channel = NotificationChannel(
                    channelId, getString(R.string.default_notification_channel_name),
                    NotificationManager.IMPORTANCE_DEFAULT
                )
                channel.enableVibration(true)
                notificationManager.createNotificationChannel(channel)
            }
        }
        if (notificationManager != null) {
            notificationManager.notify(
                NOTIFICATION_ID,
                notificationBuilder.build()
            )
        }
    }

    /*
     *
     * Error Alert Dialog
     * */
    fun showRatingDialog(mActivity: Activity?) {
        alertRatingDialog = Dialog(mActivity!!)
        alertRatingDialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertRatingDialog!!.setContentView(R.layout.rating_dialog)
        alertRatingDialog!!.setCanceledOnTouchOutside(true)
        alertRatingDialog!!.setCancelable(true)
        alertRatingDialog!!.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val window = alertRatingDialog!!.window
        val wlp = window!!.attributes
        wlp.gravity = Gravity.CENTER
        // set the custom dialog components - text, image and button
        val txtSubmitTV = alertRatingDialog!!.findViewById<TextView>(R.id.txtSubmitTV)
        val ratingbarRB = alertRatingDialog!!.findViewById<SimpleRatingBar>(R.id.ratingbarRB)
        val btnDismiss = alertRatingDialog!!.findViewById<ImageView>(R.id.btnDismiss)
        val btnCancel = alertRatingDialog!!.findViewById<TextView>(R.id.txtCancelTV)


        btnCancel.setOnClickListener {
            executeEndSessionRequest()
        }

        if (mActivity != null && !mActivity.isFinishing()) {
            alertRatingDialog!!.show()// if fragment use getActivity().isFinishing() or isAdded() method
        }


        ratingbarRB.setOnClickListener {
            rating = ratingbarRB.rating.toString()
        }
        txtSubmitTV.setOnClickListener {
            if (isNetworkAvailable(mActivity)) {
                alertRatingDialog!!.dismiss()
                executeEndSessionRequest()
            } else
                showToast(mActivity, getString(R.string.internet_connection_error))
        }
    }

    /*
       * Execute api param
       * */
    private fun mEndSessionParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["roomId"] = roomId
        mMap["rating"] = rating
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeEndSessionRequest() {
        showProgressDialog(mActivity)
        viewModel?.getSessionEndData(mActivity, mEndSessionParam(), mHeaderParam())?.observe(this,
            androidx.lifecycle.Observer<EndSessionModel?> { mSessionEndModel ->
                if (mSessionEndModel.status == 1) {
                    dismissProgressDialog()
                    mSocket!!.emit("newMessage", roomId, "endSession")
                    finish()
                } else if (mSessionEndModel?.status == 0) {
                    dismissProgressDialog()
                    showToast(mActivity, mSessionEndModel.message)
                }

            })
    }

    private fun executeMoneyDeduct(speaker_value: Int) {
        var priceNum = LetMeListenPreferences().readString(
            mActivity,
            WALLET_AMOUNT,
            ""
        )
        newWalletAmount =
            Integer.parseInt(priceNum.toString()) - Integer.parseInt(speaker_value.toString())
        LetMeListenPreferences().writeString(
            mActivity,
            WALLET_AMOUNT,
            newWalletAmount.toString()
        )

    }

    private fun mAddmoreTimeParams(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["perMinCost"] = priceNum.toString()
        mMap["timeAdded"] = newTime.toString()
        mMap["roomId"] = roomId
        Log.e("params", "**PARAM**$mMap")
        return mMap
    }

    private fun executeAddMoreTimeApi() {
        showProgressDialog(mActivity)
        viewModel?.getAddTime(mActivity, mAddmoreTimeParams(), mHeaderParam())?.observe(this,
            androidx.lifecycle.Observer<AddMoreTimeModel?> { mSessionpojo ->
                if (mSessionpojo.status == 1) {
                    timeLeftRL.visibility = View.VISIBLE
                    addmoreRL.visibility = View.GONE

                    dismissProgressDialog()
                    //new_time = speakerr_value + speaker_value
                    initCountDown(speakerr_value + speaker_value)
                    executeMoneyDeduct(totalMoney)
                    mSocket!!.emit("newMessage", roomId, newTime)
                    //  showToast(mActivity, mSessionpojo.message)
                } else {
                    dismissProgressDialog()
                    showInsufficeintAlertDialog(mActivity, getString(R.string.insufficient_balance))
                    //  showAlertDialog(mActivity, mSessionpojo.message)

                }
            })
    }

    /*
     * Set up validations for wallet Money
     * */
    private fun isWalletValidate(): Boolean {
        var flag = true
        var walletPrice = LetMeListenPreferences().readString(
            mActivity,
            WALLET_AMOUNT,
            ""
        )
        if (Integer.parseInt(walletPrice) < 10) {
            showInsufficeintAlertDialog(mActivity, getString(R.string.insufficient_balance))
            flag = false
        }

        return flag
    }


    /*
     *
     * Error Alert Dialog
     * */
    fun showInsufficeintAlertDialog(mActivity: Activity?, strMessage: String?) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_alert_insufficient)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
        val btnDismiss = alertDialog.findViewById<TextView>(R.id.btnDismiss)
        val addMoneyTV = alertDialog.findViewById<TextView>(R.id.addMoneyTV)
        txtMessageTV.text = strMessage
        btnDismiss.setOnClickListener { alertDialog.dismiss() }
        addMoneyTV.setOnClickListener {

            alertDialog.dismiss()
            startActivity(Intent(mActivity, AddMoneyToWalletActivity::class.java))
            timeStop = "timestop"
            timer!!.cancel()
        }

        alertDialog.show()

    }

}


