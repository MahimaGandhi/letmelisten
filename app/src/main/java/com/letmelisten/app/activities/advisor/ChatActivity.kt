package com.letmelisten.app.activities.advisor

import android.app.Activity
import android.app.Dialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.CountDownTimer
import android.os.Handler
import android.text.format.DateFormat
import android.util.Log
import android.view.Gravity
import android.view.KeyEvent
import android.view.View
import android.view.Window
import android.widget.*
import androidx.lifecycle.ViewModelProviders
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.google.gson.Gson
import com.iarcuschin.simpleratingbar.SimpleRatingBar
import com.letmelisten.app.LetMeListenApplication
import com.letmelisten.app.Model.*
import com.letmelisten.app.R
import com.letmelisten.app.activities.BaseActivity
import com.letmelisten.app.adapters.ChatAdapter
import com.letmelisten.app.utils.*
import com.letmelisten.app.viewmodels.StripePaymentViewModel
import io.socket.client.Socket
import io.socket.emitter.Emitter
import kotlinx.android.synthetic.main.activity_chat.*
import org.json.JSONException
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit


class ChatActivity : BaseActivity() {
    @BindView(R.id.chatRV2)
    lateinit var chatRV2: RecyclerView

    @BindView(R.id.mainViewRL)
    lateinit var mainViewRL: RelativeLayout

    @BindView(R.id.backChatRL)
    lateinit var backChatRL: RelativeLayout

    @BindView(R.id.progressBarNew)
    lateinit var progressBarNew: LinearLayout

    @BindView(R.id.sendIV)
    lateinit var sendIV: ImageView


    var new_time: Long = 0
    var time: String = ""
    var price: String = ""
    var advisorId: String = ""
    var roomId: String = ""
    var totalTime: String = ""
    var totalTimeDum: String = ""
    var planId: String = ""
    var speaker_value: Long = 0
    var speakerr_value: Long = 0
    var timer: CountDownTimer? = null
    var mPerPage: Int = 100
    var mPageNo: Int = 1
    var endTime: Long = 0
    var rating: String = ""
    var fragmentName: String = ""
    var endTimeLatest: String = ""
    var startTimeLatest: String = ""
    var currentTime: Long = 0
    private var mSocket: Socket? = null
    var mIsPagination = true
    var mHandler: Handler = Handler()
    private var viewModel: StripePaymentViewModel? = null
    private var receiver: BroadcastReceiver? = null
    var mMsgArrayList: ArrayList<DataItem?>? = ArrayList()
    lateinit var chatAdapter: ChatAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat2)
        ButterKnife.bind(this)
        viewModel = ViewModelProviders.of(this).get(StripePaymentViewModel::class.java)

        editChatET.isEnabled = false
        setStatusBar(mActivity)
        getIntentData()
        setTopSwipeRefresh()

    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        return if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (fragmentName.equals("completedFragment")) {
                finish()
            } else {
                showExitAlertDialog(mActivity,getString(R.string.are_you_sure_to_exist))
               // showRatingDialog(mActivity)
            }
            false
        } else super.onKeyDown(keyCode, event)
    }
    /*
        *
        * Error Alert Dialog
        * */
    fun showExitAlertDialog(mActivity: Activity?, strMessage: String?) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_alert)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
        val btnDismiss = alertDialog.findViewById<TextView>(R.id.btnDismiss)
        txtMessageTV.text = strMessage
        btnDismiss.setOnClickListener { alertDialog.dismiss()
        executeEndSessionRequest()}
        alertDialog.show()
    }

    override fun onDestroy() {
        super.onDestroy()
            LetMeListenPreferences().writeString(
                mActivity,
                TO_DELETE_ROOMID,
                roomId
            )
    }
    override fun onResume() {
        super.onResume()
//        LocalBroadcastManager.getInstance(mActivity)
//            .registerReceiver(object : BroadcastReceiver() {
//                override fun onReceive(context: Context, intent: Intent) {
//                    val status = intent.getStringExtra("Status")
//                    if (status == "editImage") {
//                        showToast(mActivity, "Testttttt")
//                        LetMeListenPreferences().writeString(
//                            mActivity,
//                            APP_STATUS,
//                            "app_killed"
//                        )
//                    }
//                }
//            }, IntentFilter("changeImage"))

//        if (LetMeListenPreferences().readString(
//                mActivity,
//                APP_STATUS,
//                ""
//            ).equals("app_killed")
//        ) {
//            executeEndSessionRequest()
//
//        } else {
//        }
    }

    private fun getIntentData() {
        if (intent != null) {
            if (intent.getStringExtra(NOTIFY_TYPE).toString().equals("advisor")) {
                showAlertDialog(mActivity, getString(R.string.chat_start_in_15_sec))
                advisorId = intent.getStringExtra(ADVISOR_ID).toString()
                roomId = intent.getStringExtra(ROOM_ID).toString()
                totalTime = intent.getStringExtra(CHAT_TIME).toString()
                totalTimeDum = intent.getStringExtra(CHAT_TIME).toString()
                chatUserTV.setText(intent.getStringExtra(USER_NAME).toString())
            } else {
                val mModel = intent?.getParcelableExtra<AdvisorList>(MODEL)
                chatUserTV.text = mModel?.name
                fragmentName = intent.getStringExtra(FRAGMENT_NAME).toString()
                Log.e(TAG, "**Fragment**" + fragmentName)
                if (fragmentName.equals("completedFragment")) {
                }
                else {
                    showAlertDialog(mActivity, getString(R.string.chat_start_in_15_sec))

                }
                advisorId = mModel?.advisorId.toString()
                roomId = mModel?.roomId.toString()
                totalTime = mModel?.totalTime.toString()
                totalTimeDum = mModel?.endTime.toString()
                endTime = totalTimeDum.toLong()
            }
            // getTimeDiffernce(endTime)
            setUpSocketData()
            if (fragmentName.equals("completedFragment")) {
                timeRL.visibility = View.GONE
                editChatET.isEnabled = false
                bottomLL.visibility = View.GONE
                txtChatEndedTV.visibility = View.GONE
                view.visibility = View.GONE
            } else {
                setHandlerForSec()
                bottomLL.visibility = View.VISIBLE

                view.visibility = View.VISIBLE

            }
        }
    }

    private fun getTimeDiffernce(endTime: Long) {
        val calendar = Calendar.getInstance(Locale.ENGLISH)
        calendar.timeInMillis = endTime * 1000L
        endTimeLatest = DateFormat.format("dd-MM-yyyy HH:mm:ss", calendar).toString()

        val dateFormat = SimpleDateFormat("dd-MM-yyyy HH:mm:ss")
        val dateEnd = dateFormat.parse(endTimeLatest)
        Log.e(TAG, "**TIME**" + dateEnd)

        //Getting start date
        var startTimeLatest: String = getCurrentTimeStamp()!!
        val dateCurrent = dateFormat.parse(startTimeLatest)
        Log.e(TAG, "**TIME**" + dateCurrent)

        val duration: Long = dateEnd.getTime() - dateCurrent.getTime()

        val diffInSeconds = TimeUnit.MILLISECONDS.toSeconds(duration)
        val diffInMinutes = TimeUnit.MILLISECONDS.toMinutes(duration)
        val diffInHours = TimeUnit.MILLISECONDS.toHours(duration)
        val diffInDays = TimeUnit.MILLISECONDS.toDays(duration)

        Log.e(TAG, "MIN::" + (diffInMinutes))
        // initCountDown(TimeUnit.MINUTES.toMillis(diffInMinutes))
    }

    fun getCurrentTimeStamp(): String? {
        return try {

            val dateFormat =
                SimpleDateFormat("dd-MM-yyyy HH:mm:ss")
            dateFormat.format(Date())


        } catch (e: java.lang.Exception) {
            e.printStackTrace()

            null
        }
    }


    private fun setHandlerForSec() {
        Handler().postDelayed(
            {
                editChatET.isEnabled = true
                timeRL.visibility = View.VISIBLE
                initCountDown(TimeUnit.MINUTES.toMillis(totalTime.toLong()))
            },
            15000
        )
    }

    @OnClick(
        R.id.sendRL,
        R.id.CallRL,
        R.id.backChatRL
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.sendRL -> performSendMessageClick()
            R.id.CallRL -> performCallClick()
            R.id.backChatRL -> performBackClick()


        }
    }

    private fun performBackClick() {
        if (fragmentName.equals("completedFragment")) {
            onBackPressed()
        } else {
            showExitAlertDialog(mActivity,getString(R.string.are_you_sure_to_exist))

           // showRatingDialog(mActivity)
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        mainViewRL.isFocusableInTouchMode = true
        mainViewRL.requestFocus()
        mainViewRL.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
            if (event.action == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                if (fragmentName.equals("completedFragment")) {
                    finish()
                } else {
                    showExitAlertDialog(mActivity,getString(R.string.are_you_sure_to_exist))

                   // showRatingDialog(mActivity)
                }

                return@OnKeyListener true
            }
            false
        })
    }

    /*
     * Error Alert Dialog
     * */
    fun showRatingDialog(mActivity: Activity?) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.rating_dialog)
        alertDialog.setCanceledOnTouchOutside(true)
        alertDialog.setCancelable(true)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val window = alertDialog.window
        val wlp = window!!.attributes
        wlp.gravity = Gravity.CENTER
        // set the custom dialog components - text, image and button
        val txtSubmitTV = alertDialog.findViewById<TextView>(R.id.txtSubmitTV)
        val ratingbarRB = alertDialog.findViewById<SimpleRatingBar>(R.id.ratingbarRB)
        val btnDismiss = alertDialog.findViewById<ImageView>(R.id.btnDismiss)
        val btnCancel = alertDialog.findViewById<TextView>(R.id.txtCancelTV)

        btnDismiss.setOnClickListener {
            alertDialog.dismiss()
            rating = "0"
            executeEndSessionRequest()
        }
        btnCancel.setOnClickListener {
            executeEndSessionRequest()
        }
        alertDialog.show()

        ratingbarRB.setOnClickListener {
            rating = ratingbarRB.rating.toString()
        }
        txtSubmitTV.setOnClickListener {
            if (isNetworkAvailable(mActivity)) {
                alertDialog.dismiss()
                executeEndSessionRequest()
            } else
                showToast(mActivity, getString(R.string.internet_connection_error))
        }
    }
    private fun performSendMessageClick() {
        if (isNetworkAvailable(mActivity)) {
            if (isValidate()) {
                executeSendMesagesRequest()
            }
        } else {
            showToast(mActivity, getString(R.string.internet_connection_error))
        }
    }

    private fun performCallClick() {
        showToast(mActivity, getString(R.string.coming_soon))

        // val intent = Intent(mActivity, CallingActivity::class.java)
//        intent.putExtra("time", new_time)
//        intent.putExtra("price", price)
//        intent.putExtra("advisorId", advisorId)
//        startActivity(intent)
    }

    /*
     * Set up validations for Sign In fields
     * */
    private fun isValidate(): Boolean {
        var flag = true
        if (editChatET.text.toString().trim { it <= ' ' } == "") {
            flag = false
        }
        return flag
    }

    private fun mParams(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["roomId"] = roomId
        mMap["perPage"] = "" + mPerPage
        mMap["pageNo"] = "" + mPageNo
        Log.e("params", "**PARAM**$mMap")
        return mMap
    }

    private fun executeGetAllMegesRequest() {
        // mSwipeRefreshSR.isRefreshing = true
        viewModel?.getChatMessageData(mActivity, mParams(), mHeaderParam())?.observe(this,
            androidx.lifecycle.Observer<ChatMessagesModel?> { mSessionpojo ->
                if (mSessionpojo.status == 1) {
                    mSwipeRefreshSR.isRefreshing = false
                    dismissProgressDialog()

                    if (fragmentName.equals("completedFragment")) {
                        CallRL.visibility = View.GONE
                    } else {
                        if (mSessionpojo.userDetails?.avaliableCall.equals("1")) //Call status on of advisor
                        {
                            CallRL.visibility = View.VISIBLE
                        } else {
                            CallRL.visibility = View.GONE
                        }
                    }

                    for (i in 0..mSessionpojo.data!!.size - 1) {

                        if (mSessionpojo.data.get(i)!!.userId.equals(getLoggedInUserID())) {
                            mSessionpojo.data.get(i)!!.viewType = RIGHT_VIEW_HOLDER
                        } else {
                            mSessionpojo.data.get(i)!!.viewType = LEFT_VIEW_HOLDER
                        }
                    }
                    mMsgArrayList!!.addAll(0, mSessionpojo.data)
                    //  chatAdapter.notifyDataSetChanged()
                    initRecylerView()
                    if (mPageNo == 1) {
                        chatRV2.scrollToPosition(mMsgArrayList!!.size - 1)
                    }
                } else if (mSessionpojo?.status == 0) {
                    mSwipeRefreshSR.isRefreshing = false
                    initRecylerView()
                }

            })
    }

    private fun mSendParams(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["roomId"] = roomId
        mMap["message"] = editChatET.text.toString().trim()
        Log.e("params", "**PARAM**$mMap")
        return mMap
    }

    private fun executeSendMesagesRequest() {
        progressBarNew.visibility = View.VISIBLE
        sendIV.visibility = View.GONE
//        showProgressDialog(mActivity)
        viewModel?.getSendMessageData(mActivity, mSendParams(), mHeaderParam())?.observe(this,
            androidx.lifecycle.Observer<SendMsgModel?> { mModel ->
                if (mModel.status == 1) {
//                    dismissProgressDialog()
                    progressBarNew.visibility = View.GONE
                    sendIV.visibility = View.VISIBLE
                    mModel.data!!.viewType = RIGHT_VIEW_HOLDER
                    mMsgArrayList!!.add(mModel.data)
                    chatAdapter.notifyDataSetChanged()
                    scrollToBottom()
                    // perform the sending message attempt.
                    val gson = Gson()
                    val mOjectString = gson.toJson(mModel.data)
                    Log.e(TAG, "*****Msg****" + mOjectString)
                    mSocket!!.emit("newMessage", roomId, mOjectString)
                    editChatET.setText("")
                } else if (mModel?.status == 0) {
                    sendRL.isEnabled = true
//                    dismissProgressDialog()
                    showToast(mActivity, mModel.message)
                } else if (mModel?.status == 101)//End session Alert
                {
//                    dismissProgressDialog()
                    showFinishAlertDialog(mActivity, mModel.message)
                }
            })
    }
    private fun scrollToBottom() {
        chatRV2.scrollToPosition(chatAdapter.itemCount - 1)
    }
    private fun setTopSwipeRefresh() {
        // mSwipeRefreshSR.setColorSchemeColors(resources.getColor(R.color.colorGreen),resources.getColor(R.color.colorBlack),resources.getColor(R.color.colorRed));
        mSwipeRefreshSR.setOnRefreshListener {
            if (mIsPagination == true) {
                ++mPageNo
                executeGetAllMegesRequest()
            } else {
                mSwipeRefreshSR.isRefreshing = false
            }
        }
    }
    private fun mHeaderParam(): HashMap<String, String> {
        val headers = HashMap<String, String>()
        mActivity.let {
            LetMeListenPreferences().readString(it, TOKEN, null)?.let {
                headers.put(
                    "Token",
                    it
                )
            }
        }
        Log.e(
            TAG,
            "**Token**${mActivity.let { LetMeListenPreferences().readString(it, TOKEN, null) }}"
        )
        return headers
    }
    private fun initRecylerView() {
        val layoutManager: RecyclerView.LayoutManager =
            LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false)
        chatRV2.layoutManager = layoutManager
        chatAdapter = ChatAdapter(mActivity, mMsgArrayList)
        chatRV2.adapter = chatAdapter
    }
    private fun setUpSocketData() {
        val app: LetMeListenApplication = application as LetMeListenApplication
        mSocket = app.getSocket()
        mSocket!!.emit("ConncetedChat", roomId)
        mSocket!!.on(Socket.EVENT_CONNECT, onConnect)
        mSocket!!.on(Socket.EVENT_DISCONNECT, onDisconnect)
        mSocket!!.on(Socket.EVENT_CONNECT_ERROR, onConnectError)
        mSocket!!.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError)
        mSocket!!.on("newMessage", onNewMessage)
        mSocket!!.on("ChatStatus", onUserJoined)
        mSocket!!.on("leaveChat", onUserLeft)
        mSocket!!.on("type", onTyping)
        mSocket!!.on("stop typing", onStopTyping)
        mSocket!!.connect()
        executeGetAllMegesRequest()
    }
    /*
    * Socket Chat Implementations:
    * */

    private val onConnect = Emitter.Listener {
        runOnUiThread(
            Runnable { })
    }
    private val onDisconnect = Emitter.Listener {
        runOnUiThread(
            Runnable { })
    }
    private val onConnectError = Emitter.Listener {
        runOnUiThread(Runnable { Log.e(TAG, "Error connecting") })
    }

    private val onNewMessage = Emitter.Listener { args ->
        runOnUiThread(Runnable {
            try {
                Log.e(TAG, "****MessageObject****" + args.get(1).toString())
                var value: String = args.get(1).toString()

                if (value.contains("{")) {
                    val mGson = Gson()
                    val mChatMessageData: DataItem =
                        mGson.fromJson(args.get(1).toString(), DataItem::class.java)
                    if (mChatMessageData.userId!!.equals(getLoggedInUserID())) {
                        mChatMessageData.viewType = RIGHT_VIEW_HOLDER
                    } else {
                        mChatMessageData.viewType = LEFT_VIEW_HOLDER
                    }
                    mMsgArrayList!!.add(mChatMessageData)
                    chatAdapter.notifyDataSetChanged()
                    editChatET.setText("")
                    scrollToBottom()
                } else if (value.equals("endSession")) {
                    finish()
                } else {
                    timeLeftRL.visibility = View.VISIBLE
                    txtChatEndedTV.visibility = View.GONE
                    timer!!.cancel()
                    initCountDown(speakerr_value + TimeUnit.MINUTES.toMillis(value.toLong()))
                }


            } catch (e: Exception) {
                e.printStackTrace()
            }
        })
    }
    private val onUserJoined = Emitter.Listener { args ->
        runOnUiThread(Runnable {
            var mJsonData: JSONObject? = null
            try {
                mJsonData = JSONObject(args[1].toString())
            } catch (e: JSONException) {
                e.printStackTrace()

            }
        })
    }
    private val onUserLeft = Emitter.Listener {
        runOnUiThread(
            Runnable { })
    }
    private val onTyping =
        Emitter.Listener { args -> runOnUiThread(Runnable { val isTyping = args[1] as Boolean }) }
    private val onStopTyping = Emitter.Listener {
        runOnUiThread(
            Runnable { })
    }
    private val onTypingTimeout = Runnable { }
    private fun initCountDown(long: Long) {
        editChatET.isEnabled = true
        timer = object : CountDownTimer(long, 1000) {
            override fun onTick(long: Long) {
                txtTimeLeftTV.text = "-" + String.format(
                    "%02d : %02d",
                    TimeUnit.MILLISECONDS.toMinutes(long),
                    TimeUnit.MILLISECONDS.toSeconds(long) -
                            TimeUnit.MINUTES.toSeconds(
                                TimeUnit.MILLISECONDS.toMinutes(
                                    long
                                )
                            )
                )
                speakerr_value = long
                new_time = speakerr_value
            }

            override fun onFinish() {
                // editChatET.isEnabled = false
                // txtTimeLeftTV.text = "done!"
                timeLeftRL.visibility = View.GONE
                chatEndedRL.visibility = View.VISIBLE
                editChatET.isEnabled = false
                // showRatingDialog(mActivity)
            }
        }.start()
    }

    /*
          * Execute api param
          * */
    private fun mEndSessionParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["roomId"] = roomId
        mMap["rating"] = rating
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeEndSessionRequest() {
        showProgressDialog(mActivity)
        viewModel?.getSessionEndData(mActivity, mEndSessionParam(), mHeaderParam())?.observe(this,
            androidx.lifecycle.Observer<EndSessionModel?> { mSessionEndModel ->
                if (mSessionEndModel.status == 1) {
                    dismissProgressDialog()
                    txtChatEndedTV.visibility = View.VISIBLE
                    //  editChatET.isEnabled = false
                    mSocket!!.emit("newMessage", roomId, "endSession")
                    LetMeListenPreferences().writeString(
                        mActivity,
                        APP_STATUS,
                        ""
                    )
                    finish()
                } else if (mSessionEndModel?.status == 0) {
                    dismissProgressDialog()
                    showToast(mActivity, mSessionEndModel.message)
                } })
    }
    override fun onStop() {
        super.onStop()
            LetMeListenPreferences().writeString(
                mActivity,
                TO_DELETE_ROOMID,
                ROOM_ID
            )
    }
}