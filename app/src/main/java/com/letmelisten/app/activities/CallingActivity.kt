package com.letmelisten.app.activities

import android.app.Activity
import android.app.Dialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.media.AudioManager
import android.os.Bundle
import android.os.CountDownTimer
import android.os.SystemClock
import android.util.Log
import android.view.Gravity
import android.view.KeyEvent
import android.view.View
import android.view.Window
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.ViewModelProviders
import butterknife.ButterKnife
import butterknife.OnClick
import com.bumptech.glide.Glide
import com.iarcuschin.simpleratingbar.SimpleRatingBar
import com.letmelisten.app.Model.AddMoreTimeCallModel
import com.letmelisten.app.Model.EndSessionCallModel
import com.letmelisten.app.Model.StatusMsgModel
import com.letmelisten.app.R
import com.letmelisten.app.agoraPackage.DuringCallEventHandler
import com.letmelisten.app.agoraPackage.VideoBaseActivity
import com.letmelisten.app.utils.*
import com.letmelisten.app.viewmodels.PaymentViewModel
import com.letmelisten.app.viewmodels.StripePaymentViewModel
import io.agora.rtc.IRtcEngineEventHandler
import io.agora.rtc.RtcEngine
import kotlinx.android.synthetic.main.activity_calling.*
import rjsv.circularview.CircleView
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.math.roundToInt


class CallingActivity : VideoBaseActivity(), DuringCallEventHandler {
    private var viewModel: PaymentViewModel? = null
    private var viewModel2: StripePaymentViewModel? = null
    lateinit var alertDialog: Dialog
    var rating: String = ""
    var advisorImage: String = ""
    var advisorName: String = ""
    var value: Boolean = true
    var perMinCost: String = ""
    private var receiver: BroadcastReceiver? = null
    private var newTime = 0
    var speaker_value: Long = 0
    var speakerr_value: Long = 0
    var total_cost: String = ""
    var TOKENN: String =
        "0061aab9da00ddd49eda1c48b5b940e1eb8IAAAFannzYgo9FoaXwnhO3v1NzswlkNB33WnU6Qgu6YszvL4pw4AAAAAIgDJaQ2GX5qLYQQAAQDXLYxhAgDXLYxhAwDXLYxhBADXLYxh"
    var time: String = ""
    var new_time: Long = 0
    var timeStop: String = ""
    var priceNum: Int = 0
    var totalMoney: Int = 0
    var price: String = ""
    var mTabClick = 0L
    var advisorId: String = ""
    var newWalletAmount: Int = 0
    var roomId: String = ""
    private var microphoneOn = true
    private var speakerOff = true


    var chatTime = ""
    var mType = ""
    var totalTime = ""
    var message = ""
    var userName = ""
    var userImage = ""


    @Volatile
    private var mAudioMuted = false
    var timer: CountDownTimer? = null
    private val LOG_TAG: String =
        CallingActivity::class.java.simpleName

    private val mRtcEventHandler: IRtcEngineEventHandler = object : IRtcEngineEventHandler() {
        // Tutorial Step 1
        /**
         * Occurs when a remote user (Communication)/host (Live Broadcast) leaves the channel.
         *
         * There are two reasons for users to become offline:
         *
         * Leave the channel: When the user/host leaves the channel, the user/host sends a goodbye message. When this message is received, the SDK determines that the user/host leaves the channel.
         * Drop offline: When no data packet of the user or host is received for a certain period of time (20 seconds for the communication profile, and more for the live broadcast profile), the SDK assumes that the user/host drops offline. A poor network connection may lead to false detections, so we recommend using the Agora RTM SDK for reliable offline detection.
         *
         * @param uid ID of the user or host who
         * leaves
         * the channel or goes offline.
         * @param reason Reason why the user goes offline:
         *
         * USER_OFFLINE_QUIT(0): The user left the current channel.
         * USER_OFFLINE_DROPPED(1): The SDK timed out and the user dropped offline because no data packet was received within a certain period of time. If a user quits the call and the message is not passed to the SDK (due to an unreliable channel), the SDK assumes the user dropped offline.
         * USER_OFFLINE_BECOME_AUDIENCE(2): (Live broadcast only.) The client role switched from the host to the audience.
         */
        override fun onUserOffline(uid: Int, reason: Int) { // Tutorial Step 4
            runOnUiThread { onRemoteUserLeft(uid, reason) }
        }

        /**
         * Occurs when a remote user stops/resumes sending the audio stream.
         * The SDK triggers this callback when the remote user stops or resumes sending the audio stream by calling the muteLocalAudioStream method.
         *
         * @param uid ID of the remote user.
         * @param muted Whether the remote user's audio stream is muted/unmuted:
         *
         * true: Muted.
         * false: Unmuted.
         */
        override fun onUserMuteAudio(uid: Int, muted: Boolean) { // Tutorial Step 6
            runOnUiThread { onRemoteUserVoiceMuted(uid, muted) }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_calling)
        ButterKnife.bind(this)
        setStatusBar(mActivity)
        viewModel = ViewModelProviders.of(this).get(PaymentViewModel::class.java)
        viewModel2 = ViewModelProviders.of(this).get(StripePaymentViewModel::class.java)
        setReceiverMethod()
        imgSpeakerIV.setOnClickListener {
            // Preventing multiple clicks, using threshold of 1 second
            if (SystemClock.elapsedRealtime() - mTabClick < 500) {
                return@setOnClickListener
            }
            mTabClick = SystemClock.elapsedRealtime()
            speakerOnOff()
        }
    }

    override fun initUIandEvent() {
        addEventHandler(this)
        if (intent != null) {
            //Incoming call
            var value: String = intent.getStringExtra(NOTIFY_INCOMING_TYPE).toString()
            if (value != null && !value.equals("") && !value.equals("null")) {
                if (intent.getStringExtra(NOTIFY_INCOMING_TYPE).toString()
                        .equals("incoming call")
                ) {
                    txtTimeLeftTV.visibility = View.VISIBLE
                    roomId = intent.getStringExtra(ROOM_ID).toString()
                    userName = intent.getStringExtra(USER_NAME).toString()
                    chatTime = intent.getStringExtra(CHAT_TIME).toString()
                    userImage = intent.getStringExtra(AGORA_USER_IMAGE).toString()
                    advisorId = intent.getStringExtra(ADVISOR_ID).toString()
                    Glide.with(mActivity)
                        .load(userImage)
                        .placeholder(R.drawable.back)
                        .error(R.drawable.ic_phh)
                        .into(editProfileIV)
                    txtNameTV.text = userName
                    joinChannel("TRY_DEMO", config().mUid)
                    initCountDown(TimeUnit.MINUTES.toMillis(chatTime.toLong()))
                }

            } else {
                //User calling
                advisorId = intent.getStringExtra("advisorId").toString()
                roomId = intent.getStringExtra(ROOM_ID).toString()
                time = intent.getStringExtra(CHAT_TIME).toString()
                perMinCost = intent.getStringExtra(PER_MIN_COST).toString()
                price = intent.getStringExtra(PRICE).toString()
                advisorImage = LetMeListenPreferences().readString(
                    mActivity,
                    SEL_ADVISOR_IMAGE,
                    ""
                ).toString()
                advisorName = LetMeListenPreferences().readString(
                    mActivity,
                    SEL_ADVISOR_NAME,
                    ""
                ).toString()
                Glide.with(mActivity)
                    .load(advisorImage)
                    .placeholder(R.drawable.ic_phh)
                    .error(R.drawable.ic_phh)
                    .into(editProfileIV)
                txtNameTV.text = advisorName
                joinChannel("TRY_DEMO", config().mUid)
            }

        }
    }


    override fun deInitUIandEvent() {
        doLeaveChannel()
        removeEventHandler(this)

    }

    private fun doLeaveChannel() {
        leaveChannel(config().mChannel!!)
    }

    private fun initCountDown(long: Long) {
        timer = object : CountDownTimer(long, 1000) {
            override fun onTick(long: Long) {
                txtTimeLeftTV.text = "-" + String.format(
                    "%02d : %02d",
                    TimeUnit.MILLISECONDS.toMinutes(long),
                    TimeUnit.MILLISECONDS.toSeconds(long) -
                            TimeUnit.MINUTES.toSeconds(
                                TimeUnit.MILLISECONDS.toMinutes(
                                    long
                                )
                            )
                )
                speakerr_value = long
                new_time = speakerr_value

                Log.e(LOG_TAG, "value" + speakerr_value)

            }

            override fun onFinish() {
//                doLeaveChannel()
//                txtTimeLeftTV.text = "done!"
//                addmoreERL.visibility = View.VISIBLE
                executeEndSessionRequest()
            }
        }.start()
    }


    override fun onDestroy() {
        super.onDestroy()
        //  leaveChannel(config().mChannel!!)
        RtcEngine.destroy()
        // mRtcEngine = null
    }

    // - - To Turn Mic On or Off
    private fun microphoneOnOff() {
        val rtcEngine = rtcEngine()
        when {

            !microphoneOn -> {
                imgMuteUnmuteIV.setImageResource(R.drawable.ic_mute_unmute)
                rtcEngine?.muteLocalAudioStream(false)
                microphoneOn = true
            }
            microphoneOn -> {
                imgMuteUnmuteIV.setImageResource(R.drawable.ic_mmute)
                rtcEngine?.muteLocalAudioStream(true)
                microphoneOn = false
            }
        }
    }

    // - - To Turn Speaker On or Off
    private fun speakerOnOff() {
        val rtcEngine = rtcEngine()
        when {
            !speakerOff -> {
                rtcEngine?.setEnableSpeakerphone(false)
                imgSpeakerIV.setImageResource(R.drawable.ic_speaker_mute)
                speakerOff = true
            }
            speakerOff -> {
                rtcEngine?.setEnableSpeakerphone(true)
                imgSpeakerIV.setImageResource(R.drawable.ic_speaker)
                speakerOff = false
            }
        }
    }

    // Tutorial Step 3
    fun onEncCallClicked(view: View?) {
        sendRatingRequest()
//        finish()
    }


    private fun sendRatingRequest() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error))
        } else {
            executeRatingApi()
        }
    }


    private fun mParams(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["advisorId"] = advisorId
        mMap["rating"] = rating
        Log.e("params", "**PARAM**$mMap")
        return mMap
    }

    fun mHeaderParam(): HashMap<String, String> {
        val headers = HashMap<String, String>()
        LetMeListenPreferences().readString(mActivity, TOKEN, null)?.let {
            headers.put(
                "Token",
                it
            )
        }
        return headers
    }

    private fun executeRatingApi() {
        showProgressDialog(mActivity)
        viewModel?.getRatingData(mActivity, mParams(), mHeaderParam())?.observe(this,
            androidx.lifecycle.Observer<StatusMsgModel?> { mRatingModel ->
                if (mRatingModel.status == 1) {
                    dismissProgressDialog()
                    showToast(mActivity, mRatingModel.message)
                    finish()
                } else {
                    dismissProgressDialog()
                    showAlertDialog(mActivity, mRatingModel.message)
                }
            })
    }


    // Tutorial Step 4
    private fun onRemoteUserLeft(uid: Int, reason: Int) {
//        showLongToast(
//            String.format(
//                Locale.US,
//                "user %d left %d",
//                uid and 0xFFFFFFFFL.toInt(),
//                reason
//            )
//        )
//        val tipMsg: View = findViewById(R.id.quick_tips_when_use_agora_sdk) // optional UI
//        val tipMsg: View = findViewById(R.id.quick_tips_when_use_agora_sdk) // optional UI
//        tipMsg.visibility = View.VISIBLE
    }

    // Tutorial Step 6
    private fun onRemoteUserVoiceMuted(uid: Int, muted: Boolean) {
//        showLongToast(
//            String.format(
//                Locale.US,
//                "user %d muted or unmuted %b",
//                uid and 0xFFFFFFFFL.toInt(),
//                muted
//            )
//        )
    }


    @OnClick(
        R.id.imgCallEndIV,
        R.id.imgMuteUnmuteIV,
        R.id.addmoreERL
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.imgCallEndIV -> performcallEndClick()
            R.id.imgMuteUnmuteIV -> microphoneOnOff()
            R.id.addmoreERL -> performAddMoreTimeClick()
        }
    }

    private fun performAddMoreTimeClick() {
        showTimerDialog(mActivity)
    }

    open fun showTimerDialog(context: Context) {
        val alertDialog = Dialog(context)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.bottomsheet_timer)
        alertDialog.setCanceledOnTouchOutside(true)
        alertDialog.setCancelable(true)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val window = alertDialog.window
        val wlp = window!!.attributes
        wlp.gravity = Gravity.BOTTOM
        val imgCancelIV: ImageView = alertDialog.findViewById(R.id.imgCancelIV)
        val circleView: CircleView = alertDialog.findViewById(R.id.circle_view)
//        val circleView = findViewById<View>(R.id.circle_view) as CircleView
        val txtAddTV: TextView = alertDialog.findViewById(R.id.txtAddTV)
        imgCancelIV.setOnClickListener {
            alertDialog.dismiss()
        }

        txtAddTV.setOnClickListener {
            speaker_value = TimeUnit.MINUTES.toMillis(circleView.progressValue.toLong())
            timer!!.cancel()
            priceNum = Integer.parseInt("2")
            newTime = circleView.progressValue.roundToInt()
            totalMoney = priceNum * newTime
            Log.e(LOG_TAG, "value" + speaker_value)
            executeAddMoreTimeApi()
            alertDialog.dismiss()
        }
        alertDialog.show()
    }

    /*
     *
     * Error Alert Dialog
     * */
    fun showRatingDialog(mActivity: Activity?) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.rating_dialog)
        alertDialog.setCanceledOnTouchOutside(true)
        alertDialog.setCancelable(true)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val window = alertDialog.window
        val wlp = window!!.attributes
        wlp.gravity = Gravity.CENTER
        // set the custom dialog components - text, image and button
        val txtSubmitTV = alertDialog.findViewById<TextView>(R.id.txtSubmitTV)
        val ratingbarRB = alertDialog.findViewById<SimpleRatingBar>(R.id.ratingbarRB)
        val btnDismiss = alertDialog.findViewById<ImageView>(R.id.btnDismiss)
        val btnCancel = alertDialog.findViewById<TextView>(R.id.txtCancelTV)

        btnDismiss.setOnClickListener {
            alertDialog.dismiss()
            rating = "0"
            executeEndSessionRequest()
        }
        alertDialog.show()

        ratingbarRB.setOnClickListener {
            rating = ratingbarRB.rating.toString()
        }
        txtSubmitTV.setOnClickListener {
            if (isNetworkAvailable(mActivity))
                executeEndSessionRequest()
            else
                showToast(mActivity, getString(R.string.internet_connection_error))
        }
        btnCancel.setOnClickListener {
            executeEndSessionRequest()
        }
    }

    private fun performcallEndClick() {
        showRatingDialog(mActivity)
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        return if (keyCode == KeyEvent.KEYCODE_BACK) {

            showRatingDialog(mActivity)

            false
        } else super.onKeyDown(keyCode, event)
    }

    /*
      * Execute api param
      * */
    private fun mEndSessionParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["roomId"] = roomId
        mMap["rating"] = rating
        if (LetMeListenPreferences().readString(
                mActivity,
                SWITCH_ACCOUNT,
                ""
            ).equals("advisor")
        ) {
            mMap["role"] = "2"
        } else {
            mMap["role"] = "1"
        }

        Log.e("TAG", "**PARAM**$mMap")
        return mMap
    }

    private fun executeEndSessionRequest() {
        showProgressDialog(mActivity)
        viewModel?.getCallEndData(mActivity, mEndSessionParam(), mHeaderParam())?.observe(this,
            androidx.lifecycle.Observer<EndSessionCallModel?> { mCallEndModel ->
                if (mCallEndModel.status == 1) {
                    dismissProgressDialog()
                    //  editChatET.isEnabled = false
                    // mSocket!!.emit("newMessage", roomId, "endSession")
                    finish()
                } else if (mCallEndModel?.status == 0) {
                    dismissProgressDialog()
                    showToast(mActivity, mCallEndModel.message)
                    finish()
                }
            })
    }

    private fun mAddmoreTimeParams(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["perMinCost"] = priceNum.toString()
        mMap["timeAdded"] = newTime.toString()
        mMap["roomId"] = roomId
        Log.e("params", "**PARAM**$mMap")
        return mMap
    }

    private fun executeAddMoreTimeApi() {
        showProgressDialog(mActivity)
        viewModel?.getAddMoreTimeCallRequest(mActivity, mAddmoreTimeParams(), mHeaderParam())
            ?.observe(this,
                androidx.lifecycle.Observer<AddMoreTimeCallModel?> { mAddMorepojo ->
                    if (mAddMorepojo.status == 1) {
                        dismissProgressDialog()
                        addmoreERL.visibility = View.GONE
                        joinChannel("TRY_DEMO", config().mUid)
                        // executeInitaiteCallingApi()
                        //new_time = speakerr_value + speaker_value
                        initCountDown(speakerr_value + speaker_value)
                        executeMoneyDeduct(totalMoney)
                        initCountDown(speakerr_value + speaker_value)
                        // mSocket!!.emit("newMessage", roomId, newTime)
                        //  showToast(mActivity, mSessionpojo.message)
                    } else {
                        dismissProgressDialog()
                        showInsufficeintAlertDialog(
                            mActivity,
                            getString(R.string.insufficient_balance)
                        )
                        //  showAlertDialog(mActivity, mSessionpojo.message)

                    }
                })
    }

    private fun executeMoneyDeduct(speaker_value: Int) {
        var priceNum = LetMeListenPreferences().readString(
            mActivity,
            WALLET_AMOUNT,
            ""
        )
        newWalletAmount =
            Integer.parseInt(priceNum.toString()) - Integer.parseInt(speaker_value.toString())
        LetMeListenPreferences().writeString(
            mActivity,
            WALLET_AMOUNT,
            newWalletAmount.toString()
        )
    }

    /*
     *
     * Error Alert Dialog
     * */
    fun showInsufficeintAlertDialog(mActivity: Activity?, strMessage: String?) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_alert_insufficient)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
        val btnDismiss = alertDialog.findViewById<TextView>(R.id.btnDismiss)
        val addMoneyTV = alertDialog.findViewById<TextView>(R.id.addMoneyTV)
        txtMessageTV.text = strMessage
        btnDismiss.setOnClickListener { alertDialog.dismiss() }
        addMoneyTV.setOnClickListener {
            alertDialog.dismiss()
            startActivity(Intent(mActivity, AddMoneyToWalletActivity::class.java))
            timeStop = "timestop"
            timer!!.cancel()
        }
        alertDialog.show()
    }

    private fun setReceiverMethod() {
        receiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                val call_type = intent.extras!!.getString("call_type")
                val newTimeAdd = intent.extras!!.getString("added_time")
                if (call_type.equals("answered_video_call", ignoreCase = true)) {
                    txtTimeLeftTV.visibility = View.VISIBLE
                    addmoreERL.visibility = View.GONE
                    initCountDown(TimeUnit.MINUTES.toMillis(time.toLong()))
                } else if (call_type.equals("dismiss_video_call", ignoreCase = true)) {
                    executeEndSessionRequest()
//                    finish()
                } else if (call_type.equals("add_more_time", ignoreCase = true)) {
                    timer!!.cancel()
                    var newTime: Long =
                        TimeUnit.MINUTES.toMillis(newTimeAdd!!.toLong()) + speakerr_value
                    initCountDown(newTime)
                }

            }
        }
    }

    override fun onResume() {
        super.onResume()
        if (timeStop.equals("timestop")) {
            initCountDown(new_time)
        }
        registerReceiver(receiver, IntentFilter(getString(R.string.incoming_call_receiver)))
    }

    override fun onPause() {
        super.onPause()
        unregisterReceiver(receiver)
    }

    override fun onJoinChannelSuccess(channel: String?, uid: Int, elapsed: Int) {

    }

    override fun onUserOffline(uid: Int, reason: Int) {
    }

    override fun onUserJoined(uid: Int) {
    }

    override fun onFirstRemoteVideoDecoded(uid: Int, width: Int, height: Int, elapsed: Int) {

    }

    override fun onExtraCallback(type: Int, vararg data: Any?) {

    }

}