package com.letmelisten.app.activities.advisor

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.letmelisten.app.Interfaces.LoadMoreTransactionData
import com.letmelisten.app.Model.*
import com.letmelisten.app.R
import com.letmelisten.app.activities.BaseActivity
import com.letmelisten.app.adapters.advisor.EarningsAdapter
import com.letmelisten.app.utils.LetMeListenPreferences
import com.letmelisten.app.utils.TOKEN
import com.letmelisten.app.viewmodels.ProfileEditProfileViewModel

class EarningsActivity : BaseActivity(), LoadMoreTransactionData {
    @BindView(R.id.earningsRV)
    lateinit var earningsRV: RecyclerView

    @BindView(R.id.totalBalanceTV)
    lateinit var totalBalanceTV: TextView
    var mPageNo: Int = 1
    var mPerPage: Int = 10
    var mLoadMore: LoadMoreTransactionData? = null
    var isLoading: Boolean = true
    private var viewModel: ProfileEditProfileViewModel? = null
    lateinit var earningsAdapter: EarningsAdapter
    var mTransactionAL: ArrayList<DataItemNew?>? = ArrayList()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_earnings)
        ButterKnife.bind(this)
        mLoadMore = this
        viewModel = ViewModelProviders.of(this).get(ProfileEditProfileViewModel::class.java)
        setStatusBar(mActivity)
        executeTransactionDetailRequest()
    }

    @OnClick(
        R.id.backEarningsRL
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.backEarningsRL -> onBackPressed()
        }
    }

    private fun initRecylerView() {
        val layoutManager: RecyclerView.LayoutManager =
            LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false)
        earningsRV.setLayoutManager(layoutManager)
        earningsAdapter = EarningsAdapter(mActivity, mTransactionAL, mLoadMore)
        earningsRV.setAdapter(earningsAdapter)

    }

    /*
     * Execute api param
     * */
    private fun mParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["pageNo"] = mPageNo.toString()
        mMap["perPage"] = "200"
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun mHeaderParam(): HashMap<String, String> {
        val headers = HashMap<String, String>()
        mActivity?.let {
            LetMeListenPreferences().readString(it, TOKEN, null)?.let {
                headers.put(
                    "Token",
                    it
                )
            }
        }
        Log.e(
            TAG,
            "**Token**${mActivity?.let { LetMeListenPreferences().readString(it, TOKEN, null) }}"
        )
        return headers
    }

    private fun executeTransactionDetailRequest() {
        mTransactionAL!!.clear()
        showProgressDialog(mActivity)
        mActivity?.let {
            viewModel?.transactionDetailData(mActivity, mParam(), mHeaderParam())?.observe(
                this,
                androidx.lifecycle.Observer<TransactionHistoryModel?> { mTransactionModel ->
                    if (mTransactionModel.status!!.equals(1)) {
                        isLoading = !mTransactionModel?.lastPage?.equals(true)!!
                        dismissProgressDialog()
                        totalBalanceTV.setText("$" + mTransactionModel.totalBalance)
                        mTransactionModel.data?.let { it1 -> mTransactionAL!!.addAll(it1) }
                        Log.e(TAG, "**ArrayList**" + mTransactionAL!!.size)
                        initRecylerView()

                    } else {
                        dismissProgressDialog()
                    }
                })
        }
    }

    private fun executeMoreTransactionDetailRequest() {
        mTransactionAL!!.clear()
        showProgressDialog(mActivity)
        mActivity?.let {
            viewModel?.transactionDetailData(mActivity, mParam(), mHeaderParam())?.observe(
                this,
                androidx.lifecycle.Observer<TransactionHistoryModel?> { mTransactionModel ->
                    if (mTransactionModel.status!!.equals(1)) {
                        dismissProgressDialog()
                        isLoading = !mTransactionModel?.lastPage?.equals(true)!!
                        mTransactionModel.data?.let { it1 -> mTransactionAL!!.addAll(it1) }
                        Log.e(TAG, "**ArrayList**" + mTransactionAL!!.size)
                        earningsAdapter?.notifyDataSetChanged()

                    } else {
                        dismissProgressDialog()
                    }
                })
        }
    }

    override fun onLoadMoreData(mModel: DataItemNew?) {
//        if (isLoading) {
//            ++mPageNo
//            executeMoreTransactionDetailRequest()
//        }
    }
}