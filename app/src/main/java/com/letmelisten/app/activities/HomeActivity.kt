package com.letmelisten.app.activities


import android.app.ActivityManager
import android.content.Intent
import android.os.Bundle
import android.os.SystemClock
import android.util.Log
import android.view.View
import androidx.lifecycle.ViewModelProviders
import butterknife.ButterKnife
import butterknife.OnClick
import com.letmelisten.app.Model.BadgeCountModel
import com.letmelisten.app.Model.StatusMsgModel

import com.letmelisten.app.R
import com.letmelisten.app.fragments.FavouritesFragment
import com.letmelisten.app.fragments.HomeFragment
import com.letmelisten.app.fragments.NotificationFragment
import com.letmelisten.app.fragments.ProfileFragment
import com.letmelisten.app.utils.*
import com.letmelisten.app.viewmodels.HomeViewModel
import com.letmelisten.app.viewmodels.SignupLoginViewModel
import kotlinx.android.synthetic.main.activity_home.*


class HomeActivity : BaseActivity() {
    private var viewModel: SignupLoginViewModel? = null
    private var mClickTab1: Long = 0
    private var mClickTab2: Long = 0
    private var mClickTab3: Long = 0
    private var mClickTab4: Long = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        ButterKnife.bind(this)
        viewModel = ViewModelProviders.of(this).get(SignupLoginViewModel::class.java)
        setStatusBar(mActivity)
        performHomeClick()
    }

    fun mHeaderParam(): HashMap<String, String> {
        val headers = HashMap<String, String>()
        LetMeListenPreferences().readString(mActivity, TOKEN, null)?.let {
            headers.put(
                "Token",
                it
            )
        }
        return headers
    }


    @OnClick(
        R.id.imgHomeIV,
        R.id.imgFavIV,
        R.id.imgNotiIV,
        R.id.imgProfileIV
    )
    fun onClick(view: View) {
        when (view.id) {
            R.id.imgHomeIV -> performHomeClick()
            R.id.imgFavIV -> performFavClick()
            R.id.imgNotiIV -> performNotiClick()
            R.id.imgProfileIV -> performProfileClick()
        }
    }

    private fun performProfileClick() {
        preventMultipleClick(mClickTab4)
//        txtHeadingTV.setText(getString(R.string.title_profile))
        imgProfileIV.setImageDrawable(getDrawable(R.drawable.ic_profile_ss))
        imgHomeIV.setImageDrawable(getDrawable(R.drawable.ic_home))
        imgNotiIV.setImageDrawable(getDrawable(R.drawable.ic_bell))
        imgFavIV.setImageDrawable(getDrawable(R.drawable.ic_black_heart))
        switchFragment(ProfileFragment(), PROFILE_TAG, false, null)
    }

    private fun performNotiClick() {
        chat_badge.visibility = View.GONE
        preventMultipleClick(mClickTab3)
//        txtHeadingTV.setText(getString(R.string.title_notification))
        imgNotiIV.setImageDrawable(getDrawable(R.drawable.ic_bell_ss))
        imgHomeIV.setImageDrawable(getDrawable(R.drawable.ic_home))
        imgProfileIV.setImageDrawable(getDrawable(R.drawable.ic_profile))
        imgFavIV.setImageDrawable(getDrawable(R.drawable.ic_black_heart))
        switchFragment(NotificationFragment(), NOTIFICATION_TAG, false, null)
    }

    private fun performFavClick() {
        preventMultipleClick(mClickTab2)
        preventMultipleClick()
//        txtHeadingTV.setText(getString(R.string.title_favourites))
        imgFavIV.setImageDrawable(getDrawable(R.drawable.ic_heart_selected))
        imgHomeIV.setImageDrawable(getDrawable(R.drawable.ic_home))
        imgProfileIV.setImageDrawable(getDrawable(R.drawable.ic_profile))
        imgNotiIV.setImageDrawable(getDrawable(R.drawable.ic_bell))
        switchFragment(FavouritesFragment(), FAVOURITES_TAG, false, null)
    }

    private fun performHomeClick() {
        executeGetBadgeCount()
        preventMultipleClick(mClickTab1)
//        txtHeadingTV.setText(getString(R.string.title_home))
        imgHomeIV.setImageDrawable(getDrawable(R.drawable.ic_home_ss))
        imgProfileIV.setImageDrawable(getDrawable(R.drawable.ic_profile))
        imgNotiIV.setImageDrawable(getDrawable(R.drawable.ic_bell))
        imgFavIV.setImageDrawable(getDrawable(R.drawable.ic_black_heart))
        switchFragment(HomeFragment(), HOME_TAG, false, null)
    }


    open fun preventMultipleClick(mClickTab1: Long) {
        // Preventing multiple clicks, using threshold of 1 second
        if (SystemClock.elapsedRealtime() - mClickTab1 < 2000) {
            return
        }
        this.mClickTab1 = SystemClock.elapsedRealtime()
    }
    /*
     * Execute api param
     * */
    private fun mBadgeParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = java.util.HashMap()
        if (LetMeListenPreferences().readString(
                mActivity,
                SWITCH_ACCOUNT,
                ""
            ).equals("advisor")
        ) {
            mMap["role"] = "2"
        } else {
            mMap["role"] = "1"
        }
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeGetBadgeCount() {
        viewModel?.getBadgeCount(mActivity,mBadgeParam(), mHeaderParam())?.observe(this,
            androidx.lifecycle.Observer<BadgeCountModel?> { mBadgeModel ->
                if (mBadgeModel.status == 1) {
                    if(mBadgeModel.badgeCount!=null && !mBadgeModel.badgeCount.equals("0"))
                    {
                        chat_badge.visibility = View.VISIBLE
                        chat_badge.setText(mBadgeModel.badgeCount)
                    }
                } else {
                    chat_badge.visibility = View.GONE
                }
            })
    }


}