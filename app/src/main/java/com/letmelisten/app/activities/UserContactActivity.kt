package com.letmelisten.app.activities

import android.content.Intent
import android.os.Bundle
import android.view.View
import butterknife.ButterKnife
import butterknife.OnClick
import com.letmelisten.app.R

class UserContactActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_contact)
        ButterKnife.bind(this)
        setStatusBar(mActivity)
    }
    @OnClick(
        R.id.backUserContactRL,
        R.id.msgLL
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.backUserContactRL -> performBackClick()
            R.id.msgLL -> performMessageClick()

        }
    }

    private fun performMessageClick() {
        startActivity(Intent(mActivity, ChatActivity::class.java))
    }
    private fun performBackClick() {
        startActivity(Intent(mActivity, HomeActivity::class.java))
        finish()
    }
}
