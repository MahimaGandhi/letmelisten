package com.letmelisten.app.activities

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.ConnectivityManager
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.SystemClock
import android.util.DisplayMetrics
import android.view.View
import android.view.ViewConfiguration
import android.view.ViewTreeObserver.OnGlobalLayoutListener
import android.view.Window
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.NonNull
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.ViewConfigurationCompat
import androidx.fragment.app.Fragment
import com.letmelisten.app.LetMeListenApplication
import com.letmelisten.app.Model.*
import com.letmelisten.app.R
import com.letmelisten.app.utils.IS_LOGIN
import com.letmelisten.app.utils.LetMeListenPreferences
import com.letmelisten.app.utils.USER_ID
import io.agora.rtc.RtcEngine
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.io.ByteArrayOutputStream
import java.util.*
import java.util.regex.Pattern

  public open class BaseActivity : AppCompatActivity() {
    /*
    * Get Class Name
    * */
    var TAG = this@BaseActivity.javaClass.simpleName
    private val log: Logger =
        LoggerFactory.getLogger(com.letmelisten.app.activities.BaseActivity::class.java)
    /*
    * Initialize Activity
    * */
    var mActivity: Activity = this@BaseActivity
    private var mLastClickTab1: Long = 0

    /*
    * Initialize Other Classes Objects...
    * */
    var progressDialog: Dialog? = null

    /*
   * Is User Login
   * */
    fun isUserLogin() : Boolean{
        return LetMeListenPreferences().readBoolean(mActivity, IS_LOGIN, false)
    }






    /*
Transparent status bar
 */
    fun setStatusBar(mActivity: Activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            mActivity.window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            // edited here
            mActivity.window.statusBarColor = resources.getColor(R.color.colorWhite)
        }
    }

    /*
Clear editText focus
 */
    fun setEditTextFocused(mActivity: Activity, mEditText: EditText) {
        mEditText.setOnEditorActionListener(TextView.OnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                mEditText.clearFocus()
                val imm = v.context
                    .getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(v.windowToken, 0)
                return@OnEditorActionListener true
            }
            false
        })
    }

    /*
     *Hide/Close Keyboard forcefully
     *
     * */
    open fun hideCloseKeyboard(activity: Activity) {
        val imm = activity.getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
        //Find the currently focused view, so we can grab the correct window token from it.
        var view = activity.currentFocus
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = View(activity)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    open fun convertBitmapToByteArrayUncompressed(bitmap: Bitmap): ByteArray? {
        val stream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 40, stream)
        return stream.toByteArray()
    }

    open fun getAlphaNumericString(): String? {
        val n = 20

        // chose a Character random from this String
        val AlphaNumericString = ("ABCDEFGHIJKLMNOPQRSTUVWXYZ" /*+ "0123456789"*/
                + "abcdefghijklmnopqrstuvxyz")

        // create StringBuffer size of AlphaNumericString
        val sb = StringBuilder(n)
        for (i in 0 until n) {

            // generate a random number between
            // 0 to AlphaNumericString variable length
            val index = (AlphaNumericString.length
                    * Math.random()).toInt()

            // add Character one by one in end of sb
            sb.append(
                AlphaNumericString[index]
            )
        }
        return sb.toString()
    }

    /*Switch between fragments*/
    fun switchFragment(
        fragment: Fragment?,
        Tag: String?,
        addToStack: Boolean,
        bundle: Bundle?
    ) {
        val fragmentManager = supportFragmentManager
        if (fragment != null) {
            val fragmentTransaction =
                fragmentManager.beginTransaction()
            fragmentTransaction.replace(R.id.mainFL, fragment, Tag)
            if (addToStack) fragmentTransaction.addToBackStack(Tag)
            if (bundle != null) fragment.arguments = bundle
            fragmentTransaction.commit()

//            fragmentTransaction.remove(fragment)
            fragmentManager.executePendingTransactions()
//            fragmentManager.popBackStack()
        }
    }
//    /*Switch between fragments(advisor flow)*/
//    fun switchFragment2(
//        fragment: Fragment?,
//        Tag: String?,
//        addToStack: Boolean,
//        bundle: Bundle?
//    ) {
//        val fragmentManager = supportFragmentManager
//        if (fragment != null) {
//            val fragmentTransaction =
//                fragmentManager.beginTransaction()
//            fragmentTransaction.replace(R.id.nav_host_fragment2, fragment, Tag)
//            if (addToStack) fragmentTransaction.addToBackStack(Tag)
//            if (bundle != null) fragment.arguments = bundle
//            fragmentTransaction.commit()
//            fragmentManager.executePendingTransactions()
//        }
//    }

    /*
         * Validate Email Address
         * */
    fun isValidEmailId(email: String?): Boolean {
        return Pattern.compile(
            "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                    + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                    + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                    + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$"
        ).matcher(email).matches()
    }

    /*
         * Validate Phone Number
         * */
    fun isValidPhoneNumber(phNumber: String): Boolean {
        return Pattern.compile(
            "^\\s*(?:\\+?(\\d{1,3}))?[-. (]*(\\d{3})[-. )]*(\\d{3})[-. ]*(\\d{4})(?: *x(\\d+))?\\s*$"
        ).matcher(phNumber.toString()).matches()
    }

    /*
     *
     * Error Alert Dialog
     * */
    fun showAlertDialog(mActivity: Activity?, strMessage: String?) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_alert)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
        val btnDismiss = alertDialog.findViewById<TextView>(R.id.btnDismiss)
        txtMessageTV.text = strMessage
        btnDismiss.setOnClickListener { alertDialog.dismiss() }
        alertDialog.show()
    }

    /*
    *
    * Error Alert Dialog
    * */
    fun showFinishAlertDialog(mActivity: Activity?, strMessage: String?) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_alert)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
        val btnDismiss = alertDialog.findViewById<TextView>(R.id.btnDismiss)
        txtMessageTV.text = strMessage
        btnDismiss.setOnClickListener { alertDialog.dismiss()
        finish()}
        alertDialog.show()
    }

    open fun preventMultipleClick() {
        // Preventing multiple clicks, using threshold of 1 second
        if (SystemClock.elapsedRealtime() - mLastClickTab1 < 2000) {
            return
        }
        mLastClickTab1 = SystemClock.elapsedRealtime()
    }




    /*
     * Check Internet Connections
     * */
    fun isNetworkAvailable(mContext: Context): Boolean {
        val connectivityManager =
            mContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }


    /*
     * Toast Message
     * */
    fun showToast(mActivity: Activity?, strMessage: String?) {
        Toast.makeText(mActivity, strMessage, Toast.LENGTH_SHORT).show()
    }


    /*
     * Show Progress Dialog
     * */

     fun  showProgressDialog(mActivity: Activity?) {
        progressDialog = Dialog(mActivity!!)
        progressDialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        progressDialog!!.setContentView(R.layout.dialog_progress)
        Objects.requireNonNull(progressDialog!!.window)?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        progressDialog!!.setCanceledOnTouchOutside(false)
        progressDialog!!.setCancelable(false)
        if (progressDialog != null) progressDialog!!.show()
    }


    /*
     * Hide Progress Dialog
     * */
    fun dismissProgressDialog() {
        try {
            if (this.progressDialog != null && this.progressDialog!!.isShowing()) {
                this.progressDialog!!.dismiss()
            }
        } catch (e: IllegalArgumentException) {
            // Handle or log or ignore
        } catch (e: Exception) {
            // Handle or log or ignore
        } finally {
            this.progressDialog = null
        }
    }

    /*
      * Getting User ID
      * */
    fun getLoggedInUserID(): String {
        return LetMeListenPreferences().readString(mActivity, USER_ID, "")!!
    }


}