package com.letmelisten.app.activities.advisor


import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProviders
import butterknife.ButterKnife
import butterknife.OnClick
import com.bumptech.glide.Glide
import com.github.dhaval2404.imagepicker.ImagePicker
import com.letmelisten.app.Model.CategoryListModel
import com.letmelisten.app.Model.DataItemm
import com.letmelisten.app.Model.NewEditProfileModel
import com.letmelisten.app.R
import com.letmelisten.app.activities.BaseActivity
import com.letmelisten.app.utils.*
import com.letmelisten.app.viewmodels.ProfileEditProfileViewModel
import kotlinx.android.synthetic.main.activity_edit_profile.*
import kotlinx.android.synthetic.main.activity_edit_profile2.*
import kotlinx.android.synthetic.main.activity_edit_profile2.categorySpinner
import kotlinx.android.synthetic.main.activity_edit_profile2.editFirstNameET
import kotlinx.android.synthetic.main.activity_edit_profile2.editLastNameET
import kotlinx.android.synthetic.main.activity_edit_profile2.editPhNumberET
import kotlinx.android.synthetic.main.activity_edit_profile2.editProfileIV
import kotlinx.android.synthetic.main.activity_sign_up.*
import kotlinx.android.synthetic.main.activity_sign_up_as_advisor.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.util.*


class EditProfileActivity : BaseActivity() {
    /*
      * Initialize Menifest Permissions:
      * & Camera Gallery Request @params
      * */
    var writeExternalStorage = Manifest.permission.WRITE_EXTERNAL_STORAGE
    var writeReadStorage = Manifest.permission.READ_EXTERNAL_STORAGE
    var writeCamera = Manifest.permission.CAMERA
    var mBitmap: Bitmap? = null
    private var viewModel: ProfileEditProfileViewModel? = null
    var mcategoryAL: ArrayList<DataItemm>? = ArrayList()
    var categoryId: String? = ""
    var defCategoryName: String? = ""
    var categoryName: String? = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_profile2)
        ButterKnife.bind(this)
        setStatusBar(mActivity)
        viewModel = ViewModelProviders.of(this).get(ProfileEditProfileViewModel::class.java)
        getProfileDetail()


    }

    private fun getProfileDetail() {
        Glide.with(mActivity)
            .load(LetMeListenPreferences().readString(mActivity, ADVISOR_IMAGE, null))
            .placeholder(R.drawable.ph)
            .error(R.drawable.ph)
            .into(editProfileIV)
        editFirstNameET.setText(LetMeListenPreferences().readString(mActivity, Advisor_NAME, null))
        editPhNumberET.setText(LetMeListenPreferences().readString(mActivity, ADVISOR_PHONNO, null))
        editDescription2ET.setText(
            LetMeListenPreferences().readString(
                mActivity,
                ADVISOR_DESCRIPTION,
                null
            )
        )
        defCategoryName = LetMeListenPreferences().readString(mActivity, ADVISOR_CATEGORYID, null)
        // editPaypalET.setText(LetMeListenPreferences().readString(mActivity, PAYPAL_ACCOUNT, null))
        showCursorAtEnd(editFirstNameET)
        showCursorAtEnd(editLastNameET)
        showCursorAtEnd(editPhNumberET)
        showCursorAtEnd(editDescription2ET)

        executeCategoryListRequest()
    }


    fun showCursorAtEnd(mEditText: EditText) {
        mEditText.setSelection(mEditText.text.toString().length)
    }

    @OnClick(
        R.id.txtSaveTV,
        R.id.backEditProfileRL,
        R.id.editProfileIV
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.txtSaveTV -> performSaveClick()
            R.id.backEditProfileRL -> onBackPressed()
            R.id.editProfileIV -> editProfileImageClick()
        }
    }

    private fun editProfileImageClick() {
        if (checkPermission()) {
            onSelectImageClick()
        } else {
            requestPermission()
        }
    }

    private fun checkPermission(): Boolean {
        val write = ContextCompat.checkSelfPermission(mActivity, writeExternalStorage)
        val read = ContextCompat.checkSelfPermission(mActivity, writeReadStorage)
        val camera = ContextCompat.checkSelfPermission(mActivity, writeCamera)
        return write == PackageManager.PERMISSION_GRANTED && read == PackageManager.PERMISSION_GRANTED && camera == PackageManager.PERMISSION_GRANTED
    }

    private fun requestPermission() {
        ActivityCompat.requestPermissions(
            mActivity, arrayOf(writeExternalStorage, writeReadStorage, writeCamera), 369
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            369 -> {
//                if (this[0] == PackageManager.PERMISSION_GRANTED) {
                if (checkPermission() == true) {
                    onSelectImageClick()
                } else {
                    requestPermission()
                    Log.e(TAG, "**Permission Denied**")
                }
            }
        }
    }

    fun IntArray.onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>?
    ) {
        when (requestCode) {
            369 -> {
                if (this[0] == PackageManager.PERMISSION_GRANTED) {
                    onSelectImageClick()
                } else {
                    Log.e(TAG, "**Permission Denied**")
                }
            }
        }
    }

    /**
     * Start pick image activity with chooser.
     */
    private fun onSelectImageClick() {
        ImagePicker.with(this)
            .crop()                    //Crop image(Optional), Check Customization for more option
            .compress(720)            //Final image size will be less than 1 MB(Optional)
            .maxResultSize(
                720,
                720
            )    //Final image resolution will be less than 1080 x 1080(Optional)
            .cropSquare()
            .start()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            //Image Uri will not be null for RESULT_OK
            val uri: Uri = data?.data!!

            // Use Uri object instead of File to avoid storage permissions
            editProfileIV.setImageURI(uri)
            val imageStream = contentResolver.openInputStream(uri)
            val selectedImage = BitmapFactory.decodeStream(imageStream)
            mBitmap = selectedImage
        } else if (resultCode == ImagePicker.RESULT_ERROR) {
            Toast.makeText(mActivity, "Image Not Valid", Toast.LENGTH_SHORT).show()
//            Toast.makeText(this, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(this, "Task Cancelled", Toast.LENGTH_SHORT).show()
        }
    }


    private fun performSaveClick() {
        if (isValidate())
            if (!isNetworkAvailable(mActivity)) {
                showAlertDialog(mActivity, getString(R.string.internet_connection_error))
            } else {
                executeEditProfileApi()
            }
    }

    private fun executeEditProfileApi() {
        showProgressDialog(mActivity)
        var mMultipartBody: MultipartBody.Part? = null
        if (mBitmap != null) {
            val requestFile2: RequestBody? = convertBitmapToByteArrayUncompressed(mBitmap!!)?.let {
                RequestBody.create(
                    "multipart/form-data".toMediaTypeOrNull(),
                    it
                )
            }
            mMultipartBody = requestFile2?.let {
                MultipartBody.Part.createFormData(
                    "advisorImage", getAlphaNumericString()!!.toString() + ".jpeg",
                    it
                )
            }
        }


        val advisorName: RequestBody = RequestBody.create(
            "multipart/form-data".toMediaTypeOrNull(),
            editFirstNameET.text.toString().trim()
        )


        val advisorPhoneno: RequestBody = RequestBody.create(
            "multipart/form-data".toMediaTypeOrNull(),
            editPhNumberET.text.toString().trim()
        )
        val advisorDescription: RequestBody = RequestBody.create(
            "multipart/form-data".toMediaTypeOrNull(),
            editDescription2ET.text.toString().trim()
        )

        val cateyId: RequestBody = RequestBody.create(
            "multipart/form-data".toMediaTypeOrNull(),
            categoryId.toString()
        )


        fun mHeaderParam(): HashMap<String, String> {
            val headers = HashMap<String, String>()
            LetMeListenPreferences().readString(mActivity, TOKEN, null)?.let {
                headers.put(
                    "Token",
                    it
                )
            }
            return headers
        }
        Log.e(
            TAG,
            "**allvalues**" + editDescription2ET.text.toString()
                .trim() + " " + editFirstNameET.text.toString()
                .trim() + " " + editFirstNameET.text.toString()
                .trim() + "" + editPhNumberET.text.toString().trim() + " " + cateyId.toString()
        )
        viewModel?.editAdvisorprofileData(
            mActivity,
            mHeaderParam(),
            advisorName,
            advisorPhoneno,
            advisorDescription,
            cateyId,
            mMultipartBody

        )?.observe(this,
            androidx.lifecycle.Observer<NewEditProfileModel?> { mEditProfileModel ->
                if (mEditProfileModel.status == 1) {
                    dismissProgressDialog()
                    showToast(mActivity, mEditProfileModel.message)
                    LetMeListenPreferences().writeString(
                        mActivity,
                        USER_ID,
                        mEditProfileModel.data?.userId
                    )
                    LetMeListenPreferences().writeString(
                        mActivity,
                        FIRST_NAME,
                        mEditProfileModel.data?.firstName
                    )
                    LetMeListenPreferences().writeString(
                        mActivity, LAST_NAME,
                        mEditProfileModel.data?.lastName
                    )
                    LetMeListenPreferences().writeString(
                        mActivity, USER_EMAIL,
                        mEditProfileModel.data?.email
                    )
                    LetMeListenPreferences().writeString(
                        mActivity, IS_ADVISOR,
                        mEditProfileModel.data?.isAdvisor
                    )

                    LetMeListenPreferences().writeString(
                        mActivity, ADVISOR_CATEGORYID,
                        mEditProfileModel.data?.categoryId
                    )
                    LetMeListenPreferences().writeString(
                        mActivity, Advisor_NAME,
                        mEditProfileModel.data?.advisorName
                    )

                    LetMeListenPreferences().writeString(
                        mActivity, ADVISOR_DESCRIPTION,
                        mEditProfileModel.data?.advisorDescription
                    )

                    LetMeListenPreferences().writeString(
                        mActivity, ADVISOR_PHONNO,
                        mEditProfileModel.data?.advisorPhoneno
                    )

                    LetMeListenPreferences().writeString(
                        mActivity, ADVISOR_IMAGE,
                        mEditProfileModel.data?.advisorImage
                    )



                    LetMeListenPreferences().writeString(
                        mActivity,
                        PROFILE_IMAGE,
                        mEditProfileModel.data?.profileImage
                    )
//                    LetMeListenPreferences().writeString(
//                        mActivity, VERIFIED,
//                        mEditProfileModel.data.verified
//                    )
                    LetMeListenPreferences().writeString(
                        mActivity,
                        VERIFICATION_CODE,
                        mEditProfileModel.data?.verificationCode
                    )
                    LetMeListenPreferences().writeString(
                        mActivity,
                        FB_TOKEN,
                        mEditProfileModel.data?.fbToken
                    )
                    LetMeListenPreferences().writeString(
                        mActivity,
                        GOOGLE_ID,
                        mEditProfileModel.data?.googleToken
                    )
                    LetMeListenPreferences().writeString(
                        mActivity,
                        CREATED,
                        mEditProfileModel.data?.created
                    )
                    LetMeListenPreferences().writeString(
                        mActivity,
                        PHONE,
                        mEditProfileModel.data?.phoneNo
                    )
                    LetMeListenPreferences().writeString(
                        mActivity,
                        PAYPAL_ACCOUNT,
                        mEditProfileModel.data?.paypalEmail
                    )
                    finish()
                } else {
                    dismissProgressDialog()
                    showAlertDialog(mActivity, mEditProfileModel.message)
                }
            })
    }


    /*
     * Check Validations of views
     * */
    fun isValidate(): Boolean {
        var flag = true
        if (editFirstNameET!!.text.toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_first_name))
            flag = false
        } else if (editPhNumberET!!.text.toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_ph_number))
            flag = false
        } else if (editPhNumberET.text.toString().length < 10) {
            showAlertDialog(mActivity, getString(R.string.please_enter_valid_ph_number))
            flag = false
        }
//        else if (editEmailET!!.text.toString().trim().equals("")) {
//            showAlertDialog(mActivity, getString(R.string.please_enter_email_address))
//            flag = false
//        } else if (!isValidEmailId(editEmailET!!.text.toString().trim())) {
//            showAlertDialog(mActivity, getString(R.string.please_enter_valid_email_address))
//            flag = false
//        } else if (editPasswordET!!.text.toString().trim().equals("")) {
//            showAlertDialog(mActivity, getString(R.string.please_enter_password))
//            flag = false
//        }
//        else if (editPaypalET!!.text.toString().trim().equals("")) {
//            showAlertDialog(mActivity, getString(R.string.please_enter_paypal))
//            flag = false
//        }
        return flag
    }


    /*
    * Execute api param
    * */

    private fun mHeaderParam(): HashMap<String, String> {
        val headers = HashMap<String, String>()
        mActivity?.let {
            LetMeListenPreferences().readString(it, TOKEN, null)?.let {
                headers.put(
                    "Token",
                    it
                )
            }
        }
        Log.e(
            TAG,
            "**Token**${mActivity?.let { LetMeListenPreferences().readString(it, TOKEN, null) }}"
        )
        return headers
    }

    private fun executeCategoryListRequest() {
        showProgressDialog(mActivity)
        this?.let {
            viewModel?.categoryListData(mActivity, mHeaderParam())?.observe(
                it,
                androidx.lifecycle.Observer<CategoryListModel?> { mCategoryListModel ->
                    if (mCategoryListModel.status == 1) {
                        dismissProgressDialog()

                        // showToast(getActivity(), mModel.getMessage());

                        val mModel = DataItemm()
                        mModel.categoryId = ""
                        mModel.name = "Select Category"
                        mcategoryAL?.add(mModel)
                        mCategoryListModel.data?.let { it1 -> mcategoryAL?.addAll(it1) }
//                        mcategoryAL = mCategoryListModel.data
                        setCategorySpinner()
                    }
                })
        }
    }

    private fun setCategorySpinner() {
//        val font = Typeface.createFromAsset(
//            mActivity.getAssets(),
//            "regular.ttf"
//        )
        val mColorAd: ArrayAdapter<DataItemm> = object :
            ArrayAdapter<DataItemm>(
                mActivity,
                android.R.layout.simple_spinner_item,
                mcategoryAL!!
            ) {
            override fun getDropDownView(
                position: Int,
                convertView: View?,
                parent: ViewGroup
            ): View {
                var v = convertView
                if (v == null) {
                    val mContext = this.context
                    if (mContext != null) {
                        val vi =
                            (mContext.getSystemService(LAYOUT_INFLATER_SERVICE) as LayoutInflater)
                        v = vi.inflate(R.layout.item_spinner, null)
                    }
                }
                //HideKey();
                val itemTextView = v?.findViewById<TextView>(R.id.itemTV)
                val mModel: DataItemm? = mcategoryAL?.get(position)
                itemTextView?.setText(mModel?.name)
                // itemTextView?.setTypeface(font)
                //                itemTextView.setText(categoriesArraylist[position]);
                return v!!
            }
        }
        categorySpinner.adapter = mColorAd
        //
//        /*Status Spinner Click Listener*/
        categorySpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View,
                position: Int,
                id: Long
            ) {
                //                category_sel = parent.getItemAtPosition(position).toString();
                var mModel: DataItemm? = mcategoryAL?.get(position)
                Log.e(TAG, "**CATEGORY_ID**" + mModel!!.categoryId)
                categoryId = mModel?.categoryId
                categoryName = mModel?.name

                (view as TextView).setTextColor(resources.getColor(R.color.colorBlack))
                view.setText(mModel?.name)
                view.setPadding(30, 0, 0, 0)
                view.textSize = 15f
                //view.setTypeface(font)
                categorySpinner.setSelection(position)
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }
        //Set Selections
        if (defCategoryName != null && defCategoryName!!.length > 0) {
            Log.e(TAG, "hghsd::$defCategoryName")
            for (i in mcategoryAL!!.indices) {
                if (defCategoryName == mcategoryAL!!.get(i).categoryId) {
                    categorySpinner.setSelection(i)
                }
            }
        }
    }


}