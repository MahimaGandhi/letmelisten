package com.letmelisten.app.activities

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProviders
import butterknife.ButterKnife
import butterknife.OnClick
import com.bumptech.glide.Glide
import com.github.dhaval2404.imagepicker.ImagePicker
import com.letmelisten.app.Model.EditProfileModel
import com.letmelisten.app.Model.NewEditProfileModel
import com.letmelisten.app.R
import com.letmelisten.app.utils.*
import com.letmelisten.app.viewmodels.ProfileEditProfileViewModel
import kotlinx.android.synthetic.main.activity_edit_profile.*
import kotlinx.android.synthetic.main.activity_edit_profile.editFirstNameET
import kotlinx.android.synthetic.main.activity_edit_profile.editLastNameET
import kotlinx.android.synthetic.main.activity_edit_profile.editPhNumberET
import kotlinx.android.synthetic.main.activity_edit_profile.editProfileIV
import kotlinx.android.synthetic.main.activity_edit_profile2.*
import kotlinx.android.synthetic.main.activity_sign_in.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.util.HashMap


class EditProfileActivity : BaseActivity() {
    /*
       * Initialize Menifest Permissions:
       * & Camera Gallery Request @params
       * */
    var writeExternalStorage = Manifest.permission.WRITE_EXTERNAL_STORAGE
    var writeReadStorage = Manifest.permission.READ_EXTERNAL_STORAGE
    var writeCamera = Manifest.permission.CAMERA
    var mBitmap: Bitmap? = null
    private var viewModel: ProfileEditProfileViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_profile)
        ButterKnife.bind(this)
        viewModel = ViewModelProviders.of(this).get(ProfileEditProfileViewModel::class.java)
        setStatusBar(mActivity)
        getProfileDetail()
    }

    private fun getProfileDetail() {
        Glide.with(mActivity)
            .load(LetMeListenPreferences().readString(mActivity, PROFILE_IMAGE, null))
            .placeholder(R.drawable.ph)
            .error(R.drawable.ph)
            .into(editProfileIV)
        editFirstNameET.setText(LetMeListenPreferences().readString(mActivity, FIRST_NAME, null))
        editLastNameET.setText(LetMeListenPreferences().readString(mActivity, LAST_NAME, null))
        editPhNumberET.setText(LetMeListenPreferences().readString(mActivity, PHONE, null))
        showCursorAtEnd(editFirstNameET)
        showCursorAtEnd(editLastNameET)
        showCursorAtEnd(editPhNumberET)
    }


    fun showCursorAtEnd(mEditText: EditText) {
        mEditText.setSelection(mEditText.text.toString().length)
    }

    @OnClick(
        R.id.txtSaveTV,
        R.id.backRL,
        R.id.editProfileIV
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.txtSaveTV -> performSaveClick()
            R.id.backRL -> onBackPressed()
            R.id.editProfileIV -> editProfileImageClick()
        }
    }

    private fun editProfileImageClick() {
        if (checkPermission()) {
            onSelectImageClick()
        } else {
            requestPermission()
        }
    }

    private fun checkPermission(): Boolean {
        val write = ContextCompat.checkSelfPermission(mActivity, writeExternalStorage)
        val read = ContextCompat.checkSelfPermission(mActivity, writeReadStorage)
        val camera = ContextCompat.checkSelfPermission(mActivity, writeCamera)
        return write == PackageManager.PERMISSION_GRANTED && read == PackageManager.PERMISSION_GRANTED && camera == PackageManager.PERMISSION_GRANTED
    }

    private fun requestPermission() {
        ActivityCompat.requestPermissions(
            mActivity, arrayOf(writeExternalStorage, writeReadStorage, writeCamera), 369
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            369 -> {
//                if (this[0] == PackageManager.PERMISSION_GRANTED) {
                if (checkPermission() == true) {
                    onSelectImageClick()
                } else {
                    requestPermission()
                    Log.e(TAG, "**Permission Denied**")
                }
            }
        }
    }

    fun IntArray.onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>?
    ) {
        when (requestCode) {
            369 -> {
                if (this[0] == PackageManager.PERMISSION_GRANTED) {
                    onSelectImageClick()
                } else {
                    Log.e(TAG, "**Permission Denied**")
                }
            }
        }
    }

    /**
     * Start pick image activity with chooser.
     */
    private fun onSelectImageClick() {
        ImagePicker.with(this)
            .crop()                    //Crop image(Optional), Check Customization for more option
            .compress(720)            //Final image size will be less than 1 MB(Optional)
            .maxResultSize(
                720,
                720
            )    //Final image resolution will be less than 1080 x 1080(Optional)
            .cropSquare()
            .start()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            //Image Uri will not be null for RESULT_OK
            val uri: Uri = data?.data!!

            // Use Uri object instead of File to avoid storage permissions
            editProfileIV.setImageURI(uri)
            val imageStream = contentResolver.openInputStream(uri)
            val selectedImage = BitmapFactory.decodeStream(imageStream)
            mBitmap = selectedImage
        } else if (resultCode == ImagePicker.RESULT_ERROR) {
            Toast.makeText(mActivity, "Image Not Valid", Toast.LENGTH_SHORT).show()
//            Toast.makeText(this, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(this, "Task Cancelled", Toast.LENGTH_SHORT).show()
        }
    }


    private fun performSaveClick() {
        if (isValidate())
            if (!isNetworkAvailable(mActivity)) {
                showAlertDialog(mActivity, getString(R.string.internet_connection_error))
            } else {
                executeEditProfileApi()
            }
    }

    private fun executeEditProfileApi() {
        showProgressDialog(mActivity)
        var mMultipartBody: MultipartBody.Part? = null
        if (mBitmap != null) {
            val requestFile1: RequestBody? = convertBitmapToByteArrayUncompressed(mBitmap!!)?.let {
                RequestBody.create(
                    "multipart/form-data".toMediaTypeOrNull(),
                    it
                )
            }
            mMultipartBody = requestFile1?.let {
                MultipartBody.Part.createFormData(
                    "profileImage", getAlphaNumericString()!!.toString() + ".jpeg",
                    it
                )
            }
        }
        val firstName: RequestBody = RequestBody.create(
            "multipart/form-data".toMediaTypeOrNull(),
            editFirstNameET.text.toString().trim()
        )
        val lastName: RequestBody = RequestBody.create(
            "multipart/form-data".toMediaTypeOrNull(),
            editLastNameET.text.toString().trim()
        )

        val phoneNo: RequestBody = RequestBody.create(
            "multipart/form-data".toMediaTypeOrNull(),
            editPhNumberET.text.toString().trim()
        )
        val advisor: RequestBody = RequestBody.create(
            "multipart/form-data".toMediaTypeOrNull(),
            "0"
        )
        val paypalEmail: RequestBody = RequestBody.create(
            "multipart/form-data".toMediaTypeOrNull(),
            ""
        )


        fun mHeaderParam(): HashMap<String, String> {
            val headers = HashMap<String, String>()
            LetMeListenPreferences().readString(mActivity, TOKEN, null)?.let {
                headers.put(
                    "Token",
                    it
                )
            }
            return headers
        }

        viewModel?.editprofileData(
            mActivity, mHeaderParam(), firstName, lastName, phoneNo,
           mMultipartBody
        )?.observe(this,
            androidx.lifecycle.Observer<NewEditProfileModel?> { mEditProfileModel ->
                if (mEditProfileModel.status == 1) {
                    dismissProgressDialog()
                    showToast(mActivity, mEditProfileModel.message)
                    LetMeListenPreferences().writeString(
                        mActivity,
                        USER_ID,
                        mEditProfileModel.data?.userId
                    )
                    LetMeListenPreferences().writeString(
                        mActivity,
                        FIRST_NAME,
                        mEditProfileModel.data?.firstName
                    )
                    LetMeListenPreferences().writeString(
                        mActivity, LAST_NAME,
                        mEditProfileModel.data?.lastName
                    )
                    LetMeListenPreferences().writeString(
                        mActivity, USER_EMAIL,
                        mEditProfileModel.data?.email
                    )
                    LetMeListenPreferences().writeString(
                        mActivity, IS_ADVISOR,
                        mEditProfileModel.data?.isAdvisor
                    )
                    LetMeListenPreferences().writeString(
                        mActivity,
                        PROFILE_IMAGE,
                        mEditProfileModel.data?.profileImage
                    )
//                    LetMeListenPreferences().writeString(
//                        mActivity, VERIFIED,
//                        mEditProfileModel.data.verified
//                    )
                    LetMeListenPreferences().writeString(
                        mActivity,
                        VERIFICATION_CODE,
                        mEditProfileModel.data?.verificationCode
                    )
                    LetMeListenPreferences().writeString(
                        mActivity,
                        FB_TOKEN,
                        mEditProfileModel.data?.fbToken
                    )
                    LetMeListenPreferences().writeString(
                        mActivity,
                        GOOGLE_ID,
                        mEditProfileModel.data?.googleToken
                    )
                    LetMeListenPreferences().writeString(
                        mActivity,
                        CREATED,
                        mEditProfileModel.data?.created
                    )
                    LetMeListenPreferences().writeString(
                        mActivity,
                        PHONE,
                        mEditProfileModel.data?.phoneNo
                    )
                    finish()
                } else {
                    dismissProgressDialog()
                    showAlertDialog(mActivity, mEditProfileModel.message)
                }
            })
    }

    /*
     * Check Validations of views
     * */
    fun isValidate(): Boolean {
        var flag = true
        if (editFirstNameET!!.text.toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_first_name))
            flag = false
        } else if (editLastNameET!!.text.toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_last_name))
            flag = false
        }

        else if (editPhNumberET!!.text.toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_ph_number))
            flag = false
        }
        else if (editPhNumberET.text.toString().length < 10) {
            showAlertDialog(mActivity, getString(R.string.please_enter_valid_ph_number))
            flag = false
        }
//        else if (editEmailET!!.text.toString().trim().equals("")) {
//            showAlertDialog(mActivity, getString(R.string.please_enter_email_address))
//            flag = false
//        }
//        else if (!isValidEmailId(editEmailET!!.text.toString().trim())) {
//            showAlertDialog(mActivity, getString(R.string.please_enter_valid_email_address))
//            flag = false
//        } else if (editPasswordET!!.text.toString().trim().equals("")) {
//            showAlertDialog(mActivity, getString(R.string.please_enter_password))
//            flag = false
//        }
        return flag
    }
}