package com.letmelisten.app.activities.advisor

import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import butterknife.ButterKnife
import butterknife.OnClick
import com.letmelisten.app.R
import com.letmelisten.app.activities.BaseActivity
import kotlinx.android.synthetic.main.activity_sign_up_as_advisor.*

class SignUpAsUserActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up_as_user)
        ButterKnife.bind(this)
        setStatusBar(mActivity)
        editDescriptionET.setOnTouchListener(View.OnTouchListener setOnTouchListener@{ v: View, event: MotionEvent ->
            if (editDescriptionET.hasFocus()) {
                v.parent.requestDisallowInterceptTouchEvent(true)
                when (event.action and MotionEvent.ACTION_MASK) {
                    MotionEvent.ACTION_SCROLL -> {
                        v.parent.requestDisallowInterceptTouchEvent(false)
                        return@setOnTouchListener true
                    }
                }
            }
            false
        })

    }

    @OnClick(
        R.id.backRL,
        R.id.txtSignUpTV
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.backRL -> onBackPressed()
            R.id.txtSignUpTV -> performSignUpClick()
        }
    }

    private fun performSignUpClick() {
        if (isValidate())
//            if (!isNetworkAvailable(mActivity)) {
//                showAlertDialog(mActivity, getString(R.string.internet_connection_error))
//            } else {
                finish()
//                executeSignUpApi()
            }

    /*
     * Check Validations of views
     * */
    fun isValidate(): Boolean {
        var flag = true
        if (editFirstNameET!!.text.toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_first_name))
            flag = false
        } else if (editLastNameET!!.text.toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_last_name))
            flag = false
        }else if (!isValidEmailId(editEmailET.text.toString().trim { it <= ' ' })) {
            showAlertDialog(mActivity, getString(R.string.please_enter_valid_email_address))
            flag = false
        } else if (!isValidEmailId(editEmailET!!.text.toString().trim())) {
            showAlertDialog(mActivity, getString(R.string.please_enter_valid_email_address))
            flag = false
        } else if (editPhNumberET.text.toString().trim { it <= ' ' } == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_phone))
            flag = false
        } else if (editPhNumberET.text.toString().length < 10 || editPhNumberET.text.toString().length>15) {
            showAlertDialog(mActivity, getString(R.string.please_enter_minimum_10_))
            flag = false
        }

//        else if (editDescriptionET.text.toString().trim { it <= ' ' } == "") {
//            showAlertDialog(mActivity, getString(R.string.enter_description))
//            flag = false
//        }else if (editDescriptionET.text.toString().length > 50) {
//            showAlertDialog(mActivity, getString(R.string.description_length))
//            flag = false
//        }
        return flag
    }
}