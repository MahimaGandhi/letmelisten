package com.letmelisten.app.activities

import android.app.AlertDialog
import android.content.DialogInterface
import android.os.Bundle
import android.util.Log
import android.view.View
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.RelativeLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.letmelisten.app.R
import com.letmelisten.app.utils.PAYPAL_URL

class PaypalConnectActivity : BaseActivity() {

    @BindView(R.id.webView)
    lateinit var webView: WebView

    @BindView(R.id.imgBackIV)
    lateinit var imgBackIV: RelativeLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_paypal_connect)
        ButterKnife.bind(this)
        setStatusBar(mActivity)

        if (intent != null) {
            val url = intent.extras!!.getString(PAYPAL_URL)
            Log.e(TAG, "**URL**" + url)
            val settings: WebSettings = webView.getSettings()
            settings.javaScriptEnabled = true
            webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY)
            val alertDialog = AlertDialog.Builder(this).create()
            webView.setWebViewClient(object : WebViewClient() {
                override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                    Log.i(TAG, "Processing webview url click...")
                    view.loadUrl(url)
                    return true
                }

                override fun onPageFinished(view: WebView, url: String) {
                    Log.i(TAG, "Finished loading URL: $url")
//                    if (url.contains(Constants.last_page_paypal_url)) {
//                        thankyoouBt.setVisibility(View.VISIBLE)
//                        thankyoouBt.setOnClickListener(View.OnClickListener { finish() })
//                    } else {
//                        thankyoouBt.setVisibility(View.GONE)
//                    }
                    //                    if (progressBar.isShowing()) {
//                        progressBar.dismiss();
//                    }
                }

                override fun onReceivedError(
                    view: WebView,
                    errorCode: Int,
                    description: String,
                    failingUrl: String
                ) {
                    Log.e(TAG, "Error: $description")
                    Toast.makeText(mActivity, "Oh no! $description", Toast.LENGTH_SHORT).show()
                    alertDialog.setTitle("Error")
                    alertDialog.setMessage(description)
                    alertDialog.setButton("OK",
                        DialogInterface.OnClickListener { dialog, which -> return@OnClickListener })
                    alertDialog.show()
                }
            })
            webView.loadUrl(url.toString())
        }
    }

    @OnClick(
        R.id.imgBackIV
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.imgBackIV -> onBackPressed()
        }
    }
}