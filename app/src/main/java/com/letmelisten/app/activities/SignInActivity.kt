package com.letmelisten.app.activities

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.lifecycle.ViewModelProviders
import butterknife.ButterKnife
import butterknife.OnClick
import com.facebook.*
import com.facebook.appevents.AppEventsLogger
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.FirebaseApp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.messaging.FirebaseMessaging
import com.letmelisten.app.Model.LoginModel
import com.letmelisten.app.R
import com.letmelisten.app.utils.*
import com.letmelisten.app.viewmodels.SignupLoginViewModel
import kotlinx.android.synthetic.main.activity_sign_in.*
import kotlinx.android.synthetic.main.activity_sign_in.editEmailET
import kotlinx.android.synthetic.main.activity_sign_in.editPasswordET
import kotlinx.android.synthetic.main.activity_sign_up.*
import org.json.JSONObject
import java.net.URL
import java.util.*

class SignInActivity : BaseActivity() {
    private var profile_pic: URL? = null
    var pro_pic = ""
    var google_name: String? = null
    var mGoogleSignInClient: GoogleSignInClient? = null
    protected val GOOGLE_SIGN_IN = 12324
    var id: String? = null
    var first_name: String? = null
    var last_name: String? = null
    var email: String? = null
    var social_user_name: String? = null
    var mCallbackManager: CallbackManager? = null

    //    var arr_type = ""
    var type = "0"

    //Firebase
    private var mFirebaseAuth: FirebaseAuth? = null

    private var viewModel: SignupLoginViewModel? = null
    var strDeviceToken: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_in)
        ButterKnife.bind(this)
        viewModel = ViewModelProviders.of(this).get(SignupLoginViewModel::class.java)
        setStatusBar(mActivity)
        initializeGoogle()
        getDeviceToken()
        setEditTextFocused(mActivity, editPasswordET)
//        setCurrencyAdapter()
    }


//    private fun setCurrencyAdapter() {
//        val mTypeArry = resources.getStringArray(R.array.type_array)
//        // access the spinner
//        if (typeSpinner != null) {
//            val adapter = ArrayAdapter(
//                mActivity,
//                android.R.layout.simple_spinner_item, mTypeArry
//            )
//            typeSpinner.adapter = adapter
//            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
//
//            typeSpinner.onItemSelectedListener = object :
//                AdapterView.OnItemSelectedListener {
//                override fun onItemSelected(
//                    parent: AdapterView<*>,
//                    view: View, position: Int, id: Long
//                ) {
//                    arr_type = mTypeArry[position]
//                    if (arr_type.equals("Advisor")) {
//                        type = "1"
//                        signupRL.isVisible=false
//                    } else {
//                        type = "0"
//                        signupRL.isVisible=true
//                    }
//                    Log.e(TAG, "type" + type)
//                }
//
//                override fun onNothingSelected(parent: AdapterView<*>) {
//                    // write code to perform some action
//                }
//            }
//        }
//    }


    private fun initializeGoogle() {
        FirebaseApp.initializeApp(this)
        // Configure Google Sign In
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.default_web_client_id))
            .requestEmail()
            .build()

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso)
        mFirebaseAuth = FirebaseAuth.getInstance()
        FirebaseAuth.getInstance().signOut()
        LoginManager.getInstance().logOut()
        signOut()
    }

    private fun signOut() {
        if (mGoogleSignInClient != null) {
            mGoogleSignInClient!!.signOut()
                .addOnCompleteListener(this) {
                    Log.e(TAG, "==Logout Successfully==")
                }
        }
    }

    private fun getDeviceToken() {
        FirebaseMessaging.getInstance().token
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    Log.w(TAG, "Fetching FCM registration token failed", task.exception)
                    return@OnCompleteListener
                }

                // Get new FCM registration token
                strDeviceToken = task.result!!
                LetMeListenPreferences().writeString(
                    mActivity,
                    DEVICE_TOKEN,
                    strDeviceToken
                )
                Log.e(TAG, "**Push Token**$strDeviceToken")
                // Log and toast
                Log.d(TAG, "Token********   $strDeviceToken")
            })

    }


    @OnClick(
        R.id.txtLogInTV,
        R.id.txtForgotPasswordTV,
        R.id.txtSignUpTV,
        R.id.imgGoogleIV,
        R.id.imgFacebookIV
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.txtLogInTV -> performLogInClick()
            R.id.txtForgotPasswordTV -> performForgotPwdClick()
            R.id.txtSignUpTV -> performSignUpClick()
            R.id.imgGoogleIV -> performGoogleClick()
            R.id.imgFacebookIV ->
                showToast(mActivity, getString(R.string.coming_soon))
            //performFbClick()
        }
    }

    private fun performFbClick() {

        preventMultipleClick()
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error))
        } else {
            FacebookSdk.sdkInitialize(applicationContext)
            LoginManager.getInstance().logOut()
            AppEventsLogger.activateApp(application)
            mCallbackManager = CallbackManager.Factory.create()
            loginWithFacebook()
        }
    }

    private fun loginWithFacebook() {
        if (isNetworkAvailable(mActivity)) {
            LoginManager.getInstance()
                .logInWithReadPermissions(mActivity, Arrays.asList("public_profile", "email"))
            LoginManager.getInstance()
                .registerCallback(mCallbackManager, object : FacebookCallback<LoginResult> {
                    override fun onSuccess(loginResult: LoginResult) {
                        Log.d("onSuccess: ", loginResult.accessToken.token)
                        getFacebookData(loginResult)
                    }

                    override fun onCancel() {
//                    Toast.makeText(mActivity, "Cancel", Toast.LENGTH_SHORT).show();
                    }

                    override fun onError(error: FacebookException) {
                        Toast.makeText(mActivity, error.toString(), Toast.LENGTH_SHORT).show()
                    }
                })
        } else {
            showToast(mActivity, getString(R.string.internet_connection_error))
        }
    }

    private fun getFacebookData(loginResult: LoginResult) {
        showProgressDialog(mActivity)
        val graphRequest = GraphRequest.newMeRequest(
            loginResult.accessToken
        ) { `object`, response ->
            dismissProgressDialog()
            try {
                if (`object`.has("id")) {
                    id = `object`.getString("id")
                    Log.e("LoginActivity", "id$id")
                }
                //check permission first userName
                if (`object`.has("first_name")) {
                    first_name = `object`.getString("first_name")
                    Log.e("LoginActivity", "first_name$first_name")
                }
                //check permisson last userName
                if (`object`.has("last_name")) {
                    last_name = `object`.getString("last_name")
                    Log.e("LoginActivity", "last_name$last_name")
                }
                //check permisson email
                if (`object`.has("email")) {
                    email = `object`.getString("email")
                    Log.e("LoginActivity", "email$email")
                }
                val jsonObject = JSONObject(`object`.getString("picture"))
                if (jsonObject != null) {
                    val dataObject = jsonObject.getJSONObject("data")
                    Log.e("Loginactivity", "json oject get picture$dataObject")
                    profile_pic =
                        URL("https://graph.facebook.com/$id/picture?width=500&height=500")
                    Log.e("LoginActivity", "json object=>$`object`")
                }
                pro_pic = if (profile_pic != null) {
                    profile_pic.toString()
                } else {
                    ""
                }
                social_user_name = "$first_name $last_name"
                executeLoginWithFbApi()
            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }
        }
        val bundle = Bundle()
        Log.e("LoginActivity", "bundle set")
        bundle.putString("fields", "id, first_name, last_name,email,picture")
        graphRequest.parameters = bundle
        graphRequest.executeAsync()
    }


    /*
 * Execute fb api
 * */
    private fun mFbParams(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["firstName"] = first_name!!
        mMap["lastName"] = last_name!!
        mMap["email"] = email!!
        mMap["fbToken"] = id!!
        mMap["profileImage"] = pro_pic
        mMap["deviceToken"] = strDeviceToken
        mMap["deviceType"] = "2"
        mMap["isAdvisor"] = type
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeLoginWithFbApi() {
        showProgressDialog(mActivity)
        viewModel?.signInWithFbRequest(mActivity, mFbParams())?.observe(this,
            androidx.lifecycle.Observer<LoginModel?> { mLogInWithfbModel ->
                if (mLogInWithfbModel.status == 1) {
                    dismissProgressDialog()
//                    showToast(mActivity, mLogInWithfbModel.message)
                    LetMeListenPreferences().writeBoolean(mActivity, IS_LOGIN, true)
                    LetMeListenPreferences().writeString(
                        mActivity,
                        USER_ID,
                        mLogInWithfbModel.data.userId
                    )
                    Log.e(TAG, "**USER_ID**" + USER_ID);
                    LetMeListenPreferences().writeString(
                        mActivity,
                        FIRST_NAME,
                        mLogInWithfbModel.data.firstName
                    )
                    LetMeListenPreferences().writeString(
                        mActivity, LAST_NAME,
                        mLogInWithfbModel.data.lastName
                    )
                    LetMeListenPreferences().writeString(
                        mActivity, USER_EMAIL,
                        mLogInWithfbModel.data.email
                    )


                    LetMeListenPreferences().writeString(
                        mActivity, ADVISOR_CATEGORYID,
                        mLogInWithfbModel.data.categoryId
                    )

                    LetMeListenPreferences().writeString(
                        mActivity, ADVISOR_DESCRIPTION,
                        mLogInWithfbModel.data.advisorDescription
                    )

                    LetMeListenPreferences().writeString(
                        mActivity, ADVISOR_PHONNO,
                        mLogInWithfbModel.data.advisorPhoneno
                    )

                    LetMeListenPreferences().writeString(
                        mActivity, ADVISOR_IMAGE,
                        mLogInWithfbModel.data.advisorImage
                    )

                    LetMeListenPreferences().writeString(
                        mActivity, Advisor_EMAIL,
                        mLogInWithfbModel.data.advisorEmail
                    )
                    LetMeListenPreferences().writeString(
                        mActivity, Advisor_NAME,
                        mLogInWithfbModel.data.advisorName
                    )

                    LetMeListenPreferences().writeString(
                        mActivity, IS_ADVISOR,
                        mLogInWithfbModel.data.isAdvisor
                    )
                    LetMeListenPreferences().writeString(
                        mActivity, ADV_REQ,
                        mLogInWithfbModel.data.advisorReq
                    )
                    LetMeListenPreferences().writeString(
                        mActivity,
                        PROFILE_IMAGE,
                        mLogInWithfbModel.data.profileImage
                    )
                    LetMeListenPreferences().writeString(
                        mActivity, ADVISOR_PRICE,
                        mLogInWithfbModel.data.advisorPrice
                    )
                    LetMeListenPreferences().writeString(
                        mActivity,
                        VERIFICATION_CODE,
                        mLogInWithfbModel.data.verificationCode
                    )
                    LetMeListenPreferences().writeString(
                        mActivity,
                        FB_TOKEN,
                        mLogInWithfbModel.data.fbToken
                    )
                    LetMeListenPreferences().writeString(
                        mActivity,
                        GOOGLE_ID,
                        mLogInWithfbModel.data.googleToken
                    )
                    LetMeListenPreferences().writeString(
                        mActivity,
                        CREATED,
                        mLogInWithfbModel.data.created
                    )
                    LetMeListenPreferences().writeString(
                        mActivity,
                        TOKEN,
                        mLogInWithfbModel.data.authToken
                    )
                    LetMeListenPreferences().writeString(
                        mActivity,
                        PHONE,
                        mLogInWithfbModel.data.phoneNo
                    )
                    LetMeListenPreferences().writeString(
                        mActivity,
                        WALLET_AMOUNT,
                        mLogInWithfbModel.data.walletAmount
                    )

                    LetMeListenPreferences().writeString(
                        mActivity,
                        AVALIABLE_STATUS,
                        mLogInWithfbModel.data.avaliableStatus
                    )
                    LetMeListenPreferences().writeString(
                        mActivity,
                        AVALIABLE_CALL,
                        mLogInWithfbModel.data.avaliableCall
                    )
                    LetMeListenPreferences().writeString(
                        mActivity,
                        AVALIABLE_CHAT,
                        mLogInWithfbModel.data.avaliableChat
                    )

                    val intent = Intent(mActivity, HomeActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    startActivity(intent)
                    finish()

//                    if (mLogInWithfbModel.data.isAdvisor.equals("0")) {
//                        val intent = Intent(mActivity, HomeActivity::class.java)
//                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK)
//                        startActivity(intent)
//                        finish()
//                    } else {
//                        val intent = Intent(
//                            mActivity,
//                            com.letmelisten.app.activities.advisor.HomeActivity::class.java
//                        )
//                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK)
//                        startActivity(intent)
//                        finish()
//                    }
                } else {
                    dismissProgressDialog()
                    showAlertDialog(mActivity, mLogInWithfbModel.message)
                }
            })
    }


    private fun performGoogleClick() {
        preventMultipleClick()
        val signInIntent: Intent = mGoogleSignInClient!!.signInIntent
        startActivityForResult(signInIntent, GOOGLE_SIGN_IN)
    }

    private fun handleSignInResult(completedTask: Task<GoogleSignInAccount>) {
        try {
            val account = completedTask.getResult(ApiException::class.java)
            email = account!!.email
            id = account.id
            google_name = account.displayName

            val name = account.displayName

            val idx = name!!.lastIndexOf(' ')
            if (idx == -1) {
                return
            }

            first_name = name.substring(0, idx)
            last_name = name.substring(idx + 1)
            Log.e("SPLITED NAME", first_name + " - " + last_name)

            pro_pic = if (account.photoUrl != null) {
                account.photoUrl.toString()
            } else {
                ""
            }
            executeGoogleApi()
        } catch (e: ApiException) {
            Log.e("MyTAG", "signInResult:failed code=" + e.statusCode)
            showToast(mActivity, "Failed")
        }
    }


    @RequiresApi(Build.VERSION_CODES.KITKAT)
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (mCallbackManager != null) {
            // Pass the activity result back to the Facebook SDK
            mCallbackManager!!.onActivityResult(requestCode, resultCode, data)
        }
        when (requestCode) {
            GOOGLE_SIGN_IN -> {
                val completedTask: Task<GoogleSignInAccount> =
                    GoogleSignIn.getSignedInAccountFromIntent(data)
                handleSignInResult(completedTask)
            }
        }
    }


    private fun executeGoogleApi() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error))
        } else {
            loginWithGoogle()
        }
    }

    /*
     * Execute google api
     * */

    private fun mGoogleParams(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["firstName"] = first_name!!
        mMap["lastName"] = last_name!!
        mMap["email"] = email!!
        mMap["googleToken"] = id!!
        mMap["deviceToken"] = strDeviceToken
        mMap["deviceType"] = "2"
        mMap["profileImage"] = pro_pic
        mMap["isAdvisor"] = type
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun loginWithGoogle() {
        showProgressDialog(mActivity)
        viewModel?.signInWithGoogleRequest(mActivity, mGoogleParams())?.observe(this,
            androidx.lifecycle.Observer<LoginModel?> { mLogInWithGoogleModel ->
                if (mLogInWithGoogleModel.status == 1) {
                    dismissProgressDialog()
//                    showToast(mActivity, mLogInWithGoogleModel.message)
                    LetMeListenPreferences().writeBoolean(mActivity, IS_LOGIN, true)
                    LetMeListenPreferences().writeString(
                        mActivity,
                        USER_ID,
                        mLogInWithGoogleModel.data.userId
                    )
                    LetMeListenPreferences().writeString(
                        mActivity,
                        com.letmelisten.app.utils.FIRST_NAME,
                        mLogInWithGoogleModel.data.firstName
                    )
                    LetMeListenPreferences().writeString(
                        mActivity, LAST_NAME,
                        mLogInWithGoogleModel.data.lastName
                    )
                    LetMeListenPreferences().writeString(
                        mActivity, USER_EMAIL,
                        mLogInWithGoogleModel.data.email
                    )
                    LetMeListenPreferences().writeString(
                        mActivity, Advisor_EMAIL,
                        mLogInWithGoogleModel.data.advisorEmail
                    )


                    LetMeListenPreferences().writeString(
                        mActivity, ADVISOR_CATEGORYID,
                        mLogInWithGoogleModel.data.categoryId
                    )

                    LetMeListenPreferences().writeString(
                        mActivity, ADVISOR_DESCRIPTION,
                        mLogInWithGoogleModel.data.advisorDescription
                    )

                    LetMeListenPreferences().writeString(
                        mActivity, ADVISOR_PHONNO,
                        mLogInWithGoogleModel.data.advisorPhoneno
                    )

                    LetMeListenPreferences().writeString(
                        mActivity, ADVISOR_IMAGE,
                        mLogInWithGoogleModel.data.advisorImage
                    )


                    LetMeListenPreferences().writeString(
                        mActivity, IS_ADVISOR,
                        mLogInWithGoogleModel.data.isAdvisor
                    )
                    LetMeListenPreferences().writeString(
                        mActivity, ADV_REQ,
                        mLogInWithGoogleModel.data.advisorReq
                    )
                    LetMeListenPreferences().writeString(
                        mActivity,
                        PROFILE_IMAGE,
                        mLogInWithGoogleModel.data.profileImage
                    )
                    LetMeListenPreferences().writeString(
                        mActivity, Advisor_NAME,
                        mLogInWithGoogleModel.data.advisorName
                    )
                    LetMeListenPreferences().writeString(
                        mActivity, ADVISOR_PRICE,
                        mLogInWithGoogleModel.data.advisorPrice
                    )
                    LetMeListenPreferences().writeString(
                        mActivity,
                        VERIFICATION_CODE,
                        mLogInWithGoogleModel.data.verificationCode
                    )
                    LetMeListenPreferences().writeString(
                        mActivity,
                        FB_TOKEN,
                        mLogInWithGoogleModel.data.fbToken
                    )
                    LetMeListenPreferences().writeString(
                        mActivity,
                        GOOGLE_ID,
                        mLogInWithGoogleModel.data.googleToken
                    )
                    LetMeListenPreferences().writeString(
                        mActivity,
                        CREATED,
                        mLogInWithGoogleModel.data.created
                    )

                    LetMeListenPreferences().writeString(
                        mActivity,
                        TOKEN,
                        mLogInWithGoogleModel.data.authToken
                    )
                    LetMeListenPreferences().writeString(
                        mActivity,
                        PHONE,
                        mLogInWithGoogleModel.data.phoneNo
                    )
                    LetMeListenPreferences().writeString(
                        mActivity,
                        WALLET_AMOUNT,
                        mLogInWithGoogleModel.data.walletAmount
                    )
                    LetMeListenPreferences().writeString(
                        mActivity,
                        AVALIABLE_STATUS,
                        mLogInWithGoogleModel.data.avaliableStatus
                    )
                    LetMeListenPreferences().writeString(
                        mActivity,
                        AVALIABLE_CALL,
                        mLogInWithGoogleModel.data.avaliableCall
                    )
                    LetMeListenPreferences().writeString(
                        mActivity,
                        AVALIABLE_CHAT,
                        mLogInWithGoogleModel.data.avaliableChat
                    )
                    val intent = Intent(mActivity, HomeActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    startActivity(intent)
                    finish()
//                    if (mLogInWithGoogleModel.data.isAdvisor.equals("0")) {
//                        val intent = Intent(mActivity, HomeActivity::class.java)
//                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK)
//                        startActivity(intent)
//                        finish()
//                    } else {
//                        val intent = Intent(
//                            mActivity,
//                            com.letmelisten.app.activities.advisor.HomeActivity::class.java
//                        )
//                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK)
//                        startActivity(intent)
//                        finish()
//                    }

                } else {
                    dismissProgressDialog()
                    showAlertDialog(mActivity, mLogInWithGoogleModel.message)
                }
            })

    }


    private fun performLogInClick() {
        if (isValidate())
            if (!isNetworkAvailable(mActivity)) {
                showAlertDialog(mActivity, getString(R.string.internet_connection_error))
            } else {
                executeLoginApi()
            }
    }


    private fun mParams(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["email"] = editEmailET.text.toString().trim { it <= ' ' }
        mMap["password"] = editPasswordET.text.toString().trim { it <= ' ' }
        mMap["deviceToken"] = strDeviceToken
        mMap["deviceType"] = "2"
//        mMap["isAdvisor"] = type
        Log.e("params", "**PARAM**$mMap")
        return mMap
    }

    private fun executeLoginApi() {
        showProgressDialog(mActivity)
        viewModel?.getLogInData(mActivity, mParams())?.observe(this,
            androidx.lifecycle.Observer<LoginModel?> { mLoginModel ->
                if (mLoginModel.status == 1) {
                    dismissProgressDialog()
//                    showToast(mActivity, mLoginModel.message)
                    LetMeListenPreferences().writeBoolean(mActivity, IS_LOGIN, true)
                    LetMeListenPreferences().writeString(
                        mActivity,
                        USER_ID,
                        mLoginModel.data.userId
                    )
                    Log.e(TAG, "**USER_ID**" + mLoginModel.data.userId);
                    LetMeListenPreferences().writeString(
                        mActivity,
                        FIRST_NAME,
                        mLoginModel.data.firstName
                    )
                    LetMeListenPreferences().writeString(
                        mActivity, LAST_NAME,
                        mLoginModel.data.lastName
                    )
                    LetMeListenPreferences().writeString(
                        mActivity, USER_EMAIL,
                        mLoginModel.data.email
                    )
                    LetMeListenPreferences().writeString(
                        mActivity, Advisor_NAME,
                        mLoginModel.data.advisorName
                    )
                    LetMeListenPreferences().writeString(
                        mActivity, Advisor_EMAIL,
                        mLoginModel.data.advisorEmail
                    )

                    LetMeListenPreferences().writeString(
                        mActivity, ADVISOR_CATEGORYID,
                        mLoginModel.data.categoryId
                    )

                    LetMeListenPreferences().writeString(
                        mActivity, ADVISOR_DESCRIPTION,
                        mLoginModel.data.advisorDescription
                    )

                    LetMeListenPreferences().writeString(
                        mActivity, ADVISOR_PHONNO,
                        mLoginModel.data.advisorPhoneno
                    )

                    LetMeListenPreferences().writeString(
                        mActivity, ADVISOR_IMAGE,
                        mLoginModel.data.advisorImage
                    )

                    LetMeListenPreferences().writeString(
                        mActivity, IS_ADVISOR,
                        mLoginModel.data.isAdvisor
                    )


                    LetMeListenPreferences().writeString(
                        mActivity, ADV_REQ,
                        mLoginModel.data.advisorReq
                    )
                    LetMeListenPreferences().writeString(
                        mActivity,
                        PROFILE_IMAGE,
                        mLoginModel.data.profileImage
                    )
                    LetMeListenPreferences().writeString(
                        mActivity, ADVISOR_PRICE,
                        mLoginModel.data.advisorPrice
                    )
                    LetMeListenPreferences().writeString(
                        mActivity,
                        VERIFICATION_CODE,
                        mLoginModel.data.verificationCode
                    )
                    LetMeListenPreferences().writeString(
                        mActivity,
                        FB_TOKEN,
                        mLoginModel.data.fbToken
                    )
                    LetMeListenPreferences().writeString(
                        mActivity,
                        GOOGLE_ID,
                        mLoginModel.data.googleToken
                    )
                    LetMeListenPreferences().writeString(
                        mActivity,
                        CREATED,
                        mLoginModel.data.created
                    )
                    LetMeListenPreferences().writeString(
                        mActivity,
                        TOKEN,
                        mLoginModel.data.authToken
                    )
                    LetMeListenPreferences().writeString(
                        mActivity,
                        PHONE,
                        mLoginModel.data.phoneNo
                    )
                    LetMeListenPreferences().writeString(
                        mActivity,
                        WALLET_AMOUNT,
                        mLoginModel.data.walletAmount
                    )
                    LetMeListenPreferences().writeString(
                        mActivity,
                        AVALIABLE_STATUS,
                        mLoginModel.data.avaliableStatus
                    )
                    LetMeListenPreferences().writeString(
                        mActivity,
                        AVALIABLE_CALL,
                        mLoginModel.data.avaliableCall
                    )
                    LetMeListenPreferences().writeString(
                        mActivity,
                        AVALIABLE_CHAT,
                        mLoginModel.data.avaliableChat
                    )
                    val intent = Intent(mActivity, HomeActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    startActivity(intent)
                    finish()
//                    if (mLoginModel.data.isAdvisor.equals("0")) {
//                        val intent = Intent(mActivity, HomeActivity::class.java)
//                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK)
//                        startActivity(intent)
//                        finish()
//                    } else {
//                        val intent = Intent(
//                            mActivity,
//                            com.letmelisten.app.activities.advisor.HomeActivity::class.java
//                        )
//                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK)
//                        startActivity(intent)
//                        finish()
//                    }
                } else {
                    dismissProgressDialog()
                    showAlertDialog(mActivity, mLoginModel.message)
                }
            })
    }

    private fun performForgotPwdClick() {
        startActivity(Intent(mActivity, ForgotPwdActivity::class.java))
//        finish()
    }

    private fun performSignUpClick() {
        startActivity(Intent(mActivity, SignUpActivity::class.java))
//        finish()
    }

    /*
     * Check Validations of views
     * */
    fun isValidate(): Boolean {
        var flag = true
        if (editEmailET!!.text.toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_mail))
            flag = false
        } else if (!isValidEmailId(editEmailET!!.text.toString().trim())) {
            showAlertDialog(mActivity, getString(R.string.please_enter_valid_email_address))
            flag = false
        } else if (editPasswordET!!.text.toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_password))
            flag = false
        }
        return flag
    }
}