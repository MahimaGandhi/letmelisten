package com.letmelisten.app.activities

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.InputType
import android.util.Log
import android.view.Gravity
import android.view.View
import android.view.Window
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.widget.ImageView
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.bumptech.glide.Glide
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.letmelisten.app.Interfac.FavUpdateInterface

import com.letmelisten.app.Model.AdvisorDetailsModel
import com.letmelisten.app.Model.Data
import com.letmelisten.app.Model.FavUnfavModel
import com.letmelisten.app.Model.PlanDetail
import com.letmelisten.app.R
import com.letmelisten.app.adapters.PricingAdapter
import com.letmelisten.app.utils.*
import com.letmelisten.app.viewmodels.HomeViewModel
import kotlinx.android.synthetic.main.activity_add_money_to_wallet.*
import kotlinx.android.synthetic.main.activity_sign_in.*
import kotlinx.android.synthetic.main.activity_user_detail.*
import rjsv.circularview.CircleView
import rjsv.circularview.CircleViewAnimation
import rjsv.circularview.enumerators.AnimationStyle
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList
import kotlin.collections.MutableMap
import kotlin.collections.set
import kotlin.math.roundToInt

class UserDetailActivity : BaseActivity(), PricingAdapter.OnItemClickListener {
    var behavior: BottomSheetBehavior<*>? = null

    @BindView(R.id.pricingRV)
    lateinit var pricingRV: RecyclerView

    lateinit var pricingAdapter: PricingAdapter
    var advisorId: String = ""
    private var viewModel: HomeViewModel? = null
    var mDetailsList: ArrayList<Data>? = ArrayList()
    var mPlanDetailList: ArrayList<PlanDetail>? = ArrayList()
    var onItemClickListener: PricingAdapter.OnItemClickListener? = null
    var onItemClickk: FavUpdateInterface? = null

    private var mPos = 0
    private var planId = ""
    private var mPrice = ""
    private var mTime = ""
    private var timeSelected = ""
    private var mTotalPrice = 0
    private var perMinCost = 0
    private var time = 0
    var mFavStatus: Boolean = false
    var value: Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_detail)
        ButterKnife.bind(this)
        setStatusBar(mActivity)
        viewModel = ViewModelProviders.of(this).get(HomeViewModel::class.java)
        onItemClickListener = this
        getIntentData()
        getAdvisorDetails()
    }

    private fun getIntentData() {
        if (intent != null) {
            advisorId = intent.getStringExtra("advisorId").toString()
        }
    }

    private fun getAdvisorDetails() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error))
        } else {
            executeGetAdvisorDetailsApi()
        }
    }


    private fun mParams(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["advisorId"] = advisorId
        Log.e("params", "c$mMap")
        return mMap
    }

    fun mHeaderParam(): HashMap<String, String> {
        val headers = HashMap<String, String>()
        LetMeListenPreferences().readString(mActivity, TOKEN, null)?.let {
            headers.put(
                "Token",
                it
            )
        }
        return headers
    }

    private fun executeGetAdvisorDetailsApi() {
        showProgressDialog(mActivity)
        viewModel?.getAdvisorDetails(mActivity, mParams(), mHeaderParam())?.observe(this,
            androidx.lifecycle.Observer<AdvisorDetailsModel?> { mAdvisorModel ->
                if (mAdvisorModel.status == 1) {
                    dismissProgressDialog()
                    Glide.with(mActivity)
                        .load(mAdvisorModel.data.advisorImage)
                        .placeholder(R.drawable.ic_phh)
                        .error(R.drawable.ic_phh)
                        .into(imgAdvIV)
                    LetMeListenPreferences().writeString(
                        mActivity, SEL_ADVISOR_IMAGE,
                        mAdvisorModel.data.advisorImage
                    )
                    LetMeListenPreferences().writeString(
                        mActivity, SEL_ADVISOR_NAME,
                        mAdvisorModel.data.advisorName
                    )
                    // txtNameTV.text = mAdvisorModel.data.firstName + " " + mAdvisorModel.data.lastName
                    txtNameTV.text = mAdvisorModel.data.advisorName
                    perMinTV.text = "Per min $" + mAdvisorModel.data.perMin
                    txtPerMinCost.text = "$" + mAdvisorModel.data.perMin
                    perMinCost = mAdvisorModel.data.perMin
//                    txtHeaderNameTV.text =
//                        mAdvisorModel.data.firstName + " " + mAdvisorModel.data.lastName
                    txtHeaderNameTV.text = mAdvisorModel.data.advisorName
                    txtPriceTV.text = "$ " + mAdvisorModel.data.advisorPrice
                    txtDescTV.text = mAdvisorModel.data.advisorDescription
                    ratingbarRB.rating = mAdvisorModel.data.rating.toFloat()
                    txtReviewsTV.text =
                        "(" + mAdvisorModel.data.totalReviews.toString() + " " + mActivity.getString(
                            R.string.reviews
                        ) + ")"
                    if (mAdvisorModel!!.data.isFav.equals("1")) {
                        imgFavIV.setImageResource(R.drawable.ic_heart_selected)
                        mFavStatus = true
                    } else {
                        mFavStatus = false
                        imgFavIV.setImageResource(R.drawable.fav)
                    }
                    mPlanDetailList = mAdvisorModel.data.plan_details
                    setDetailsAdapter()
                } else {
                    dismissProgressDialog()
                    showAlertDialog(mActivity, mAdvisorModel.message)
                }
            })
    }


    private fun setDetailsAdapter() {
        val layoutManager: RecyclerView.LayoutManager =
            LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false)
        pricingRV.layoutManager = layoutManager
        pricingAdapter = PricingAdapter(mActivity, mPlanDetailList, onItemClickListener)
        pricingRV.adapter = pricingAdapter
    }

    private fun setBottomSheetBehaviour() {
        bottomsheet.isVisible = true
        bottomSheetCall.isVisible = false
        bottomSheetTimer.isVisible = true
    }


    @OnClick(
        R.id.backUserDetailRL,
        R.id.priceListingRL,
        R.id.typeOfServiceLL,
        R.id.timeLL,
        R.id.imgCancelIV,
        R.id.imgAddIV,
        R.id.txtPayTV,
        R.id.imgFavIV
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.backUserDetailRL -> onBackPressed()
            R.id.priceListingRL -> performPriceListingClick()
            R.id.typeOfServiceLL -> performTypeOfServiceClick()
            R.id.timeLL -> performTimeClick()
            R.id.imgCancelIV -> performCancelClick()
            R.id.imgAddIV -> performbottomLLClick()
            R.id.txtPayTV -> performPayClick()
            R.id.imgFavIV -> performFavUnfavClick()
        }
    }

    private fun performFavUnfavClick() {
        if (isNetworkAvailable(mActivity))
            executeFavUnfavRequest()
        else
            showToast(mActivity, getString(R.string.internet_connection_error))
    }


    /*
     * Execute api param
     * */
    private fun mFavUnfavParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["advisorId"] = advisorId
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun mFavUnfavHeaderParam(): HashMap<String, String> {
        val headers = HashMap<String, String>()
        mActivity?.let {
            LetMeListenPreferences().readString(it, TOKEN, null)?.let {
                headers.put(
                    "Token",
                    it
                )
            }
        }
        return headers
    }

    private fun executeFavUnfavRequest() {
        showProgressDialog(mActivity)
        mActivity?.let {
            viewModel?.getFavUnfavData(mActivity, mFavUnfavParam(), mFavUnfavHeaderParam())
                ?.observe(
                    this,
                    androidx.lifecycle.Observer<FavUnfavModel?> { mFavUnfavModel ->
                        if (mFavUnfavModel.status.equals(1)) {
                            dismissProgressDialog()
                            showToast(mActivity, mFavUnfavModel.message)
                            if (mFavUnfavModel.isFav.equals("1")) {
                                imgFavIV.setImageDrawable(getDrawable(R.drawable.ic_heart_selected))
                                onItemClickk?.onItemClickk(advisorId, true)
                            } else {
                                imgFavIV.setImageDrawable(getDrawable(R.drawable.fav))
                                onItemClickk?.onItemClickk(advisorId, false)
                            }

                        } else if (mFavUnfavModel.status == 0) {
                            dismissProgressDialog()
                            showToast(mActivity, mFavUnfavModel.message)
                        } else {
                            dismissProgressDialog()
                            showToast(mActivity, mFavUnfavModel.message)
                        }
                    })
        }
    }

    private fun performPayClick() {
        if (isValidate()) {
            val intent = Intent(mActivity, WalletActivity::class.java)
            intent.putExtra(CHAT_TIME, timeSelected)
            intent.putExtra(ADVISOR_ID, advisorId)
            intent.putExtra(PRICE, mTotalPrice.toString())
            intent.putExtra(PLAN_ID, "")
            intent.putExtra(PER_MIN_COST, txtPerMinCost.text)
            intent.putExtra(SERVICE_TYPE, txtSelectionTV.text.toString().trim())
            startActivity(intent)
        }
    }

    private fun performbottomLLClick() {
        if (value == true) {
            imgAddIV.setImageDrawable(getDrawable(R.drawable.ic_add_user_detail))
            bottomLL.isVisible = true
            value = false
        } else if (value == false) {
            imgAddIV.setImageDrawable(getDrawable(R.drawable.ic_hide_user_detail))
            bottomLL.isVisible = false
            value = true
        }
    }

    private fun performCancelClick() {
        bottomsheet.isVisible = false
        bottomSheetTimer.isVisible = false
    }

    open fun showCallDialog(context: Context) {
        val alertDialog = Dialog(context)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.bottomsheet_call)
        alertDialog.setCanceledOnTouchOutside(true)
        alertDialog.setCancelable(true)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val window = alertDialog.window
        val wlp = window!!.attributes
        wlp.gravity = Gravity.BOTTOM
        val txtChatTV = alertDialog.findViewById<TextView>(R.id.txtChatTV)
        val txtCallTV = alertDialog.findViewById<TextView>(R.id.txtCallTV)
        val txtCancelTV = alertDialog.findViewById<TextView>(R.id.txtCancelTV)

        txtCancelTV.setOnClickListener { alertDialog.dismiss() }
        txtChatTV.setOnClickListener {
            imgserviceIV.setImageDrawable(getDrawable(R.drawable.ic_chat))
            txtSelectionTV.text = getString(R.string.chatt)
            alertDialog.dismiss()
        }
        txtCallTV.setOnClickListener {
            imgserviceIV.setImageDrawable(getDrawable(R.drawable.ic_call))
            txtSelectionTV.text = getString(R.string.call)
           // showToast(mActivity, getString(R.string.coming_soon))

            alertDialog.dismiss()
        }
        alertDialog.show()
    }


    open fun showTimerDialog(context: Context) {
        val alertDialog = Dialog(context)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.bottomsheet_timer)
        alertDialog.setCanceledOnTouchOutside(true)
        alertDialog.setCancelable(true)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val window = alertDialog.window
        val wlp = window!!.attributes
        wlp.gravity = Gravity.BOTTOM
        val imgCancelIV: ImageView = alertDialog.findViewById(R.id.imgCancelIV)
        val txtAddTV: TextView = alertDialog.findViewById(R.id.txtAddTV)
        val circleView: CircleView = alertDialog.findViewById(R.id.circle_view)
        imgCancelIV.setOnClickListener {
            alertDialog.dismiss()
        }
        txtAddTV.setOnClickListener {
            val circleViewAnimation = CircleViewAnimation(circleView)
                .setAnimationStyle(AnimationStyle.CONTINUOUS)
                .setDuration(circleView.progressValue)
                .setCustomAnimationListener(object : Animation.AnimationListener {
                    override fun onAnimationStart(animation: Animation) {
                        // Animation Starts
                    }

                    override fun onAnimationEnd(animation: Animation) {
                        // Animation Ends
                    }

                    override fun onAnimationRepeat(animation: Animation) {}
                }).setTimerOperationOnFinish {
                    // Run when the duration reaches 0. Regardless of the AnimationLifecycle or main thread.
                    // Runs and triggers on background.
                }
                .setCustomInterpolator(LinearInterpolator())


            time = circleView.progressValue.roundToInt()
//            circleView.progressValue= time.toFloat()
            // txtTotalCost.text =""+perMinCost * time
            txtTotalCost.text = "$" + perMinCost * time
            mTotalPrice = perMinCost * time
            txtTimeSelectedTV.text = time.toString() + getString(R.string.min)
            timeSelected = time.toString()
            alertDialog.dismiss()
        }
        alertDialog.show()
    }

    private fun performTypeOfServiceClick() {
        showCallDialog(mActivity)
    }

    private fun performTimeClick() {
        showTimerDialog(mActivity)
    }

    private fun performPriceListingClick() {
//        startActivity(Intent(mActivity, WalletActivity::class.java))
    }

    override fun onItemClick(positon: Int, id: String, time: String, price: String) {
        this.mPos = positon
        this.planId = id
        this.mTime = time
        this.mPrice = price
        val intent = Intent(mActivity, WalletActivity::class.java)
        intent.putExtra(CHAT_TIME, mTime)
        intent.putExtra(PRICE, mPrice)
        intent.putExtra(ADVISOR_ID, advisorId)
        intent.putExtra(PLAN_ID, planId)
        intent.putExtra(PER_MIN_COST, txtPerMinCost.text)
        intent.putExtra(SERVICE_TYPE, getString(R.string.chatt))
        startActivity(intent)
    }

    /*
     * Check Validations of views
     * */
    fun isValidate(): Boolean {
        var flag = true
        if (time.equals(0)) {
            showAlertDialog(mActivity, getString(R.string.please_select_time))
            flag = false
        }
        if (txtSelectionTV.text.toString().trim().equals("Select")) {
            showAlertDialog(mActivity, getString(R.string.please_select_service))
            flag = false
        }
        return flag
    }


//
//    override fun onItemClick(id: String, value:Boolean) {
//        for (i in mDetailsList!!.indices) {
//            if (mDetailsList!![i].userId.equals(id)) {
//                if (value.equals(true)) {
//                    imgFavIV.setImageResource(R.drawable.ic_heart_selected)
//                    mFavStatus = true
//                } else {
//                    mFavStatus = false
//                    imgFavIV.setImageResource(R.drawable.fav)
//                }
//            }
//        }
//}

}