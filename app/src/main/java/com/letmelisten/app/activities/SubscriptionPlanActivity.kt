package com.letmelisten.app.activities

import android.content.Intent
import android.os.Bundle
import android.view.View
import butterknife.ButterKnife
import butterknife.OnClick
import com.letmelisten.app.R

class SubscriptionPlanActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_subscription_plan)
        ButterKnife.bind(this)
        setStatusBar(mActivity)
    }

    @OnClick(
        R.id.backSubscriptionPlanRL,
        R.id.subscriptionRL2
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.backSubscriptionPlanRL -> onBackPressed()
            R.id.subscriptionRL2 -> performSubscriptionClick()
        }
    }

    private fun performSubscriptionClick() {
        startActivity(Intent(mActivity, UserContactActivity::class.java))
    }
}