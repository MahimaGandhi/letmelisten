package com.letmelisten.app.activities

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Typeface
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.AdapterView.OnItemSelectedListener
import android.widget.ArrayAdapter
import android.widget.TextView
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.lifecycle.ViewModelProviders
import butterknife.ButterKnife
import butterknife.OnClick
import com.github.dhaval2404.imagepicker.ImagePicker
import com.letmelisten.app.Model.*
import com.letmelisten.app.R
import com.letmelisten.app.utils.*
import com.letmelisten.app.viewmodels.ProfileEditProfileViewModel
import kotlinx.android.synthetic.main.activity_add_money_to_wallet.*
import kotlinx.android.synthetic.main.activity_edit_profile.*
import kotlinx.android.synthetic.main.activity_sign_up.editEmailET
import kotlinx.android.synthetic.main.activity_sign_up_as_advisor.*
import kotlinx.android.synthetic.main.activity_sign_up_as_advisor.editFirstNameET
import kotlinx.android.synthetic.main.activity_sign_up_as_user.*
import kotlinx.android.synthetic.main.activity_sign_up_as_user.editDescriptionET
import kotlinx.android.synthetic.main.activity_sign_up_as_user.editPhNumberET
import kotlinx.android.synthetic.main.fragment_profile.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File
import java.util.*


class SignUpAsAdvisorActivity : BaseActivity() {

    /*
      * Initialize Menifest Permissions:
      * & Camera Gallery Request @params
      * */
    var writeExternalStorage = Manifest.permission.WRITE_EXTERNAL_STORAGE
    var writeReadStorage = Manifest.permission.READ_EXTERNAL_STORAGE
    var writeCamera = Manifest.permission.CAMERA
    var docFilePath: String? = null
    var docAdvisorPath: String? = null
    var categoryId: String? = ""
    var categoryName: String? = ""
    private var viewModel: ProfileEditProfileViewModel? = null
    var mBitmap: Bitmap? = null
    var mBitmapAdvisor: Bitmap? = null
    var mcategoryAL: ArrayList<DataItemm>? = ArrayList()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up_as_advisor)
        ButterKnife.bind(this)
        setStatusBar(mActivity)
        viewModel = ViewModelProviders.of(this).get(ProfileEditProfileViewModel::class.java)
        editDescriptionET.setOnTouchListener(View.OnTouchListener setOnTouchListener@{ v: View, event: MotionEvent ->
            if (editDescriptionET.hasFocus()) {
                v.parent.requestDisallowInterceptTouchEvent(true)
                when (event.action and MotionEvent.ACTION_MASK) {
                    MotionEvent.ACTION_SCROLL -> {
                        v.parent.requestDisallowInterceptTouchEvent(false)
                        return@setOnTouchListener true
                    }
                }
            }
            false
        })
        executeCategoryListRequest()
    }

    private fun getDocument() {
        val intent = Intent()
//        intent.type = "application/msword,application/pdf"
        intent.type = "application/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(intent, 123)
//        val intent = Intent(Intent.ACTION_GET_CONTENT)
////        intent.type = "application/msword,applicat  ion/pdf"
//        intent.type = "application/pdf"
//        intent.addCategory(Intent.CATEGORY_OPENABLE)
//        // Only the system receives the ACTION_OPEN_DOCUMENT, so no need to test.
//        startActivityForResult(intent, 123)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

////        / dhaval2404 cropper /
//        if (resultCode == RESULT_OK) {
//            //Image Uri will not be null for RESULT_OK
//                //val fileUri = data?.data
//            if (data != null) {
//                val mFileUri = data.data
//                Log.e("TAG", mFileUri.toString())
//
//                val imageStream = mFileUri?.let { contentResolver.openInputStream(it) }
//                val selectedImage = BitmapFactory.decodeStream(imageStream)
//                mBitmap = selectedImage
//
//                showToast(mActivity, mFileUri.toString())
//
//            } else if (resultCode == ImagePicker.RESULT_ERROR) {
//                Toast.makeText(mActivity, "Image Not Valid", Toast.LENGTH_SHORT).show()
//            } else {
//                Toast.makeText(mActivity, "Task Cancelled", Toast.LENGTH_SHORT).show()
//            }
//        }

        if (resultCode == RESULT_OK) {
            if (requestCode == 123) {
                val fileuri = data?.data
                docFilePath = getFileNameByUri(this, fileuri!!)
//            val imageStream = contentResolver.openInputStream(fileuri)
//            val selectedImage
//                    = BitmapFactory.decodeStream(imageStream)
//            mBitmap = selectedImage
                mBitmap = BitmapFactory.decodeFile(docFilePath)
                // mBitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, fileuri)

                if (docFilePath != null) {
                    path.isVisible = true
                    path.text = docFilePath
                    imgUploadDoc.setImageDrawable(getDrawable(R.drawable.ic_up_ph))
                }
            } else if (requestCode == 369) {
                //Image Uri will not be null for RESULT_OK
                val uri: Uri = data?.data!!

                // Use Uri object instead of File to avoid storage permissions
                editAdvisorProfileIV.setImageURI(uri)
                val imageStream = contentResolver.openInputStream(uri)
                val selectedImage = BitmapFactory.decodeStream(imageStream)
                mBitmap = selectedImage
            } else {
                //Image Uri will not be null for RESULT_OK
                val uri: Uri = data?.data!!
                docAdvisorPath = getFileNameByUri(this, uri!!)
                // Use Uri object instead of File to avoid storage permissions
                editAdvisorProfileIV.setImageURI(uri)
                val imageStream = contentResolver.openInputStream(uri)
                val selectedImage = BitmapFactory.decodeStream(imageStream)
                mBitmapAdvisor = selectedImage
            }
        } else if (resultCode == ImagePicker.RESULT_ERROR) {
            Toast.makeText(mActivity, "Image Not Valid", Toast.LENGTH_SHORT).show()
//            Toast.makeText(this, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(this, "Task Cancelled", Toast.LENGTH_SHORT).show()
        }
    }


// get file path

    // get file path
    private fun getFileNameByUri(context: Context, uri: Uri): String {
        val filepath = File(uri.path).name
        return filepath
    }


    private fun performSignUpClick() {
        if (isValidate())
            if (!isNetworkAvailable(mActivity)) {
                showAlertDialog(mActivity, getString(R.string.internet_connection_error))
            } else {
                executeSignUpApi()
            }
    }

    private fun executeSignUpApi() {
        showProgressDialog(mActivity)
        var mMultipartBody: MultipartBody.Part? = null
        var mMultipartBody2: MultipartBody.Part? = null
        if (mBitmap != null) {
            val requestFile1: RequestBody? = convertBitmapToByteArrayUncompressed(mBitmap!!)?.let {
                RequestBody.create(
                    "multipart/form-data".toMediaTypeOrNull(),
                    it
                )
            }

            mMultipartBody = requestFile1?.let {
                MultipartBody.Part.createFormData(
                    "document", docFilePath,
                    it
                )
            }
        }


        if (mBitmapAdvisor != null) {
            val requestFile2: RequestBody? =
                convertBitmapToByteArrayUncompressed(mBitmapAdvisor!!)?.let {
                    RequestBody.create(
                        "multipart/form-data".toMediaTypeOrNull(),
                        it
                    )
                }

            mMultipartBody2 = requestFile2?.let {
                MultipartBody.Part.createFormData(
                    "advisorImage", docAdvisorPath,
                    it
                )
            }
        }


//        var mMultipartBody: MultipartBody.Part? = null
//        if (docFilePath != null) {
//            val requestFile1: RequestBody? =docFilePath?.let {
//                RequestBody.create(
//                    "multipart/form-data".toMediaTypeOrNull(),
//                    it
//                )
//            }
//            mMultipartBody = requestFile1?.let {
//                MultipartBody.Part.createFormData(
//                    "document", docFilePath,
//                    it
//                )
//            }
//        }

        val advisorName: RequestBody = RequestBody.create(
            "multipart/form-data".toMediaTypeOrNull(),
            editFirstNameET.text.toString().trim()
        )
        val categoryId: RequestBody = RequestBody.create(
            "multipart/form-data".toMediaTypeOrNull(),
            categoryId.toString()
        )

        val advisorPhoneno: RequestBody = RequestBody.create(
            "multipart/form-data".toMediaTypeOrNull(),
            editPhNumberET.text.toString().trim()
        )


        val advisorDescription: RequestBody = RequestBody.create(
            "multipart/form-data".toMediaTypeOrNull(),
            editDescriptionET.text.toString().trim()
        )
        val advisorEmail: RequestBody = RequestBody.create(
            "multipart/form-data".toMediaTypeOrNull(),
            editEmailET.text.toString().trim()
        )

        fun mHeaderParam(): HashMap<String, String> {
            val headers = HashMap<String, String>()
            LetMeListenPreferences().readString(mActivity, TOKEN, null)?.let {
                headers.put(
                    "Token",
                    it
                )
            }
            return headers
        }

        viewModel?.signupAsAdvisorData(
            mActivity,
            mHeaderParam(),
            advisorName,
            advisorEmail,
            advisorDescription,
            advisorPhoneno,
            categoryId,
            mMultipartBody,
            mMultipartBody2
        )?.observe(this,
            androidx.lifecycle.Observer<SignupAsAdvisor?> { mSignupAsAdvisorModel ->
                if (mSignupAsAdvisorModel.status == 1) {
                    //LetMeListenPreferences().writeString(mActivity, ADVISOR_ADD, "AdvisorAdd")
                    dismissProgressDialog()
                    showToast(mActivity, mSignupAsAdvisorModel.message)
                    LetMeListenPreferences().writeString(
                        mActivity,
                        ADV_ID,
                        mSignupAsAdvisorModel.data.userId
                    )
                    LetMeListenPreferences().writeString(
                        mActivity,
                        ADV_FIRST_NAME,
                        mSignupAsAdvisorModel.data.firstName
                    )

                    LetMeListenPreferences().writeString(
                        mActivity,
                        AVALIABLE_STATUS,
                        mSignupAsAdvisorModel.data.avaliableStatus
                    )
                    LetMeListenPreferences().writeString(
                        mActivity,
                        AVALIABLE_CALL,
                        mSignupAsAdvisorModel.data.avaliableCall
                    )
                    LetMeListenPreferences().writeString(
                        mActivity, ADV_LAST_NAME,
                        mSignupAsAdvisorModel.data.lastName
                    )
                    LetMeListenPreferences().writeString(
                        mActivity, ADV_EMAIL,
                        mSignupAsAdvisorModel.data.email
                    )
                    LetMeListenPreferences().writeString(
                        mActivity, IS_ADVISOR,
                        mSignupAsAdvisorModel.data.isAdvisor
                    )

                    LetMeListenPreferences().writeString(
                        mActivity, ADVISOR_CATEGORYID,
                        mSignupAsAdvisorModel.data.categoryId
                    )

                    LetMeListenPreferences().writeString(
                        mActivity, ADVISOR_DESCRIPTION,
                        mSignupAsAdvisorModel.data.advisorDescription
                    )

                    LetMeListenPreferences().writeString(
                        mActivity, ADVISOR_PHONNO,
                        mSignupAsAdvisorModel.data.advisorPhoneno
                    )

                    LetMeListenPreferences().writeString(
                        mActivity, ADVISOR_IMAGE,
                        mSignupAsAdvisorModel.data.advisorImage
                    )

                    LetMeListenPreferences().writeString(
                        mActivity,
                        ADV_PROFILE_IMAGE,
                        mSignupAsAdvisorModel.data.profileImage
                    )
                    LetMeListenPreferences().writeString(
                        mActivity, ADV_REQ,
                        mSignupAsAdvisorModel.data.advisorReq
                    )
                    LetMeListenPreferences().writeString(
                        mActivity,
                        ADV_VERIFICATION_CODE,
                        mSignupAsAdvisorModel.data.verificationCode
                    )
                    LetMeListenPreferences().writeString(
                        mActivity,
                        ADV_FB_TOKEN,
                        mSignupAsAdvisorModel.data.fbToken
                    )
                    LetMeListenPreferences().writeString(
                        mActivity,
                        ADV_GOOGLE_ID,
                        mSignupAsAdvisorModel.data.googleToken
                    )
                    LetMeListenPreferences().writeString(
                        mActivity,
                        ADV_CREATED,
                        mSignupAsAdvisorModel.data.created
                    )
                    LetMeListenPreferences().writeString(
                        mActivity,
                        ADV_PHONE,
                        mSignupAsAdvisorModel.data.phoneNo
                    )
                    LetMeListenPreferences().writeString(
                        mActivity,
                        ADV_PAYPAL_ACCOUNT,
                        mSignupAsAdvisorModel.data.paypalEmail
                    )
                    LetMeListenPreferences().writeString(
                        mActivity, Advisor_EMAIL,
                        mSignupAsAdvisorModel.data.advisorEmail
                    )

                    LetMeListenPreferences().writeString(
                        mActivity, Advisor_NAME,
                        mSignupAsAdvisorModel.data.advisorName
                    )
                    finish()
                } else {
                    dismissProgressDialog()
                    showAlertDialog(mActivity, mSignupAsAdvisorModel.message)
                }
            })
    }

    @OnClick(
        R.id.backRL,
        R.id.txtSignUpTV,
        R.id.editAdvisorProfileIV,
        R.id.imgUploadDoc
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.backRL -> onBackPressed()
            R.id.txtSignUpTV -> performSignUpClick()
            R.id.imgUploadDoc -> performUploadDocClick()
            R.id.editAdvisorProfileIV -> editProfileImageClick()
        }
    }

    private fun editProfileImageClick() {
        if (checkPermission()) {
            onSelectImageClick()
        } else {
            requestPermission()
        }
    }

    /**
     * Start pick image activity with chooser.
     */
    private fun onSelectImageClick() {
        ImagePicker.with(this)
            .crop()                    //Crop image(Optional), Check Customization for more option
            .compress(720)            //Final image size will be less than 1 MB(Optional)
            .maxResultSize(
                720,
                720
            )    //Final image resolution will be less than 1080 x 1080(Optional)
            .cropSquare()
            .start()

//        ImagePicker.Companion.with(this)
//            .crop()                    //Crop image(Optional), Check Customization for more option
//            .compress(720)			//Final image size will be less than 1 MB(Optional)
//            .maxResultSize(720, 720)    //Final image resolution will be less than 1080 x 1080(Optional)
//            .cropSquare()
//            .start();

    }


    private fun checkPermission(): Boolean {
        val write = ContextCompat.checkSelfPermission(mActivity, writeExternalStorage)
        val read = ContextCompat.checkSelfPermission(mActivity, writeReadStorage)
        val camera = ContextCompat.checkSelfPermission(mActivity, writeCamera)
        return write == PackageManager.PERMISSION_GRANTED && read == PackageManager.PERMISSION_GRANTED && camera == PackageManager.PERMISSION_GRANTED
    }

    private fun requestPermission() {
        ActivityCompat.requestPermissions(
            mActivity, arrayOf(writeExternalStorage, writeReadStorage, writeCamera), 369
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            369 -> {
//                if (this[0] == PackageManager.PERMISSION_GRANTED) {
                if (checkPermission() == true) {
                    onSelectImageClick()
                } else {
                    requestPermission()
                    Log.e(TAG, "**Permission Denied**")
                }
            }
        }
    }

    private fun performUploadDocClick() {
        getDocument()
    }


    /*
     * Check Validations of views
     * */
    fun isValidate(): Boolean {
        var flag = true

        if (editFirstNameET.text.toString().trim { it <= ' ' } == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_name))
            flag = false
        }
//        else if (editLastNameET!!.text.toString().trim().equals("")) {
//            showAlertDialog(mActivity, getString(R.string.please_enter_last_name))
//            flag = false
//        }
        else if (editEmailET.text.toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_mail))
            flag = false
        } else if (!isValidEmailId(editEmailET.text.toString().trim { it <= ' ' })) {
            showAlertDialog(mActivity, getString(R.string.please_enter_valid_email_address))
            flag = false
        } else if (!isValidEmailId(editEmailET!!.text.toString().trim())) {
            showAlertDialog(mActivity, getString(R.string.please_enter_valid_email_address))
            flag = false
        } else if (!editPhNumberET.text.toString().trim()
                .equals("") && editPhNumberET.text.toString().length < 10
        ) {
            showAlertDialog(mActivity, getString(R.string.please_enter_valid_ph_number))
            flag = false
        } else if (categoryName
                .equals("Select Category")
        ) {
            showAlertDialog(mActivity, getString(R.string.please_select_category))
            flag = false
        }


//        else if (editPhNumberET.text.toString().trim { it <= ' ' } == "") {
//            showAlertDialog(mActivity, getString(R.string.please_enter_phone))
//            flag = false
//        }
//
//        else if (editPhNumberET.text.toString().length < 10 || editPhNumberET.text.toString().length>15) {
//            showAlertDialog(mActivity, getString(R.string.please_enter_minimum_10_))
//            flag = false
//        }

        else if (editDescriptionET.text.toString().trim { it <= ' ' } == "") {
            showAlertDialog(mActivity, getString(R.string.enter_description))
            flag = false
        }
//        else if (editDescriptionET.text.toString().length > 50) {
//            showAlertDialog(mActivity, getString(R.string.description_length))
//            flag = false
//        }
        else if (docFilePath.toString().trim { it <= ' ' } == "" || docFilePath == null) {
            showAlertDialog(mActivity, getString(R.string.please_enter_doc))
            flag = false
        }
        return flag
    }


    /*
    * Execute api param
    * */

    private fun mHeaderParam(): HashMap<String, String> {
        val headers = HashMap<String, String>()
        mActivity?.let {
            LetMeListenPreferences().readString(it, TOKEN, null)?.let {
                headers.put(
                    "Token",
                    it
                )
            }
        }
        Log.e(
            TAG,
            "**Token**${mActivity?.let { LetMeListenPreferences().readString(it, TOKEN, null) }}"
        )
        return headers
    }

    private fun executeCategoryListRequest() {
        showProgressDialog(mActivity)
        this?.let {
            viewModel?.categoryListData(mActivity, mHeaderParam())?.observe(
                it,
                androidx.lifecycle.Observer<CategoryListModel?> { mCategoryListModel ->
                    if (mCategoryListModel.status == 1) {
                        dismissProgressDialog()

                        // showToast(getActivity(), mModel.getMessage());

                        val mModel = DataItemm()
                        mModel.categoryId = ""
                        mModel.name = "Select Category"
                        mcategoryAL?.add(mModel)
                        mCategoryListModel.data?.let { it1 -> mcategoryAL?.addAll(it1) }
//                        mcategoryAL = mCategoryListModel.data
                        setCategorySpinner()
                    }
                })
        }
    }

    private fun setCategorySpinner() {
//        val font = Typeface.createFromAsset(
//            mActivity.getAssets(),
//            "regular.ttf"
//        )
        val mColorAd: ArrayAdapter<DataItemm> = object :
            ArrayAdapter<DataItemm>(
                mActivity,
                android.R.layout.simple_spinner_item,
                mcategoryAL!!
            ) {
            override fun getDropDownView(
                position: Int,
                convertView: View?,
                parent: ViewGroup
            ): View {
                var v = convertView
                if (v == null) {
                    val mContext = this.context
                    if (mContext != null) {
                        val vi =
                            (mContext.getSystemService(LAYOUT_INFLATER_SERVICE) as LayoutInflater)
                        v = vi.inflate(R.layout.item_spinner, null)
                    }
                }
                //HideKey();
                val itemTextView = v?.findViewById<TextView>(R.id.itemTV)
                val mModel: DataItemm? = mcategoryAL?.get(position)
                itemTextView?.setText(mModel?.name)
                //  itemTextView?.setTypeface(font)
                //                itemTextView.setText(categoriesArraylist[position]);
                return v!!
            }
        }
        categorySpinner.adapter = mColorAd
        //
//        /*Status Spinner Click Listener*/
        categorySpinner.onItemSelectedListener = object : OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View,
                position: Int,
                id: Long
            ) {
                //                category_sel = parent.getItemAtPosition(position).toString();
                var mModel: DataItemm? = mcategoryAL?.get(position)
                Log.e(TAG, "**CATEGORY_ID**" + mModel!!.categoryId)
                categoryId = mModel?.categoryId
                categoryName = mModel?.name
                if (position == 0) {
                    (view as TextView).setTextColor(resources.getColor(R.color.colorLightGreyText))
                } else {
                    (view as TextView).setTextColor(resources.getColor(R.color.colorBlack))
                }

                view.setText(mModel?.name)
                view.setPadding(30, 0, 0, 0)
                view.textSize = 15f
                // view.setTypeface(font)
                categorySpinner.setSelection(position)
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }
    }

}