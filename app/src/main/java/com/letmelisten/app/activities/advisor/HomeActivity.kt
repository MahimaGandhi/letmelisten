package com.letmelisten.app.activities.advisor

import android.Manifest
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProviders
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import butterknife.ButterKnife
import butterknife.OnClick
import com.letmelisten.app.Model.BadgeCountModel
import com.letmelisten.app.Model.EndSessionModel
import com.letmelisten.app.R
import com.letmelisten.app.activities.BaseActivity
import com.letmelisten.app.fragments.advisor.HomeFragment
import com.letmelisten.app.fragments.advisor.NotificationFragment
import com.letmelisten.app.fragments.advisor.ProfileFragment
import com.letmelisten.app.utils.*
import com.letmelisten.app.viewmodels.SignupLoginViewModel
import com.letmelisten.app.viewmodels.StripePaymentViewModel
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.activity_home2.imgHomeIV
import kotlinx.android.synthetic.main.activity_home2.imgNotiIV
import kotlinx.android.synthetic.main.activity_home2.imgProfileIV


class HomeActivity : BaseActivity() {
    var roomId: String = ""
    var recordAudio = Manifest.permission.RECORD_AUDIO
    var writeExternalStorage = Manifest.permission.WRITE_EXTERNAL_STORAGE
    private var viewModel: SignupLoginViewModel? = null
    private var viewModel2: StripePaymentViewModel? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home2)
        ButterKnife.bind(this)
        viewModel = ViewModelProviders.of(this).get(SignupLoginViewModel::class.java)
        viewModel2 = ViewModelProviders.of(this).get(StripePaymentViewModel::class.java)

        setStatusBar(mActivity)

        if (!LetMeListenPreferences().readString(
                mActivity,
                TO_DELETE_ROOMID,
                ""
            ).equals("")
        ) {
            roomId = LetMeListenPreferences().readString(
                mActivity,
                TO_DELETE_ROOMID,
                ""
            ).toString()
            LetMeListenPreferences().writeString(
                mActivity,
                TO_DELETE_ROOMID,
                ""
            )
            executeEndSessionRequest()
        } else {
            performHomeClick()
        }

    }

    override fun onResume() {
        super.onResume()
        if (checkPermission()) {
        } else {
            requestPermission()
        }
    }

    /*
       * Check permission
       * */
    private fun checkPermission(): Boolean {
        val recordAudtioINT = ContextCompat.checkSelfPermission(mActivity, recordAudio)
        val writeExternalStorageINT =
            ContextCompat.checkSelfPermission(mActivity, writeExternalStorage)
        return recordAudtioINT == PackageManager.PERMISSION_GRANTED && writeExternalStorageINT == PackageManager.PERMISSION_GRANTED
    }

    private fun requestPermission() {
        ActivityCompat.requestPermissions(
            mActivity,
            arrayOf(recordAudio, writeExternalStorage),
            369
        )
    }

    @JvmName("onRequestPermissionsResult1")
    fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>?,
        grantResults: IntArray
    ) {
        when (requestCode) {
            369 ->                 // If request is cancelled, the result arrays are empty.
                if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Log.e("TAG", "**Permission Denied**")
                    //showLongToast("You can denied the permission now you are not able for video call.")
                }
        }
    }


    @OnClick(
        R.id.imgHomeIV,
        R.id.imgNotiIV,
        R.id.imgProfileIV
    )
    fun onClick(view: View) {
        when (view.id) {
            R.id.imgHomeIV -> performHomeClick()
            R.id.imgNotiIV -> performNotiClick()
            R.id.imgProfileIV -> performProfileClick()
        }
    }

    private fun performProfileClick() {
        imgProfileIV.setImageDrawable(getDrawable(R.drawable.ic_profile_ss))
        imgHomeIV.setImageDrawable(getDrawable(R.drawable.ic_home))
        imgNotiIV.setImageDrawable(getDrawable(R.drawable.ic_bell))
        switchFragment(ProfileFragment(), PROFILE_TAG, false, null)
    }

    private fun performNotiClick() {
        chat_badge.visibility = View.GONE
        imgNotiIV.setImageDrawable(getDrawable(R.drawable.ic_bell_ss))
        imgHomeIV.setImageDrawable(getDrawable(R.drawable.ic_home))
        imgProfileIV.setImageDrawable(getDrawable(R.drawable.ic_profile))
        switchFragment(NotificationFragment(), NOTIFICATION_TAG, false, null)
    }

    private fun performHomeClick() {
        executeGetBadgeCount()
        imgHomeIV.setImageDrawable(getDrawable(R.drawable.ic_home_ss))
        imgProfileIV.setImageDrawable(getDrawable(R.drawable.ic_profile))
        imgNotiIV.setImageDrawable(getDrawable(R.drawable.ic_bell))
        switchFragment(HomeFragment(), HOME_TAG, false, null);
    }

    /*
        * Execute api param
        * */
    private fun mBadgeParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = java.util.HashMap()
        if (LetMeListenPreferences().readString(
                mActivity,
                SWITCH_ACCOUNT,
                ""
            ).equals("advisor")
        ) {
            mMap["role"] = "2"
        } else {
            mMap["role"] = "1"
        }
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    fun mHeaderParam(): HashMap<String, String> {
        val headers = HashMap<String, String>()
        LetMeListenPreferences().readString(mActivity, TOKEN, null)?.let {
            headers.put(
                "Token",
                it
            )
        }
        return headers
    }

    private fun executeGetBadgeCount() {
        viewModel?.getBadgeCount(mActivity, mBadgeParam(), mHeaderParam())?.observe(this,
            androidx.lifecycle.Observer<BadgeCountModel?> { mBadgeModel ->
                if (mBadgeModel.status == 1) {
                    if (mBadgeModel.badgeCount != null && !mBadgeModel.badgeCount.equals("0")) {
                        chat_badge.visibility = View.VISIBLE
                        chat_badge.setText(mBadgeModel.badgeCount)
                    }

                } else {
                    chat_badge.visibility = View.GONE
                }
            })
    }


    /*
      * Execute api param
      * */
    private fun mEndSessionParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = java.util.HashMap()
        mMap["roomId"] = roomId
        Log.e(
            TAG, "VALUE::ROOM" + LetMeListenPreferences().readString(
                mActivity,
                TO_DELETE_ROOMID,
                ""
            )
        )
        mMap["rating"] = "0"
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeEndSessionRequest() {
        showProgressDialog(mActivity)
        viewModel2?.getSessionEndData(mActivity, mEndSessionParam(), mHeaderParam())?.observe(this,
            androidx.lifecycle.Observer<EndSessionModel?> { mSessionEndModel ->
                if (mSessionEndModel.status == 1) {
                    dismissProgressDialog()
                    performHomeClick()

                } else if (mSessionEndModel?.status == 0) {
                    dismissProgressDialog()
                    showToast(mActivity, mSessionEndModel.message)
                    showToast(mActivity, "0")
                }

            })
    }

}
