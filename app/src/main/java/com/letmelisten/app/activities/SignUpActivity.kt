package com.letmelisten.app.activities

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.TextView
import androidx.lifecycle.ViewModelProviders
import butterknife.ButterKnife
import butterknife.OnClick
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.FirebaseApp
import com.google.firebase.messaging.FirebaseMessaging
import com.letmelisten.app.Model.StatusMsgModel
import com.letmelisten.app.R
import com.letmelisten.app.viewmodels.SignupLoginViewModel
import kotlinx.android.synthetic.main.activity_sign_up.*
import java.util.*

class SignUpActivity : BaseActivity() {
    private var viewModel: SignupLoginViewModel? = null
    var strDeviceToken: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)
        ButterKnife.bind(this)
        viewModel = ViewModelProviders.of(this).get(SignupLoginViewModel::class.java)
        setStatusBar(mActivity)
        setEditTextFocused(mActivity, editPasswordET)
        FirebaseApp.initializeApp(this)
        getDeviceToken()
    }


    private fun getDeviceToken() {
        FirebaseMessaging.getInstance().token
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    Log.w(TAG, "Fetching FCM registration token failed", task.exception)
                    return@OnCompleteListener
                }

                // Get new FCM registration token
                strDeviceToken = task.result!!

                Log.e(TAG, "**Push Token**$strDeviceToken")
                // Log and toast
                Log.d(TAG, "Token********   $strDeviceToken")
            })

    }


    override fun onBackPressed() {
        super.onBackPressed()
    }

    @OnClick(
        R.id.txtSignUpTV,
        R.id.txtSignInTV
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.txtSignInTV -> performSignInClick()
            R.id.txtSignUpTV -> performSignUpClick()
        }
    }

    private fun performSignInClick() {
        startActivity(Intent(mActivity, SignInActivity::class.java))
//        finish()
    }


    private fun mParams(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["firstName"] = editFirstNameET.text.toString().trim { it <= ' ' }
        mMap["lastName"] = editLastNameET.text.toString().trim { it <= ' ' }
        mMap["email"] = editEmailET.text.toString().trim { it <= ' ' }
        mMap["password"] = editPasswordET.text.toString().trim { it <= ' ' }
        mMap["deviceToken"] = strDeviceToken
        mMap["deviceType"] = "2"
        mMap["isAdvisor"] = "0"
        mMap["phoneNo"] = editPhNumberET.text.toString().trim()
        Log.e("params", "**PARAM**$mMap")
        return mMap
    }

    private fun executeSignUpApi() {
        showProgressDialog(mActivity)
        viewModel?.getSignUpData(mActivity, mParams())?.observe(this,
            androidx.lifecycle.Observer<StatusMsgModel?> { mSignUpModel ->
                if (mSignUpModel.status == 1) {
                    dismissProgressDialog()
                    showSignUpAlertDialog(mActivity,mSignUpModel.message)
//                    showToast(mActivity,mSignUpModel.message)
//                    startActivity(Intent(mActivity, SignInActivity::class.java))
//                    finish()
                } else {
                    dismissProgressDialog()
                    showAlertDialog(mActivity, mSignUpModel.message)
                }
            })
    }

    /*
    *
    * Alert Dialog
    * */
    fun showSignUpAlertDialog(mActivity: Activity?, strMessage: String?) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_alert)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
        val btnDismiss = alertDialog.findViewById<TextView>(R.id.btnDismiss)
        txtMessageTV.text = strMessage
        btnDismiss.setOnClickListener { alertDialog.dismiss()
            startActivity(Intent(mActivity, SignInActivity::class.java))
            finish()
        }
        alertDialog.show()
    }

    private fun performSignUpClick() {
        if (isValidate())
            if (!isNetworkAvailable(mActivity)) {
                showAlertDialog(mActivity, getString(R.string.internet_connection_error))
            } else {
                executeSignUpApi()
            }
    }

    /*
     * Check Validations of views
     * */
    fun isValidate(): Boolean {
        var flag = true
        if (editFirstNameET!!.text.toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_first_name))
            flag = false
        } else if (editLastNameET!!.text.toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_last_name))
            flag = false
        } else if (editEmailET!!.text.toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_mail))
            flag = false
        } else if (!isValidEmailId(editEmailET.text.toString().trim { it <= ' ' })) {
            showAlertDialog(mActivity, getString(R.string.please_enter_valid_email_address))
            flag = false
        } else if (!isValidEmailId(editEmailET!!.text.toString().trim())) {
            showAlertDialog(mActivity, getString(R.string.please_enter_valid_email_address))
            flag = false
        } else if (editPasswordET.text.toString().trim { it <= ' ' } == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_password))
            flag = false
        } else if (editPasswordET.text.toString().length < 6) {
            showAlertDialog(mActivity, getString(R.string.please_enter_minimum_6_))
            flag = false
        }
        return flag
    }
}