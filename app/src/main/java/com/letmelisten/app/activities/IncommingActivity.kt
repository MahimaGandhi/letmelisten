package com.letmelisten.app.activities

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.app.NotificationManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.media.MediaPlayer
import android.media.Ringtone
import android.media.RingtoneManager
import android.os.*
import android.util.Log
import android.view.*
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProviders
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.bumptech.glide.Glide
import com.iarcuschin.simpleratingbar.SimpleRatingBar
import com.kloxus.app.retrofit.ApiInterface
import com.letmelisten.app.Model.EndSessionCallModel
import com.letmelisten.app.R
import com.letmelisten.app.agoraPackage.DuringCallEventHandler
import com.letmelisten.app.agoraPackage.VideoBaseActivity
import com.letmelisten.app.fcm.NotificationGenerator
import com.letmelisten.app.fcm.NotificationReciever
import com.letmelisten.app.retrofit.ApiClient
import com.letmelisten.app.utils.*
import com.letmelisten.app.viewmodels.PaymentViewModel
import io.agora.rtc.Constants
import io.agora.rtc.IRtcEngineEventHandler
import io.agora.rtc.RtcEngine
import kotlinx.android.synthetic.main.activity_calling.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import java.util.concurrent.TimeUnit

class IncommingActivity : BaseActivity() {

    private var viewModel: PaymentViewModel? = null
    var agoraAppId = ""
    var callToken = ""
    var chatTime = ""
    var mType = ""
    var totalTime = ""
    var message = ""
    var userName = ""
    var mRoomIdNew = ""
    var userImage = ""
    var channelName = ""
    var tryRoomId: String = ""
    var advisor_id: String = ""
    var nm: NotificationManager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        showWhenLockedAndTurnScreenOn()
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_incomming)
        ButterKnife.bind(this)
        viewModel = ViewModelProviders.of(this).get(PaymentViewModel::class.java)

        getIntentData()
    }

    private fun getIntentData() {
        if (intent != null) {
            //Incoming call
            txtTimeLeftTV.visibility = View.VISIBLE
            mRoomIdNew = intent.getStringExtra(TRY_ROOM)!!
            userName = intent.getStringExtra(USER_NAME)!!
            chatTime = intent.getStringExtra(CHAT_TIME)!!
            userImage = intent.getStringExtra(AGORA_USER_IMAGE)!!
            advisor_id = intent.getStringExtra(ADVISOR_ID)!!



            Glide.with(mActivity)
                .load(userImage)
                .placeholder(R.drawable.back)
                .error(R.drawable.ic_phh)
                .into(editProfileIV)
            txtNameTV.text = userName


        }
    }

    @OnClick(
        R.id.endCallIV,
        R.id.picCallIV
    )
    fun onViewClicked(view: View) {
        when (view.id) {

            R.id.endCallIV -> executeEndCallRequest()
            R.id.picCallIV -> executePicCallRequest()
        }
    }

    private fun executePicCallRequest() {
        preventMultipleClick()
        executeAnswerCallRequest()
    }

    private fun executeEndCallRequest() {
        preventMultipleClick()
        executeEndSessionRequest()
    }

    private fun showWhenLockedAndTurnScreenOn() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O_MR1) {
            setShowWhenLocked(true)
            setTurnScreenOn(true)
        } else {
            window.addFlags(
                WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                        or WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
            )
        }
    }

    /*
     * Execute api param
     * */
    private fun mEndSessionParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["roomId"] = mRoomIdNew
        mMap["rating"] = ""
        mMap["role"] = "2" //Advisor
        Log.e("TAG", "**PARAM**$mMap")
        return mMap
    }

    fun mHeaderParam(): HashMap<String, String> {
        val headers = HashMap<String, String>()
        LetMeListenPreferences().readString(mActivity, TOKEN, null)?.let {
            headers.put(
                "Token",
                it
            )
        }
        return headers
    }


    private fun executeEndSessionRequest() {
        showProgressDialog(mActivity)
        viewModel?.getCallEndData(mActivity, mEndSessionParam(), mHeaderParam())?.observe(this,
            androidx.lifecycle.Observer<EndSessionCallModel?> { mCallEndModel ->
                if (mCallEndModel.status == 1) {
                    NotificationReciever.ringtone!!.stop()
                    cancelNotification()
                    dismissProgressDialog()
                    finish()
                } else if (mCallEndModel?.status == 0) {
                    NotificationReciever.ringtone!!.stop()
                    dismissProgressDialog()
                    showToast(mActivity, mCallEndModel.message)
                    finish()
                }
            })
    }

    /*
* Execute api param
* */
    private fun mCallAnswerParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["roomId"] = mRoomIdNew
        Log.e("TAG", "**PARAM**$mMap")
        return mMap
    }

    private fun executeAnswerCallRequest() {
        val mApiInterface: ApiInterface =
            ApiClient().getApiClient()!!.create(ApiInterface::class.java)

        mApiInterface.answerCallRequest(mCallAnswerParam(), mHeaderParam())
            .enqueue(object : Callback<EndSessionCallModel?> {
                override fun onResponse(
                    call: Call<EndSessionCallModel?>,
                    response: Response<EndSessionCallModel?>
                ) {

                    val mModel: EndSessionCallModel? = response.body()
                    if (mModel!!.status == 1) {
                        NotificationReciever.ringtone!!.stop()
                        cancelNotification()
                        Log.e("TAG", "RUNNING::" + mModel.status)
                        val playIntent = Intent(mActivity, CallingActivity::class.java)
                        playIntent.putExtra(ADVISOR_ID, advisor_id)
                        playIntent.putExtra(NOTIFY_INCOMING_TYPE, "incoming call")
                        playIntent.putExtra(ROOM_ID, mRoomIdNew)
                        playIntent.putExtra(USER_NAME, userName)
                        playIntent.putExtra(ADVISOR_NAME, userName)
                        playIntent.putExtra(AGORA_APP_ID, agoraAppId)
                        playIntent.putExtra(
                            AGORA_CHANNEL_NAME,
                            channelName
                        )
                        playIntent.putExtra(AGORA_TOKEN, callToken)
                        playIntent.putExtra(
                            AGORA_USER_IMAGE,
                            userImage
                        )
                        playIntent.putExtra(CHAT_TIME, chatTime)
                        playIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                        startActivity(playIntent)
                        finish()
                    } else {
                        NotificationReciever.ringtone!!.stop()
                        //showToast(mActivity, "error")
                    }
                }
                override fun onFailure(call: Call<EndSessionCallModel?>, t: Throwable) {
                }
            })

    }

    fun cancelNotification() {
        if (mActivity != null) {
            nm = mActivity.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        }
        nm?.cancelAll()

    }
}
