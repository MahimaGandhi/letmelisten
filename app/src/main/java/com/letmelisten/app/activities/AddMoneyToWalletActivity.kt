package com.letmelisten.app.activities

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.view.View.OnTouchListener
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProviders
import butterknife.ButterKnife
import butterknife.OnClick
import com.github.dewinjm.monthyearpicker.MonthYearPickerDialogFragment
import com.letmelisten.app.Model.AccessTokenPaypalModel
import com.letmelisten.app.Model.PaypalTransactionModel
import com.letmelisten.app.Model.PaypalUrlModel
import com.letmelisten.app.Model.StripeMoneyWalletModel
import com.letmelisten.app.R
import com.letmelisten.app.utils.*
import com.letmelisten.app.viewmodels.StripePaymentViewModel
import com.paypal.checkout.PayPalCheckout
import com.paypal.checkout.approve.OnApprove
import com.paypal.checkout.config.CheckoutConfig
import com.paypal.checkout.config.Environment
import com.paypal.checkout.config.SettingsConfig
import com.paypal.checkout.createorder.CreateOrder
import com.paypal.checkout.createorder.CurrencyCode
import com.paypal.checkout.createorder.OrderIntent
import com.paypal.checkout.createorder.UserAction
import com.paypal.checkout.error.OnError
import com.paypal.checkout.order.Amount
import com.paypal.checkout.order.AppContext
import com.paypal.checkout.order.Order
import com.paypal.checkout.order.PurchaseUnit
import com.paypal.checkout.paymentbutton.PayPalButton
import com.stripe.android.Stripe
import com.stripe.android.TokenCallback
import com.stripe.android.model.Card
import com.stripe.android.model.Token
import kotlinx.android.synthetic.main.activity_add_money_to_wallet.*
import kotlinx.android.synthetic.main.activity_wallet.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import java.util.*


class AddMoneyToWalletActivity : BaseActivity() {


    private var viewModel: StripePaymentViewModel? = null
    var arr_cards = ""
    var yearSelected = 0
    var monthSelected = 0
    var cardNumber: String = ""
    var cvc: String = ""
    var payID: String = ""
    var strToken: String = ""
    var card: Card? = null
    var stripe: Stripe? = null
    var accessToken: String? = null
    var stripe_token = ""
    var expirayDate: String = ""
    var expiraryYear: String = ""
    var paypalStatus: String = ""
    var paypalUrl: String = ""
    var payPalButton: PayPalButton? = null

    companion object {
        var walletMoney: String = ""
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_money_to_wallet)
        ButterKnife.bind(this)
        payPalButton = findViewById<PayPalButton>(R.id.payPalButton)

        setStatusBar(mActivity)
        performCrediteClick()
        viewModel = ViewModelProviders.of(this).get(StripePaymentViewModel::class.java)
        txtExpiryDateTV.showSoftInputOnFocus = false

        txtExpiryDateTV.setOnTouchListener(OnTouchListener { v, event ->
            if (MotionEvent.ACTION_UP == event.action) performExpiryDateClick()// Do whatever you want to do man, but don't trouble your mother
            false
        })
        setCardsAdapter()
        val config = CheckoutConfig(
            application = application,
            clientId = "Aa146oJXEPTpbNSYnHjEQ7YAfFncrjuwGhH-e9I-vNBSUuLL_DgZE3b4vYbYLXWAy35_oeRG-_ffHDTs",
            environment = Environment.SANDBOX,
            returnUrl = "com.letmelisten.app://paypalpay",
            currencyCode = CurrencyCode.USD,
            userAction = UserAction.PAY_NOW,
            settingsConfig = SettingsConfig(
                loggingEnabled = true
            )
        )
        PayPalCheckout.setConfig(config)
        getPayment()
    }

    override fun onResume() {
        super.onResume()
        executeGetPaypalUrlApi()
    }

    private fun setCardsAdapter() {
        val mStatusArry = resources.getStringArray(R.array.cards)
        // access the spinner
        if (cardsSpinner != null) {
            val adapter = ArrayAdapter(
                this,
                android.R.layout.simple_spinner_item, mStatusArry
            )
            cardsSpinner.adapter = adapter
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

            cardsSpinner.onItemSelectedListener = object :
                AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    parent: AdapterView<*>,
                    view: View, position: Int, id: Long
                ) {
                    arr_cards = mStatusArry[position]
                    if (arr_cards == "") {
//                        gender="1"
                    } else if (arr_cards == "") {
//                        gender="2"
                    } else {
//                        gender="3"
                    }
//                    Log.e(TAG, "gender" + gender)
//                    GenderRL.setBackgroundResource(R.drawable.bg_et_selected)
                }

                override fun onNothingSelected(parent: AdapterView<*>) {
                    // write code to perform some action
                }
            }
        }
    }


    @OnClick(
        R.id.imgBackIV,
        R.id.txtAddMoneyTV,
        R.id.creditLL,
        R.id.paypallLL,
        R.id.connectPaypalTV
    )

    fun onClick(view: View) {
        when (view.id) {
            R.id.imgBackIV -> performCancelClick()
            R.id.txtAddMoneyTV -> performSaveClick()

            R.id.connectPaypalTV ->
                setConnectWithPaypal()
            // R.id.payPalButton -> setPaypalCheckout()

            R.id.creditLL -> performCrediteClick()
            R.id.paypallLL -> performPaypalClick()
        }
    }

    private fun setPaypalCheckout() {
        if (paypalStatus.equals("Not Verified")) {
            showAlertDialog(mActivity, getString(R.string.please_connect_paypal_account))
        } else {

            val config = CheckoutConfig(
                application = application,
                clientId = "Aa146oJXEPTpbNSYnHjEQ7YAfFncrjuwGhH-e9I-vNBSUuLL_DgZE3b4vYbYLXWAy35_oeRG-_ffHDTs",
                environment = Environment.SANDBOX,
                returnUrl = "com.letmelisten.app://paypalpay",
                currencyCode = CurrencyCode.USD,
                userAction = UserAction.PAY_NOW,
                settingsConfig = SettingsConfig(
                    loggingEnabled = true
                )
            )
            PayPalCheckout.setConfig(config)
            //  getPayment()

        }
    }

    private fun getPayment() {
        // Getting the amount from editText
        // Getting the amount from editText
        val amount: String = addMoneyAmountEET.getText().toString().trim()
        Log.e(TAG, "**AMOUNT**" + amount)
        payPalButton?.setup(
            createOrder = CreateOrder { createOrderActions ->
                val amount: String = addMoneyAmountEET.getText().toString().trim()
                Log.e(TAG, "**AMOUNT**" + amount)

                val order = Order(
                    intent = OrderIntent.CAPTURE,
                    appContext = AppContext(
                        userAction = UserAction.PAY_NOW
                    ),
                    purchaseUnitList = listOf(
                        PurchaseUnit(
                            amount = Amount(
                                currencyCode = CurrencyCode.USD,
                                value = amount

                            )
                        )
                    )
                )
                createOrderActions.create(order)


            },
            onApprove = OnApprove { approval ->
                approval.orderActions.capture { captureOrderResult ->
                    Log.e("CaptureOrder", "CaptureOrderResult: $captureOrderResult")
                    payID = approval.data.orderId
                    executeGetPaypalTokenApi()
                }
            },
            onError = OnError.invoke { showAlertDialog(mActivity,getString(R.string.please_enter_money)) }

            )

    }

    private fun performPaypalClick() {
        creditPaymentLL.visibility = View.GONE
        paypalPaymentLL.visibility = View.VISIBLE
        txtAddMoneyTV.visibility = View.GONE
        imgCardIV.setImageDrawable(getDrawable(R.drawable.ic_unactive_rb))
        imgPaypalIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
    }

    private fun performCrediteClick() {
        creditPaymentLL.visibility = View.VISIBLE
        paypalPaymentLL.visibility = View.GONE
        txtAddMoneyTV.visibility = View.VISIBLE
        imgCardIV.setImageDrawable(getDrawable(R.drawable.ic_active_rb))
        imgPaypalIV.setImageDrawable(getDrawable(R.drawable.ic_unactive_rb))
    }

    private fun performCancelClick() {
        onBackPressed()

    }

    @SuppressLint("SetTextI18n")
    private fun performExpiryDateClick() {
        hideCloseKeyboard(mActivity)
        //Set default values
        val calendar = Calendar.getInstance()
        calendar.clear()
        calendar.set(2021, 0, 1)
        yearSelected = calendar[Calendar.YEAR]
        monthSelected = calendar[Calendar.MONTH]
        val dialogFragment = MonthYearPickerDialogFragment
            .getInstance(monthSelected, yearSelected)
        dialogFragment.show(supportFragmentManager, null)
        dialogFragment.setStyle(DialogFragment.STYLE_NORMAL, R.style.AlertDialogCustom)
        dialogFragment.setOnDateSetListener { year, monthOfYear ->
            if (year < 2021) {
                showAlertDialog(mActivity, "Please select future year")
            } else {
                txtExpiryDateTV.setText((monthOfYear + 1).toString() + "/" + year.toString())
                expirayDate = (monthOfYear + 1).toString()
                expiraryYear = year.toString()
            }
        }
    }


    /*
     *
     * Convert Month Name to Month Number
     * */
    fun getMonthNumber(strMonth: String): String? {
        if (strMonth == Constant().MONTHS_OF_YEAR.get(0)) {
            return "01"
        }
        if (strMonth == Constant().MONTHS_OF_YEAR.get(1)) {
            return "02"
        }
        if (strMonth == Constant().MONTHS_OF_YEAR.get(2)) {
            return "03"
        }
        if (strMonth == Constant().MONTHS_OF_YEAR.get(3)) {
            return "04"
        }
        if (strMonth == Constant().MONTHS_OF_YEAR.get(4)) {
            return "05"
        }
        if (strMonth == Constant().MONTHS_OF_YEAR.get(5)) {
            return "06"
        }
        if (strMonth == Constant().MONTHS_OF_YEAR.get(6)) {
            return "07"
        }
        if (strMonth == Constant().MONTHS_OF_YEAR.get(7)) {
            return "08"
        }
        if (strMonth == Constant().MONTHS_OF_YEAR.get(8)) {
            return "09"
        }
        if (strMonth == Constant().MONTHS_OF_YEAR.get(9)) {
            return "10"
        }
        if (strMonth == Constant().MONTHS_OF_YEAR.get(10)) {
            return "11"
        }
        return if (strMonth == Constant().MONTHS_OF_YEAR.get(11)) {
            "12"
        } else {
            null
        }
    }


    /*
     * Card Validations
     * */
    private fun isValidate(): Boolean {
        var flag = true
        if (editCardHolderNameET.text.toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_card_holder))
            flag = false
        } else if (editCardNumberET.text.toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_card_number))
            flag = false
        } else if (!CardValidator().validateCardNumber(editCardNumberET.text.toString().trim())) {
            showAlertDialog(mActivity, getString(R.string.please_enter_valid_carnumber))
            flag = false
        } else if (txtExpiryDateTV.text.toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_expiry_date))
            flag = false
        } else if (editCVVNumberET.text.toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_cvv))
            flag = false
        } else if (addMoneyAmountET.text.toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_money))
            flag = false
        }
        return flag
    }

    /*
    * Card Validations
    * */
    private fun isPaypalValidate(): Boolean {
        var flag = true
        if (addMoneyAmountET.text.toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_money))
            flag = false
        }
        return flag
    }


    private fun isCardValid(): Boolean {
        val validation = card!!.validateCard()
        if (validation) {
            return true
        } else if (!card!!.validateNumber()) {
            showAlertDialog(mActivity, getString(R.string.The_card_invalid))
        } else if (!card!!.validateExpMonth()) {
            showAlertDialog(mActivity, getString(R.string.expiration_month_invalid))
        } else if (!card!!.validateExpYear()) {
            showAlertDialog(mActivity, getString(R.string.expiration_year_invalid))
        } else if (!card!!.validateCVC()) {
            showAlertDialog(mActivity, getString(R.string.cvc_invalid))
        } else {
            showAlertDialog(mActivity, getString(R.string.card_details_invalid))
        }
        return false
    }

    private fun performSaveClick() {
        txtAddMoneyTV.isEnabled = false
        if (isValidate()) {
            //  chargeCard(10);
            onNextClick()
        } else {
            txtAddMoneyTV.isEnabled = true
        }
//            if (isNetworkAvailable(mActivity))
//                executeCardTokenApi()
//            else showToast(
//                mActivity,
//                getString(R.string.internet_connection_error)
//            )
    }

    private fun onNextClick() {

        showProgressDialog(mActivity)
        cardNumber = editCardNumberET.text.toString().trim { it <= ' ' }
        cvc = editCVVNumberET.text.toString().trim { it <= ' ' }
        card = Card(cardNumber, Integer.valueOf(expirayDate), Integer.valueOf(expiraryYear), cvc)
        if (isCardValid()) {
            getStripeToken(card!!)
        } else {
            //showToast(mActivity, getString(R.string.error));
        }
    }

    /*
     * To Get Token
     **/
    private fun getStripeToken(card: Card) {
        stripe = Stripe()
        stripe!!.createToken(card, PUBLISHABLE_KEY, object : TokenCallback() {
            override fun onError(error: Exception) {
                Log.e("ERROR", error.toString())
            }

            override fun onSuccess(token: Token) {
                Log.e("TOKEN::", java.lang.String.valueOf(token))
                stripe_token = token.id
                Log.e(TAG, "TOKEN::$stripe_token")
                if (stripe_token != null) {
                    if (!isNetworkAvailable(mActivity)) {
                        showToast(mActivity, getString(R.string.internet_connection_error))
                    } else {
                        executeAddWalletApi()
//                        showToast(mActivity, getString(R.string.payment_successfull))
//                        LetMeListenPreferences().writeString(
//                            mActivity,
//                            WALLET_AMOUNT,
//                            addMoneyAmountET.text.toString().trim()
//                        )
//                        finish()
                        //executeAddStripePymentApi()
                    }
                }
            }
        })
    }

    private fun mParams(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["amount"] = addMoneyAmountET.text.toString().trim { it <= ' ' }
        mMap["stripeToken"] = stripe_token
        Log.e("params", "**PARAM**$mMap")
        return mMap
    }

    private fun executeAddWalletApi() {
        //   showProgressDialog(mActivity)
        viewModel?.getStripeWalletMoneyData(mActivity, mParams(), mHeaderParam())?.observe(this,
            androidx.lifecycle.Observer<StripeMoneyWalletModel?> { mWallettpojo ->
                if (mWallettpojo.status == 1) {
                    txtAddMoneyTV.isEnabled = true
                    showToast(mActivity, mWallettpojo.message)
                    if (mWallettpojo.Amount != null) {
                        if (mWallettpojo.Amount.walletAmount != null && !mWallettpojo.Amount.walletAmount.equals(
                                ""
                            )
                        ) {
                            walletMoney = mWallettpojo.Amount.walletAmount
                            Log.e(
                                TAG, "Wallet_money::" + walletMoney
                            )
                            LetMeListenPreferences().writeString(
                                mActivity,
                                WALLET_AMOUNT,
                                mWallettpojo.Amount.walletAmount
                            )
                        }
                    }

                    finish()
                } else {
                    txtAddMoneyTV.isEnabled = true
                    dismissProgressDialog()
                    showAlertDialog(mActivity, mWallettpojo.message)
                }
            })
    }

    private fun mHeaderParam(): HashMap<String, String> {
        val headers = HashMap<String, String>()
        mActivity.let {
            LetMeListenPreferences().readString(it, TOKEN, null)?.let {
                headers.put(
                    "Token",
                    it
                )
            }
        }
        Log.e(
            TAG,
            "**Token**${mActivity.let { LetMeListenPreferences().readString(it, TOKEN, null) }}"
        )
        return headers
    }


    private fun executeGetPaypalUrlApi() {
        showProgressDialog(mActivity)

        val loggedUserId: RequestBody = RequestBody.create(
            "multipart/form-data".toMediaTypeOrNull(),
            getLoggedInUserID()
        )


        fun mHeaderParam(): HashMap<String, String> {
            val headers = HashMap<String, String>()
            LetMeListenPreferences().readString(mActivity, TOKEN, null)?.let {
                headers.put(
                    "Token",
                    it
                )
            }
            return headers
        }

        viewModel?.connectPaypalApi(
            mActivity,
            mHeaderParam(),
            loggedUserId
        )?.observe(this,
            androidx.lifecycle.Observer<PaypalUrlModel?> { mPaypalModel ->
                if (mPaypalModel.status == 1) {
                    dismissProgressDialog()
                    paypalStatus = mPaypalModel.accountstatus.toString()
                    if (paypalStatus.equals("Not Verified")) {
                        connectPaypalTV.visibility = View.VISIBLE
                    } else {
                        connectPaypalTV.visibility = View.GONE
                    }
                    paypalUrl = mPaypalModel.login_url.toString()
                } else {
                    dismissProgressDialog()
                    // showAlertDialog(mActivity, mPaypalModel.message)
                }
            })
    }


    private fun executeGetPaypalTokenApi() {
        showProgressDialog(mActivity)

        val loggedUserId: RequestBody = RequestBody.create(
            "multipart/form-data".toMediaTypeOrNull(),
            getLoggedInUserID()
        )

        fun mHeaderParam(): HashMap<String, String> {
            val headers = HashMap<String, String>()
            LetMeListenPreferences().readString(mActivity, TOKEN, null)?.let {
                headers.put(
                    "Token",
                    it
                )
            }
            return headers
        }

        viewModel?.paypalTokenApi(
            mActivity,
            mHeaderParam(), loggedUserId
        )?.observe(this,
            androidx.lifecycle.Observer<AccessTokenPaypalModel?> { mTokenPaypalModel ->
                if (mTokenPaypalModel.status == 1) {
                    // dismissProgressDialog()
                    accessToken = mTokenPaypalModel.accessToken
                    executePaypalTransactionApi()

                } else {
                    dismissProgressDialog()

                }
            })
    }


    private fun executePaypalTransactionApi() {
        // showProgressDialog(mActivity)
        val accessToken: RequestBody = RequestBody.create(
            "multipart/form-data".toMediaTypeOrNull(),
            accessToken.toString()
        )

        val payId: RequestBody = RequestBody.create(
            "multipart/form-data".toMediaTypeOrNull(), payID
        )

        fun mHeaderParam(): HashMap<String, String> {
            val headers = HashMap<String, String>()
            LetMeListenPreferences().readString(mActivity, TOKEN, null)?.let {
                headers.put(
                    "Token",
                    it
                )
            }
            return headers
        }

        viewModel?.paypalTransactionApi(
            mActivity,
            mHeaderParam(),
            accessToken,
            payId
        )?.observe(this,
            androidx.lifecycle.Observer<PaypalTransactionModel?> { mTokenPaypalModel ->
                if (mTokenPaypalModel.status == 1) {
                    dismissProgressDialog()
                    LetMeListenPreferences().writeString(
                        mActivity,
                        WALLET_AMOUNT,
                        mTokenPaypalModel.amount?.walletAmount
                    )
                    finish()
                } else {
                    dismissProgressDialog()
                }
            })
    }


    private fun setConnectWithPaypal() {
        val i = Intent(mActivity, PaypalConnectActivity::class.java)
        i.putExtra(PAYPAL_URL, paypalUrl)
        startActivity(i)
    }
}