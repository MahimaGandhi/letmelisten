package com.letmelisten.app.activities

import android.app.Activity
import android.app.ActivityManager
import android.content.Intent
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Handler
import android.util.Base64
import android.util.Log
import android.widget.Toast
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.FirebaseApp
import com.google.firebase.messaging.FirebaseMessaging

import com.letmelisten.app.R
import com.letmelisten.app.activities.advisor.ChatActivity
import com.letmelisten.app.utils.*
import org.json.JSONException
import org.json.JSONObject
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException


class SplashActivity : BaseActivity() {
    val SPLASH_TIME_OUT = 1500L
    var strDeviceToken: String? = null
    var mTitle = ""
    var mName = ""
    var mRoomID = ""
    var userID = ""
    var totalAmount = ""
    var detailsID = ""
    var advisorId = ""
    var created = ""
    var mType = ""
    var totalTime = ""
    var message = ""
    var notificationType = ""
    var mAdvisorName = ""
    var userName = ""
    var isNotification = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        setStatusBar(mActivity)
        printKeyHash(mActivity)
        // hashFromSHA1("38:2D:A1:98:15:4F:C3:3F:29:13:C7:7F:40:E2:9A:BA:10:2B:4F:91");
        if (intent.extras != null) {
            var data: JSONObject? = null
            Log.e(TAG, "**HJSGB**" + data)
            if (intent.extras!!.getString("data")!=null) {
                data = JSONObject(intent.extras!!.getString("notificationData"))
                try {
                    if (!data!!.isNull("notification_type")) {
                        mType = data.getString("notification_type")
                        Log.e(TAG, "**NOTIFICATION_TYPE**" + mType)
                    }
                    if (!data.isNull("title")) {
                        mTitle = data.getString("title")
                        Log.e(TAG, "**TITLE**" + mTitle)
                    }

                    if (!data.isNull("description")) {
                        message = data.getString("description")
                    }
                    if (!data.isNull("notificationType")) {
                        notificationType = data.getString("notificationType")
                    }
                    if (!data.isNull("roomId")) {
                        mRoomID = data.getString("roomId")
                        Log.e(TAG, "**ROOM_ID**" + mRoomID)
                    }
                    if (!data.isNull("advisorName")) {
                        mAdvisorName = data.getString("advisorName")
                        Log.e(TAG, "**ADVISOR_NAME**" + mAdvisorName)
                    }
                    if (!data.isNull("user_name")) {
                        userName = data.getString("user_name")
                    }

                    if (!data.isNull("totalTime")) {
                        totalTime = data.getString("totalTime")
                    }
                    if (!data.isNull("totalAmount")) {
                        totalAmount = data.getString("totalAmount")
                    }
                    if (!data.isNull("advisorId")) {
                        advisorId = data.getString("advisorId")
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }

        }

        if (notificationType != null && !notificationType.equals("")) {
            isNotification = true
            if (notificationType != null && !notificationType.equals("")) {
                if (notificationType.equals("3")) {
                    intent = Intent(this, ChatActivity::class.java)
                    intent.putExtra(NOTIFY_TYPE, "advisor")
                    intent.putExtra(CHAT_TIME, totalTime)
                    intent.putExtra(PRICE, totalAmount)
                    intent.putExtra(ADVISOR_ID, advisorId)
                    intent.putExtra(PLAN_ID, "")
                    intent.putExtra(ROOM_ID, mRoomID)
                    intent.putExtra(PER_MIN_COST, "2")
                    intent.putExtra(USER_NAME, userName)
                } else if (notificationType.equals("5")) {
                    intent = Intent(this, com.letmelisten.app.activities.ChatActivity::class.java)
                    intent.putExtra(NOTIFY_TYPE, "user")
                    intent.putExtra(CHAT_TIME, totalTime)
                    intent.putExtra(PRICE, totalAmount)
                    intent.putExtra(ADVISOR_ID, advisorId)
                    intent.putExtra(PLAN_ID, "")
                    intent.putExtra(ROOM_ID, mRoomID)
                    intent.putExtra(PER_MIN_COST, "2")
                    intent.putExtra(USER_NAME, userName)
                    intent.putExtra(ADVISOR_NAME, mAdvisorName)
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    FIRST_TIME = false
                } else {
                    intent = Intent(mActivity, HomeActivity::class.java)
                }
            } else {
                intent = Intent(mActivity, HomeActivity::class.java)
            }
            startActivity(intent)
            finish()
            if (!isNotification) {
                setUpSplash()
            }
        } else {
            setUpSplash()
        }
        FirebaseApp.initializeApp(this)
        getDeviceToken()

    }

    private fun getDeviceToken() {

        FirebaseMessaging.getInstance().token
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    Log.w(TAG, "Fetching FCM registration token failed", task.exception)
                    return@OnCompleteListener
                }

                // Get new FCM registration token
                strDeviceToken = task.result!!
                LetMeListenPreferences().writeString(
                    mActivity,
                    DEVICE_TOKEN,
                    strDeviceToken
                )
                Log.e(TAG, "**Push Token**$strDeviceToken")
                // Log and toast
                Log.d(TAG, "Token********   $strDeviceToken")
            })

    }

    private fun setUpSplash() {
        Handler().postDelayed({
            if (isUserLogin()) {
                if (LetMeListenPreferences().readString(mActivity, SWITCH_ACCOUNT, null)
                        .equals("user")
                ) {
                    val mIntent = Intent(mActivity, HomeActivity::class.java)
                    startActivity(mIntent)
                    finish()

                } else if (LetMeListenPreferences().readString(mActivity, SWITCH_ACCOUNT, null)
                        .equals("advisor")
                ) {

                    val mIntent = Intent(
                        mActivity,
                        com.letmelisten.app.activities.advisor.HomeActivity::class.java
                    )
                    startActivity(mIntent)
                    finish()
                } else {
                    val mIntent = Intent(mActivity, HomeActivity::class.java)
                    startActivity(mIntent)
                    finish()
                }


//                if (LetMeListenPreferences().readString(mActivity, IS_ADVISOR, null).equals("0")) {
//                    val mIntent = Intent(mActivity, HomeActivity::class.java)
//                    startActivity(mIntent)
//                    finish()
//                } else {
//                    val mIntent = Intent(
//                        mActivity,
//                        com.letmelisten.app.activities.advisor.HomeActivity::class.java
//                    )
//                    startActivity(mIntent)
//                    finish()
//                }
            } else if (!isUserLogin()) {
                val mIntent = Intent(mActivity, SignInActivity::class.java)
                startActivity(mIntent)
                finish()
            } else {
                val mIntent = Intent(mActivity, SignInActivity::class.java)
                startActivity(mIntent)
                finish()
            }
        }, SPLASH_TIME_OUT)
    }


    fun printKeyHash(context: Activity): String? {
        val packageInfo: PackageInfo
        var key: String? = null
        try {
            //getting application package name, as defined in manifest
            val packageName = context.applicationContext.packageName

            //Retriving package info
            packageInfo = context.packageManager.getPackageInfo(
                packageName,
                PackageManager.GET_SIGNATURES
            )
            Log.e("Package Name=", context.applicationContext.packageName)
            for (signature in packageInfo.signatures) {
                val md = MessageDigest.getInstance("SHA")
                md.update(signature.toByteArray())
                key = String(Base64.encode(md.digest(), 0))
                Log.e(TAG, "Key Hash=++++++=$key")
            }
        } catch (e1: PackageManager.NameNotFoundException) {
            Log.e("Name not found", e1.toString())
        } catch (e: NoSuchAlgorithmException) {
            Log.e("No such an algorithm", e.toString())
        } catch (e: java.lang.Exception) {
            Log.e("Exception", e.toString())
        }
        return key
    }

    private fun isMyServiceRunning(): Boolean {
        val manager = getSystemService(ACTIVITY_SERVICE) as ActivityManager
        Log.v("Running activities", manager.toString())
        for (service in manager.getRunningServices(Int.MAX_VALUE)) {
            if ("com.*.*.service.Service" == service.service.className) {
                return true
            }
        }
        //Log.v("Currently running services in andorid phone", "other services")
        return false
    }
}