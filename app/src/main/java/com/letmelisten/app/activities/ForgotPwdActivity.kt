package com.letmelisten.app.activities

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.lifecycle.ViewModelProviders
import butterknife.ButterKnife
import butterknife.OnClick
import com.letmelisten.app.Model.StatusMsgModel
import com.letmelisten.app.R
import com.letmelisten.app.viewmodels.SignupLoginViewModel
import kotlinx.android.synthetic.main.activity_forgot_pwd.*
import kotlinx.android.synthetic.main.activity_sign_in.*
import java.util.HashMap

class ForgotPwdActivity : BaseActivity() {
    private var viewModel: SignupLoginViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgot_pwd)
        ButterKnife.bind(this)
        viewModel = ViewModelProviders.of(this).get(SignupLoginViewModel::class.java)
        setStatusBar(mActivity)
        setEditTextFocused(mActivity, forgotPwdEmailET)
    }

    @OnClick(
        R.id.backImageView,
        R.id.txtSubmitTV
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.backImageView -> onBackPressed()
            R.id.txtSubmitTV -> performSubmitClick()
        }
    }


    private fun performSubmitClick() {
        if (isValidate())
            if (!isNetworkAvailable(mActivity)) {
                showAlertDialog(mActivity, getString(R.string.internet_connection_error))
            } else {
                executeForgotPasswordApi()
            }
    }


    private fun mParams(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["email"] = forgotPwdEmailET.text.toString().trim { it <= ' ' }
        Log.e("params", "**PARAM**$mMap")
        return mMap
    }

    private fun executeForgotPasswordApi() {
        showProgressDialog(mActivity)
        viewModel?.getForgotPasswordData(mActivity, mParams())?.observe(this,
            androidx.lifecycle.Observer<StatusMsgModel?> { mSignUpModel ->
                if (mSignUpModel.status == 1) {
                    dismissProgressDialog()
                    showToast(mActivity,mSignUpModel.message)
                    startActivity(Intent(mActivity, SignInActivity::class.java))
                    finish()
                } else {
                    dismissProgressDialog()
                    showAlertDialog(mActivity, mSignUpModel.message)
                }
            })
    }


    override fun onBackPressed() {
        startActivity(Intent(this, SignInActivity::class.java))
        finish()
    }

    /**
     * Check Validations of views
     * */
    fun isValidate(): Boolean {
        var flag = true
        if (forgotPwdEmailET!!.text.toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_email_address))
            flag = false
        } else if (!isValidEmailId(forgotPwdEmailET!!.text.toString().trim())) {
            showAlertDialog(mActivity, getString(R.string.please_enter_valid_email_address))
            flag = false
        }
        return flag
    }
}