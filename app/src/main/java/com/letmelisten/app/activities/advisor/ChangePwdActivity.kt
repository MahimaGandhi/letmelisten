package com.letmelisten.app.activities.advisor

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.lifecycle.ViewModelProviders
import butterknife.ButterKnife
import butterknife.OnClick
import com.letmelisten.app.Model.StatusMsgModel
import com.letmelisten.app.R
import com.letmelisten.app.activities.BaseActivity
import com.letmelisten.app.utils.*
import com.letmelisten.app.viewmodels.ProfileEditProfileViewModel
import kotlinx.android.synthetic.main.activity_change_pwd.*
import java.util.*
import kotlin.collections.HashMap


class ChangePwdActivity : BaseActivity() {
    private var viewModel: ProfileEditProfileViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change_pwd)
        ButterKnife.bind(this)
        viewModel = ViewModelProviders.of(this).get(ProfileEditProfileViewModel::class.java)
        setStatusBar(mActivity)
        setEditTextFocused(mActivity, confirmPasswordET)
    }


    @OnClick(
        R.id.txtUpdatePwdTV,
        R.id.backChangePwdIV
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.txtUpdatePwdTV -> performUdatePwdClick()
            R.id.backChangePwdIV -> onBackPressed()
        }
    }

    private fun performUdatePwdClick() {
        if (isValidate())
            if (!isNetworkAvailable(mActivity)) {
                showAlertDialog(mActivity, getString(R.string.internet_connection_error))
            } else {
                executeChangePwdRequest()
            }
    }

    /*
     * Execute api param
     * */
    private fun mParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["newPassword"] = newPasswordET.text.toString().trim { it <= ' ' }
        mMap["oldPassword"] = currentPasswordET.text.toString().trim { it <= ' ' }
        mMap["confirmPassword"] = confirmPasswordET.text.toString().trim { it <= ' ' }
        mMap["isAdvisor"] = "0"
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun mHeaderParam(): HashMap<String, String> {
    val headers = HashMap<String, String>()
        LetMeListenPreferences().readString(mActivity, TOKEN,null)?.let {
            headers.put("Token",
                it
            )
        }
        return headers
    }

    private fun executeChangePwdRequest() {
        showProgressDialog(mActivity)
        viewModel?.changePasswordData(mActivity, mParam(),mHeaderParam())?.observe(this,
            androidx.lifecycle.Observer<StatusMsgModel?> { mChangePasswordModel ->
                if (mChangePasswordModel.status == 1) {
                    dismissProgressDialog()
                    showToast(mActivity, mChangePasswordModel.message)
                    LetMeListenPreferences().writeString(mActivity, PASSWORD,
                        confirmPasswordET.text.toString())
                    currentPasswordET.setText("")
                    newPasswordET.setText("")
                    confirmPasswordET.setText("")
                    finish()
                } else {
                    dismissProgressDialog()
                    showAlertDialog(mActivity, mChangePasswordModel.message)
                }
            })
    }

    /*
      * Check Validations of views
      * */
    fun isValidate(): Boolean {
        var flag = true
        if (currentPasswordET!!.text.toString().trim() == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_current_password))
            flag = false
        } else if (newPasswordET!!.text.toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_new_password))
            flag = false
        } else if (confirmPasswordET!!.text.toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_retype_password))
            flag = false
        } else if (!newPasswordET!!.text.toString().trim()
                .equals(confirmPasswordET!!.text.toString().trim())
        ) {
            showAlertDialog(mActivity, getString(R.string.the_new_re_typed))
            flag = false
        }
        return flag
    }
}