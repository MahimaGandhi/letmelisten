package com.letmelisten.app.activities

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.letmelisten.app.Interfaces.HomeTopItemClickInterface
import com.letmelisten.app.Interfaces.LoadHistoryData
import com.letmelisten.app.Model.Data
import com.letmelisten.app.Model.DataX
import com.letmelisten.app.Model.FavoriteListModel
import com.letmelisten.app.Model.SessionHistoryModel
import com.letmelisten.app.R
import com.letmelisten.app.adapters.HistoryAdapter
import com.letmelisten.app.utils.LetMeListenPreferences
import com.letmelisten.app.utils.TOKEN
import com.letmelisten.app.viewmodels.HomeViewModel
import kotlinx.android.synthetic.main.fragment_home.*

class HistoryActivity : BaseActivity(),HomeTopItemClickInterface,LoadHistoryData {
    @BindView(R.id.historyRV)
    lateinit var historyRV: RecyclerView
    var onClickListener: HomeTopItemClickInterface? = null
    var mLoadMore: LoadHistoryData? = null
    var mHistoryList: ArrayList<DataX>? = ArrayList()
    var mPageNo: Int = 1
    var mPerPage: Int = 10
    var isLoading: Boolean = true
    private var viewModel: HomeViewModel? = null

    lateinit var historyAdapter: HistoryAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_history)
        ButterKnife.bind(this)
        viewModel = ViewModelProviders.of(this).get(HomeViewModel::class.java)
        setStatusBar(mActivity)
        onClickListener = this
        mLoadMore = this
//        initRecyclerView(mView)
        if (mHistoryList != null) {
            mHistoryList!!.clear()
        }

        getHistoryData()
//        initRecylerView()
    }


    private fun getHistoryData() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error))
        } else {
            executeHistoryRequest()
        }
    }

    /*
     * Execute api param
     * */
    private fun mParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["pageNo"] = mPageNo.toString()
        mMap["perPage"] = mPerPage.toString()
        mMap["loginAs"] = "1"   //User
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun mHeaderParam(): HashMap<String, String> {
        val headers = HashMap<String, String>()
        mActivity?.let {
            LetMeListenPreferences().readString(it, TOKEN, null)?.let {
                headers.put(
                    "Token",
                    it
                )
            }
        }
        Log.e(TAG, "**Token**${mActivity?.let { LetMeListenPreferences().readString(it, TOKEN,null) }}")
        return headers
    }

    private fun executeHistoryRequest() {
        showProgressDialog(mActivity)
        mActivity?.let {
            viewModel?.getHistoryListData(mActivity, mParam(), mHeaderParam())?.observe(
                this,
                androidx.lifecycle.Observer<SessionHistoryModel?> { mHistoryModel ->
                    if (mHistoryModel.status.equals(1)) {
                        dismissProgressDialog()
                        isLoading = !mHistoryModel.lastPage.equals(true)
                        mHistoryList = mHistoryModel.data
                        setHistoryAdapter()
                        txtNoDataFountTV.visibility = View.GONE
                    } else {
                        dismissProgressDialog()
//                        showToast(mActivity,mHistoryModel.message)
                        txtNoDataFountTV.visibility = View.VISIBLE
                        txtNoDataFountTV.text = mHistoryModel.message
                    }
                })
        }
    }


//    var mLoadMoreScrollListner: LoadHistoryData = object : LoadHistoryData {
//        override fun onLoadMoreListner(mModel: Data) {
//            if (isLoading) {
//                ++mPageNo
//                executeMoreHomeDataRequest()
//            }
//        }
//    }

    /*
    * Execute api param
    * */
    private fun mLoadMoreParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = java.util.HashMap()
        mMap["pageNo"] = mPageNo.toString()
        mMap["perPage"] = mPerPage.toString()
        mMap["loginAs"] = "1"
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }


    private fun executeMoreHomeDataRequest() {
       // mProgressRL.visibility = View.VISIBLE
        mActivity?.let {
            viewModel?.getHistoryListData(mActivity, mLoadMoreParam(), mHeaderParam())?.observe(
                this,
                androidx.lifecycle.Observer<SessionHistoryModel?> { mHistoryModel ->
                    if (mHistoryModel.status == 1) {
                     //   mProgressRL.visibility = View.GONE
                        mHistoryModel.data.let {
                            mHistoryList!!.addAll<DataX>(
                                it
                            )
                        }
                        historyAdapter.notifyDataSetChanged()
                        isLoading = !mHistoryModel.lastPage.equals(true)

                    } else if (mHistoryModel.status == 0) {
                       // mProgressRL.visibility = View.GONE
//                    showToast(mActivity, mGetFavModel.message)
                    }
                })
        }
    }

    override fun onItemmClick(positon: Int, id: String) {

    }

    @OnClick(
        R.id.backHistoryRL
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.backHistoryRL -> onBackPressed()
        }
    }

    private fun setHistoryAdapter() {
        val layoutManager: RecyclerView.LayoutManager =
            LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false)
        historyRV.setLayoutManager(layoutManager)
        historyAdapter = HistoryAdapter(mActivity, mHistoryList, onClickListener, mLoadMore)
        historyRV.setAdapter(historyAdapter)

    }

    override fun onLoadMoreData(mModel: DataX) {
        if (isLoading) {
            ++mPageNo
            executeMoreHomeDataRequest()
        }
    }
}