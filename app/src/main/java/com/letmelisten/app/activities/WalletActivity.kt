package com.letmelisten.app.activities

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProviders
import butterknife.ButterKnife
import butterknife.OnClick
import com.letmelisten.app.Model.CallingModel
import com.letmelisten.app.Model.SessionCreateModel
import com.letmelisten.app.R
import com.letmelisten.app.utils.*
import com.letmelisten.app.viewmodels.PaymentViewModel
import com.letmelisten.app.viewmodels.StripePaymentViewModel
import kotlinx.android.synthetic.main.activity_wallet.*
import java.util.*


class WalletActivity : BaseActivity() {
    var recordAudio = Manifest.permission.RECORD_AUDIO
    var camera = Manifest.permission.CAMERA
    var writeExternalStorage = Manifest.permission.WRITE_EXTERNAL_STORAGE
    private var viewModel: StripePaymentViewModel? = null
    private var viewModel2: PaymentViewModel? = null
    var total_cost: String = ""
    var time: String = ""
    var click_type: String = ""
    var price: String = ""
    var perMinCost: String = ""
    var service_type: String = ""
    var advisorId: String = ""
    var planId: String = ""
    var roomId: String = ""
    var newWalletAmount: Int = 0
    var strServiceType: String? = null
    var walletAmount: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_wallet)
        ButterKnife.bind(this)
        setStatusBar(mActivity)
        viewModel = ViewModelProviders.of(this).get(StripePaymentViewModel::class.java)
        viewModel2 = ViewModelProviders.of(this).get(PaymentViewModel::class.java)

        getIntentData()

    }

    override fun onResume() {
        super.onResume()
        walletAmount = LetMeListenPreferences().readString(
            mActivity,
            WALLET_AMOUNT,
            ""
        )
        if (walletAmount != null && !walletAmount.equals("")) {
            walletAmountTV.text = "$" + walletAmount
        } else {
            walletAmountTV.text = "$0"
        }
    }

    private fun getIntentData() {
        if (intent != null) {

            time = intent.getStringExtra(CHAT_TIME).toString()
            if (!intent.getStringExtra(CLICK_TYPE).toString().equals("") || !intent.getStringExtra(
                    CLICK_TYPE
                ).toString().equals("null")
            ) {
                click_type = intent.getStringExtra(CLICK_TYPE).toString()
            }

            Log.e(TAG, "TIME_VALUE" + time.toString())
            price = intent.getStringExtra(PRICE).toString()
            advisorId = intent.getStringExtra(ADVISOR_ID).toString()
            planId = intent.getStringExtra(PLAN_ID).toString()
            perMinCost = intent.getStringExtra(PER_MIN_COST).toString()
            service_type = intent.getStringExtra(SERVICE_TYPE).toString()
            if (service_type.equals(getString(R.string.chatt))) {
                strServiceType = "1"
            } else {
                strServiceType = "2"
            }
        }
    }

    @OnClick(
        R.id.backRL,
        R.id.addMoneyRL,
        R.id.balanceLL
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.backRL -> onBackPressed()
            R.id.addMoneyRL -> addMoneyToWallet()
            R.id.balanceLL -> performBalanceClick()
        }
    }

    private fun performBalanceClick() {
        if (click_type.equals("setting")) {

        } else {
            if (isValidate()) {
                if (checkPermission()) {
                    executeSessionCreateApi()
                } else {
                    requestPermission()
                }

            }
        }

    }

    /*
     * Set up validations for wallet Money
     * */
    private fun isValidate(): Boolean {
        var flag = true
        Log.e(TAG, "**Wallet_Amount**" + walletAmountTV.text.toString())
        var subWalletMoney = Integer.parseInt(walletAmountTV.text.toString().substring(1))
        if (subWalletMoney.equals("0")) {

            showAlertDialog(mActivity, getString(R.string.please_add_money_in_wallet))
            flag = false
        } else if (subWalletMoney < 10) {
            showAlertDialog(mActivity, getString(R.string.insufficient_balance))
            flag = false
        }

        return flag
    }

    private fun addMoneyToWallet() {
        startActivity(Intent(mActivity, AddMoneyToWalletActivity::class.java))
    }


    private fun mParams(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["advisorId"] = advisorId
        mMap["perMinCost"] = perMinCost
//        mMap["planId"] = planId
        mMap["totalTime"] = time
        mMap["totalAmount"] = price
        mMap["serviceType"] = strServiceType    //1 for chat and 2 for call
        Log.e("params", "**PARAM**$mMap")
        return mMap
    }

    private fun executeSessionCreateApi() {
        showProgressDialog(mActivity)
        viewModel?.getSessionCreateData(mActivity, mParams(), mHeaderParam())?.observe(this,
            androidx.lifecycle.Observer<SessionCreateModel?> { mSessionpojo ->
                if (mSessionpojo.status == 1) {

                    roomId = mSessionpojo.data?.roomId!!
                    executeMoneyDeduct()
                    dismissProgressDialog()
                    //  val intent = Intent(mActivity, ChatActivity::class.java)
                    if (service_type.equals(getString(R.string.chatt))) {
                        val intent = Intent(mActivity, ConnectivityActivity::class.java)
                        intent.putExtra(CHAT_TIME, time)
                        intent.putExtra(PRICE, price)
                        intent.putExtra(ADVISOR_ID, advisorId)
                        intent.putExtra(PER_MIN_COST, perMinCost)
                        intent.putExtra(ROOM_ID, mSessionpojo.data?.roomId)
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent)
                        finish()
                    } else {
                        executeInitaiteCallingApi()
//

                    }
                } else {
                    dismissProgressDialog()
                    showAlertDialog(mActivity, mSessionpojo.message)
                }
            })
    }

    /*
     * Check permission
     * */
    private fun checkPermission(): Boolean {
        val recordAudtioINT = ContextCompat.checkSelfPermission(mActivity, recordAudio)
        val writeExternalStorageINT =
            ContextCompat.checkSelfPermission(mActivity, writeExternalStorage)
        return recordAudtioINT == PackageManager.PERMISSION_GRANTED && writeExternalStorageINT == PackageManager.PERMISSION_GRANTED
    }

    private fun requestPermission() {
        ActivityCompat.requestPermissions(
            mActivity,
            arrayOf(recordAudio, writeExternalStorage),
            369
        )
    }

    @JvmName("onRequestPermissionsResult1")
    fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>?,
        grantResults: IntArray
    ) {
        when (requestCode) {
            369 ->                 // If request is cancelled, the result arrays are empty.
                if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    executeSessionCreateApi()
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Log.e("TAG", "**Permission Denied**")
                    //showLongToast("You can denied the permission now you are not able for video call.")
                }
        }
    }

    private fun executeMoneyDeduct() {
        var priceNum = Integer.parseInt(walletAmountTV.text.toString().substring(1))
        newWalletAmount = Integer.parseInt(priceNum.toString()) - Integer.parseInt(price)
        Log.e(TAG, "**NEW_VALUE**" + newWalletAmount)
        LetMeListenPreferences().writeString(
            mActivity,
            WALLET_AMOUNT,
            newWalletAmount.toString()
        )
    }

    private fun mHeaderParam(): HashMap<String, String> {
        val headers = HashMap<String, String>()
        mActivity.let {
            LetMeListenPreferences().readString(it, TOKEN, null)?.let {
                headers.put(
                    "Token",
                    it
                )
            }
        }
        Log.e(
            TAG,
            "**Token**${mActivity.let { LetMeListenPreferences().readString(it, TOKEN, null) }}"
        )
        return headers
    }

    /*
        * Execute api param
        * */
    private fun mCallingParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["advisorId"] = advisorId
        mMap["roomId"] = roomId
        mMap["token"] = getString(R.string.agora_access_token)
        mMap["appId"] = getString(R.string.agora_app_id)
        mMap["channelName"] = "voiceDemoChannel1"
        Log.e("TAG", "**PARAM**$mMap")
        return mMap
    }

    private fun executeInitaiteCallingApi() {
        showProgressDialog(mActivity)
        viewModel2?.callingRequestData(mActivity, mCallingParam(), mHeaderParam())?.observe(this,
            androidx.lifecycle.Observer<CallingModel?> { mCallingModel ->
                if (mCallingModel.status == 1) {
                    dismissProgressDialog()
                    val intent = Intent(mActivity, CallingActivity::class.java)
                    intent.putExtra("advisorId", advisorId)
                    intent.putExtra(CHAT_TIME, time)
                    intent.putExtra(ROOM_ID, roomId)
                    startActivity(intent)
                } else {
                    dismissProgressDialog()
                    showAlertDialog(mActivity, mCallingModel.message)
                }
            })
    }

}