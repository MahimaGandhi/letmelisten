package com.letmelisten.app.activities.advisor

import android.content.Intent
import android.os.Bundle
import com.letmelisten.app.R
import com.letmelisten.app.activities.BaseActivity
import com.letmelisten.app.activities.SignInActivity


class SplashActivity : BaseActivity() {
    val SPLASH_TIME_OUT = 1500L
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash2)
        setStatusBar(mActivity)
        setUpSplash()
    }

    private fun setUpSplash() {
        val mThread = object : Thread() {
            override fun run() {
                sleep(SPLASH_TIME_OUT)
                val i = Intent(mActivity, SignInActivity::class.java)
                startActivity(i)
                finish()
            }
        }
        mThread.start()
    }
}