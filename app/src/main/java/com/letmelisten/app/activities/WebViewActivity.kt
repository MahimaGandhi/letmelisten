package com.letmelisten.app.activities

import android.os.Bundle
import android.view.View
import android.webkit.WebViewClient
import butterknife.ButterKnife
import butterknife.OnClick
import com.letmelisten.app.R
import com.letmelisten.app.utils.*
import kotlinx.android.synthetic.main.activity_web_view.*

class WebViewActivity : BaseActivity() {

    var linkType : String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_web_view)
        ButterKnife.bind(this)
        setStatusBar(mActivity)
        getIntentData()
    }


    @OnClick(R.id.imgBackIV)

    fun onClick(view : View) {
        when (view.id) {
            R.id.imgBackIV -> performBackClick()
        }
    }


    private fun getIntentData() {
        if (intent != null){
            linkType = intent.getStringExtra(LINK_TYPE).toString()
            if (linkType.equals(LINK_ABOUT)){
                txtHeadingTV.setText(getString(R.string.about))
                setUpWebView(ABOUT_WEB_LINK)
            }else if (linkType.equals(LINK_TERMS)){
                txtHeadingTV.setText(getString(R.string.terms))
                setUpWebView(TERMS_WEB_LINK)
            }else if (linkType.equals(LINK_PP)){
                txtHeadingTV.setText(getString(R.string.privacy_policy))
                setUpWebView(PP_WEB_LINK)
            }else if (linkType.equals(LINK_HELP)){
                txtHeadingTV.setText(getString(R.string.help))
                setUpWebView(HELP_WEB_LINK)
            }
        }
    }

    private fun setUpWebView(mUrl : String){
        mWebViewWV.webViewClient = WebViewClient()
        // this will load the url of the website
        mWebViewWV.loadUrl(mUrl)
        // this will enable the javascript settings
        mWebViewWV.settings.javaScriptEnabled = true
        // if you want to enable zoom feature
        mWebViewWV.settings.setSupportZoom(true)
    }

    private fun performBackClick() {
        onBackPressed()
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }
}