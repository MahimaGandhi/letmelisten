package com.letmelisten.app.activities.advisor

import android.content.Intent
import android.os.Bundle
import android.view.View
import butterknife.ButterKnife
import butterknife.OnClick
import com.letmelisten.app.R
import com.letmelisten.app.activities.BaseActivity
import com.letmelisten.app.activities.SignUpActivity
import kotlinx.android.synthetic.main.activity_sign_in.*

class SignInActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_in2)
        ButterKnife.bind(this)
        setStatusBar(mActivity)
        setEditTextFocused(mActivity, editPasswordET)
    }

    @OnClick(
        R.id.txtLogInTV,
        R.id.txtForgotPasswordTV,
        R.id.txtSignUpTV
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.txtLogInTV -> performLogInClick()
            R.id.txtForgotPasswordTV -> performForgotPwdClick()
            R.id.txtSignUpTV -> performSignUpClick()
        }
    }

    private fun performLogInClick() {
        if (isValidate()) {
            startActivity(Intent(mActivity,HomeActivity::class.java))
            finish()
        }
    }

    private fun performForgotPwdClick() {
        startActivity(Intent(mActivity,ForgotPwdActivity::class.java))
        finish()
    }

    private fun performSignUpClick() {
        startActivity(Intent(mActivity, SignUpActivity::class.java))
        finish()
    }

    /*
     * Check Validations of views
     * */
    fun isValidate(): Boolean {
        var flag = true
        if (editEmailET!!.text.toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_email_address))
            flag = false
        } else if (!isValidEmailId(editEmailET!!.text.toString().trim())) {
            showAlertDialog(mActivity, getString(R.string.please_enter_valid_email_address))
            flag = false
        } else if (editPasswordET!!.text.toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_password))
            flag = false
        }
        return flag
    }
}