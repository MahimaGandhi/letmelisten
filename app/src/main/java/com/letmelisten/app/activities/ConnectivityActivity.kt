package com.letmelisten.app.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import com.bumptech.glide.Glide
import com.letmelisten.app.Model.AdvisorDetailsModel
import com.letmelisten.app.Model.EndSessionModel
import com.letmelisten.app.R
import com.letmelisten.app.utils.*
import com.letmelisten.app.viewmodels.HomeViewModel
import com.letmelisten.app.viewmodels.StripePaymentViewModel
import kotlinx.android.synthetic.main.activity_chat.*
import kotlinx.android.synthetic.main.activity_user_detail.*
import kotlinx.android.synthetic.main.fragment_profile.*
import java.util.HashMap

class ConnectivityActivity : BaseActivity() {
    var time: String = ""
    var price: String = ""
    var perMinCost: String = ""
    var advisorId: String = ""
    var planId: String = ""
    var roomId: String = ""
    private var viewModel: HomeViewModel? = null

    @BindView(R.id.connectingNameTV)
    lateinit var connectingNameTV: TextView

    @BindView(R.id.profileAdvisorIV)
    lateinit var profileAdvisorIV: ImageView

    @BindView(R.id.profileUserIV)
    lateinit var profileUserIV: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_connectivity)
        ButterKnife.bind(this)
        viewModel = ViewModelProviders.of(this).get(HomeViewModel::class.java)
        getIntentData()

        FIRST_TIME = true
    }


    private fun getIntentData() {
        if (intent != null) {
            time = intent.getStringExtra(CHAT_TIME).toString()
            price = intent.getStringExtra(PRICE).toString()
            advisorId = intent.getStringExtra(ADVISOR_ID).toString()
            planId = intent.getStringExtra(PLAN_ID).toString()
            roomId = intent.getStringExtra(ROOM_ID).toString()
            perMinCost = intent.getStringExtra(PER_MIN_COST).toString()
            executeGetAdvisorDetailsApi()
        }
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        return if (keyCode == KeyEvent.KEYCODE_BACK) {
            executeEndSessionRequest()
            false
        } else super.onKeyDown(keyCode, event)
    }

    private fun mParams(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["advisorId"] = advisorId
        Log.e("params", "**PARAM**$mMap")
        return mMap
    }

    fun mHeaderParam(): HashMap<String, String> {
        val headers = HashMap<String, String>()
        LetMeListenPreferences().readString(mActivity, TOKEN, null)?.let {
            headers.put(
                "Token",
                it
            )
        }
        return headers
    }

    private fun executeGetAdvisorDetailsApi() {
        showProgressDialog(mActivity)
        viewModel?.getAdvisorDetails(mActivity, mParams(), mHeaderParam())?.observe(this,
            androidx.lifecycle.Observer<AdvisorDetailsModel?> { mAdvisorModel ->
                if (mAdvisorModel.status == 1) {
                    dismissProgressDialog()
                    Glide.with(mActivity)
                        .load(mAdvisorModel.data.advisorImage)
                        .placeholder(R.drawable.ic_phh)
                        .error(R.drawable.ic_phh)
                        .into(profileAdvisorIV)


                    mActivity?.let {
                        Glide.with(it).load(
                            LetMeListenPreferences().readString(
                                mActivity,
                                PROFILE_IMAGE,
                                null
                            )
                        )
                            .placeholder(R.drawable.ph)
                            .error(R.drawable.ph)
                            .into(profileUserIV)
                    }

//                    connectingNameTV.text =
//                        mAdvisorModel.data.firstName + " " + mAdvisorModel.data.lastName
                    connectingNameTV.text = mAdvisorModel.data.advisorName
                } else {
                    dismissProgressDialog()
                    showAlertDialog(mActivity, mAdvisorModel.message)
                }
            })
    }

    /*
             * Execute api param
             * */
    private fun mEndSessionParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["roomId"] = roomId
        mMap["rating"] = ""
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeEndSessionRequest() {
        showProgressDialog(mActivity)
        viewModel?.getSessionEndData(mActivity, mEndSessionParam(), mHeaderParam())?.observe(this,
            androidx.lifecycle.Observer<EndSessionModel?> { mSessionEndModel ->
                if (mSessionEndModel.status == 1) {
                    dismissProgressDialog()
                    finish()
                } else if (mSessionEndModel?.status == 0) {
                    dismissProgressDialog()
                    showToast(mActivity, mSessionEndModel.message)
                }
            })
    }

    override fun onResume() {
        super.onResume()

        if (!FIRST_TIME){
            finish()
        }
    }
}