package com.letmelisten.app

import android.app.Application
import android.content.Intent
import android.content.SharedPreferences
import android.text.TextUtils
import android.util.Log
import com.letmelisten.app.agoraPackage.AGEventHandler
import com.letmelisten.app.agoraPackage.CurrentUserSettings
import com.letmelisten.app.agoraPackage.EngineConfig
import com.letmelisten.app.agoraPackage.MyEngineEventHandler
import com.letmelisten.app.utils.SocketURL
import io.agora.rtc.Constants
import io.agora.rtc.RtcEngine
import io.socket.client.IO
import io.socket.client.Socket
import org.slf4j.LoggerFactory
import java.net.URISyntaxException


class LetMeListenApplication : Application() {
    val CONNECTION_TIMEOUT = 50000 * 1000 //120 Seconds    private RequestQueue mRequestQueue;

    private val TAG = "LetMeListen"
    private var mInstance: LetMeListenApplication? = null
    var languageToLoad = ""
    var preferences: SharedPreferences? = null

    private val mVideoSettings = CurrentUserSettings()

    private val log = LoggerFactory.getLogger(this.javaClass)
    private var mRtcEngine: RtcEngine? = null
    private var mConfig: EngineConfig? = null
    private var mEventHandler: MyEngineEventHandler? = null

    fun rtcEngine(): RtcEngine? {
        return mRtcEngine
    }

    fun config(): EngineConfig? {
        return mConfig
    }

    fun userSettings(): CurrentUserSettings? {
        return mVideoSettings
    }

    fun addEventHandler(handler: AGEventHandler?) {
        if (handler != null) {
            mEventHandler?.addEventHandler(handler)
        }
    }

    fun remoteEventHandler(handler: AGEventHandler?) {
        if (handler != null) {
            mEventHandler?.removeEventHandler(handler)
        }
    }

    /*
         * Socket
         * */
    private var mSocket: Socket? = null

    @Synchronized
    fun getInstance(): LetMeListenApplication? {
        return mInstance
    }






    override fun onCreate() {
        super.onCreate()
        mInstance = this
        try {
            mSocket = IO.socket(SocketURL)
        } catch (e: URISyntaxException) {
            throw RuntimeException(e)
        }

        createRtcEngine()
       // startService(Intent(this, MyService::class.java))
    }

    fun getSocket(): Socket? {
        return mSocket
    }
    private fun createRtcEngine() {
        val context = applicationContext
        val appId = context.getString(R.string.agora_app_id)
        if (TextUtils.isEmpty(appId)) {
            throw RuntimeException("NEED TO use your App ID, get your own ID at https://dashboard.agora.io/")
        }
        mEventHandler = MyEngineEventHandler()
        mRtcEngine = try {
            // Creates an RtcEngine instance
            RtcEngine.create(context, appId, mEventHandler)
        } catch (e: Exception) {
            log.error(Log.getStackTraceString(e))
            throw RuntimeException(
                """
                 NEED TO check rtc sdk init fatal error
                 ${Log.getStackTraceString(e)}
                 """.trimIndent()
            )
        }

        /*
          Sets the channel profile of the Agora RtcEngine.
          The Agora RtcEngine differentiates channel profiles and applies different optimization
          algorithms accordingly. For example, it prioritizes smoothness and low latency for a
          video call, and prioritizes video quality for a video broadcast.
         */mRtcEngine?.setChannelProfile(Constants.CHANNEL_PROFILE_COMMUNICATION)

        // Enables the video module.
//        mRtcEngine?.enableVideo()
        /*
          Enables the onAudioVolumeIndication callback at a set time interval to report on which
          users are speaking and the speakers' volume.
          Once this method is enabled, the SDK returns the volume indication in the
          onAudioVolumeIndication callback at the set time interval, regardless of whether any user
          is speaking in the channel.
         */mRtcEngine?.enableAudioVolumeIndication(200, 3, false)
        mConfig = EngineConfig()
    }

    override fun onTerminate() {
        super.onTerminate()
    }

}