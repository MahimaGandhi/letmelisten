package com.letmelisten.app.fcm

import android.app.NotificationManager
import android.content.BroadcastReceiver

import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.kloxus.app.retrofit.ApiInterface
import com.letmelisten.app.Model.EndSessionCallModel
import com.letmelisten.app.activities.CallingActivity
import com.letmelisten.app.fcm.NotificationReciever.Companion.ringtone
import com.letmelisten.app.retrofit.ApiClient
import com.letmelisten.app.utils.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.HashMap


class NotificationBroadcast : BroadcastReceiver() {
    var nm: NotificationManager? = null
    var mContext: Context? = null
    var mIntent: Intent? = null
    var roomIds: String? = null

    override fun onReceive(p0: Context?, p1: Intent?) {
        mContext = p0
        mIntent = p1
        if (p1?.action.equals(NotificationGenerator.NOTIFY_ANSWER)) {
            ringtone!!.stop()
            roomIds = p1?.getStringExtra(ROOM_ID)
            executeAnswerCallRequest()


        } else if (p1?.action.equals(NotificationGenerator.DECLINE_ANSWER)) {
            roomIds = p1?.getStringExtra(ROOM_ID)
            ringtone!!.stop()
            executeEndSessionRequestApi()

        }
    }

    private fun sendBrCast(conext: Context) {
        val intent = Intent("dissMissCall")
        intent.putExtra("Status", "call_end")
        LocalBroadcastManager.getInstance(conext).sendBroadcast(intent)
    }


    fun cancelNotification(p0: Context?) {
        if (p0 != null) {
            nm = p0.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        }

        // nm?.cancel(NotificationGenerator.NOTIFICATION_ID_CUSTOM_BIG) // Notification ID to cancel
        nm?.cancelAll()

    }

    private fun mEndSessionParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["roomId"] = roomIds
        mMap["rating"] = "0"
        mMap["role"] = "2"     //role=1 for user 2 for advisor
        Log.e("TAG", "**PARAM**$mMap")
        return mMap
    }


    fun mHeaderParam(): HashMap<String, String> {
        val headers = HashMap<String, String>()
        LetMeListenPreferences().readString(mContext!!, com.letmelisten.app.utils.TOKEN, null)
            ?.let {
                headers.put(
                    "Token",
                    it
                )
            }
        return headers
    }

    private fun executeEndSessionRequestApi() {
        val mApiInterface: ApiInterface =
            ApiClient().getApiClient()!!.create(ApiInterface::class.java)

        mApiInterface.endSessionCallRequest(mEndSessionParam(), mHeaderParam())
            .enqueue(object : Callback<EndSessionCallModel?> {
                override fun onResponse(
                    call: Call<EndSessionCallModel?>,
                    response: Response<EndSessionCallModel?>
                ) {

                    val mModel: EndSessionCallModel? = response.body()
                    if (mModel!!.status == 1) {
                        cancelNotification(mContext)
                    } else {
                        cancelNotification(mContext)
                    }
                }

                override fun onFailure(call: Call<EndSessionCallModel?>, t: Throwable) {
                }
            })
    }


    /*
 * Execute api param
 * */
    private fun mCallAnswerParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["roomId"] = roomIds
        Log.e("TAG", "**PARAM**$mMap")
        return mMap
    }

    private fun executeAnswerCallRequest() {
        val mApiInterface: ApiInterface =
            ApiClient().getApiClient()!!.create(ApiInterface::class.java)

        mApiInterface.answerCallRequest(mCallAnswerParam(), mHeaderParam())
            .enqueue(object : Callback<EndSessionCallModel?> {
                override fun onResponse(
                    call: Call<EndSessionCallModel?>,
                    response: Response<EndSessionCallModel?>
                ) {

                    val mModel: EndSessionCallModel? = response.body()
                    if (mModel!!.status == 1) {
                        cancelNotification(mContext)

                        //Set Intent
                        val playIntent = Intent(mContext, CallingActivity::class.java)
                        playIntent.putExtra(ADVISOR_ID, mIntent?.getStringExtra(ADVISOR_ID))
                        playIntent.putExtra(NOTIFY_INCOMING_TYPE, "incoming call")
                        playIntent.putExtra(ROOM_ID, mIntent?.getStringExtra(ROOM_ID))
                        playIntent.putExtra(USER_NAME, mIntent?.getStringExtra(USER_NAME))
                        playIntent.putExtra(ADVISOR_NAME, mIntent?.getStringExtra(USER_NAME))
                        playIntent.putExtra( AGORA_USER_IMAGE, mIntent?.getStringExtra(AGORA_USER_IMAGE))
                        playIntent.putExtra(CHAT_TIME, mIntent?.getStringExtra(CHAT_TIME))
                        playIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                            mContext!!.startActivity(playIntent)
                    } else {
                        cancelNotification(mContext)
                    }
                }

                override fun onFailure(call: Call<EndSessionCallModel?>, t: Throwable) {
                }
            })

    }
}