package com.letmelisten.app.fcm
import android.app.*
import android.content.Context
import android.content.Intent
import android.media.Ringtone
import android.media.RingtoneManager
import android.os.Build
import android.os.Vibrator
import android.widget.RemoteViews
import androidx.core.app.NotificationCompat
import com.letmelisten.app.R
import com.letmelisten.app.activities.IncommingActivity
import com.letmelisten.app.utils.*


class NotificationGenerator {
    private var vib: Vibrator? = null
    var ringtone: Ringtone? = null

    companion object {
        var NOTIFY_ANSWER: String = "com.letmelisten.notification.answer";
        var DECLINE_ANSWER: String = "com.letmelisten.notification.decline";
        val NOTIFICATION_ID_CUSTOM_BIG = 9


        val NOTIFICATION_ID_OPEN_ACTIVITY = 9


        var expandedView: RemoteViews? = null
        var nms: NotificationManager? = null
        var nc: NotificationCompat.Builder? = null


        private fun setListeners(view: RemoteViews, context: Context) {
        }

        fun customBigNotification(
            context: Context,
            advisorId: String,
            mRoomID: String,
            mAdvisorName: String,
            agoraAppId: String,
            channelName: String,
            callToken: String,
            userImage: String,
            totalTime: String,
            userName: String, )
        {
            val channelId = context.getString(R.string.default_notification_channel_id)
            val notifyIntent = Intent(context, NotificationBroadcast::class.java)
            notifyIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK


            expandedView = RemoteViews(context.packageName, R.layout.notification_large)

            //Intent for Answer Call
            val ansCall = Intent(context, NotificationBroadcast::class.java).setAction(NOTIFY_ANSWER)
            ansCall.putExtra(ADVISOR_ID, advisorId)
            ansCall.putExtra(ROOM_ID, mRoomID)
            ansCall.putExtra(USER_NAME, userName)
            ansCall.putExtra(ADVISOR_NAME, mAdvisorName)
            ansCall.putExtra(AGORA_USER_IMAGE, userImage)
            ansCall.putExtra(CHAT_TIME, totalTime)
            val pPrevious =
                PendingIntent.getBroadcast(context, 0, ansCall, PendingIntent.FLAG_ONE_SHOT)

            //Intent for Decline Call
            val declineCall =
                Intent(context, NotificationBroadcast::class.java).setAction(DECLINE_ANSWER)
            declineCall.putExtra(ROOM_ID, mRoomID)
            val pDecline =
                PendingIntent.getBroadcast(context, 0, declineCall, PendingIntent.FLAG_ONE_SHOT)


            expandedView!!.setOnClickPendingIntent(R.id.btnAnswer, pPrevious)
            expandedView!!.setOnClickPendingIntent(R.id.btnDecline, pDecline)


            //Use To open Full screen intent when screeen off for incoming call
            val pendingIntent = PendingIntent.getActivity(context, 0, notifyIntent, PendingIntent.FLAG_ONE_SHOT)


            // Use To open Full screen intent when screeen off for incoming call
            val fullScreenIntent = Intent(context, IncommingActivity::class.java)
            fullScreenIntent.putExtra(ADVISOR_ID, advisorId)
            fullScreenIntent.putExtra(TRY_ROOM, mRoomID)
            fullScreenIntent.putExtra(USER_NAME, userName)
            fullScreenIntent.putExtra(ADVISOR_NAME, mAdvisorName)
            fullScreenIntent.putExtra(AGORA_USER_IMAGE, userImage)
            fullScreenIntent.putExtra(CHAT_TIME, totalTime)
            fullScreenIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            fullScreenIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK )
            val fullScreenPendingIntent = PendingIntent.getActivity(context, 0, fullScreenIntent, PendingIntent.FLAG_UPDATE_CURRENT)



            val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE)
            nc = NotificationCompat.Builder(context, channelId)
                .setContent(expandedView)
                .setCustomContentView(expandedView)
                .setCustomBigContentView(expandedView)
                .setPriority(Notification.PRIORITY_HIGH)
                .setSmallIcon(R.drawable.ic_notify_app)
                .setContentTitle("Let Me Listen")
                .setSound(defaultSoundUri)
                .setContentText("Incoming call")
                .setCustomHeadsUpContentView(expandedView)
                .setOngoing(true)
                .setContentIntent(pendingIntent)
                .setFullScreenIntent(fullScreenPendingIntent, true)

            nms = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

            // Since android Oreo notification channel is needed.
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                if (nms != null) {
                    val channel = NotificationChannel(
                        channelId, context.getString(R.string.default_notification_channel_name),
                        NotificationManager.IMPORTANCE_HIGH
                    )
                    channel.enableVibration(true)
                    nms!!.createNotificationChannel(channel)
                }
            }
            if (nms != null) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    nms!!.notify(NOTIFICATION_ID_CUSTOM_BIG, nc!!.build())
                } else {
                    nms!!.notify(NOTIFICATION_ID_CUSTOM_BIG, nc!!.build())
                }
            }
        }
    }


}