package com.letmelisten.app.fcm

import android.annotation.SuppressLint
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.Ringtone
import android.media.RingtoneManager
import android.os.Build
import android.os.Vibrator
import android.util.Log
import android.widget.RemoteViews
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.letmelisten.app.R
import com.letmelisten.app.activities.advisor.ChatActivity
import com.letmelisten.app.utils.*
import org.json.JSONObject


class NotificationReciever : FirebaseMessagingService() {

    var nm: NotificationManager? = null
    val TAG: String = "NotificationReciever"
    var NOTIFICATION_ID = 1
    var mTitle = ""
    var mName = ""
    var mRoomID = ""
    var userID = ""
    var totalAmount = ""
    var detailsID = ""
    var channelName = ""
    var agoraAppId = ""
    var callToken = ""
    var advisorId = ""
    var created = ""
    var mType = ""
    var totalTime = ""
    var message = ""
    var timeAddedCall = ""
    var notificationType = ""
    var mAdvisorName = ""
    var userName = ""
    var userImage = ""
    private var vib: Vibrator? = null

    companion object {
        var ringtone: Ringtone? = null
    }


    @RequiresApi(Build.VERSION_CODES.M)
    override fun onMessageReceived(p0: RemoteMessage) {
        super.onMessageReceived(p0)
        if (p0.data.size > 0) {
            try {
                var mTempData = p0.data.get("notificationData")
                var mNewData = mTempData!!.split("=")
                val mJsonObject: JSONObject = JSONObject(mNewData[0])
                Log.e(TAG, "**Notifications Data**" + mJsonObject)
                if (!mJsonObject.isNull("notification_type")) {
                    mType = mJsonObject.getString("notification_type")
                }
                if (!mJsonObject.isNull("title")) {
                    mTitle = mJsonObject.getString("title")
                }
                if (!mJsonObject.isNull("channelName")) {
                    channelName = mJsonObject.getString("channelName")
                }
                if (!mJsonObject.isNull("token")) {
                    callToken = mJsonObject.getString("token")
                }
                if (!mJsonObject.isNull("appId")) {
                    agoraAppId = mJsonObject.getString("appId")
                }
                if (!mJsonObject.isNull("description")) {
                    message = mJsonObject.getString("description")
                }
                if (!mJsonObject.isNull("notificationType")) {
                    notificationType = mJsonObject.getString("notificationType")
                }
                if (!mJsonObject.isNull("roomId")) {
                    mRoomID = mJsonObject.getString("roomId")
                }
                if (!mJsonObject.isNull("advisorName")) {
                    mAdvisorName = mJsonObject.getString("advisorName")
                }
                if (!mJsonObject.isNull("user_name")) {
                    userName = mJsonObject.getString("user_name")
                }
                if (!mJsonObject.isNull("totalTime")) {
                    totalTime = mJsonObject.getString("totalTime")
                }
                if (!mJsonObject.isNull("totalAmount")) {
                    totalAmount = mJsonObject.getString("totalAmount")
                }
                if (!mJsonObject.isNull("advisorId")) {
                    advisorId = mJsonObject.getString("advisorId")
                }
                if (!mJsonObject.isNull("profileImage")) {
                    userImage = mJsonObject.getString("profileImage")
                }
                if (!mJsonObject.isNull("timeAdded")) {
                    timeAddedCall = mJsonObject.getString("timeAdded")
                }
            } catch (e: Exception) {
                Log.e(TAG, "onMessageReceived: $e")
            }
        }
        if (notificationType.equals("6")) {    //Incoming call Request
            NotificationGenerator.customBigNotification(
                applicationContext,
                advisorId,
                mRoomID,
                mAdvisorName,
                agoraAppId,
                channelName,
                callToken,
                userImage,
                totalTime,
                userName
            )
            setAlartWithRingtone()
        } else {
            sendNotification(mTitle, message)
        }

    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun sendNotification(mTitle: String, message: String) {
        var intent: Intent? = null
        var pendingIntent: PendingIntent? = null
        if (notificationType.equals("3")) {         //Session start by user
            intent = Intent(this, ChatActivity::class.java)
            intent.putExtra(NOTIFY_TYPE, "advisor")
            intent.putExtra(CHAT_TIME, totalTime)
            intent.putExtra(PRICE, totalAmount)
            intent.putExtra(ADVISOR_ID, advisorId)
            intent.putExtra(PLAN_ID, "")
            intent.putExtra(ROOM_ID, mRoomID)
            intent.putExtra(PER_MIN_COST, "2")
            intent.putExtra(USER_NAME, userName)
            pendingIntent = PendingIntent.getActivity(
                this, 0, intent,
                PendingIntent.FLAG_ONE_SHOT
            )
        }

        if (notificationType.equals("5")) {         //session request accept by advisor
            intent = Intent(this, com.letmelisten.app.activities.ChatActivity::class.java)
            intent.putExtra(NOTIFY_TYPE, "user")
            intent.putExtra(CHAT_TIME, totalTime)
            intent.putExtra(PRICE, totalAmount)
            intent.putExtra(ADVISOR_ID, advisorId)
            intent.putExtra(PLAN_ID, "")
            intent.putExtra(ROOM_ID, mRoomID)
            intent.putExtra(PER_MIN_COST, "2")
            intent.putExtra(USER_NAME, userName)
            intent.putExtra(ADVISOR_NAME, mAdvisorName)
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            pendingIntent = PendingIntent.getActivity(
                this, 0, intent,
                PendingIntent.FLAG_ONE_SHOT
            )
            pendingIntent.send()
            FIRST_TIME = false
        }


        if (notificationType.equals("7")) { //Answerd Call
            // when call answered
            val intent = Intent()
            intent.action = getString(R.string.incoming_call_receiver)
            intent.putExtra("call_type", "answered_video_call")
            sendBroadcast(intent)


        }

        if (notificationType.equals("8")) { //Call End
            // when call disconnect
            cancelNotification(applicationContext)
            val intent = Intent()
            intent.action = getString(R.string.incoming_call_receiver)
            intent.putExtra("call_type", "dismiss_video_call")
            sendBroadcast(intent)

        }

        if (notificationType.equals("9")) { //Add More Time
            val intent = Intent()
            intent.action = getString(R.string.incoming_call_receiver)
            intent.putExtra("call_type", "add_more_time")
            intent.putExtra("added_time", timeAddedCall)
            sendBroadcast(intent)

        }

        if (notificationType.equals("10")) { //Missed call

            cancelNotification(applicationContext)
            if (ringtone != null) {
                ringtone!!.stop()
            }

            // when call disconnect
            val intent = Intent()
            intent.action = getString(R.string.incoming_call_receiver)
            intent.putExtra("call_type", "dismiss_video_call")
            sendBroadcast(intent)

        }

        val channelId = getString(R.string.default_notification_channel_id)
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder: NotificationCompat.Builder =
            NotificationCompat.Builder(this, channelId)
                .setSmallIcon(R.drawable.ic_notify_app)
                .setColor(getResources().getColor(R.color.colorGreen))
                .setContentTitle(mTitle)
                .setContentText(this.message)
                .setAutoCancel(true)
                .setPriority(Notification.PRIORITY_HIGH)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent)
        val notificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            if (notificationManager != null) {
                val channel = NotificationChannel(
                    channelId, getString(R.string.default_notification_channel_name),
                    NotificationManager.IMPORTANCE_HIGH
                )
                channel.enableVibration(true)
                notificationManager.createNotificationChannel(channel)

            }
        }
        if (notificationManager != null) {
            notificationManager.notify(
                NOTIFICATION_ID,
                notificationBuilder.build()
            )
        }
    }

    //    /*
//    * Alarm with Ringtone
//    * */
    private fun setAlartWithRingtone() {
        val uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE)
        ringtone = RingtoneManager.getRingtone(applicationContext, uri)
        //  ringtone.setLooping(true);
        vib = getSystemService(VIBRATOR_SERVICE) as Vibrator
        vib!!.vibrate(500)
        ringtone!!.play()
    }

    fun cancelNotification(p0: Context?) {
        if (p0 != null) {
            nm = p0.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        }

      //  nm?.cancel(NotificationGenerator.NOTIFICATION_ID_CUSTOM_BIG) // Notification ID to cancel
       nm?.cancelAll()
    }
}