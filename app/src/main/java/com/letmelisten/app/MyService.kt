package com.letmelisten.app

import android.app.Service
import android.content.Intent
import android.os.IBinder
import android.util.Log
import android.widget.Toast
import androidx.localbroadcastmanager.content.LocalBroadcastManager


class MyService : Service() {
    private var mRunning = false
    override fun onBind(p0: Intent?): IBinder? {
        Toast.makeText(applicationContext,"runnung",Toast.LENGTH_LONG)
        return null
    }

    override fun onCreate() {
        super.onCreate()
        mRunning = false;
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        return super.onStartCommand(intent, flags, startId)
        if (!mRunning) {
            mRunning = true;
            Log.e("TAG","VALUE::"+"rumminh")
        }
        return super.onStartCommand(intent, flags, startId);
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    override fun onTaskRemoved(rootIntent: Intent?) {
        //Toast.makeText(applicationContext,"runnung",Toast.LENGTH_LONG);
        val intent = Intent("changeImage")
        intent.putExtra("Status", "editImage")
        LocalBroadcastManager.getInstance(applicationContext).sendBroadcast(intent)
        //stopSelf()
    }

}