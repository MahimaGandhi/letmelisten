package com.letmelisten.app.Interfaces

import com.letmelisten.app.Model.AdvisorList
import com.letmelisten.app.Model.DataX

interface AdvisorListPaginationInterface {
    public fun onLoadMoreData(mModel : AdvisorList)
}