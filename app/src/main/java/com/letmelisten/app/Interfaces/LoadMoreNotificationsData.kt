package com.letmelisten.app.Interfaces

import com.letmelisten.app.Model.NotificationDataNew

interface LoadMoreNotificationsData {
    public fun onLoadMoreData(mModel : NotificationDataNew)
}