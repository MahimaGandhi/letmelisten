package com.letmelisten.app.Interfaces

import com.letmelisten.app.Model.Data

interface LoadMoreData {
    public fun onLoadMoreData(mModel : Data)
}