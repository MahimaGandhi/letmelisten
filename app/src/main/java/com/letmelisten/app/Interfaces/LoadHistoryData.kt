package com.letmelisten.app.Interfaces
import com.letmelisten.app.Model.DataX

interface LoadHistoryData {
    public fun onLoadMoreData(mModel : DataX)
}