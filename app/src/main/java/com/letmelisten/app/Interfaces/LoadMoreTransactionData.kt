package com.letmelisten.app.Interfaces

import com.letmelisten.app.Model.DataItemNew
import com.letmelisten.app.Model.NotificationDataNew

public interface LoadMoreTransactionData
{
    public fun onLoadMoreData(mModel : DataItemNew?)

}