package  com.kloxus.app.retrofit

import com.letmelisten.app.Model.*
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*

interface ApiInterface {

    @POST("signUp.php")
    fun signUpRequest(
        @Body mParams: Map<String?, String?>?
    ): Call<StatusMsgModel>

    @POST("login_v2.php")
    fun signInRequest(
        @Body mParams: Map<String?, String?>?
    ): Call<LoginModel>


    @POST("forgetPassword.php")
    fun forgotPasswordRequest(
        @Body mParams: Map<String?, String?>?
    ): Call<StatusMsgModel>


    @POST("VoipPush.php")
    fun callingRequest(
        @Body mParams: Map<String?, String?>?, @HeaderMap headers: Map<String, String>
    ): Call<CallingModel>

    @POST("badgeCount.php")
    fun badgeRequest(
        @Body mParams: Map<String?, String?>?,  @HeaderMap headers: Map<String, String>
    ): Call<BadgeCountModel>

//

    @POST("facebookLogin_v2.php")
    fun signInWithFbRequest(
        @Body mParams: Map<String?, String?>?
    ): Call<LoginModel>

    @POST("googleLogin_v2.php")
    fun loginWithGoogleRequest(
        @Body mParams: Map<String?, String?>?
    ): Call<LoginModel>

    @POST("logout.php")
    fun logoutRequest(
        @HeaderMap headers: Map<String, String>
    ): Call<StatusMsgModel>


    @POST("statusUpdate.php")
    fun statusRequest(
        @Body mParams: Map<String?, String?>?, @HeaderMap headers: Map<String, String>
    ): Call<StatusUpdateModel>

    @POST("home.php")
    fun homeRequest(
        @Body mParams: Map<String?, String?>?, @HeaderMap headers: Map<String, String>
    ): Call<HomeModel>

    @POST("changePassword.php")
    fun changePasswordRequest(
        @Body mParams: Map<String?, String?>?, @HeaderMap headers: Map<String, String>
    ): Call<StatusMsgModel>

    @POST("favouriteUnfavourite.php")
    fun favUnfavRequest(
        @Body mParams: Map<String?, String?>?, @HeaderMap headers: Map<String, String>
    ): Call<FavUnfavModel>


    @POST("favouriteListing.php")
    fun favListRequest(
        @Body mParams: Map<String?, String?>?, @HeaderMap headers: Map<String, String>
    ): Call<FavoriteListModel>

    @POST("getProfileDetails.php")
    fun profileDetailRequest(
        @HeaderMap headers: Map<String, String>
    ): Call<ProfileDetailModel>


    @POST("sessionsHistory.php")
    fun sessionHistoryListRequest(
        @Body mParams: Map<String?, String?>?, @HeaderMap headers: Map<String, String>
    ): Call<SessionHistoryModel>

    @POST("getAdvisorDetails.php")
    fun advisorDetailsRequest(
        @Body mParams: Map<String?, String?>?, @HeaderMap headers: Map<String, String>
    ): Call<AdvisorDetailsModel>

    @POST("session.php")
    fun createSessionRequest(
        @Body mParams: Map<String?, String?>?, @HeaderMap headers: Map<String, String>
    ): Call<SessionCreateModel>

    @POST("rating.php")
    fun ratingRequest(
        @Body mParams: Map<String?, String?>?, @HeaderMap headers: Map<String, String>
    ): Call<StatusMsgModel>

    @POST("notificationListing.php")
    fun notificationsRequest(
        @Body mParams: Map<String?, String?>?, @HeaderMap headers: Map<String, String>
    ): Call<NotificationModelNew>


    @POST("addMoney.php")
    fun addMoneyRequest(
        @Body mParams: Map<String?, String?>?, @HeaderMap headers: Map<String, String>
    ): Call<StripeMoneyWalletModel>


    @POST("session.php")
    fun sessionRequest(
        @Body mParams: Map<String?, String?>?, @HeaderMap headers: Map<String, String>
    ): Call<SessionCreateModel>

    @POST("addTime.php")
    fun addTimeRequest(
        @Body mParams: Map<String?, String?>?, @HeaderMap headers: Map<String, String>
    ): Call<AddMoreTimeModel>


    @POST("getAllChatMessages.php")
    fun chatMessageRequest(
        @Body mParams: Map<String?, String?>?, @HeaderMap headers: Map<String, String>
    ): Call<ChatMessagesModel>

    @POST("sendMsg.php")
    fun sendMessageRequest(
        @Body mParams: Map<String?, String?>?, @HeaderMap headers: Map<String, String>
    ): Call<SendMsgModel>

    @POST("sessionListing.php")
    fun sessionListRequest(
        @Body mParams: Map<String?, String?>?, @HeaderMap headers: Map<String, String>
    ): Call<AdvisorSessionListModel>

    @POST("categoryListing.php")
    fun categoryListRequest(
        @HeaderMap headers: Map<String, String>
    ): Call<CategoryListModel>


    @POST("endSession.php")
    fun endSessionRequest(
        @Body mParams: Map<String?, String?>?, @HeaderMap headers: Map<String, String>
    ): Call<EndSessionModel>


    @POST("endSessionVoip.php")
    fun endSessionCallRequest(
        @Body mParams: Map<String?, String?>?, @HeaderMap headers: Map<String, String>
    ): Call<EndSessionCallModel>

    @POST("addTimeVoip.php")
    fun addMoreTimeRequest(
        @Body mParams: Map<String?, String?>?, @HeaderMap headers: Map<String, String>
    ): Call<AddMoreTimeCallModel>


    @POST("answerCall.php")
    fun answerCallRequest(
        @Body mParams: Map<String?, String?>?, @HeaderMap headers: Map<String, String>
    ): Call<EndSessionCallModel>

    @POST("transactionsHistory.php")
    fun transactionHistoryRequest(
        @Body mParams: Map<String?, String?>?, @HeaderMap headers: Map<String, String>
    ): Call<TransactionHistoryModel>

    @Multipart
    @POST("editProfile.php")
    fun editProfileRequest(
        @HeaderMap headers: Map<String, String>,
        @Part("firstName") firstName: RequestBody?,
        @Part("lastName") lastName: RequestBody?,
        @Part("phoneNo") phoneNo: RequestBody?,
        @Part profileImage: MultipartBody.Part?,
    ): Call<NewEditProfileModel?>?


    @Multipart
    @POST("advisorEdit.php")
    fun editProfileRequest(
        @HeaderMap headers: Map<String, String>,
        @Part("advisorName") advisorName: RequestBody?,
        @Part("advisorPhoneno") advisorPhoneNo: RequestBody?,
        @Part("advisorDescription") advisorDescription: RequestBody?,
        @Part("categoryId") categoryId: RequestBody?,
        @Part advisorImage: MultipartBody.Part?,
    ): Call<NewEditProfileModel?>?

    @Multipart
    @POST("PaypalLogin.php")
    fun connectPaypalRequest(
        @HeaderMap headers: Map<String, String>,
        @Part("userId") userId: RequestBody?
    ): Call<PaypalUrlModel?>?

    @Multipart
    @POST("getAccessTokenForPaypal.php")
    fun PaypalTokenRequest(
        @HeaderMap headers: Map<String, String>,
        @Part("userId") userId: RequestBody?
    ): Call<AccessTokenPaypalModel?>?



    @Multipart
    @POST("getPaypalTransactionDetails.php")
    fun PaypalTransactionRequest(
        @HeaderMap headers: Map<String, String>,
        @Part("accessToken") firstName: RequestBody?,
        @Part("payID") email: RequestBody?
    ): Call<PaypalTransactionModel?>?



    @Multipart
    @POST("advisorReq.php")
    fun signupAsAdvisorRequest(
        @HeaderMap headers: Map<String, String>,
        @Part("advisorName") firstName: RequestBody?,
        @Part("advisorEmail") email: RequestBody?,
        @Part("advisorDescription") lastName: RequestBody?,
        @Part("advisorPhoneno") phoneNo: RequestBody?,
        @Part("categoryId") categyId: RequestBody?,
        @Part document: MultipartBody.Part?,
        @Part advisorImage: MultipartBody.Part?
    ): Call<SignupAsAdvisor?>?

}