package com.letmelisten.app.adapters.advisor

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.letmelisten.app.Interfaces.LoadMoreTransactionData
import com.letmelisten.app.Model.DataItemNew
import com.letmelisten.app.R
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class EarningsAdapter(
    mActivity: Activity,
    var mTransactionAL: ArrayList<DataItemNew?>?,
   var  mLoadMore: LoadMoreTransactionData?

) :
    RecyclerView.Adapter<EarningsAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view: View = layoutInflater.inflate(R.layout.item_earnings, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var mModel: DataItemNew? = mTransactionAL?.get(position)
        holder.nameEarningsTV.setText(mModel!!.firstName+" "+mModel.lastName)
        holder.amountEarningsTV.setText("$"+mModel.amount)
        holder.dateEarningsTV.setText(mModel.amount)


        if (mTransactionAL!!.size % 10 == 0 && mTransactionAL!!.size - 1 == position) {
            mLoadMore?.onLoadMoreData(mModel)
        }



        if (mModel.transactionTime!= null && !mModel.transactionTime.equals("")) {
            holder.dateEarningsTV.text = (gettingLongToFormatedDay(mModel.transactionTime!!.toLong())+" at "+gettingLongToFormatedTime(
                mModel.transactionTime!!.toLong())+" "+gettingLongToFormatedDate(mModel.transactionTime!!.toLong()))

        }

//            holder.dateEarningsTV.setText(gettingLongToFormatedTime(mModel.transactionTime!!.toLong()))
//
//            var  thisDate:String = mModel.transactionTime!!
//            var  nextDate:String  =  ""
//            if (position == mTransactionAL!!.size - 1) {
//                nextDate = thisDate;
//            } else {
//                nextDate = mTransactionAL!!.get(position + 1)?.transactionTime.toString()
//            }
//            // enable section heading if it's the first one, or
//            // different from the previous one
//            if (nextDate.equals(thisDate)) {
//                if (position == mTransactionAL!!.size - 1) {
//                    holder.dateEarningsTV.setVisibility(View.VISIBLE);
//                } else {
//                    holder.dateEarningsTV.setVisibility(View.GONE);
//                }
//            } else {
//                holder.dateEarningsTV.setVisibility(View.VISIBLE);
//            }
//        }
    }
    override fun getItemCount(): Int {
        return mTransactionAL!!.size
    }
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var nameEarningsTV: TextView
        var msgEarningsTV: TextView
        var amountEarningsTV: TextView
        var dateEarningsTV: TextView
        init {
            nameEarningsTV = itemView.findViewById<View>(R.id.nameEarningsTV) as TextView
            msgEarningsTV = itemView.findViewById<View>(R.id.msgEarningsTV) as TextView
            amountEarningsTV = itemView.findViewById<View>(R.id.amountEarningsTV) as TextView
            dateEarningsTV = itemView.findViewById<View>(R.id.dateEarningsTV) as TextView
        }
    }
    public fun gettingLongToFormatedTime(strDateTime: Long): String? {
        //convert seconds to milliseconds
        val date = Date(strDateTime * 1000)
        // format of the date
        val jdf = SimpleDateFormat("HH:mm aa")
        return jdf.format(date)
    }

    fun gettingLongToFormatedDay(strDateTime: Long): String? {
        //convert seconds to milliseconds
        val date = Date(strDateTime * 1000)
        // format of the date
        val jdf = SimpleDateFormat("EEEE")
        return jdf.format(date)
    }
    fun gettingLongToFormatedDate(strDateTime: Long): String? {
        //convert seconds to milliseconds
        val date = Date(strDateTime * 1000)
        // format of the date
        val jdf = SimpleDateFormat("dd MMM yyyy")
        return jdf.format(date)
    }

}