package com.letmelisten.app.adapters.advisor

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.letmelisten.app.fragments.advisor.AllFragment
import com.letmelisten.app.fragments.advisor.ConfirmedFragment
import com.letmelisten.app.fragments.advisor.PendingFragment

class HomePagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm!! ,
    BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
    override fun getCount(): Int {
        return 3;
    }



//    override fun getPageTitle(position: Int): CharSequence = pFragmentTitle[position]
//
//    fun addFragment1(fm: AllFragment, title: String) {
//        pFragmentList.add(fm)
//        pFragmentTitle.add(title)
//    }
//
//    fun addFragment2(fm: PendingFragment, title: String) {
//        pFragmentList.add(fm)
//        pFragmentTitle.add(title)
//    }
//
//    fun addFragment3(fm: ConfirmedFragment, title: String) {
//        pFragmentList.add(fm)
//        pFragmentTitle.add(title)
//    }

    override fun getPageTitle(position: Int): CharSequence? {
        when(position) {
            0 -> {
                return "All"
            }
            1 -> {
                return "Pending"
            }
            2 -> {
                return "Confirmed"
            }
        }
        return super.getPageTitle(position)
    }


    override fun getItem(position: Int): Fragment {
        // For a given position, return two different potential fragments based on a condition
         var fragment: Fragment? = null
         if (position == 0) {
             fragment = AllFragment()
         } else if (position == 1) {
             fragment = PendingFragment()
         } else if (position == 2) {
             fragment = ConfirmedFragment()
         }
         return fragment!!
    }

    override fun getItemPosition(`object`: Any): Int {
        return super.getItemPosition(`object`)
        return if (`object` is Fragment) {
            POSITION_UNCHANGED // don't force a reload
        } else {
            // POSITION_NONE means something like: this fragment is no longer valid
            // triggering the ViewPager to re-build the instance of this fragment.
            POSITION_NONE
        }
    }

    /*// Force a refresh of the page when a different fragment is displayed
    fun getItemPosition(`object`: Any?): Int {
        // this method will be called for every fragment in the ViewPager
        return if (`object` is SomePermanantCachedFragment) {
            POSITION_UNCHANGED // don't force a reload
        } else {
            // POSITION_NONE means something like: this fragment is no longer valid
            // triggering the ViewPager to re-build the instance of this fragment.
            POSITION_NONE
        }
    }*/
}


