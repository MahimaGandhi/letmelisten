package com.letmelisten.app.adapters

import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RatingBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.letmelisten.app.Interfaces.LoadMoreScrollListner
import com.letmelisten.app.Model.DataItemss
import com.letmelisten.app.R
import com.letmelisten.app.activities.UserDetailActivity


class HomeAdapter(
    val mActivity: Activity?,
    var mArrayList: ArrayList<DataItemss?>?,
    var onItemClickListener: OnItemClickListener? = null,
    var mLoadMoreScrollListner: LoadMoreScrollListner
) :
    RecyclerView.Adapter<HomeAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(mActivity)
        val view: View = layoutInflater.inflate(R.layout.item_home, parent, false)
        return ViewHolder(view)
    }


    //    MuteInterface muteInterface;
    interface OnItemClickListener {
        fun onItemClick(positon: Int, id: String)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var mModel: DataItemss? = mArrayList!!.get(position)
        // holder.txtNameTV.text = mModel?.firstName + "" + mModel!!.lastName
        holder.txtNameTV.text = mModel!!.advisorName
        holder.txtCaptionTV.text = mModel!!.advisorDescription
//        if (mModel.totalReviews > 0) {
//            holder.txtReviewsTV.text =
//                "(" + mModel.totalReviews.toString() + " " + mActivity!!.getString(R.string.reviews) + ")"
//        } else {
//            holder.txtReviewsTV.isVisible = false
//        }
        if (mModel.advisorPrice != "") {
            holder.txtPriceTV.text = "$" + mModel.advisorPrice
        }
        if (mModel.rating != null) {
            holder.ratingbarRB.rating = mModel.rating!!.toFloat()
        }
        if (mModel.isFav.equals("1")) {
            holder.imageFavIconIV.setImageResource(R.drawable.ic_heart_selected)
        } else {
            holder.imageFavIconIV.setImageResource(R.drawable.fav)
        }

        if (mActivity != null) {
            Glide.with(mActivity)
                .load(mModel.advisorImage)
                .placeholder(R.drawable.ic_home_dummy)
                .error(R.drawable.ic_home_dummy)
                .into(holder.imageHomeIV)
        }

        if (mArrayList!!.size % 10 == 0 && mArrayList!!.size - 1 == position) {
            mLoadMoreScrollListner.onLoadMoreListner(mModel)
        }

        holder.imageFavIconIV.setOnClickListener({
            mModel.userId?.let { it1 -> onItemClickListener!!.onItemClick(position, it1) }
        })

        holder.homeLL.setOnClickListener {
            val context = holder.homeLL.context
            val intent = Intent(context, UserDetailActivity::class.java)
            intent.putExtra("advisorId", mModel.userId)
            context.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        return mArrayList!!.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var txtNameTV: TextView
        var txtPriceTV: TextView
        var txtCaptionTV: TextView
        var txtReviewsTV: TextView
        var imageHomeIV: ImageView
        var imageFavIconIV: ImageView
        var homeLL: LinearLayout
        var ratingbarRB: RatingBar


        init {
            imageHomeIV = itemView.findViewById<View>(R.id.imageHomeIV) as ImageView
            txtCaptionTV = itemView.findViewById<View>(R.id.txtCaptionTV) as TextView
            txtNameTV = itemView.findViewById<View>(R.id.txtNameTV) as TextView
            txtPriceTV = itemView.findViewById<View>(R.id.txtPriceTV) as TextView
            txtReviewsTV = itemView.findViewById<View>(R.id.txtReviewsTV) as TextView
            imageFavIconIV = itemView.findViewById<View>(R.id.imageFavIconIV) as ImageView
            homeLL = itemView.findViewById<View>(R.id.homeLL) as LinearLayout
            ratingbarRB = itemView.findViewById<View>(R.id.ratingbarRB) as RatingBar

        }
    }

}
