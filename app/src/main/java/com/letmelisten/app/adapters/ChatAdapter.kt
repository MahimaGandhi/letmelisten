package com.letmelisten.app.adapters

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.letmelisten.app.Model.DataItem
import com.letmelisten.app.R
import com.letmelisten.app.utils.LEFT_VIEW_HOLDER
import com.letmelisten.app.utils.RIGHT_VIEW_HOLDER
import java.text.SimpleDateFormat
import java.util.*

class ChatAdapter(val mActivity: Activity?, var mArrayList: ArrayList<DataItem?>?) :
    RecyclerView.Adapter<ChatAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view: View = layoutInflater.inflate(R.layout.item_chat, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (holder.itemViewType == RIGHT_VIEW_HOLDER) {
            holder.senderRL.visibility = View.VISIBLE
            holder.receiverRL.visibility = View.GONE
            holder.senderTV.text = mArrayList?.get(position)?.message
            holder.senderTimeTV.text = unixToDate(mArrayList?.get(position)?.created!!.toLong())
            mActivity?.let {
                Glide.with(it)
                    .load(mArrayList!!.get(position)!!.profileImage)
                    .placeholder(R.drawable.ph)
                    .into(holder.senderIV)
            }
        } else if (holder.itemViewType == LEFT_VIEW_HOLDER) {
            holder.senderRL.visibility = View.GONE
            holder.receiverRL.visibility = View.VISIBLE
            holder.receiverTV.text = mArrayList?.get(position)?.message
            holder.receiverTimeTV.text = unixToDate(mArrayList?.get(position)?.created!!.toLong())
            mActivity?.let {
                Glide.with(it)
                    .load(mArrayList!!.get(position)!!.profileImage)
                    .placeholder(R.drawable.ph)
                    .into(holder.receiverIV)
            }
        }

    }

    override fun getItemCount(): Int {
        return mArrayList?.size!!
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var senderTimeTV: TextView
        var receiverTimeTV: TextView
        var receiverIV: ImageView
        var senderTV: TextView
        var receiverTV: TextView
        var receiverNameTV: TextView
        var senderRL: RelativeLayout
        var receiverRL: RelativeLayout
        var senderIV: ImageView


        init {
            receiverIV = itemView.findViewById<View>(R.id.receiverIV) as ImageView
            senderTimeTV = itemView.findViewById<View>(R.id.senderTimeTV) as TextView
            receiverTimeTV = itemView.findViewById<View>(R.id.receiverTimeTV) as TextView
            senderTV = itemView.findViewById<View>(R.id.senderTV) as TextView
            receiverTV = itemView.findViewById<View>(R.id.receiverTV) as TextView
            receiverNameTV = itemView.findViewById<View>(R.id.receiverNameTV) as TextView
            senderRL = itemView.findViewById<View>(R.id.senderRL) as RelativeLayout
            receiverRL = itemView.findViewById<View>(R.id.receiverRL) as RelativeLayout
            senderIV = itemView.findViewById<View>(R.id.senderIV) as ImageView


        }
    }

    override fun getItemViewType(position: Int): Int {
        if (mArrayList!![position]!!.viewType == 1) return RIGHT_VIEW_HOLDER
        else if (mArrayList!![position]!!.viewType == 0) return LEFT_VIEW_HOLDER
        return -1
    }

    fun unixToDate(timeStamp: Long): String? {
        val time = java.util.Date(timeStamp as Long * 1000)
        val sdf = SimpleDateFormat("dd MMM yyyy, hh:mm a")
        val sdfDate = SimpleDateFormat("dd MMM yyyy")
        val sdfTime = SimpleDateFormat("hh:mm a")
        val currentDate: String =
            SimpleDateFormat("dd MMM yyyy", Locale.getDefault()).format(Date())
//        if (sdfDate.format(time).equals(currentDate)) {
//            return sdfTime.format(time)
//
//        } else {
            return sdfTime.format(time)

       // }
    }

}