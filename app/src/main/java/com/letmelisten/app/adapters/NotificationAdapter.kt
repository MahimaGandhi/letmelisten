package com.letmelisten.app.adapters

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.letmelisten.app.Interfaces.LoadMoreNotificationsData
import com.letmelisten.app.Model.Data
import com.letmelisten.app.Model.NotificationDataNew
import com.letmelisten.app.R
import de.hdodenhof.circleimageview.CircleImageView
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class NotificationAdapter(
    val mActivity: Activity?,
    var mArrayList: ArrayList<NotificationDataNew>?,
    var mLoadMoreScrollListner: LoadMoreNotificationsData? = null
) :
    RecyclerView.Adapter<NotificationAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view: View = layoutInflater.inflate(R.layout.item_notification, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.durationLL.visibility = View.GONE
        var mModel: NotificationDataNew = mArrayList!!.get(position)
        holder.txtMsgTV.text = mModel.description
        if (!mModel.created.equals("")) {
            holder.txtTimeTV.text =
                (gettingLongToFormatedDay(mModel.created.toLong()) + " at " + gettingLongToFormatedTime(
                    mModel.created.toLong()
                ) + " " + gettingLongToFormatedDate(mModel.created.toLong()))
        }

        if (mActivity != null) {
            Glide.with(mActivity)
                .load(mModel.profileImage)
                .placeholder(R.drawable.ph)
                .error(R.drawable.ph)
                .into(holder.imageNotificationIV)
        }

        if (mArrayList!!.size % 10 == 0 && mArrayList!!.size - 1 == position) {
            mLoadMoreScrollListner?.onLoadMoreData(mModel)
        }
        if (mModel.notificationType.equals("8")) {
            holder.durationLL.visibility = View.VISIBLE
            holder.durationTV.setText(mModel.duration)
        }

        holder.mainRL.setOnClickListener({
//            onItemClickListener!!.onItemmClick(position, mModel.userId)
        })
    }

    override fun getItemCount(): Int {
        return mArrayList!!.size
    }


    fun gettingLongToFormatedDate(strDateTime: Long): String? {
        //convert seconds to milliseconds
        val date = Date(strDateTime * 1000)
        // format of the date
        val jdf = SimpleDateFormat("dd MMM yyyy")
        return jdf.format(date)
    }

    fun gettingLongToFormatedDay(strDateTime: Long): String? {
        //convert seconds to milliseconds
        val date = Date(strDateTime * 1000)
        // format of the date
        val jdf = SimpleDateFormat("EEEE")
        return jdf.format(date)
    }

    fun gettingLongToFormatedTime(strDateTime: Long): String? {
        //convert seconds to milliseconds
        val date = Date(strDateTime * 1000)
        // format of the date
        val jdf = SimpleDateFormat("HH:mm aa")
        return jdf.format(date)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var txtMsgTV: TextView
        var txtTimeTV: TextView
        var durationTV: TextView
        var imageNotificationIV: CircleImageView
        var mainRL: RelativeLayout
        var durationLL: LinearLayout


        init {
            imageNotificationIV =
                itemView.findViewById<View>(R.id.imageNotificationIV) as CircleImageView
            txtTimeTV = itemView.findViewById<View>(R.id.txtTimeTV) as TextView
            txtMsgTV = itemView.findViewById<View>(R.id.txtMsgTV) as TextView
            mainRL = itemView.findViewById<View>(R.id.mainRL) as RelativeLayout
            durationTV = itemView.findViewById<View>(R.id.durationTV) as TextView
            durationLL = itemView.findViewById<View>(R.id.durationLL) as LinearLayout
        }
    }

}
