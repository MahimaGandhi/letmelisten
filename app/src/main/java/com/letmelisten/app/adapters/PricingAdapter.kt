package com.letmelisten.app.adapters

import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import com.letmelisten.app.Model.Data
import com.letmelisten.app.Model.HomeData
import com.letmelisten.app.Model.PlanDetail
import com.letmelisten.app.R
import com.letmelisten.app.activities.SubscriptionPlanActivity
import com.letmelisten.app.activities.UserDetailActivity
import com.letmelisten.app.activities.WalletActivity


class PricingAdapter(val mActivity: Activity?,
                     var mArrayList: ArrayList<PlanDetail>?,
                     var onItemClickListener: PricingAdapter.OnItemClickListener? = null):
    RecyclerView.Adapter<PricingAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view: View = layoutInflater.inflate(R.layout.iten_pricing, parent, false)
        return ViewHolder(view)
    }


    //    MuteInterface muteInterface;
    interface OnItemClickListener {
        fun onItemClick(positon: Int, id: String, time: String, price: String)
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var mModel: PlanDetail = mArrayList!!.get(position)
        holder.txtTimeTV.text = mModel.minute + mActivity!!.getString(R.string.min)
     //   holder.txtCallingMsgTV.text = mModel.description
        holder.txtPriceTV.text = mActivity!!.getString(R.string.dollar) +mModel.price


        holder.typeServiceRL.setOnClickListener({
            onItemClickListener!!.onItemClick(position, mModel.planId,mModel.minute,mModel.price)
        })


//        holder.typeServiceRL.setOnClickListener {
//            val context = holder.typeServiceRL.context
//            val intent = Intent(context, WalletActivity::class.java)
//            intent.putExtra("plan_id",mModel.planId)
//            context.startActivity(intent)
//        }
    }

    override fun getItemCount(): Int {
        return mArrayList!!.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var txtCallingMsgTV: TextView
        var txtTimeTV: TextView
        var txtPriceTV: TextView
        var typeServiceRL: RelativeLayout


        init {
            txtPriceTV = itemView.findViewById<View>(R.id.txtPriceTV) as TextView
            txtTimeTV = itemView.findViewById<View>(R.id.txtTimeTV) as TextView
            txtCallingMsgTV = itemView.findViewById<View>(R.id.txtCallingMsgTV) as TextView
            typeServiceRL = itemView.findViewById<View>(R.id.typeServiceRL) as RelativeLayout


        }
    }

}
