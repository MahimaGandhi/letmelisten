package com.letmelisten.app.adapters.advisor

import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.letmelisten.app.Interfaces.AdvisorListPaginationInterface
import com.letmelisten.app.Model.AdvisorList
import com.letmelisten.app.R
import com.letmelisten.app.activities.advisor.ChatActivity
import com.letmelisten.app.utils.FRAGMENT_NAME
import com.letmelisten.app.utils.MODEL
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class AllAdapter(
    val activity: FragmentActivity?,
    var mAdvisorList: ArrayList<AdvisorList>,
    var mAdvisorListPaginationInterface: AdvisorListPaginationInterface,
    var adapterName: String
) :
    RecyclerView.Adapter<AllAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view: View = layoutInflater.inflate(R.layout.item_all, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var mModel: AdvisorList = mAdvisorList.get(position)
        activity?.let {
            Glide.with(it)
                .load(mModel.profileImage)
                .placeholder(R.drawable.ph)
                .into(holder.imgProfilePicIV)
        }

        if (adapterName.equals("completedFragment")) {
            holder.txtNameTV.text = "Chat ended with " + mModel!!.name
            holder.txtMsgTV.text = mModel.lastMessage
            holder.txtTimeTV.visibility = View.GONE
            holder.txtTimeTV.setText(gettingLongToFormatedTime(mModel.endTime!!.toLong()))
            Log.e("TAG", "**MSSG**" + mModel.lastMessage)
        } else {
            holder.txtNameTV.text = mModel!!.name
            holder.txtMsgTV.setText("Tap to Chat")
            holder.txtTimeTV.visibility = View.VISIBLE
            holder.txtTimeTV.setText(gettingLongToFormatedTime(mModel.startTime!!.toLong()))
        }

        holder.mainLL.setOnClickListener {
            val context = holder.mainLL.context
            val intent = Intent(context, ChatActivity::class.java)
            intent.putExtra(MODEL, mModel)
            intent.putExtra(FRAGMENT_NAME, adapterName)
            context.startActivity(intent)
        }

        if (mAdvisorList.size % 10 == 0 && mAdvisorList.size - 1 == position) {
            mAdvisorListPaginationInterface.onLoadMoreData(mModel)
        }

    }

    override fun getItemCount(): Int {
        return mAdvisorList.size
    }

    public fun gettingLongToFormatedTime(strDateTime: Long): String? {
        //convert seconds to milliseconds
        val date = Date(strDateTime * 1000)
        // format of the date
        val jdf = SimpleDateFormat("HH:mm aa")
        return jdf.format(date)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var imgProfilePicIV: ImageView
        var onlineIV: ImageView
        var txtNameTV: TextView
        var txtMsgTV: TextView
        var txtTimeTV: TextView
        var mainLL: LinearLayout

        init {
            imgProfilePicIV = itemView.findViewById<View>(R.id.imgProfilePicIV) as ImageView
            onlineIV = itemView.findViewById<View>(R.id.onlineIV) as ImageView
            txtNameTV = itemView.findViewById<View>(R.id.txtNameTV) as TextView
            txtMsgTV = itemView.findViewById<View>(R.id.txtMsgTV) as TextView
            txtTimeTV = itemView.findViewById<View>(R.id.txtTimeTV) as TextView
            mainLL = itemView.findViewById<View>(R.id.mainLL) as LinearLayout

        }
    }
}
