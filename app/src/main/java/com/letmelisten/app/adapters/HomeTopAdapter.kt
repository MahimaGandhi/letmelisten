package com.letmelisten.app.adapters

import android.app.Activity
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.letmelisten.app.Interfaces.HomeTopItemClickInterface
import com.letmelisten.app.Model.CategoriesItem
import com.letmelisten.app.Model.CategoriesItems
import com.letmelisten.app.Model.Category
import com.letmelisten.app.Model.HomeData
import com.letmelisten.app.R


class HomeTopAdapter(
    val mActivity: Activity?,
    var mArrayList: ArrayList<CategoriesItems?>?,
    var checkedPosition: Int,
    var onItemClickListener: HomeTopItemClickInterface? = null
) :
    RecyclerView.Adapter<HomeTopAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(mActivity)
        val view: View = layoutInflater.inflate(R.layout.item_home_top, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var mModel: CategoriesItems? = mArrayList!!.get(position)
        holder.txtTitleTV.text = mModel!!.name

        if (mModel.isSelected.equals(true)) {
            holder.txtTitleTV.setBackground(mActivity!!.getDrawable(R.drawable.bg_home_top))
            holder.txtTitleTV.setTextColor(mActivity!!.getResources().getColor(R.color.colorWhite))
        }
//        else {
//            holder.txtTitleTV.setBackground(mActivity!!.getDrawable(R.drawable.bg_home_top_unsel))
//            holder.txtTitleTV.setTextColor(mActivity!!.getResources().getColor(R.color.colorBlack))
//        }

        if (checkedPosition == position) {
            holder.txtTitleTV.setBackground(mActivity!!.getDrawable(R.drawable.bg_home_top))
            holder.txtTitleTV.setTextColor(mActivity!!.getResources().getColor(R.color.colorWhite))
        } else {
            holder.txtTitleTV.setBackground(mActivity!!.getDrawable(R.drawable.bg_home_top_unsel))
            holder.txtTitleTV.setTextColor(mActivity!!.getResources().getColor(R.color.colorBlack))
        }

        holder.txtTitleTV.setOnClickListener {
            Log.e("TAG", "ID::" + mModel.category_id)
            mModel.isSelected = false;
            checkedPosition = position
            notifyDataSetChanged();
//            mMealPlanSelection.getTabSelection(checkedPosition);

            mModel.category_id?.let { it1 -> onItemClickListener?.onItemmClick(position, it1) }
        }
    }

    override fun getItemCount(): Int {
        return mArrayList!!.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var txtTitleTV: TextView

        init {
            txtTitleTV = itemView.findViewById<View>(R.id.txtTitleTV) as TextView
        }
    }

}
