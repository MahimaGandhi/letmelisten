package com.letmelisten.app.adapters

import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.letmelisten.app.Interfaces.HomeTopItemClickInterface
import com.letmelisten.app.Interfaces.LoadMoreData
import com.letmelisten.app.Interfaces.LoadMoreScrollListner
import com.letmelisten.app.Model.Data
import com.letmelisten.app.Model.HomeData
import com.letmelisten.app.R
import com.letmelisten.app.activities.UserDetailActivity
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class FavouritesAdapter(val mActivity: Activity?,
                        var mArrayList: ArrayList<Data>?,
                        var onItemClickListener: HomeTopItemClickInterface? = null,
                        var mLoadMoreScrollListner: LoadMoreData?=null
) :
    RecyclerView.Adapter<FavouritesAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view: View = layoutInflater.inflate(R.layout.item_favourites, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var mModel: Data = mArrayList!!.get(position)
       // holder.txtNameTV.text = mModel.firstName + " " + mModel.lastName
        holder.txtNameTV.text = mModel.advisorName
        if (!mModel.created.equals("")){
            holder.txtDateTV.text = (gettingLongToFormatedTime(mModel.created.toLong())+" "+gettingLongToFormatedDate(mModel.created.toLong()))
        }

        if (mActivity != null) {
            Glide.with(mActivity)
                .load(mModel.advisorImage)
                .placeholder(R.drawable.ic_phh)
                .error(R.drawable.ic_phh)
                .into(holder.imageFavIV)
        }

        if (mArrayList!!.size % 10 == 0 && mArrayList!!.size - 1 == position) {
            mLoadMoreScrollListner?.onLoadMoreData(mModel)
        }

        holder.selFavIV.setOnClickListener({
            onItemClickListener!!.onItemmClick(position, mModel.userId)
        })

        holder.itemView.setOnClickListener({
            val intent = Intent(mActivity, UserDetailActivity::class.java)
            intent.putExtra("advisorId", mModel.userId)
            mActivity?.startActivity(intent)
        })



    }

    override fun getItemCount(): Int {
        return mArrayList!!.size
    }

    fun gettingLongToFormatedDate(strDateTime: Long): String? {
        //convert seconds to milliseconds
        val date = Date(strDateTime * 1000)
        // format of the date
        val jdf = SimpleDateFormat("dd MMM yyyy")
        return jdf.format(date)
    }

    fun gettingLongToFormatedTime(strDateTime: Long): String? {
        //convert seconds to milliseconds
        val date = Date(strDateTime * 1000)
        // format of the date
        val jdf = SimpleDateFormat("HH:mm aa")
        return jdf.format(date)
    }
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var txtNameTV: TextView
        var txtDateTV: TextView
        var imageFavIV: ImageView
        var selFavIV: ImageView


        init {
            imageFavIV = itemView.findViewById<View>(R.id.imageFavIV) as ImageView
            txtDateTV = itemView.findViewById<View>(R.id.txtDateTV) as TextView
            txtNameTV = itemView.findViewById<View>(R.id.txtNameTV) as TextView
            selFavIV = itemView.findViewById<View>(R.id.selFavIV) as ImageView


        }
    }

}
