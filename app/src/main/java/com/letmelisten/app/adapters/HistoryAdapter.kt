package com.letmelisten.app.adapters

import android.app.Activity
import android.opengl.Visibility
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.letmelisten.app.Interfaces.HomeTopItemClickInterface
import com.letmelisten.app.Interfaces.LoadHistoryData
import com.letmelisten.app.Model.Data
import com.letmelisten.app.Model.DataX
import com.letmelisten.app.R
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class HistoryAdapter(
    val mActivity: Activity?,
    var mArrayList: ArrayList<DataX>?,
    var onItemClickListener: HomeTopItemClickInterface? = null,
    var mLoadMoreScrollListner: LoadHistoryData? = null
) :
    RecyclerView.Adapter<HistoryAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view: View = layoutInflater.inflate(R.layout.item_history, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var mModel: DataX = mArrayList!!.get(position)
        //holder.historyNameTV.text = mModel.firstName + " " + mModel.lastName
        holder.historyNameTV.text = mModel.title
        holder.historyTimeTV.setText(" " + mModel.duration)
        if (!mModel.sessionEnd.equals("")){
            holder.txtTimeTV.text = (gettingLongToFormatedDay(mModel.sessionEnd.toLong())+" at "+gettingLongToFormatedTime(mModel.sessionEnd.toLong())+" "+gettingLongToFormatedDate(mModel.sessionEnd.toLong()))
        }
//        if (mModel.created != null && !mModel.created.equals("")) {
//            holder.historyTimeTV.text =
//                (gettingLongToFormatedTime(mModel.created.toLong()) + " " + gettingLongToFormatedDate(
//                    mModel.created.toLong()
//                ))
//
//        }
//        holder.historyStatusTV.text = mModel.advisorDescription
//        holder.historyStatusTV.isVisible = false

        if (mArrayList!!.size % 10 == 0 && mArrayList!!.size - 1 == position) {
            mLoadMoreScrollListner?.onLoadMoreData(mModel)
        }


    }

    override fun getItemCount(): Int {
        return mArrayList!!.size
    }

    fun gettingLongToFormatedDate(strDateTime: Long): String? {
        //convert seconds to milliseconds
        val date = Date(strDateTime * 1000)
        // format of the date
        val jdf = SimpleDateFormat("dd MMM yyyy")
        return jdf.format(date)
    }

    fun gettingLongToFormatedTime(strDateTime: Long): String? {
        //convert seconds to milliseconds
        val date = Date(strDateTime * 1000)
        // format of the date
        val jdf = SimpleDateFormat("HH:mm aa")
        return jdf.format(date)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var historyNameTV: TextView
        var txtTimeTV: TextView

        // var historyStatusTV: TextView
        var historyTimeTV: TextView

        init {
            historyNameTV = itemView.findViewById<View>(R.id.historyNameTV) as TextView
            //  historyStatusTV = itemView.findViewById<View>(R.id.historyStatusTV) as TextView
            historyTimeTV = itemView.findViewById<View>(R.id.historyTimeTV) as TextView
            txtTimeTV = itemView.findViewById<View>(R.id.txtTimeTV) as TextView


        }
    }
    fun gettingLongToFormatedDay(strDateTime: Long): String? {
        //convert seconds to milliseconds
        val date = Date(strDateTime * 1000)
        // format of the date
        val jdf = SimpleDateFormat("EEEE")
        return jdf.format(date)
    }
}
