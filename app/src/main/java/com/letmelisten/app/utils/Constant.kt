package com.letmelisten.app.utils


/*
* Base Server Url
* */
const val BASE_URL = "https://www.dharmani.com/letMeListen/webservices/"

/*
* Fragment Tags
* */
var FIRST_TIME: Boolean = true
const val HOME_TAG = "home_tag"
const val FAVOURITES_TAG = "favourites_tag"
const val NOTIFICATION_TAG = "notification_tag"
const val PROFILE_TAG = "profile_tag"
const val PUBLISHABLE_KEY = "pk_test_51JUS7iJ3XoLMRYVnKpR01bRRdE11rfnPrp5HJQrC6aUcRZTrY911Vvj5z3QWTACUjt55diQLeresblgaCv2qdGoO00u4MzXpuN"

/*
* Shared Preferences Keys
* */
const val USER_ID = "id"
const val ADV_ID = "Adv_id"
const val FIRST_NAME = "first_name"
const val CURRENT_TIME = "currentTime"
const val ADV_FIRST_NAME = "adv_first_name"
const val LAST_NAME = "last_name"
const val ADV_LAST_NAME = "adv_last_name"
const val IS_LOGIN = "is_login"
const val USER_EMAIL = "email"
const val Advisor_EMAIL = "advisorEmail"
const val Advisor_NAME = "advisorName"
const val ADV_EMAIL = "adv_email"
const val IS_ADVISOR = "is_advisor"
const val PHONE = "phone"
const val ADV_PHONE = "adv_phone"
const val PASSWORD = "password"
const val PROFILE_IMAGE = "profile_image"
const val ADV_PROFILE_IMAGE = "adv_profile_image"
const val TOKEN = "token"
const val ADV_REQ = "adv_req"
const val ADVISOR_PRICE = "ad_price"
const val FB_TOKEN = "fb_token"
const val ADV_FB_TOKEN = "adv_fb_token"
const val VERIFICATION_CODE = "veri_code"
const val ADV_VERIFICATION_CODE = "adv_veri_code"
const val WALLET_AMOUNT = "wallet_amount"
const val GOOGLE_ID = "google_id"
const val APP_STATUS = "app_status"
const val ADV_GOOGLE_ID = "adv_google_id"
const val CREATED = "created"
const val ADV_CREATED = "adv_created"
val DEVICE_TOKEN = "device_token"
val PAYPAL_ACCOUNT = "paypal_account"
val ADV_PAYPAL_ACCOUNT = "adv_paypal_account"
val SocketURL = "https://jaohar-uk.herokuapp.com"
const val ROOM_ID = "room_id"
const val ADVISOR_ID = "advisor_id"
const val PRICE = "price"
const val TRY_ROOM = "try_rrom"
const val PLAN_ID = "plan_id"
const val TIME = "time"
const val PER_MIN_COST = "time"
const val MODEL = "model"
const val CHAT_TIME = "chatTime"
const val CLICK_TYPE = "click_type"
const val NOTIFY_TYPE = "NOTIFYtIME"
const val USER_NAME = "userName"
const val FRAGMENT_NAME = "fragmentName"
const val AVALIABLE_STATUS = "status"
const val AVALIABLE_CALL = "call"
const val AVALIABLE_CHAT = "chat"
const val ADVISOR_CATEGORYID = "advisor_categoryId"
const val ADVISOR_DESCRIPTION = "advisor_description"
const val ADVISOR_PHONNO = "advisor_phneno"
const val ADVISOR_IMAGE = "advisor_image"
const val SWITCH_ACCOUNT = "switch_account"
const val ADVISOR_NAME = "advisor_name"
const val PAYPAL_URL = "paypalUrl"
const val SERVICE_TYPE = "service_type"
const val SEL_ADVISOR_IMAGE = "advisor_image"
const val SEL_ADVISOR_NAME = "advisor_name"
const val AGORA_APP_ID = "agora_app_id"
const val AGORA_CHANNEL_NAME = "agora_channel_name"
const val AGORA_TOKEN = "agora_token"
const val AGORA_USER_IMAGE = "agora_user_image"
const val NOTIFY_CLICK_TYPE = "notify_click_type"
const val TO_DELETE_ROOMID = "todeleteRoomid"
const val NOTIFY_INCOMING_TYPE = "notify_incoming_call"

/*
* Links Constants
* */
const val LINK_TYPE = "link_type"
const val LINK_ABOUT = "link_about"
const val LINK_PP = "link_pp"
const val LINK_TERMS = "link_terms"
const val LINK_HELP = "link_help"

const val ABOUT_WEB_LINK = "https://www.dharmani.com/letMeListen/aboutUs.html"
const val TERMS_WEB_LINK = "https://www.dharmani.com/letMeListen/TermsAndConditions.html"
const val PP_WEB_LINK = "https://www.dharmani.com/letMeListen/privacyPolicy.html"
const val HELP_WEB_LINK = "https://www.dharmani.com/letMeListen/help.html"


class Constant {





    var MONTHS_OF_YEAR = arrayOf(
        "0",
        "1",
        "2",
        "3",
        "4",
        "5",
        "6",
        "7",
        "8",
        "9",
        "10",
        "11"
    )


    object AppError {
        const val NO_CONNECTION_ERROR = 3
    }

}


/*
* View Holder
* */
const val LEFT_VIEW_HOLDER = 0
const val RIGHT_VIEW_HOLDER = 1
const val LAST_MSG = "last_msg"
const val ADAPTER_NAME = "adapter_name"

