package com.letmelisten.app.viewmodels

import android.app.Activity
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.kloxus.app.retrofit.ApiInterface
import com.letmelisten.app.Model.*
import com.letmelisten.app.activities.BaseActivity
import com.letmelisten.app.retrofit.ApiClient
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.http.Multipart

class ProfileEditProfileViewModel : ViewModel() {
    private var mEditProfileModel: MutableLiveData<NewEditProfileModel?>? = null

    fun editprofileData(
        activity: Activity?,
        headers: Map<String, String>,
        firstName: RequestBody,
        lastName: RequestBody,
        phoneNo: RequestBody,
        profileImage: MultipartBody.Part?,
    ): LiveData<NewEditProfileModel?>? {
        mEditProfileModel = MutableLiveData<NewEditProfileModel?>()
        val ApiInterface: ApiInterface =
            ApiClient().getApiClient()!!.create(ApiInterface::class.java)
        ApiInterface.editProfileRequest(
            headers,
            firstName,
            lastName,
            phoneNo,
            profileImage
        )
            ?.enqueue(object : Callback<NewEditProfileModel?> {
                override fun onResponse(
                    call: Call<NewEditProfileModel?>,
                    response: Response<NewEditProfileModel?>
                ) {
                    if (response.body() != null) {
                        mEditProfileModel!!.setValue(response.body())
                    } else {
                    }
                }

                override fun onFailure(call: Call<NewEditProfileModel?>, t: Throwable) {
                    BaseActivity().dismissProgressDialog()
                }
            })
        return mEditProfileModel
    }



    private var mEditAvisorProfileModel: MutableLiveData<NewEditProfileModel?>? = null

    fun editAdvisorprofileData(
        activity: Activity?,
        headers: Map<String, String>,
        advisorName: RequestBody,
        advisorPhoneno: RequestBody,
        advisorDescription: RequestBody,
        categoryId: RequestBody,
        advisorImage: MultipartBody.Part?,
    ): LiveData<NewEditProfileModel?>? {
        mEditAvisorProfileModel = MutableLiveData<NewEditProfileModel?>()
        val ApiInterface: ApiInterface =
            ApiClient().getApiClient()!!.create(ApiInterface::class.java)
        ApiInterface.editProfileRequest(
            headers,
            advisorName,
            advisorPhoneno,
            advisorDescription,
            categoryId,
            advisorImage
        )
            ?.enqueue(object : Callback<NewEditProfileModel?> {
                override fun onResponse(
                    call: Call<NewEditProfileModel?>,
                    response: Response<NewEditProfileModel?>
                ) {
                    if (response.body() != null) {
                        mEditAvisorProfileModel!!.setValue(response.body())
                    } else {
                    }
                }

                override fun onFailure(call: Call<NewEditProfileModel?>, t: Throwable) {
                    BaseActivity().dismissProgressDialog()
                }
            })
        return mEditAvisorProfileModel
    }






    private var mSignupAsAdModel: MutableLiveData<SignupAsAdvisor?>? = null
    fun signupAsAdvisorData(
        activity: Activity?,
        headers: Map<String, String>,
        firstName: RequestBody,
        lastName: RequestBody,
        phoneNo: RequestBody,
        email: RequestBody,
        categyId: RequestBody,
        document: MultipartBody.Part?,
        advisorImage: MultipartBody.Part?
    ): LiveData<SignupAsAdvisor?>? {
        mSignupAsAdModel = MutableLiveData<SignupAsAdvisor?>()
        val ApiInterface: ApiInterface =
            ApiClient().getApiClient()!!.create(ApiInterface::class.java)
        ApiInterface.signupAsAdvisorRequest(
            headers,
            firstName,
            lastName,
            phoneNo,
            email,
            categyId,
            document,
            advisorImage
        )
            ?.enqueue(object : Callback<SignupAsAdvisor?> {
                override fun onResponse(
                    call: Call<SignupAsAdvisor?>,
                    response: Response<SignupAsAdvisor?>
                ) {
                    if (response.body() != null) {
                        mSignupAsAdModel!!.setValue(response.body())
                    } else {
                    }
                }

                override fun onFailure(call: Call<SignupAsAdvisor?>, t: Throwable) {
                    BaseActivity().dismissProgressDialog()
                }
            })
        return mSignupAsAdModel
    }


    private var mLogoutModel: MutableLiveData<StatusMsgModel?>? = null
    fun logoutData(activity: Activity?, mHeaders: Map<String, String>): LiveData<StatusMsgModel?>? {
        mLogoutModel = MutableLiveData<StatusMsgModel?>()
        val ApiInterface: ApiInterface =
            ApiClient().getApiClient()!!.create(ApiInterface::class.java)
        ApiInterface.logoutRequest(mHeaders).enqueue(object : Callback<StatusMsgModel?> {
            override fun onResponse(
                call: Call<StatusMsgModel?>,
                response: Response<StatusMsgModel?>
            ) {
                if (response.body() != null) {
                    mLogoutModel!!.setValue(response.body())
                } else {
                }
            }

            override fun onFailure(call: Call<StatusMsgModel?>, t: Throwable) {
                BaseActivity().dismissProgressDialog()
            }
        })
        return mLogoutModel
    }


    private var mChangePasswordModel: MutableLiveData<StatusMsgModel?>? = null
    fun changePasswordData(
        activity: Activity?,
        mParams: Map<String?, String?>?,
        mHeaders: Map<String, String>
    ): LiveData<StatusMsgModel?>? {
        mChangePasswordModel = MutableLiveData<StatusMsgModel?>()
        val ApiInterface: ApiInterface =
            ApiClient().getApiClient()!!.create(ApiInterface::class.java)
        ApiInterface.changePasswordRequest(mParams, mHeaders)
            .enqueue(object : Callback<StatusMsgModel?> {
                override fun onResponse(
                    call: Call<StatusMsgModel?>,
                    response: Response<StatusMsgModel?>
                ) {
                    if (response.body() != null) {
                        mChangePasswordModel!!.setValue(response.body())
                    } else {
                    }
                }

                override fun onFailure(call: Call<StatusMsgModel?>, t: Throwable) {
                    BaseActivity().dismissProgressDialog()
                }
            })
        return mChangePasswordModel
    }


    private var mstatusModel: MutableLiveData<StatusUpdateModel?>? = null
    fun updateStatusData(
        activity: Activity?,
        mParams: Map<String?, String?>?,
        mHeaders: Map<String, String>
    ): LiveData<StatusUpdateModel?>? {
        mstatusModel = MutableLiveData<StatusUpdateModel?>()
        val ApiInterface: ApiInterface =
            ApiClient().getApiClient()!!.create(ApiInterface::class.java)
        ApiInterface.statusRequest(mParams, mHeaders)
            .enqueue(object : Callback<StatusUpdateModel?> {
                override fun onResponse(
                    call: Call<StatusUpdateModel?>,
                    response: Response<StatusUpdateModel?>
                ) {
                    if (response.body() != null) {
                        mstatusModel!!.setValue(response.body())
                    } else {
                    }
                }

                override fun onFailure(call: Call<StatusUpdateModel?>, t: Throwable) {
                    BaseActivity().dismissProgressDialog()
                }
            })
        return mstatusModel
    }


    private var mProfileDetailModel: MutableLiveData<ProfileDetailModel?>? = null
    fun profileDetailData(
        activity: Activity?,
        mHeaders: Map<String, String>
    ): LiveData<ProfileDetailModel?>? {
        mProfileDetailModel = MutableLiveData<ProfileDetailModel?>()
        val ApiInterface: ApiInterface =
            ApiClient().getApiClient()!!.create(ApiInterface::class.java)
        ApiInterface.profileDetailRequest(mHeaders).enqueue(object : Callback<ProfileDetailModel?> {
            override fun onResponse(
                call: Call<ProfileDetailModel?>,
                response: Response<ProfileDetailModel?>
            ) {
                if (response.body() != null) {
                    mProfileDetailModel!!.setValue(response.body())
                } else {
                }
            }

            override fun onFailure(call: Call<ProfileDetailModel?>, t: Throwable) {
                BaseActivity().dismissProgressDialog()
            }
        })
        return mProfileDetailModel
    }


    private var mCategoryListModel: MutableLiveData<CategoryListModel?>? = null
    fun categoryListData(
        activity: Activity?,

        mHeaders: Map<String, String>
    ): LiveData<CategoryListModel?>? {
        mCategoryListModel = MutableLiveData<CategoryListModel?>()
        val ApiInterface: ApiInterface =
            ApiClient().getApiClient()!!.create(ApiInterface::class.java)
        ApiInterface.categoryListRequest(mHeaders).enqueue(object : Callback<CategoryListModel?> {
            override fun onResponse(
                call: Call<CategoryListModel?>,
                response: Response<CategoryListModel?>
            ) {
                if (response.body() != null) {
                    mCategoryListModel!!.setValue(response.body())
                } else {
                }
            }

            override fun onFailure(call: Call<CategoryListModel?>, t: Throwable) {
                BaseActivity().dismissProgressDialog()
            }
        })
        return mCategoryListModel
    }




    private var mTRansactionModel: MutableLiveData<TransactionHistoryModel?>? = null
    fun transactionDetailData(
        activity: Activity?,
        mParams: Map<String?, String?>?,
        mHeaders: Map<String, String>
    ): LiveData<TransactionHistoryModel?>? {
        mTRansactionModel = MutableLiveData<TransactionHistoryModel?>()
        val ApiInterface: ApiInterface =
            ApiClient().getApiClient()!!.create(ApiInterface::class.java)
        ApiInterface.transactionHistoryRequest(mParams,mHeaders).enqueue(object : Callback<TransactionHistoryModel?> {
            override fun onResponse(
                call: Call<TransactionHistoryModel?>,
                response: Response<TransactionHistoryModel?>
            ) {
                if (response.body() != null) {
                    mTRansactionModel!!.setValue(response.body())
                } else {
                }
            }

            override fun onFailure(call: Call<TransactionHistoryModel?>, t: Throwable) {
                BaseActivity().dismissProgressDialog()
            }
        })
        return mTRansactionModel
    }



}
