package com.letmelisten.app.viewmodels

import android.app.Activity
import android.util.Log

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.kloxus.app.retrofit.ApiInterface
import com.letmelisten.app.Model.AdvisorSessionListModel
import com.letmelisten.app.Model.*
import com.letmelisten.app.activities.BaseActivity
import com.letmelisten.app.retrofit.ApiClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response







class AdvisorViewModel : ViewModel() {
    private var mSessionModel: MutableLiveData<AdvisorSessionListModel?>? = null
    fun getSessionListData(
        activity: Activity?,
        mParams: Map<String?, String?>?,
        headers: Map<String, String>
    ): LiveData<AdvisorSessionListModel?>? {
        mSessionModel = MutableLiveData<AdvisorSessionListModel?>()
        val ApiInterface: ApiInterface =
            ApiClient().getApiClient()!!.create(ApiInterface::class.java)
        ApiInterface.sessionListRequest(mParams, headers).enqueue(object : Callback<AdvisorSessionListModel?> {
            override fun onResponse(call: Call<AdvisorSessionListModel?>, response: Response<AdvisorSessionListModel?>) {
                if (response.body() != null) {
                    mSessionModel?.setValue(response.body())
                } else {
                    Log.e("TAG","VALUE::"+"error")
                }
            }

            override fun onFailure(call: Call<AdvisorSessionListModel?>, t: Throwable) {
                BaseActivity().dismissProgressDialog()
            }
        })
        return mSessionModel
    }





}