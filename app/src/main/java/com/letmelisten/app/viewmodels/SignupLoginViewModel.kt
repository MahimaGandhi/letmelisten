package com.letmelisten.app.viewmodels

import android.app.Activity
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.kloxus.app.retrofit.ApiInterface
import com.letmelisten.app.Model.BadgeCountModel
import com.letmelisten.app.Model.LoginModel
import com.letmelisten.app.Model.StatusMsgModel
import com.letmelisten.app.activities.BaseActivity
import com.letmelisten.app.retrofit.ApiClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SignupLoginViewModel : ViewModel()
{
    private var mSignuppojo: MutableLiveData<StatusMsgModel?>? = null
    fun getSignUpData(activity: Activity?,  mParams: Map<String?, String?>?): LiveData<StatusMsgModel?>? {
        mSignuppojo = MutableLiveData<StatusMsgModel?>()
        val ApiInterface: ApiInterface = ApiClient().getApiClient()!!.create(ApiInterface::class.java)
        ApiInterface.signUpRequest(mParams).enqueue(object : Callback<StatusMsgModel?> {
            override fun onResponse(call: Call<StatusMsgModel?>, response: Response<StatusMsgModel?>) {
                if (response.body() != null) {
                    mSignuppojo!!.setValue(response.body())
                } else {
                }
            }

            override fun onFailure(call: Call<StatusMsgModel?>, t: Throwable) {
                BaseActivity().dismissProgressDialog()
            }
        })
        return mSignuppojo
    }


    private var mLogInModel: MutableLiveData<LoginModel?>? = null
    fun getLogInData(activity: Activity?, mParams: Map<String?, String?>?): LiveData<LoginModel?>? {
        mLogInModel = MutableLiveData<LoginModel?>()
        val ApiInterface: ApiInterface = ApiClient().getApiClient()!!.create(ApiInterface::class.java)
        ApiInterface.signInRequest(mParams).enqueue(object : Callback<LoginModel?> {
            override fun onResponse(call: Call<LoginModel?>, response: Response<LoginModel?>) {
                if (response.body() != null) {
                    mLogInModel!!.setValue(response.body())
                } else {
                }
            }

            override fun onFailure(call: Call<LoginModel?>, t: Throwable) {
                BaseActivity().dismissProgressDialog()
            }
        })
        return mLogInModel
    }


    private var mLogInWithfbModel: MutableLiveData<LoginModel?>? = null
    fun signInWithFbRequest(activity: Activity?, mParams: Map<String?, String?>?): LiveData<LoginModel?>? {
        mLogInWithfbModel = MutableLiveData<LoginModel?>()
        val ApiInterface: ApiInterface = ApiClient().getApiClient()!!.create(ApiInterface::class.java)
        ApiInterface.signInWithFbRequest(mParams).enqueue(object : Callback<LoginModel?> {
            override fun onResponse(call: Call<LoginModel?>, response: Response<LoginModel?>) {
                if (response.body() != null) {
                    mLogInWithfbModel!!.setValue(response.body())
                } else {
                }
            }

            override fun onFailure(call: Call<LoginModel?>, t: Throwable) {
                BaseActivity().dismissProgressDialog()
            }
        })
        return mLogInWithfbModel
    }

    private var mLogInWithGoogleModel: MutableLiveData<LoginModel?>? = null
    fun signInWithGoogleRequest(activity: Activity?, mParams: Map<String?, String?>?): LiveData<LoginModel?>? {
        mLogInWithGoogleModel = MutableLiveData<LoginModel?>()
        val ApiInterface: ApiInterface = ApiClient().getApiClient()!!.create(ApiInterface::class.java)
        ApiInterface.loginWithGoogleRequest(mParams).enqueue(object : Callback<LoginModel?> {
            override fun onResponse(call: Call<LoginModel?>, response: Response<LoginModel?>) {
                if (response.body() != null) {
                    mLogInWithGoogleModel!!.setValue(response.body())
                } else {
                }
            }

            override fun onFailure(call: Call<LoginModel?>, t: Throwable) {
                BaseActivity().dismissProgressDialog()
            }
        })
        return mLogInWithGoogleModel
    }


    private var mStatusModel: MutableLiveData<StatusMsgModel?>? = null
    fun getForgotPasswordData(activity: Activity?, mParams: Map<String?, String?>?): LiveData<StatusMsgModel?>? {
        mStatusModel = MutableLiveData<StatusMsgModel?>()
        val ApiInterface: ApiInterface = ApiClient().getApiClient()!!.create(ApiInterface::class.java)
        ApiInterface.forgotPasswordRequest(mParams).enqueue(object : Callback<StatusMsgModel?> {
            override fun onResponse(call: Call<StatusMsgModel?>, response: Response<StatusMsgModel?>) {
                if (response.body() != null) {
                    mStatusModel!!.setValue(response.body())
                } else {
                }
            }

            override fun onFailure(call: Call<StatusMsgModel?>, t: Throwable) {
                BaseActivity().dismissProgressDialog()
            }
        })
        return mStatusModel
    }



    private var mBadgeModel: MutableLiveData<BadgeCountModel?>? = null
    fun getBadgeCount(activity: Activity?, mParams: Map<String?, String?>?,mHeaders: Map<String, String>): LiveData<BadgeCountModel?>? {
        mBadgeModel = MutableLiveData<BadgeCountModel?>()
        val ApiInterface: ApiInterface = ApiClient().getApiClient()!!.create(ApiInterface::class.java)
        ApiInterface.badgeRequest(mParams,mHeaders).enqueue(object : Callback<BadgeCountModel?> {
            override fun onResponse(call: Call<BadgeCountModel?>, response: Response<BadgeCountModel?>) {
                if (response.body() != null) {
                    mBadgeModel!!.setValue(response.body())
                } else {
                }
            }

            override fun onFailure(call: Call<BadgeCountModel?>, t: Throwable) {
                BaseActivity().dismissProgressDialog()
            }
        })
        return mBadgeModel
    }

}
