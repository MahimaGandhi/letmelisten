package com.letmelisten.app.viewmodels

import android.app.Activity
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.kloxus.app.retrofit.ApiInterface
import com.letmelisten.app.Model.*
import com.letmelisten.app.activities.BaseActivity
import com.letmelisten.app.retrofit.ApiClient
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class StripePaymentViewModel: ViewModel()
{
    private var mWalletpojo: MutableLiveData<StripeMoneyWalletModel?>? = null
    fun getStripeWalletMoneyData(activity: Activity?, mParams: Map<String?, String?>?,headers: Map<String, String>): LiveData<StripeMoneyWalletModel?>? {
        mWalletpojo = MutableLiveData<StripeMoneyWalletModel?>()
        val ApiInterface: ApiInterface = ApiClient().getApiClient()!!.create(ApiInterface::class.java)
        ApiInterface.addMoneyRequest(mParams,headers).enqueue(object : Callback<StripeMoneyWalletModel?> {
            override fun onResponse(call: Call<StripeMoneyWalletModel?>, response: Response<StripeMoneyWalletModel?>) {
                if (response.body() != null) {
                    mWalletpojo!!.setValue(response.body())
                } else {
                }
            }

            override fun onFailure(call: Call<StripeMoneyWalletModel?>, t: Throwable) {
                BaseActivity().dismissProgressDialog()
            }
        })
        return mWalletpojo
    }



    private var mSessionpojo: MutableLiveData<SessionCreateModel?>? = null
    fun getSessionCreateData(activity: Activity?, mParams: Map<String?, String?>?,headers: Map<String, String>): LiveData<SessionCreateModel?>? {
        mSessionpojo = MutableLiveData<SessionCreateModel?>()
        val ApiInterface: ApiInterface = ApiClient().getApiClient()!!.create(ApiInterface::class.java)
        ApiInterface.sessionRequest(mParams,headers).enqueue(object : Callback<SessionCreateModel?> {
            override fun onResponse(call: Call<SessionCreateModel?>, response: Response<SessionCreateModel?>) {
                if (response.body() != null) {
                    mSessionpojo!!.setValue(response.body())
                } else {
                }
            }

            override fun onFailure(call: Call<SessionCreateModel?>, t: Throwable) {
                BaseActivity().dismissProgressDialog()
            }
        })
        return mSessionpojo
    }


    private var mgetMssgpojo: MutableLiveData<ChatMessagesModel?>? = null
    fun getChatMessageData(activity: Activity?, mParams: Map<String?, String?>?,headers: Map<String, String>): LiveData<ChatMessagesModel?>? {
        mgetMssgpojo = MutableLiveData<ChatMessagesModel?>()
        val ApiInterface: ApiInterface = ApiClient().getApiClient()!!.create(ApiInterface::class.java)
        ApiInterface.chatMessageRequest(mParams,headers).enqueue(object : Callback<ChatMessagesModel?> {
            override fun onResponse(call: Call<ChatMessagesModel?>, response: Response<ChatMessagesModel?>) {
                if (response.body() != null) {
                    mgetMssgpojo!!.setValue(response.body())
                } else {
                }
            }

            override fun onFailure(call: Call<ChatMessagesModel?>, t: Throwable) {
                BaseActivity().dismissProgressDialog()
            }
        })
        return mgetMssgpojo
    }

    private var mSendMssgpojo: MutableLiveData<SendMsgModel?>? = null
    fun getSendMessageData(activity: Activity?, mParams: Map<String?, String?>?,headers: Map<String, String>): LiveData<SendMsgModel?>? {
        mSendMssgpojo = MutableLiveData<SendMsgModel?>()
        val ApiInterface: ApiInterface = ApiClient().getApiClient()!!.create(ApiInterface::class.java)
        ApiInterface.sendMessageRequest(mParams,headers).enqueue(object : Callback<SendMsgModel?> {
            override fun onResponse(call: Call<SendMsgModel?>, response: Response<SendMsgModel?>) {
                if (response.body() != null) {
                    mSendMssgpojo!!.setValue(response.body())
                } else {
                }
            }

            override fun onFailure(call: Call<SendMsgModel?>, t: Throwable) {
                BaseActivity().dismissProgressDialog()
            }
        })
        return mSendMssgpojo
    }


    private var mSessionEndModel: MutableLiveData<EndSessionModel?>? = null
    fun getSessionEndData(
        activity: Activity?,
        mParams: Map<String?, String?>?,
        headers: Map<String, String>
    ): LiveData<EndSessionModel?>? {
        mSessionEndModel = MutableLiveData<EndSessionModel?>()
        val ApiInterface: ApiInterface =
            ApiClient().getApiClient()!!.create(ApiInterface::class.java)
        ApiInterface.endSessionRequest(mParams, headers).enqueue(object : Callback<EndSessionModel?> {
            override fun onResponse(call: Call<EndSessionModel?>, response: Response<EndSessionModel?>) {
                if (response.body() != null) {
                    mSessionEndModel?.setValue(response.body())
                } else {
                    Log.e("TAG","VALUE::"+"error")
                }
            }

            override fun onFailure(call: Call<EndSessionModel?>, t: Throwable) {
                BaseActivity().dismissProgressDialog()
            }
        })
        return mSessionEndModel
    }

    private var mAddTimeModel: MutableLiveData<AddMoreTimeModel?>? = null
    fun getAddTime(
        activity: Activity?,
        mParams: Map<String?, String?>?,
        headers: Map<String, String>
    ): LiveData<AddMoreTimeModel?>? {
        mAddTimeModel = MutableLiveData<AddMoreTimeModel?>()
        val ApiInterface: ApiInterface =
            ApiClient().getApiClient()!!.create(ApiInterface::class.java)
        ApiInterface.addTimeRequest(mParams, headers).enqueue(object : Callback<AddMoreTimeModel?> {
            override fun onResponse(call: Call<AddMoreTimeModel?>, response: Response<AddMoreTimeModel?>) {
                if (response.body() != null) {
                    mAddTimeModel?.setValue(response.body())
                } else {
                    Log.e("TAG","VALUE::"+"error")
                }
            }

            override fun onFailure(call: Call<AddMoreTimeModel?>, t: Throwable) {
                BaseActivity().dismissProgressDialog()
            }
        })
        return mAddTimeModel
    }





    private var mPaypalModel: MutableLiveData<PaypalUrlModel?>? = null
    fun connectPaypalApi(
        activity: Activity?,
        headers: Map<String, String>,
        userId: RequestBody,
    ): LiveData<PaypalUrlModel?>? {
        mPaypalModel = MutableLiveData<PaypalUrlModel?>()
        val ApiInterface: ApiInterface =
            ApiClient().getApiClient()!!.create(ApiInterface::class.java)
        ApiInterface.connectPaypalRequest(
            headers,
            userId
        )
            ?.enqueue(object : Callback<PaypalUrlModel?> {
                override fun onResponse(
                    call: Call<PaypalUrlModel?>,
                    response: Response<PaypalUrlModel?>
                ) {
                    if (response.body() != null) {
                        mPaypalModel!!.setValue(response.body())
                    } else {
                    }
                }

                override fun onFailure(call: Call<PaypalUrlModel?>, t: Throwable) {
                    BaseActivity().dismissProgressDialog()
                }
            })
        return mPaypalModel
    }

    private var mPaypalTokenModel: MutableLiveData<AccessTokenPaypalModel?>? = null
    fun paypalTokenApi(
        activity: Activity?,
        headers: Map<String, String>,
        userId: RequestBody
    ): LiveData<AccessTokenPaypalModel?>? {
        mPaypalTokenModel = MutableLiveData<AccessTokenPaypalModel?>()
        val ApiInterface: ApiInterface =
            ApiClient().getApiClient()!!.create(ApiInterface::class.java)
        ApiInterface.PaypalTokenRequest(
            headers,
            userId
        )
            ?.enqueue(object : Callback<AccessTokenPaypalModel?> {
                override fun onResponse(
                    call: Call<AccessTokenPaypalModel?>,
                    response: Response<AccessTokenPaypalModel?>
                ) {
                    if (response.body() != null) {
                        mPaypalTokenModel!!.setValue(response.body())
                    } else {
                    }
                }

                override fun onFailure(call: Call<AccessTokenPaypalModel?>, t: Throwable) {
                    BaseActivity().dismissProgressDialog()
                }
            })
        return mPaypalTokenModel
    }


    private var mPaypalTransactionModel: MutableLiveData<PaypalTransactionModel?>? = null
    fun paypalTransactionApi(
        activity: Activity?,
        headers: Map<String, String>,
        accessToken: RequestBody,
        payID: RequestBody
    ): LiveData<PaypalTransactionModel?>? {
        mPaypalTransactionModel = MutableLiveData<PaypalTransactionModel?>()
        val ApiInterface: ApiInterface =
            ApiClient().getApiClient()!!.create(ApiInterface::class.java)
        ApiInterface.PaypalTransactionRequest(
            headers,
            accessToken,
            payID
        )
            ?.enqueue(object : Callback<PaypalTransactionModel?> {
                override fun onResponse(
                    call: Call<PaypalTransactionModel?>,
                    response: Response<PaypalTransactionModel?>
                ) {
                    if (response.body() != null) {
                        mPaypalTransactionModel!!.setValue(response.body())
                    } else {
                    }
                }

                override fun onFailure(call: Call<PaypalTransactionModel?>, t: Throwable) {
                    BaseActivity().dismissProgressDialog()
                }
            })
        return mPaypalTransactionModel
    }
}