package com.letmelisten.app.viewmodels

import android.app.Activity
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.kloxus.app.retrofit.ApiInterface
import com.letmelisten.app.Model.*
import com.letmelisten.app.activities.BaseActivity
import com.letmelisten.app.retrofit.ApiClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PaymentViewModel : ViewModel() {
    private var mStartSessionModel: MutableLiveData<SessionCreateModel?>? = null
    fun getCreateSessionData(
        activity: Activity?,
        mParams: Map<String?, String?>?,
        headers: Map<String, String>
    ): LiveData<SessionCreateModel?>? {
        mStartSessionModel = MutableLiveData<SessionCreateModel?>()
        val ApiInterface: ApiInterface =
            ApiClient().getApiClient()!!.create(ApiInterface::class.java)
        ApiInterface.createSessionRequest(mParams, headers).enqueue(object : Callback<SessionCreateModel?> {
            override fun onResponse(call: Call<SessionCreateModel?>, response: Response<SessionCreateModel?>) {
                if (response.body() != null) {
                    mStartSessionModel!!.setValue(response.body())
                } else {
                }
            }

            override fun onFailure(call: Call<SessionCreateModel?>, t: Throwable) {
                BaseActivity().dismissProgressDialog()
            }
        })
        return mStartSessionModel
    }


    private var mRatingModel: MutableLiveData<StatusMsgModel?>? = null
    fun getRatingData(
        activity: Activity?,
        mParams: Map<String?, String?>?,
        headers: Map<String, String>
    ): LiveData<StatusMsgModel?>? {
        mRatingModel = MutableLiveData<StatusMsgModel?>()
        val ApiInterface: ApiInterface =
            ApiClient().getApiClient()!!.create(ApiInterface::class.java)
        ApiInterface.ratingRequest(mParams, headers).enqueue(object : Callback<StatusMsgModel?> {
            override fun onResponse(
                call: Call<StatusMsgModel?>,
                response: Response<StatusMsgModel?>
            ) {
                if (response.body() != null) {
                    mRatingModel!!.setValue(response.body())
                } else {
                }
            }

            override fun onFailure(call: Call<StatusMsgModel?>, t: Throwable) {
                BaseActivity().dismissProgressDialog()
            }
        })
        return mRatingModel
    }

    private var mNotificationsModel: MutableLiveData<NotificationModelNew?>? = null
    fun getNotificationsData(
        activity: Activity?,
        mParams: Map<String?, String?>?,
        headers: Map<String, String>
    ): LiveData<NotificationModelNew?>? {
        mNotificationsModel = MutableLiveData<NotificationModelNew?>()
        val ApiInterface: ApiInterface =
            ApiClient().getApiClient()!!.create(ApiInterface::class.java)
        ApiInterface.notificationsRequest(mParams, headers).enqueue(object : Callback<NotificationModelNew?> {
            override fun onResponse(
                call: Call<NotificationModelNew?>,
                response: Response<NotificationModelNew?>
            ) {
                if (response.body() != null) {
                    mNotificationsModel!!.setValue(response.body())
                } else {
                    Toast.makeText(activity,"ERROR",Toast.LENGTH_LONG).show()
                }
            }

            override fun onFailure(call: Call<NotificationModelNew?>, t: Throwable) {
                BaseActivity().dismissProgressDialog()
            }
        })
        return mNotificationsModel
    }

//    private var mHistoryListModel: MutableLiveData<SessionHistoryModel?>? = null
//    fun getHistoryListData(
//        activity: Activity?,
//        mParams: Map<String?, String?>?,
//        headers: Map<String, String>
//    ): LiveData<SessionHistoryModel?>? {
//        mHistoryListModel = MutableLiveData<SessionHistoryModel?>()
//        val ApiInterface: ApiInterface =
//            ApiClient().getApiClient()!!.create(ApiInterface::class.java)
//        ApiInterface.sessionHistoryListRequest(mParams, headers).enqueue(object : Callback<SessionHistoryModel?> {
//            override fun onResponse(
//                call: Call<SessionHistoryModel?>,
//                response: Response<SessionHistoryModel?>
//            ) {
//                if (response.body() != null) {
//                    mHistoryListModel!!.setValue(response.body())
//                } else {
//                }
//            }
//
//            override fun onFailure(call: Call<SessionHistoryModel?>, t: Throwable) {
//                BaseActivity().dismissProgressDialog()
//            }
//        })
//        return mHistoryListModel
//    }
//
//    private var mAdvDetailsModel: MutableLiveData<AdvisorDetailsModel?>? = null
//    fun getAdvisorDetails(
//        activity: Activity?,
//        mParams: Map<String?, String?>?,
//        headers: Map<String, String>
//    ): LiveData<AdvisorDetailsModel?>? {
//        mAdvDetailsModel = MutableLiveData<AdvisorDetailsModel?>()
//        val ApiInterface: ApiInterface =
//            ApiClient().getApiClient()!!.create(ApiInterface::class.java)
//        ApiInterface.advisorDetailsRequest(mParams, headers).enqueue(object : Callback<AdvisorDetailsModel?> {
//            override fun onResponse(
//                call: Call<AdvisorDetailsModel?>,
//                response: Response<AdvisorDetailsModel?>
//            ) {
//                if (response.body() != null) {
//                    mAdvDetailsModel!!.setValue(response.body())
//                } else {
//                }
//            }
//
//            override fun onFailure(call: Call<AdvisorDetailsModel?>, t: Throwable) {
//                BaseActivity().dismissProgressDialog()
//            }
//        })
//        return mAdvDetailsModel
//    }

    private var mCallingRequestModel: MutableLiveData<CallingModel?>? = null
    fun callingRequestData(
        activity: Activity?,
        mParams: Map<String?, String?>?,
        mHeaders: Map<String, String>
    ): LiveData<CallingModel?>? {
        mCallingRequestModel = MutableLiveData<CallingModel?>()
        val ApiInterface: ApiInterface =
            ApiClient().getApiClient()!!.create(ApiInterface::class.java)
        ApiInterface.callingRequest(mParams, mHeaders)
            .enqueue(object : Callback<CallingModel?> {
                override fun onResponse(
                    call: Call<CallingModel?>,
                    response: Response<CallingModel?>
                ) {
                    if (response.body() != null) {
                        mCallingRequestModel!!.setValue(response.body())
                    } else {
                    }
                }

                override fun onFailure(call: Call<CallingModel?>, t: Throwable) {
                    BaseActivity().dismissProgressDialog()
                }
            })
        return mCallingRequestModel
    }

//
//    private var mAdvDetailsModel: MutableLiveData<AdvisorDetailsModel?>? = null
//    fun getAdvisorDetails(
//        activity: Activity?,
//        mParams: Map<String?, String?>?,
//        headers: Map<String, String>
//    ): LiveData<AdvisorDetailsModel?>? {
//        mAdvDetailsModel = MutableLiveData<AdvisorDetailsModel?>()
//        val ApiInterface: ApiInterface =
//            ApiClient().getApiClient()!!.create(ApiInterface::class.java)
//        ApiInterface.advisorDetailsRequest(mParams, headers).enqueue(object : Callback<AdvisorDetailsModel?> {
//            override fun onResponse(
//                call: Call<AdvisorDetailsModel?>,
//                response: Response<AdvisorDetailsModel?>
//            ) {
//                if (response.body() != null) {
//                    mAdvDetailsModel!!.setValue(response.body())
//                } else {
//                }
//            }
//
//            override fun onFailure(call: Call<AdvisorDetailsModel?>, t: Throwable) {
//                BaseActivity().dismissProgressDialog()
//            }
//        })
//        return mAdvDetailsModel
//    }

    private var mSessionEndModel: MutableLiveData<EndSessionModel?>? = null
    fun getSessionEndData(
        activity: Activity?,
        mParams: Map<String?, String?>?,
        headers: Map<String, String>
    ): LiveData<EndSessionModel?>? {
        mSessionEndModel = MutableLiveData<EndSessionModel?>()
        val ApiInterface: ApiInterface =
            ApiClient().getApiClient()!!.create(ApiInterface::class.java)
        ApiInterface.endSessionRequest(mParams, headers).enqueue(object : Callback<EndSessionModel?> {
            override fun onResponse(call: Call<EndSessionModel?>, response: Response<EndSessionModel?>) {
                if (response.body() != null) {
                    mSessionEndModel?.setValue(response.body())
                } else {
                    Log.e("TAG","VALUE::"+"error")
                }
            }

            override fun onFailure(call: Call<EndSessionModel?>, t: Throwable) {
                BaseActivity().dismissProgressDialog()
            }
        })
        return mSessionEndModel
    }
    private var mSessionEndCallModel: MutableLiveData<EndSessionCallModel?>? = null
    fun getCallEndData(
        activity: Activity?,
        mParams: Map<String?, String?>?,
        headers: Map<String, String>
    ): LiveData<EndSessionCallModel?>? {
        mSessionEndCallModel = MutableLiveData<EndSessionCallModel?>()
        val ApiInterface: ApiInterface =
            ApiClient().getApiClient()!!.create(ApiInterface::class.java)
        ApiInterface.endSessionCallRequest(mParams, headers).enqueue(object : Callback<EndSessionCallModel?> {
            override fun onResponse(call: Call<EndSessionCallModel?>, response: Response<EndSessionCallModel?>) {
                if (response.body() != null) {
                    mSessionEndCallModel?.setValue(response.body())
                } else {
                    Log.e("TAG","VALUE::"+"error")
                }
            }

            override fun onFailure(call: Call<EndSessionCallModel?>, t: Throwable) {
                BaseActivity().dismissProgressDialog()
            }
        })
        return mSessionEndCallModel
    }

    private var mAddMoreCallModel: MutableLiveData<AddMoreTimeCallModel?>? = null
    fun getAddMoreTimeCallRequest(
        activity: Activity?,
        mParams: Map<String?, String?>?,
        headers: Map<String, String>
    ): LiveData<AddMoreTimeCallModel?>? {
        mAddMoreCallModel = MutableLiveData<AddMoreTimeCallModel?>()
        val ApiInterface: ApiInterface =
            ApiClient().getApiClient()!!.create(ApiInterface::class.java)
        ApiInterface.addMoreTimeRequest(mParams, headers).enqueue(object : Callback<AddMoreTimeCallModel?> {
            override fun onResponse(call: Call<AddMoreTimeCallModel?>, response: Response<AddMoreTimeCallModel?>) {
                if (response.body() != null) {
                    mAddMoreCallModel?.setValue(response.body())
                } else {
                    Log.e("TAG","VALUE::"+"error")
                }
            }

            override fun onFailure(call: Call<AddMoreTimeCallModel?>, t: Throwable) {
                BaseActivity().dismissProgressDialog()
            }
        })
        return mAddMoreCallModel
    }
    private var mAnswerCallModel: MutableLiveData<EndSessionCallModel?>? = null
    fun getCallAnswerData(
        activity: Activity?,
        mParams: Map<String?, String?>?,
        headers: Map<String, String>
    ): LiveData<EndSessionCallModel?>? {
        mAnswerCallModel = MutableLiveData<EndSessionCallModel?>()
        val ApiInterface: ApiInterface =
            ApiClient().getApiClient()!!.create(ApiInterface::class.java)
        ApiInterface.answerCallRequest(mParams, headers).enqueue(object : Callback<EndSessionCallModel?> {
            override fun onResponse(call: Call<EndSessionCallModel?>, response: Response<EndSessionCallModel?>) {
                if (response.body() != null) {
                    mAnswerCallModel?.setValue(response.body())
                } else {
                    Log.e("TAG","VALUE::"+"error")
                }
            }

            override fun onFailure(call: Call<EndSessionCallModel?>, t: Throwable) {
                BaseActivity().dismissProgressDialog()
            }
        })
        return mAnswerCallModel
    }
}


