package com.letmelisten.app.viewmodels

import android.app.Activity
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.kloxus.app.retrofit.ApiInterface
import com.letmelisten.app.Model.*
import com.letmelisten.app.activities.BaseActivity
import com.letmelisten.app.retrofit.ApiClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class HomeViewModel : ViewModel() {
    private var mHomeModel: MutableLiveData<HomeModel?>? = null
    fun getHomeData(
        activity: Activity?,
        mParams: Map<String?, String?>?,
        headers: Map<String, String>
    ): LiveData<HomeModel?>? {
        mHomeModel = MutableLiveData<HomeModel?>()
        val ApiInterface: ApiInterface =
            ApiClient().getApiClient()!!.create(ApiInterface::class.java)
        ApiInterface.homeRequest(mParams, headers).enqueue(object : Callback<HomeModel?> {
            override fun onResponse(call: Call<HomeModel?>, response: Response<HomeModel?>) {
                if (response.body() != null) {
                    mHomeModel!!.setValue(response.body())
                } else {
                }
            }

            override fun onFailure(call: Call<HomeModel?>, t: Throwable) {
                BaseActivity().dismissProgressDialog()
            }
        })
        return mHomeModel
    }

    private var mFavUnfavModel: MutableLiveData<FavUnfavModel?>? = null
    fun getFavUnfavData(
        activity: Activity?,
        mParams: Map<String?, String?>?,
        headers: Map<String, String>
    ): LiveData<FavUnfavModel?>? {
        mFavUnfavModel = MutableLiveData<FavUnfavModel?>()
        val ApiInterface: ApiInterface =
            ApiClient().getApiClient()!!.create(ApiInterface::class.java)
        ApiInterface.favUnfavRequest(mParams, headers).enqueue(object : Callback<FavUnfavModel?> {
            override fun onResponse(
                call: Call<FavUnfavModel?>,
                response: Response<FavUnfavModel?>
            ) {
                if (response.body() != null) {
                    mFavUnfavModel!!.setValue(response.body())
                } else {
                }
            }

            override fun onFailure(call: Call<FavUnfavModel?>, t: Throwable) {
                BaseActivity().dismissProgressDialog()
            }
        })
        return mFavUnfavModel
    }

    private var mFavListModel: MutableLiveData<FavoriteListModel?>? = null
    fun getFavListData(
        activity: Activity?,
        mParams: Map<String?, String?>?,
        headers: Map<String, String>
    ): LiveData<FavoriteListModel?>? {
        mFavListModel = MutableLiveData<FavoriteListModel?>()
        val ApiInterface: ApiInterface =
            ApiClient().getApiClient()!!.create(ApiInterface::class.java)
        ApiInterface.favListRequest(mParams, headers).enqueue(object : Callback<FavoriteListModel?> {
            override fun onResponse(
                call: Call<FavoriteListModel?>,
                response: Response<FavoriteListModel?>
            ) {
                if (response.body() != null) {
                    mFavListModel!!.setValue(response.body())
                } else {
                }
            }

            override fun onFailure(call: Call<FavoriteListModel?>, t: Throwable) {
                BaseActivity().dismissProgressDialog()
            }
        })
        return mFavListModel
    }

    private var mHistoryListModel: MutableLiveData<SessionHistoryModel?>? = null
    fun getHistoryListData(
        activity: Activity?,
        mParams: Map<String?, String?>?,
        headers: Map<String, String>
    ): LiveData<SessionHistoryModel?>? {
        mHistoryListModel = MutableLiveData<SessionHistoryModel?>()
        val ApiInterface: ApiInterface =
            ApiClient().getApiClient()!!.create(ApiInterface::class.java)
        ApiInterface.sessionHistoryListRequest(mParams, headers).enqueue(object : Callback<SessionHistoryModel?> {
            override fun onResponse(
                call: Call<SessionHistoryModel?>,
                response: Response<SessionHistoryModel?>
            ) {
                if (response.body() != null) {
                    mHistoryListModel!!.setValue(response.body())
                } else {
                }
            }

            override fun onFailure(call: Call<SessionHistoryModel?>, t: Throwable) {
                BaseActivity().dismissProgressDialog()
            }
        })
        return mHistoryListModel
    }

    private var mAdvDetailsModel: MutableLiveData<AdvisorDetailsModel?>? = null
    fun getAdvisorDetails(
        activity: Activity?,
        mParams: Map<String?, String?>?,
        headers: Map<String, String>
    ): LiveData<AdvisorDetailsModel?>? {
        mAdvDetailsModel = MutableLiveData<AdvisorDetailsModel?>()
        val ApiInterface: ApiInterface =
            ApiClient().getApiClient()!!.create(ApiInterface::class.java)
        ApiInterface.advisorDetailsRequest(mParams, headers).enqueue(object : Callback<AdvisorDetailsModel?> {
            override fun onResponse(
                call: Call<AdvisorDetailsModel?>,
                response: Response<AdvisorDetailsModel?>
            ) {
                if (response.body() != null) {
                    mAdvDetailsModel!!.setValue(response.body())
                } else {
                }
            }

            override fun onFailure(call: Call<AdvisorDetailsModel?>, t: Throwable) {
                BaseActivity().dismissProgressDialog()
            }
        })
        return mAdvDetailsModel
    }

    private var mSessionEndModel: MutableLiveData<EndSessionModel?>? = null
    fun getSessionEndData(
        activity: Activity?,
        mParams: Map<String?, String?>?,
        headers: Map<String, String>
    ): LiveData<EndSessionModel?>? {
        mSessionEndModel = MutableLiveData<EndSessionModel?>()
        val ApiInterface: ApiInterface =
            ApiClient().getApiClient()!!.create(ApiInterface::class.java)
        ApiInterface.endSessionRequest(mParams, headers).enqueue(object : Callback<EndSessionModel?> {
            override fun onResponse(call: Call<EndSessionModel?>, response: Response<EndSessionModel?>) {
                if (response.body() != null) {
                    mSessionEndModel?.setValue(response.body())
                } else {
                    Log.e("TAG","VALUE::"+"error")
                }
            }

            override fun onFailure(call: Call<EndSessionModel?>, t: Throwable) {
                BaseActivity().dismissProgressDialog()
            }
        })
        return mSessionEndModel
    }

}


