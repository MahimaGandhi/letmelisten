package com.letmelisten.app.fragments

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import butterknife.Unbinder
import com.bumptech.glide.Glide
import com.letmelisten.app.Model.NotificationModelNew
import com.letmelisten.app.Model.ProfileDetailModel
import com.letmelisten.app.Model.StatusMsgModel
import com.letmelisten.app.R
import com.letmelisten.app.activities.*
import com.letmelisten.app.activities.advisor.ChangePwdActivity
import com.letmelisten.app.activities.advisor.HomeActivity
import com.letmelisten.app.utils.*
import com.letmelisten.app.viewmodels.ProfileEditProfileViewModel
import kotlinx.android.synthetic.main.fragment_notification.*
import kotlinx.android.synthetic.main.fragment_profile.*
import java.util.*


class ProfileFragment : BaseFragment() {

    @BindView(R.id.switchToAdvisorRL)
    lateinit var switchToAdvisorRL: RelativeLayout

    @BindView(R.id.signUpAsAdvisorRL)
    lateinit var signUpAsAdvisorRL: RelativeLayout

    lateinit var mUnbinder: Unbinder
    private var viewModel: ProfileEditProfileViewModel? = null


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val mView: View = inflater.inflate(R.layout.fragment_profile, container, false)
        mUnbinder = ButterKnife.bind(this, mView)
        viewModel = ViewModelProviders.of(this).get(ProfileEditProfileViewModel::class.java)


        return mView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


    }

    override fun onResume() {
        super.onResume()
        if (!activity?.let { isNetworkAvailable(it) }!!) {
            showAlertDialog(activity, getString(R.string.internet_connection_error))
        } else {
            executeProfileDetailRequest()
        }


    }

    private fun setDataOnWidgets() {

        //Gone Visibility--When signup as advisor api hits first time
//        if (LetMeListenPreferences().readString(requireActivity(), ADVISOR_ADD, null) != null) {
//            if (LetMeListenPreferences().readString(requireActivity(), ADVISOR_ADD, null)
//                    .equals("AdvisorAdd")
//            ) {
//                signUpAsAdvisorRL.visibility = View.GONE
//            }
//        }

        activity?.let {
            Glide.with(it)
                .load(LetMeListenPreferences().readString(requireActivity(), PROFILE_IMAGE, null))
                .placeholder(R.drawable.ph)
                .error(R.drawable.ph)
                .into(profileIV)
        }
        profileEmailTV.text = activity?.let {
            LetMeListenPreferences().readString(
                it,
                USER_EMAIL, null
            )
        }

//        profileNameTV.text= context?.let { LetMeListenPreferences().readString(it, FIRST_NAME,null) }
        profileNameTV.text = activity?.let {
            LetMeListenPreferences().readString(
                it,
                FIRST_NAME,
                null
            )
        } + " " + activity?.let {
            LetMeListenPreferences().readString(
                it, LAST_NAME, null
            )
        }
    }

    @OnClick(
        R.id.editRL,
        R.id.historyRL,
        R.id.termsConditionsRL,
        R.id.privacyRL,
        R.id.aboutRL,
        R.id.changePwdRL,
        R.id.logoutRL,
        R.id.WalletRL,
        R.id.signUpAsAdvisorRL,
        R.id.switchToAdvisorRL
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.editRL -> performEditProfileClick()
            R.id.historyRL -> performHistoryClick()
            R.id.aboutRL -> performAboutClick()
            R.id.termsConditionsRL -> performTermsClick()
            R.id.privacyRL -> performPrivacyClick()
            R.id.changePwdRL -> performChangePwdClick()
            R.id.logoutRL -> performLogoutClick()
            R.id.WalletRL -> performWalletClick()
            R.id.signUpAsAdvisorRL -> performSignUpAsAdvisorClick()
            R.id.switchToAdvisorRL -> performSwitchToAdvisorClick()
        }
    }

    private fun performSwitchToAdvisorClick() {
        activity?.let {
            LetMeListenPreferences().writeString(
                it,
                SWITCH_ACCOUNT,
                "advisor"
            )
        }
        startActivity(Intent(activity, HomeActivity::class.java))
    }

    private fun performSignUpAsAdvisorClick() {
        startActivity(Intent(activity, SignUpAsAdvisorActivity::class.java))
    }

    private fun performWalletClick() {
        startActivity(Intent(activity, WalletActivity::class.java).putExtra(CLICK_TYPE,"setting"))

    }

    private fun performLogoutClick() {
        showSignoutAlert(activity, getString(R.string.logout_sure))
    }


    fun showSignoutAlert(mActivity: Activity?, strMessage: String?) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.signout_alert)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        // set the custom dialog components - text, image and button
        val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
        val btnNo = alertDialog.findViewById<TextView>(R.id.btnNo)
        val btnYes = alertDialog.findViewById<TextView>(R.id.btnYes)
        txtMessageTV.text = strMessage
        btnNo.setOnClickListener { alertDialog.dismiss() }
        alertDialog.show()
        btnYes.setOnClickListener {
            logout()
            alertDialog.dismiss()
        }
    }


    private fun logout() {
        if (!activity?.let { isNetworkAvailable(it) }!!) {
            showAlertDialog(activity, getString(R.string.internet_connection_error))
        } else {
            executeLogoutApi()
        }
    }


    private fun performChangePwdClick() {
        startActivity(Intent(activity, ChangePwdActivity::class.java))
    }

    private fun performTermsClick() {
        preventMultipleViewClick()
        val i = Intent(activity, WebViewActivity::class.java)
        i.putExtra(LINK_TYPE, LINK_TERMS)
        startActivity(i)
    }

    private fun performAboutClick() {
        preventMultipleViewClick()
        val i = Intent(activity, WebViewActivity::class.java)
        i.putExtra(LINK_TYPE, LINK_ABOUT)
        startActivity(i)
    }

    private fun performPrivacyClick() {
        preventMultipleViewClick()
        val i = Intent(activity, WebViewActivity::class.java)
        i.putExtra(LINK_TYPE, LINK_PP)
        startActivity(i)
    }

    private fun performEditProfileClick() {
        startActivity(Intent(activity, EditProfileActivity::class.java))
    }

    private fun performHistoryClick() {
        startActivity(Intent(activity, HistoryActivity::class.java))
    }

    override fun onDestroy() {
        super.onDestroy()
        if (mUnbinder != null) {
            mUnbinder.unbind()
        }

    }


//    /*
//     * Execute api param
//     * */
//    private fun mlogoutParam(): MutableMap<String?, String?> {
//        val mMap: MutableMap<String?, String?> = HashMap()
//        mMap["isAdvisor"] = "0"
//        Log.e("params", "**PARAM**$mMap")
//        return mMap
//    }

    private fun mLogoutHeader(): HashMap<String, String> {
        val headers = HashMap<String, String>()
        activity?.let {
            LetMeListenPreferences().readString(it, TOKEN, null)?.let {
                headers.put(
                    "Token",
                    it
                )
            }
        }
        Log.e("header", "**PARAM**$headers")
        return headers
    }

    private fun executeLogoutApi() {
        showProgressDialog(activity)
        viewModel?.logoutData(activity, mLogoutHeader())?.observe(this,
            androidx.lifecycle.Observer<StatusMsgModel?> { mLogoutModel ->
                if (mLogoutModel.status == 1) {
                    dismissProgressDialog()
//                    showToast(activity, mLogoutModel.message)
                    val preferences: SharedPreferences = activity?.let {
                        LetMeListenPreferences().getPreferences(
                            it
                        )
                    }!!
                    val editor = preferences.edit()
                    editor.clear()
                    editor.apply()
                    this.requireActivity().onBackPressed()
                    val mIntent = Intent(activity, SignInActivity::class.java)
                    mIntent.flags =
                        Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
                    mIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP // To clean up all activities
                    startActivity(mIntent)
                    requireActivity().finish()
                    requireActivity().finishAffinity()
                } else {
                    dismissProgressDialog()
                    showAlertDialog(activity, mLogoutModel.message)
                }
            })
    }


    /*
     * Execute api param
     * */

    private fun mHeaderParam(): HashMap<String, String> {
        val headers = HashMap<String, String>()
        activity?.let {
            LetMeListenPreferences().readString(it, TOKEN, null)?.let {
                headers.put(
                    "Token",
                    it
                )
            }
        }
        Log.e(
            TAG,
            "**Token**${activity?.let { LetMeListenPreferences().readString(it, TOKEN, null) }}"
        )
        return headers
    }

    private fun executeProfileDetailRequest() {
        showProgressDialog(activity)
        activity?.let {
            viewModel?.profileDetailData(activity, mHeaderParam())?.observe(
                it,
                androidx.lifecycle.Observer<ProfileDetailModel?> { mProfileModel ->
                    if (mProfileModel != null && !mProfileModel.equals("")) {
                        if (mProfileModel.status == 1) {
                            dismissProgressDialog()
                            if (mProfileModel.data?.isAdvisor.equals("0") && mProfileModel.data?.advisorReq.equals(
                                    "0"
                                )
                            ) {
                                signUpAsAdvisorRL.visibility = View.VISIBLE
                                switchToAdvisorRL.visibility = View.GONE
                            } else if (mProfileModel.data?.isAdvisor.equals("0") && mProfileModel.data?.advisorReq.equals(
                                    "1"
                                )
                            ) {
                                signUpAsAdvisorRL.visibility = View.GONE
                                switchToAdvisorRL.visibility = View.GONE
                            } else if (mProfileModel.data?.isAdvisor.equals("1")) {
                                if (activity != null) {
                                    signUpAsAdvisorRL.visibility = View.GONE
                                    switchToAdvisorRL.visibility = View.VISIBLE
                                }
                            }
                            setDataOnWidgets()
                        } else {
                            dismissProgressDialog()
                        }
                    }

                })
        }
    }


}