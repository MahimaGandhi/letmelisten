package com.letmelisten.app.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.Unbinder
import com.letmelisten.app.Interfac.FavUpdateInterface

import com.letmelisten.app.Interfaces.HomeTopItemClickInterface
import com.letmelisten.app.Interfaces.LoadMoreScrollListner
import com.letmelisten.app.Model.*
import com.letmelisten.app.R
import com.letmelisten.app.adapters.HomeAdapter
import com.letmelisten.app.adapters.HomeTopAdapter
import com.letmelisten.app.utils.LetMeListenPreferences
import com.letmelisten.app.utils.TOKEN
import com.letmelisten.app.viewmodels.HomeViewModel
import kotlinx.android.synthetic.main.fragment_home.*


class HomeFragment : BaseFragment(), HomeAdapter.OnItemClickListener, HomeTopItemClickInterface,
    FavUpdateInterface {
    @BindView(R.id.homeRV)
    lateinit var homeRV: RecyclerView

    @BindView(R.id.homeTopRV)
    lateinit var homeTopRV: RecyclerView

    lateinit var mUnbinder: Unbinder
    lateinit var homeAdapter: HomeAdapter
    lateinit var homeTopAdapter: HomeTopAdapter

    var mPageNo: Int = 1
    var mPerPage: Int = 10
    var isLoading: Boolean = true
    var isSel: Boolean = true
    var topFilterType = 0

    private var mPos = 0
    private var mId = ""
    private var mCatId = "1"
    private var checkedPosition = 0
    var onItemClickListener: HomeAdapter.OnItemClickListener? = null
    var onClickListener: HomeTopItemClickInterface? = null

    private var viewModel: HomeViewModel? = null
    private var mCAt: Category? = null
    var mHomeList: ArrayList<DataItemss?>? = ArrayList()
    var mCatList: ArrayList<CategoriesItems?>? = ArrayList()
    var img_fav = false
    var onItemClickk: FavUpdateInterface? = null


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var mView: View = inflater.inflate(R.layout.fragment_home, container, false)
        mUnbinder = ButterKnife.bind(this, mView)
        viewModel = ViewModelProviders.of(this).get(HomeViewModel::class.java)
        onItemClickListener = this
        onClickListener = this
        onItemClickk = this
        if (mHomeList != null) {
            mHomeList!!.clear()
        }
        if (mCatList != null) {
            mCatList!!.clear()
        }
        return mView
    }

    override fun onResume() {
        super.onResume()
        getHomeData()
    }

//    private fun setTopTabs() {
//        for (i in mCatList!!.indices) {
//            mCAt!!.isSelected = i == 0
//            mCatList!!.add(mCAt!!)
//        }
//    }

    private fun setHomeAdapter() {
        val layoutManager: RecyclerView.LayoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        homeRV.layoutManager = layoutManager
        homeAdapter = HomeAdapter(activity, mHomeList, onItemClickListener, mLoadMoreScrollListner)
        homeRV.adapter = homeAdapter
//        homeAdapter.notifyDataSetChanged()
    }

    private fun setTopHomeAdapter() {

        val layoutManager: RecyclerView.LayoutManager =
            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        homeTopRV.layoutManager = layoutManager
        homeTopAdapter = HomeTopAdapter(activity, mCatList, checkedPosition, onClickListener)
        homeTopRV.adapter = homeTopAdapter
//        homeTopAdapter.notifyDataSetChanged()
    }

    private fun getHomeData() {
        if (!activity?.let { isNetworkAvailable(it) }!!) {
            showAlertDialog(activity, getString(R.string.internet_connection_error))
        } else {
            executeHomeRequest()
        }
    }

    /*
     * Execute api param
     * */
    private fun mParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["pageNo"] = mPageNo.toString()
        mMap["perPage"] = mPerPage.toString()
        mMap["categoryId"] = mCatId
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun mHeaderParam(): HashMap<String, String> {
        val headers = HashMap<String, String>()
        activity?.let {
            LetMeListenPreferences().readString(it, TOKEN, null)?.let {
                headers.put(
                    "Token",
                    it
                )
            }
        }
        Log.e(
            TAG,
            "**Token**${activity?.let { LetMeListenPreferences().readString(it, TOKEN, null) }}"
        )
        return headers
    }

    private fun executeHomeRequest() {
        showProgressDialog(activity)
        activity?.let {
            viewModel?.getHomeData(activity, mParam(), mHeaderParam())?.observe(
                it,
                androidx.lifecycle.Observer<HomeModel?> { mHomeModel ->
                    if (mHomeModel.status!!.equals(1)) {
                        dismissProgressDialog()
                        isLoading = !mHomeModel.lastPage!!.equals(true)
                        mHomeList = mHomeModel.data
//                        for (i in mHomeModel.data!!.indices) {
                        mCatList = mHomeModel.categories

                        // }
                        if (activity != null) {
                            if (isSel == true) {
                                mCatList!!.get(0)!!.isSelected = true
                                setTopHomeAdapter()
                                isSel = false
                            }
                        }
                        if (activity != null) {
                            setHomeAdapter()
                            if (mHomeModel.message.equals("No Advisor Found.")) {
                                txtNoDataFountTV.visibility = View.VISIBLE
                                txtNoDataFountTV.text = mHomeModel.message
                            } else {
                                txtNoDataFountTV.visibility = View.GONE
                            }

                        }


                    } else {
                        if (activity != null) {
                            dismissProgressDialog()
                            showToast(activity, mHomeModel.message)
                            txtNoDataFountTV.visibility = View.VISIBLE
                            txtNoDataFountTV.text = mHomeModel.message
                        }

                    }
                })
        }
    }


    var mLoadMoreScrollListner: LoadMoreScrollListner = object : LoadMoreScrollListner {
        override fun onLoadMoreListner(mModel: DataItemss?) {
            if (isLoading) {
                ++mPageNo
                // executeMoreHomeDataRequest()
            }
        }
    }

    /*
    * Execute api param
    * */
    private fun mLoadMoreParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = java.util.HashMap()
        mMap["pageNo"] = mPageNo.toString()
        mMap["perPage"] = mPerPage.toString()
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }


//    private fun executeMoreHomeDataRequest() {
//        mProgressRL.visibility = View.VISIBLE
//        activity?.let {
//            viewModel?.getHomeData(activity, mLoadMoreParam(), mHeaderParam())?.observe(
//                it,
//                androidx.lifecycle.Observer<NewHomeResponce?> { mHomeModel ->
//
//
//
//
//                    if (mHomeModel.status == 1) {
//                        mProgressRL.visibility = View.GONE
//                        mHomeModel.data.let {
//                            mHomeList!!.addAll<NewHomeResponce>(
//                                it
//                            )
//                        }
//                        if (mHomeList!!.size > 0) {
//                            // setProductAdapter();
//                        } else {
//                            homeAdapter.notifyDataSetChanged()
//                        }
//
//                        isLoading = !mHomeModel.lastPage.equals(true)
//
//                    } else if (mHomeModel.status == 0) {
//                        mProgressRL.visibility = View.GONE
////                    showToast(mActivity, mGetFavModel.message)
//                    }
//                })
//        }
//    }


    override fun onDestroy() {
        super.onDestroy()
        mUnbinder.unbind()
    }


    private fun performFavUnFavClick(mPos: Int) {
        if (!activity?.let { isNetworkAvailable(it) }!!) {
            showAlertDialog(activity, getString(R.string.internet_connection_error))
        } else {
            executeFavUnfavRequest(mPos)
        }
    }

    /*
     * Execute api param
     * */
    private fun mFavUnfavParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["advisorId"] = mId
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun mFavUnfavHeaderParam(): HashMap<String, String> {
        val headers = HashMap<String, String>()
        activity?.let {
            LetMeListenPreferences().readString(it, TOKEN, null)?.let {
                headers.put(
                    "Token",
                    it
                )
            }
        }
        return headers
    }

    private fun executeFavUnfavRequest(mPos: Int) {
        showProgressDialog(activity)
        activity?.let {
            viewModel?.getFavUnfavData(activity, mFavUnfavParam(), mFavUnfavHeaderParam())?.observe(
                it,
                androidx.lifecycle.Observer<FavUnfavModel?> { mFavUnfavModel ->
                    if (mFavUnfavModel.status.equals(1)) {
                        dismissProgressDialog()
                        showToast(activity, mFavUnfavModel.message)
                        if (mFavUnfavModel.isFav.equals("1")) {
                            mHomeList!!.get(mPos)!!.isFav = "1"

                            homeAdapter.notifyDataSetChanged()
                        } else {
                            mHomeList!!.get(mPos)!!.isFav = "0"

                            homeAdapter.notifyDataSetChanged()
                        }
                    } else if (mFavUnfavModel.status == 0) {
                        dismissProgressDialog()
                        showToast(activity, mFavUnfavModel.message)
                    } else {
                        dismissProgressDialog()
                        showToast(activity, mFavUnfavModel.message)
                    }
                })
        }
    }

    override fun onItemClick(positon: Int, id: String) {
        this.mPos = positon
        this.mId = id
        performFavUnFavClick(mPos)
    }

    override fun onItemmClick(positon: Int, id: String) {
        this.mPos = positon
        mCatId = id
//        checkedPosition = checkedPosition

//        for (i in mCatList!!.indices) {
//            if (i.equals(positon)) {
//                mCatList!!.get(i).isSelected=true
//            } else {
//                mCatList!!.get(i).isSelected=false
//            }
//        }
//        homeTopAdapter.notifyDataSetChanged()
        mPageNo = 1
        if (mHomeList != null) {
            mHomeList!!.clear()
        }
        getHomeData()
    }


    override fun onItemClickk(id: String, value: Boolean) {
        for (i in mHomeList!!.indices) {
            if (mHomeList!![i]!!.userId.equals(id)) {
                if (value.equals(true)) {
                    mHomeList!![i]!!.isFav = "1"
                    homeAdapter.notifyDataSetChanged()
                } else {
                    mHomeList!![i]!!.isFav = "0"
                    homeAdapter.notifyDataSetChanged()
                }
            }
        }
    }


}