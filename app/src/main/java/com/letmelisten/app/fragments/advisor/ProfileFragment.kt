package com.letmelisten.app.fragments.advisor

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.CompoundButton
import android.widget.TextView
import androidx.appcompat.widget.SwitchCompat
import androidx.lifecycle.ViewModelProviders
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import butterknife.Unbinder
import com.bumptech.glide.Glide
import com.letmelisten.app.Model.StatusMsgModel
import com.letmelisten.app.Model.StatusUpdateModel
import com.letmelisten.app.R
import com.letmelisten.app.activities.HomeActivity
import com.letmelisten.app.activities.SignInActivity
import com.letmelisten.app.activities.WebViewActivity
import com.letmelisten.app.activities.advisor.*
import com.letmelisten.app.fragments.BaseFragment
import com.letmelisten.app.utils.*
import com.letmelisten.app.viewmodels.ProfileEditProfileViewModel
import kotlinx.android.synthetic.main.activity_chat.*
import kotlinx.android.synthetic.main.fragment_profile.*
import java.util.*


class ProfileFragment : BaseFragment() {

    @BindView(R.id.statusSS)
    lateinit var statusSS: SwitchCompat

    @BindView(R.id.chatSS)
    lateinit var chatSS: SwitchCompat

    @BindView(R.id.callSS)
    lateinit var callSS: SwitchCompat

    var statusType: String = ""
    lateinit var mUnbinder: Unbinder
    private var viewModel: ProfileEditProfileViewModel? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val mView: View = inflater.inflate(R.layout.fragment_profile2, container, false)
        mUnbinder = ButterKnife.bind(this, mView)
        viewModel = ViewModelProviders.of(this).get(ProfileEditProfileViewModel::class.java)
        statusSS.setTag(null);
        callSS.setTag(null);

        // setSwitchButtonClick()

        statusSS.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { buttonView, isChecked ->

            if (statusSS.getTag() != null) {
                statusType = "1"

                if (isChecked) {
                    chatSS.setEnabled(true)
                    callSS.setEnabled(true)
                    executeStatusApi()
                } else {
                    callSS.setTag(null);
                    callSS.setEnabled(false)
                    callSS.setChecked(false)
                    executeStatusApi()

                }
            }


        })

        callSS.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { buttonView, isChecked ->

            if (callSS.getTag() != null) {
                statusType = "2"
                executeStatusApi()
            }

        })
//

        return mView
    }

    private fun setSwitchButtonClick() {


    }


    override fun onResume() {
        super.onResume()
        setDataOnWidgets()
    }

    private fun setDataOnWidgets() {


        if (LetMeListenPreferences().readString(requireActivity(), AVALIABLE_STATUS, null)
                .equals("1")
        ) {
            statusSS.setChecked(true)
        }

        if (LetMeListenPreferences().readString(requireActivity(), AVALIABLE_CALL, null)
                .equals("1")
        ) {
            callSS.setChecked(true)
        }
        statusSS.setTag("AddAction")
        callSS.setTag("AddAction")
        activity?.let {
            Glide.with(it)
                .load(LetMeListenPreferences().readString(requireActivity(), ADVISOR_IMAGE, null))
                .placeholder(R.drawable.ph)
                .error(R.drawable.ph)
                .into(profileIV)
        }
        profileEmailTV.text = activity?.let {
            LetMeListenPreferences().readString(
                it,
                Advisor_EMAIL, null
            )
        }

//        profileNameTV.text= context?.let { LetMeListenPreferences().readString(it, FIRST_NAME,null) }
        profileNameTV.text = activity?.let {
            LetMeListenPreferences().readString(
                it,
                Advisor_NAME,
                null
            )
//        } + " " + activity?.let {
//            LetMeListenPreferences().readString(
//                it, LAST_NAME, null
//            )
        }
    }

    @OnClick(
        R.id.editRL,
        R.id.historyRL,
        R.id.changePwdRL,
        R.id.earningsRL,
        R.id.logoutRL,
        R.id.helpRL,
        R.id.statusRL,
        R.id.callRL,
        R.id.chatRL,
        R.id.switchToAsAUserRL
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.editRL -> performEditProfileClick()
            R.id.historyRL -> performHistoryClick()
            R.id.changePwdRL -> performChangePwdClick()
            R.id.earningsRL -> performEarningsClick()
            R.id.logoutRL -> performLogoutClick()
            R.id.helpRL -> performHelpClick()
            R.id.statusRL -> performStatusClick()
            R.id.callRL -> performCallClick()
            R.id.chatRL -> performChatClick()
            R.id.switchToAsAUserRL -> performSignUpAsUserClick()
        }
    }

    private fun performChatClick() {
        showToast(activity, getString(R.string.coming_soon))
    }

    private fun performCallClick() {

    }

    private fun performStatusClick() {

    }

    private fun performSignUpAsUserClick() {
        activity?.let {
            LetMeListenPreferences().writeString(
                it,
                SWITCH_ACCOUNT,
                "user"
            )
        }
//        showToast(activity,getString(R.string.coming_soon))
        startActivity(Intent(activity, HomeActivity::class.java))
    }

    private fun performHelpClick() {
        preventMultipleViewClick()
        val i = Intent(activity, WebViewActivity::class.java)
        i.putExtra(LINK_TYPE, LINK_HELP)
        startActivity(i)
    }


    private fun performLogoutClick() {
        showSignoutAlert(activity, getString(R.string.logout_sure))
    }

    fun showSignoutAlert(mActivity: Activity?, strMessage: String?) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.signout_alert)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        // set the custom dialog components - text, image and button
        val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
        val btnNo = alertDialog.findViewById<TextView>(R.id.btnNo)
        val btnYes = alertDialog.findViewById<TextView>(R.id.btnYes)
        txtMessageTV.text = strMessage
        btnNo.setOnClickListener { alertDialog.dismiss() }
        alertDialog.show()
        btnYes.setOnClickListener {
            logout()
            alertDialog.dismiss()
        }
    }

    private fun logout() {
        if (!activity?.let { isNetworkAvailable(it) }!!) {
            showAlertDialog(activity, getString(R.string.internet_connection_error))
        } else {
            executeLogoutApi()
        }
    }

//    /*
//     * Execute api param
//     * */
//    private fun mlogoutParam(): MutableMap<String?, String?> {
//        val mMap: MutableMap<String?, String?> = HashMap()
//        mMap["isAdvisor"] = "0"
//        Log.e("params", "**PARAM**$mMap")
//        return mMap
//    }

    private fun mLogoutHeader(): HashMap<String, String> {
        val headers = HashMap<String, String>()
        activity?.let {
            LetMeListenPreferences().readString(it, TOKEN, null)?.let {
                headers.put(
                    "Token",
                    it
                )
            }
        }
        Log.e("header", "**PARAM**$headers")
        return headers
    }

    private fun executeLogoutApi() {
        showProgressDialog(activity)
        viewModel?.logoutData(activity, mLogoutHeader())?.observe(this,
            androidx.lifecycle.Observer<StatusMsgModel?> { mLogoutModel ->
                if (mLogoutModel.status == 1) {
                    dismissProgressDialog()
                    showToast(activity, mLogoutModel.message)
                    val preferences: SharedPreferences = activity?.let {
                        LetMeListenPreferences().getPreferences(
                            it
                        )
                    }!!
                    val editor = preferences.edit()
                    editor.clear()
                    editor.apply()
                    this.requireActivity().onBackPressed()
                    val mIntent = Intent(activity, SignInActivity::class.java)
                    mIntent.flags =
                        Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
                    mIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP // To clean up all activities
                    startActivity(mIntent)
                    requireActivity().finish()
                    requireActivity().finishAffinity()
                } else {
                    dismissProgressDialog()
                    showAlertDialog(activity, mLogoutModel.message)
                }
            })
    }

    private fun mStatusParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["type"] = statusType   //type=1->Status , type=2 ->call, type=3 -> chat
        Log.e("params", "**PARAM**$mMap")
        return mMap
    }

    private fun executeStatusApi() {
        showProgressDialog(activity)
        viewModel?.updateStatusData(activity, mStatusParam(), mLogoutHeader())?.observe(this,
            androidx.lifecycle.Observer<StatusUpdateModel?> { mStatusModel ->
                if (mStatusModel.status == 1) {
                    dismissProgressDialog()
//                    showToast(activity, mStatusModel.message)
                    activity?.let {
                        LetMeListenPreferences().writeString(
                            it,
                            AVALIABLE_STATUS,
                            mStatusModel.data?.avaliableStatus
                        )
                    }

                    activity?.let {
                        LetMeListenPreferences().writeString(
                            it,
                            AVALIABLE_CALL,
                            mStatusModel.data?.avaliableCall
                        )
                    }
                    activity?.let {
                        LetMeListenPreferences().writeString(
                            it,
                            AVALIABLE_CHAT,
                            mStatusModel.data?.avaliableChat
                        )
                    }

                } else {
                    dismissProgressDialog()
                    // showToast(activity, mStatusModel.message)
                    activity?.let {
                        LetMeListenPreferences().writeString(
                            it,
                            AVALIABLE_STATUS,
                            mStatusModel.data?.avaliableStatus
                        )
                    }

                    activity?.let {
                        LetMeListenPreferences().writeString(
                            it,
                            AVALIABLE_CALL,
                            mStatusModel.data?.avaliableCall
                        )
                    }
                    activity?.let {
                        LetMeListenPreferences().writeString(
                            it,
                            AVALIABLE_CHAT,
                            mStatusModel.data?.avaliableChat
                        )
                    }

                }
            })
    }

    private fun performEditProfileClick() {
        startActivity(Intent(activity, EditProfileActivity::class.java))
    }

    private fun performHistoryClick() {
        startActivity(Intent(activity, HistoryActivity::class.java))
    }

    private fun performChangePwdClick() {
        startActivity(Intent(activity, ChangePwdActivity::class.java))
    }

    private fun performEarningsClick() {
      //  showToast(activity, getString(R.string.coming_soon))
        startActivity(Intent(activity, EarningsActivity::class.java))
    }

    override fun onDestroy() {
        super.onDestroy()
        mUnbinder.unbind()
    }
}