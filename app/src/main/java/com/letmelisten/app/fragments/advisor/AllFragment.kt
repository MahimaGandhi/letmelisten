package com.letmelisten.app.fragments.advisor

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.Unbinder
import com.letmelisten.app.Interfaces.AdvisorListPaginationInterface
import com.letmelisten.app.Model.AdvisorList
import com.letmelisten.app.Model.AdvisorSessionListModel
import com.letmelisten.app.R
import com.letmelisten.app.adapters.advisor.AllAdapter
import com.letmelisten.app.fragments.BaseFragment
import com.letmelisten.app.utils.ADAPTER_NAME
import com.letmelisten.app.utils.LetMeListenPreferences
import com.letmelisten.app.utils.TOKEN
import com.letmelisten.app.viewmodels.AdvisorViewModel
import kotlinx.android.synthetic.main.fragment_notification.*


class AllFragment : BaseFragment() {

    @BindView(R.id.allRV)
    lateinit var allRV: RecyclerView

    @BindView(R.id.txtNoDataFountTV)
    lateinit var txtNoDataFountTV: TextView

    lateinit var mUnbinder: Unbinder
    lateinit var allAdapter: AllAdapter

    private var viewModel: AdvisorViewModel? = null
    var mPageNo: Int = 1
    var isLoading: Boolean = true
    var mAdvisorList: ArrayList<AdvisorList> = ArrayList()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var mView: View = inflater.inflate(R.layout.fragment_all, container, false)
        mUnbinder = ButterKnife.bind(this, mView)
        viewModel = ViewModelProviders.of(this).get(AdvisorViewModel::class.java)
        return mView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

    }

    override fun onResume() {
        super.onResume()

        executeSessionListRequest()
    }

    override fun onDestroy() {
        super.onDestroy()
        mUnbinder.unbind()
    }

    /*
         * Execute api param
         * */
    private fun mParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["pageNo"] = "1"
        mMap["perPage"] = "200"
        mMap["requestType"] = "0"
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun mHeaderParam(): HashMap<String, String> {
        val headers = HashMap<String, String>()
        activity?.let {
            LetMeListenPreferences().readString(it, TOKEN, null)?.let {
                headers.put(
                    "Token",
                    it
                )
            }
        }
        Log.e(
            TAG,
            "**Token**${activity?.let { LetMeListenPreferences().readString(it, TOKEN, null) }}"
        )
        return headers
    }

    private fun executeSessionListRequest() {
        mAdvisorList.clear()
        showProgressDialog(activity)

        activity?.let {
            viewModel?.getSessionListData(activity, mParam(), mHeaderParam())?.observe(
                it,
                androidx.lifecycle.Observer<AdvisorSessionListModel?> { mSessionModel ->
                    if (mSessionModel.status!!.equals(1)) {
                        dismissProgressDialog()
                        isLoading = !mSessionModel.lastPage!!.equals(true)
                        mAdvisorList = mSessionModel.data
                        initRecyclerView()
                        txtNoDataFountTV.visibility = View.GONE
                    } else {
                        txtNoDataFountTV.visibility = View.VISIBLE
                        txtNoDataFountTV.text = mSessionModel.message
                        dismissProgressDialog()
                        //showAlertDialog(activity, mSessionModel.message)
                    }
                })
        }
    }
    var mAdvisorListPaginationInterface: AdvisorListPaginationInterface =
        object : AdvisorListPaginationInterface {
            override fun onLoadMoreData(mModel: AdvisorList) {
                if (isLoading) {
                    ++mPageNo
                    //   executeSessionListRequest()
                }
            }
        }
    private fun initRecyclerView() {
        val layoutManager: RecyclerView.LayoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        allRV.layoutManager = layoutManager
        allAdapter = AllAdapter(
            activity, mAdvisorList, mAdvisorListPaginationInterface, "allFragment"
        )
        allRV.adapter = allAdapter
    }
}