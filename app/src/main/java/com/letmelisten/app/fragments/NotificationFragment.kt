package com.letmelisten.app.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.Unbinder
import com.letmelisten.app.Interfaces.LoadMoreNotificationsData
import com.letmelisten.app.Model.*
import com.letmelisten.app.R
import com.letmelisten.app.adapters.NotificationAdapter
import com.letmelisten.app.utils.LetMeListenPreferences
import com.letmelisten.app.utils.SWITCH_ACCOUNT
import com.letmelisten.app.utils.TOKEN
import com.letmelisten.app.viewmodels.PaymentViewModel
import kotlinx.android.synthetic.main.fragment_home.mProgressRL
import kotlinx.android.synthetic.main.fragment_notification.*
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.MutableMap
import kotlin.collections.set

class NotificationFragment : BaseFragment(), LoadMoreNotificationsData {

    @BindView(R.id.notificationRV)
    lateinit var notificationRV: RecyclerView
    var rolee:String = ""
    lateinit var mUnbinder: Unbinder
    lateinit var notificationAdapter: NotificationAdapter
    private var viewModel: PaymentViewModel? = null
    var mLoadMore: LoadMoreNotificationsData? = null
    var mNotificationsList: ArrayList<NotificationDataNew>? = ArrayList()
    var mPageNo: Int = 1
    var mPerPage: Int = 10
    var isLoading: Boolean = true

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var mView: View = inflater.inflate(R.layout.fragment_notification, container, false)
        if (activity != null) {
            mUnbinder = ButterKnife.bind(this, mView)
        }

        viewModel = ViewModelProviders.of(this).get(PaymentViewModel::class.java)
        mLoadMore = this
        if (mNotificationsList != null) {
            mNotificationsList!!.clear()
        }
//showAlertDialog(activity,getString(R.string.coming_soon))
        getNotificationsData()
//        initRecyclerView(mView)
        return mView
    }


    private fun getNotificationsData() {
        if (!activity?.let { isNetworkAvailable(it) }!!) {
            showAlertDialog(activity, getString(R.string.internet_connection_error))
        } else {
            executeNotificationsRequest()
        }
    }

    /*
     * Execute api param
     * */
    private fun mParam(): MutableMap<String?, String?> {
        if(activity?.let { LetMeListenPreferences().readString(it, SWITCH_ACCOUNT,"").equals("advisor") }!!)
        {
         rolee = "2"
        }
        else {
          rolee = "1"
        }

        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["pageNo"] = mPageNo.toString()
        mMap["perPage"] = mPerPage.toString()
        mMap["role"] =rolee
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun mHeaderParam(): HashMap<String, String> {
        val headers = HashMap<String, String>()
        activity?.let {
            LetMeListenPreferences().readString(it, TOKEN, null)?.let {
                headers.put(
                    "Token",
                    it
                )
            }
        }
        Log.e(
            TAG,
            "**Token**${activity?.let { LetMeListenPreferences().readString(it, TOKEN, null) }}"
        )
        return headers
    }

    private fun executeNotificationsRequest() {
        showProgressDialog(activity)
        activity?.let {
            viewModel?.getNotificationsData(activity, mParam(), mHeaderParam())?.observe(
                it,
                androidx.lifecycle.Observer<NotificationModelNew?> { mNotificationsModel ->
                    if (mNotificationsModel.status.equals(1)) {
                        dismissProgressDialog()
                        isLoading = !mNotificationsModel.lastPage.equals(true)
                        mNotificationsList = mNotificationsModel.data
                        if (activity != null) {
                            setNotificationsAdapter()
                            txtNoDataFountTV.visibility = View.GONE
                        }

                    } else {
                        dismissProgressDialog()
//                        showToast(activity,mFavModel.message)
                        if (activity != null) {
                            txtNoDataFountTV.visibility = View.VISIBLE
                            txtNoDataFountTV.text = mNotificationsModel.message
                        }

                    }
                })
        }
    }


    private fun setNotificationsAdapter() {
        val layoutManager: RecyclerView.LayoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        notificationRV.layoutManager = layoutManager
        notificationAdapter = NotificationAdapter(activity, mNotificationsList, mLoadMore)
        notificationRV.adapter = notificationAdapter

    }

    /*
    * Execute api param
    * */
    private fun mLoadMoreParam(): MutableMap<String?, String?> {
        if(activity?.let { LetMeListenPreferences().readString(it, SWITCH_ACCOUNT,"").equals("advisor") }!!)
        {
            rolee = "2"
        }
        else {
            rolee = "1"
        }

        val mMap: MutableMap<String?, String?> = java.util.HashMap()
        mMap["pageNo"] = mPageNo.toString()
        mMap["perPage"] = mPerPage.toString()
        mMap["role"] =rolee
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }


    private fun executeMoreNotificationDataRequest() {
        mProgressRL.visibility = View.VISIBLE
        activity?.let {
            viewModel?.getNotificationsData(activity, mLoadMoreParam(), mHeaderParam())?.observe(
                it,
                androidx.lifecycle.Observer<NotificationModelNew?> { mNotificationsModel ->
                    if (mNotificationsModel.status == 1) {
                        if(activity!=null)
                        {
                            mProgressRL.visibility = View.GONE
                            mNotificationsModel.data.let {
                                mNotificationsList!!.addAll<NotificationDataNew>(
                                    it
                                )
                            }
                            notificationAdapter.notifyDataSetChanged()
                            isLoading = !mNotificationsModel.lastPage.equals(true)
                        }
                    } else if (mNotificationsModel.status == 0) {
                        if(activity!=null)
                        {
                            mProgressRL.visibility = View.GONE
                        }

//                    showToast(mActivity, mGetFavModel.message)
                    }
                })
        }
    }

    override fun onLoadMoreData(mModel: NotificationDataNew) {
        if (isLoading) {
            ++mPageNo
            executeMoreNotificationDataRequest()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        mUnbinder.unbind()
    }
}