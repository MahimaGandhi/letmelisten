package com.letmelisten.app.fragments.advisor

import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import butterknife.Unbinder
import com.letmelisten.app.R
import com.letmelisten.app.fragments.BaseFragment


class HomeFragment : BaseFragment() {
    /**
     * Splash Close Time
     */
    val SPLASH_TIME_OUT = 100

    @BindView(R.id.activeAdvisorTV)
    lateinit var activeAdvisorTV: TextView

    @BindView(R.id.completedAdvisorTV)
    lateinit var completedAdvisorTV: TextView

    @BindView(R.id.completedAdvisorView)
    lateinit var completedAdvisorView: View

    @BindView(R.id.activeAdvisorView)
    lateinit var activeAdvisorView: View

    @BindView(R.id.activeAdvisorLL)
    lateinit var activeAdvisorLL: LinearLayout

    @BindView(R.id.completedAdvisorLL)
    lateinit var completedAdvisorLL: LinearLayout

    lateinit var mUnbinder: Unbinder


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstancesState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.fragment_home2, container, false)
        mUnbinder = ButterKnife.bind(this, view)
        Handler().postDelayed(
            {
                performActiveAdvisorClick()
            },
            SPLASH_TIME_OUT.toLong()
        )
        return view
    }

    override fun onResume() {
        super.onResume()

    }

    @OnClick(
        R.id.activeAdvisorLL,
        R.id.completedAdvisorLL
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.activeAdvisorLL -> performActiveAdvisorClick()
            R.id.completedAdvisorLL -> performCompletedClick()
        }
    }

    private fun performActiveAdvisorClick() {
        activeAdvisorTV.setTextColor(resources.getColor(R.color.colorBlack))
        completedAdvisorTV.setTextColor(resources.getColor(R.color.colorLightGreyText))
        completedAdvisorView.background = resources.getDrawable(R.drawable.bg_line_view_us)
        activeAdvisorView.background = resources.getDrawable(R.drawable.bg_line_view_s)
        switchFragment(AllFragment(), "", false, null)
    }

    private fun performCompletedClick() {
        activeAdvisorTV.setTextColor(resources.getColor(R.color.colorLightGreyText))
        completedAdvisorTV.setTextColor(resources.getColor(R.color.colorBlack))
        activeAdvisorView.background = resources.getDrawable(R.drawable.bg_line_view_us)
        completedAdvisorView.background = resources.getDrawable(R.drawable.bg_line_view_s)
        switchFragment(ConfirmedFragment(), "", false, null)
    }


    /*Switch between fragments*/
    override fun switchFragment(
        fragment: Fragment?,
        Tag: String?,
        addToStack: Boolean,
        bundle: Bundle?
    ) {
        val fragmentManager = activity?.supportFragmentManager
        if (fragment != null) {
            val fragmentTransaction =
                fragmentManager?.beginTransaction()
            fragmentTransaction?.replace(R.id.conatinerLL, fragment, Tag)
            if (addToStack) fragmentTransaction?.addToBackStack(Tag)
            if (bundle != null) fragment.arguments = bundle
            fragmentTransaction?.commit()

//            fragmentTransaction.remove(fragment)
            fragmentManager?.executePendingTransactions()
//            fragmentManager.popBackStack()
        }
    }
}

