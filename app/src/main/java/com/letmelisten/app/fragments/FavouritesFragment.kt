package com.letmelisten.app.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.Unbinder
import com.kloxus.app.retrofit.ApiInterface
import com.letmelisten.app.Interfaces.HomeTopItemClickInterface
import com.letmelisten.app.Interfaces.LoadMoreData
import com.letmelisten.app.Interfaces.LoadMoreScrollListner
import com.letmelisten.app.Model.Data
import com.letmelisten.app.Model.FavUnfavModel
import com.letmelisten.app.Model.FavoriteListModel
import com.letmelisten.app.Model.HomeData
import com.letmelisten.app.R
import com.letmelisten.app.adapters.FavouritesAdapter
import com.letmelisten.app.adapters.HomeAdapter
import com.letmelisten.app.utils.LetMeListenPreferences
import com.letmelisten.app.utils.TOKEN
import com.letmelisten.app.viewmodels.HomeViewModel
import kotlinx.android.synthetic.main.fragment_home.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class FavouritesFragment : BaseFragment(),HomeTopItemClickInterface,LoadMoreData{

    @BindView(R.id.favouritesRV)
    lateinit var favouritesRV: RecyclerView

    lateinit var mUnbinder: Unbinder
    lateinit var favouritesAdapter: FavouritesAdapter
    var onClickListener: HomeTopItemClickInterface? = null
    var mLoadMore: LoadMoreData? = null
    var mFaveList: ArrayList<Data>? = ArrayList()
    var mPageNo: Int = 1
    var mPerPage: Int = 10
    var isLoading: Boolean = true
    private var viewModel: HomeViewModel? = null
    private var mPos = 0
    private var mId = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var mView: View = inflater.inflate(R.layout.fragment_favourites, container, false)
        mUnbinder = ButterKnife.bind(this, mView)
        viewModel = ViewModelProviders.of(this).get(HomeViewModel::class.java)
        onClickListener = this
        mLoadMore = this
//        initRecyclerView(mView)

        return mView
    }

    override fun onResume() {
        super.onResume()
        if (mFaveList != null) {
            mFaveList!!.clear()
        }
        getFavData()
    }

    private fun setFavAdapter() {
        val layoutManager: RecyclerView.LayoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        favouritesRV.setLayoutManager(layoutManager)
        favouritesAdapter = FavouritesAdapter(activity, mFaveList, onClickListener, mLoadMore)
        favouritesRV.setAdapter(favouritesAdapter)

    }

    override fun onDestroy() {
        super.onDestroy()
        mUnbinder.unbind()
    }


    private fun getFavData() {
        if (!activity?.let { isNetworkAvailable(it) }!!) {
            showAlertDialog(activity, getString(R.string.internet_connection_error))
        } else {
            executeFavRequest()
        }
    }

    /*
     * Execute api param
     * */
    private fun mParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["pageNo"] = mPageNo.toString()
        mMap["perPage"] = mPerPage.toString()
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun mHeaderParam(): HashMap<String, String> {
        val headers = HashMap<String, String>()
        activity?.let {
            LetMeListenPreferences().readString(it, TOKEN, null)?.let {
                headers.put(
                    "Token",
                    it
                )
            }
        }
        Log.e(TAG, "**Token**${activity?.let { LetMeListenPreferences().readString(it, TOKEN,null) }}")
        return headers
    }

    private fun executeFavRequest() {
        showProgressDialog(activity)
        activity?.let {
            viewModel?.getFavListData(activity, mParam(), mHeaderParam())?.observe(
                it,
                androidx.lifecycle.Observer<FavoriteListModel?> { mFavModel ->
                    if (mFavModel.status.equals(1)) {
                        dismissProgressDialog()
                        isLoading = !mFavModel.lastPage.equals(true)
                        mFaveList = mFavModel.data
                        if (activity != null ) {
                            setFavAdapter()
                            if(txtNoDataFountTV!=null)
                            {
                                txtNoDataFountTV.visibility = View.GONE
                            }
                        }
                    } else {
                        dismissProgressDialog()
//                        showToast(activity,mFavModel.message)
                        if (activity != null) {
                            txtNoDataFountTV.visibility = View.VISIBLE
                            txtNoDataFountTV.text = mFavModel.message
                        }

                    }
                })
        }
    }

    /*
    * Execute api param
    * */
    private fun mLoadMoreParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = java.util.HashMap()
        mMap["pageNo"] = mPageNo.toString()
        mMap["perPage"] = mPerPage.toString()
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }


    private fun executeMoreHomeDataRequest() {
        mProgressRL.visibility = View.VISIBLE
        activity?.let {
            viewModel?.getFavListData(activity, mLoadMoreParam(), mHeaderParam())?.observe(
                it,
                androidx.lifecycle.Observer<FavoriteListModel?> { mFavModel ->
                    if (mFavModel.status == 1) {
                        mProgressRL.visibility = View.GONE
                        mFavModel.data.let {
                            mFaveList!!.addAll<Data>(
                                it
                            )
                        }
                        favouritesAdapter.notifyDataSetChanged()
                        isLoading = !mFavModel.lastPage.equals(true)

                    } else if (mFavModel.status == 0) {
                        mProgressRL.visibility = View.GONE
//                    showToast(mActivity, mGetFavModel.message)
                    }
                })
        }
    }

    override fun onItemmClick(positon: Int, id: String) {
        this.mPos = positon
        this.mId = id
        performFavUnFavClick(mPos)
    }

    override fun onLoadMoreData(mModel: Data) {
        if (isLoading) {
            ++mPageNo
            executeMoreHomeDataRequest()
        }
    }


    private fun performFavUnFavClick(mPos: Int) {
        if (activity?.let { isNetworkAvailable(it) }!!)
            executeFavUnFavRequest(mPos)
        else
            showToast(activity, getString(R.string.internet_connection_error))
    }

    /*
      * Execute api param
      * */
    private fun mFavUnfavParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["advisorId"] = mId
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }


    private fun mFavUnfavHeaderParam(): HashMap<String, String> {
        val headers = HashMap<String, String>()
        activity?.let {
            LetMeListenPreferences().readString(it, TOKEN, null)?.let {
                headers.put(
                    "Token",
                    it
                )
            }
        }
        return headers
    }

    private fun executeFavUnFavRequest(mPos: Int) {
        showProgressDialog(activity)
        activity?.let {
            viewModel?.getFavUnfavData(activity, mFavUnfavParam(), mFavUnfavHeaderParam())?.observe(
                it,
                androidx.lifecycle.Observer<FavUnfavModel?> { mFavUnfavModel ->
                    if (mFavUnfavModel.status.equals(1)) {
                        dismissProgressDialog()
                        showToast(activity, mFavUnfavModel.message)
                        mFaveList!!.removeAt(mPos)
                        favouritesAdapter.notifyDataSetChanged()
                        if (mFaveList!!.size > 0) {
                            favouritesRV.visibility = View.VISIBLE
                            txtNoDataFountTV.visibility = View.GONE
                        } else {
                            favouritesRV.visibility = View.GONE
                            txtNoDataFountTV.visibility = View.VISIBLE
//                            txtNoDataFountTV.text = mFavUnfavModel.message
                        }
                    }
                    else if (mFavUnfavModel.status == 0) {
                        dismissProgressDialog()
                        showToast(activity, mFavUnfavModel.message)
                    } else {
                        dismissProgressDialog()
                        showToast(activity, mFavUnfavModel.message)
                    }
                })
        }
    }

}