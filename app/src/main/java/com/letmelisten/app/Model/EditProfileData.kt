package com.letmelisten.app.Model

data class EditProfileData(
    val allowPush: String,
    val appleToken: String,
    val created: String,
    val disabled: String,
    val email: String,
    val fbToken: String,
    val firstName: String,
    val googleToken: String,
    val isAdvisor: String,
    val lastName: String,
    val password: String,
    val passwordChange: String,
    val passwordChangeCode: String,
    val paypalEmail: String,
    val phoneNo: String,
    val profileImage: String,
    val userId: String,
    val verificationCode: String,
    val verified: String
)