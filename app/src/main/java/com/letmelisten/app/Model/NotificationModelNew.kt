package com.letmelisten.app.Model

data class NotificationModelNew(
    val `data`: ArrayList<NotificationDataNew>,
    val lastPage: String,
    val message: String,
    val status: Int
)