package com.letmelisten.app.Model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class StatusUpdateModel(

	@field:SerializedName("data")
	val data: Datas? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
) : Parcelable

@Parcelize
data class Datas(

	@field:SerializedName("avaliableStatus")
	val avaliableStatus: String? = null,

	@field:SerializedName("avaliableChat")
	val avaliableChat: String? = null,

	@field:SerializedName("avaliableCall")
	val avaliableCall: String? = null
) : Parcelable
