package com.letmelisten.app.Model

data class Amount(
    val amountAdded: String,
    val walletAmount: String
)
