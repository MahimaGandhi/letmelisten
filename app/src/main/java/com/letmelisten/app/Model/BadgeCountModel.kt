package com.letmelisten.app.Model

import com.google.gson.annotations.SerializedName

data class BadgeCountModel(

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("badgeCount")
	val badgeCount: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
)
