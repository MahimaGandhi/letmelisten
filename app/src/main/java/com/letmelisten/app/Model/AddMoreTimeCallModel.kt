package com.letmelisten.app.Model

import com.google.gson.annotations.SerializedName

data class AddMoreTimeCallModel(

	@field:SerializedName("data")
	val data: DataXXx? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
)

data class DataXXx(

	@field:SerializedName("perMinCost")
	val perMinCost: String? = null,

	@field:SerializedName("isBusy")
	val isBusy: String? = null,

	@field:SerializedName("advisorId")
	val advisorId: String? = null,

	@field:SerializedName("sessionStart")
	val sessionStart: String? = null,

	@field:SerializedName("totalTime")
	val totalTime: String? = null,

	@field:SerializedName("created")
	val created: String? = null,

	@field:SerializedName("sessionEnd")
	val sessionEnd: String? = null,

	@field:SerializedName("rating")
	val rating: String? = null,

	@field:SerializedName("sessionId")
	val sessionId: String? = null,

	@field:SerializedName("newWalletAmount")
	val newWalletAmount: String? = null,

	@field:SerializedName("userJoin")
	val userJoin: String? = null,

	@field:SerializedName("userId")
	val userId: String? = null,

	@field:SerializedName("roomId")
	val roomId: String? = null,

	@field:SerializedName("token")
	val token: String? = null,

	@field:SerializedName("totalAmount")
	val totalAmount: String? = null,

	@field:SerializedName("timeAdded")
	val timeAdded: String? = null,

	@field:SerializedName("advisorJoin")
	val advisorJoin: String? = null,

	@field:SerializedName("appId")
	val appId: String? = null,

	@field:SerializedName("startTime")
	val startTime: String? = null,

	@field:SerializedName("channelName")
	val channelName: String? = null,

	@field:SerializedName("endTime")
	val endTime: String? = null,

	@field:SerializedName("status")
	val status: String? = null
)
