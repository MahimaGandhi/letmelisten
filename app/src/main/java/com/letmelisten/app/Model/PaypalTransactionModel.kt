package com.letmelisten.app.Model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PaypalTransactionModel(

	@field:SerializedName("amount")
	val amount: Amountt? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
) : Parcelable

@Parcelize
data class Amountt(

	@field:SerializedName("amountAdded")
	val amountAdded: String? = null,

	@field:SerializedName("walletAmount")
	val walletAmount: String? = null
) : Parcelable
