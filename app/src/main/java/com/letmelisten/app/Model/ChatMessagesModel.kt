package com.letmelisten.app.Model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ChatMessagesModel(
	val lastPage: String? = null,
	val data: ArrayList<DataItem?>? = null,
	val message: String? = null,
	val userDetails: UserDetails? = null,
	val status: Int? = null
) : Parcelable

@Parcelize
data class UserDetails(
	val lastName: String? = null,
	val emailVerification: String? = null,
	val document: String? = null,
	val avaliableCall: String? = null,
	val profileImage: String? = null,
	val phoneNo: String? = null,
	val password: String? = null,
	val passwordChange: String? = null,
	val advisorDescription: String? = null,
	val fbToken: String? = null,
	val customerId: String? = null,
	val advisorPrice: String? = null,
	val disabled: String? = null,
	val email: String? = null,
	val passwordChangeCode: String? = null,
	val avaliableStatus: String? = null,
	val created: String? = null,
	val advisorReq: String? = null,
	val appleToken: String? = null,
	val allowPush: String? = null,
	val paypalEmail: String? = null,
	val userId: String? = null,
	val googleToken: String? = null,
	val verificationCode: String? = null,
	val walletAmount: String? = null,
	val firstName: String? = null,
	val isAdvisor: String? = null,
	val avaliableChat: String? = null,
	val categoryId: String? = null,

) : Parcelable

@Parcelize
data class DataItem(
	val chatId: String? = null,
	val created: String? = null,
	val profileImage: String? = null,
	val message: String? = null,
	val title: String? = null,
	val userId: String? = null,
	val chatStart: String? = null,
	val callEnd: String? = null,
	val chatEnd: String? = null,
	val roomId: String? = null,
	val isTitle: String? = null,
	val name: String? = null,
	val callStart: String? = null,
	var viewType : Int = -1
) : Parcelable
