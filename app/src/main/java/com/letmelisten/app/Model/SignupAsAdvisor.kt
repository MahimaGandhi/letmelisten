package com.letmelisten.app.Model

data class SignupAsAdvisor(
    val `data`: DataXX,
    val message: String,
    val status: Int
)