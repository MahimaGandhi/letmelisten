package com.letmelisten.app.Model

data class SessionHistoryModel(
    val `data`: ArrayList<DataX>,
    val lastPage: String,
    val message: String,
    val status: Int
)