package com.letmelisten.app.Model

data class NotificationsModel(
    val `data`: ArrayList<NotificationsData>,
    val lastPage: String,
    val message: String,
    val status: Int
)