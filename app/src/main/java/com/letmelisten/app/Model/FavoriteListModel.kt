package com.letmelisten.app.Model

data class FavoriteListModel(
    val `data`: ArrayList<Data>,
    val lastPage: String,
    val message: String,
    val status: Int
)