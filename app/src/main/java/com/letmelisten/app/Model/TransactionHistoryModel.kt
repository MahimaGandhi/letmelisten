package com.letmelisten.app.Model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class TransactionHistoryModel(

	@field:SerializedName("lastPage")
	val lastPage: String? = null,

	@field:SerializedName("data")
	val data: ArrayList<DataItemNew?>? = null,

	@field:SerializedName("totalBalance")
	val totalBalance: String? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
) : Parcelable

@Parcelize
data class DataItemNew(

	@field:SerializedName("emailVerification")
	val emailVerification: String? = null,

	@field:SerializedName("transaction_time")
	val transactionTime: String? = null,

	@field:SerializedName("avaliableCall")
	val avaliableCall: String? = null,

	@field:SerializedName("profileImage")
	val profileImage: String? = null,

	@field:SerializedName("type")
	val type: String? = null,

	@field:SerializedName("password")
	val password: String? = null,

	@field:SerializedName("advisorDescription")
	val advisorDescription: String? = null,

	@field:SerializedName("loginAs")
	val loginAs: String? = null,

	@field:SerializedName("avaliableStatus")
	val avaliableStatus: String? = null,

	@field:SerializedName("created")
	val created: String? = null,

	@field:SerializedName("appleToken")
	val appleToken: String? = null,

	@field:SerializedName("allowPush")
	val allowPush: String? = null,

	@field:SerializedName("paypalEmail")
	val paypalEmail: String? = null,

	@field:SerializedName("accessToken")
	val accessToken: String? = null,

	@field:SerializedName("advisorEmail")
	val advisorEmail: String? = null,

	@field:SerializedName("googleToken")
	val googleToken: String? = null,

	@field:SerializedName("firstName")
	val firstName: String? = null,

	@field:SerializedName("paypalCity")
	val paypalCity: String? = null,

	@field:SerializedName("paypalPostalCode")
	val paypalPostalCode: String? = null,

	@field:SerializedName("isAdvisor")
	val isAdvisor: String? = null,

	@field:SerializedName("advisorImage")
	val advisorImage: String? = null,

	@field:SerializedName("avaliableChat")
	val avaliableChat: String? = null,

	@field:SerializedName("paypalCountry")
	val paypalCountry: String? = null,

	@field:SerializedName("lastName")
	val lastName: String? = null,

	@field:SerializedName("document")
	val document: String? = null,

	@field:SerializedName("paypalName")
	val paypalName: String? = null,

	@field:SerializedName("paypalStreet1")
	val paypalStreet1: String? = null,

	@field:SerializedName("phoneNo")
	val phoneNo: String? = null,

	@field:SerializedName("passwordChange")
	val passwordChange: String? = null,

	@field:SerializedName("paypalState")
	val paypalState: String? = null,

	@field:SerializedName("fbToken")
	val fbToken: String? = null,

	@field:SerializedName("customerId")
	val customerId: String? = null,

	@field:SerializedName("advisorPrice")
	val advisorPrice: String? = null,

	@field:SerializedName("disabled")
	val disabled: String? = null,

	@field:SerializedName("paypalID")
	val paypalID: String? = null,

	@field:SerializedName("email")
	val email: String? = null,

	@field:SerializedName("advisorPhoneno")
	val advisorPhoneno: String? = null,

	@field:SerializedName("paypalToken")
	val paypalToken: String? = null,

	@field:SerializedName("paypalMiddleName")
	val paypalMiddleName: String? = null,

	@field:SerializedName("passwordChangeCode")
	val passwordChangeCode: String? = null,

	@field:SerializedName("Amount")
	val amount: String? = null,

	@field:SerializedName("advisorReq")
	val advisorReq: String? = null,

	@field:SerializedName("advisorName")
	val advisorName: String? = null,

	@field:SerializedName("userId")
	val userId: String? = null,

	@field:SerializedName("isFav")
	val isFav: String? = null,

	@field:SerializedName("verificationCode")
	val verificationCode: String? = null,

	@field:SerializedName("walletAmount")
	val walletAmount: String? = null,

	@field:SerializedName("paypalVerifiedAccount")
	val paypalVerifiedAccount: String? = null,

	@field:SerializedName("categoryId")
	val categoryId: String? = null,

	@field:SerializedName("refreshToken")
	val refreshToken: String? = null
) : Parcelable
