package com.letmelisten.app.Model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class AddMoreTimeModel(
	val data: DataMore,
	val message: String? = null,
	val status: Int? = null
) : Parcelable

@Parcelize
data class DataMore(
	val perMinCost: String? = null,
	val isBusy: String? = null,
	val advisorId: String? = null,
	val totalTime: String? = null,
	val created: String? = null,
	val rating: String? = null,
	val sessionId: String? = null,
	val newWalletAmount: String? = null,
	val userId: String? = null,
	val roomId: String? = null,
	val totalAmount: String? = null,
	val timeAdded: String? = null,
	val startTime: String? = null,
	val endTime: String? = null,
	val status: String? = null
) : Parcelable
