package com.letmelisten.app.Model

data class AdvisorDetailsModel(
    val `data`: Data,
    val message: String,
    val status: Int
)