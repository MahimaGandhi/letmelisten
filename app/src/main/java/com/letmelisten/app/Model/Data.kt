package com.letmelisten.app.Model

data class Data(
    val advisorDescription: String,
    val advisorPrice: String,
    val advisorReq: String,
    val allowPush: String,
    val advisorImage: String,
    val advisorPhoneno: String,
    val authToken: String,
    val advisorName: String,
    val appleToken: String,
    val avaliableCall: String,
    val avaliableChat: String,
    val advisorEmail: String,
    val avaliableStatus: String,
    val categoryId: String,
    val created: String,
    val disabled: String,
    val document: String,
    val email: String,
    val emailVerification: String,
    val fbToken: String,
    val firstName: String,
    val googleToken: String,
    val isAdvisor: String,
    val isFav: String,
    val lastName: String,
    val password: String,
    val passwordChange: String,
    val passwordChangeCode: String,
    val paypalEmail: String,
    val phoneNo: String,
    val plan_details: ArrayList<PlanDetail>,
    val profileImage: String,
    val rating: String,
    val userId: String,
    val verificationCode: String,
    val walletAmount: String,
    val totalReviews: Int,
    val perMin: Int
)