package com.letmelisten.app.Model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SendMsgModel(
	val data: DataItem? = null,
	val message: String? = null,
	val status: Int? = null
) : Parcelable
