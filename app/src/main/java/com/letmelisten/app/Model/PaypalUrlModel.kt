package com.letmelisten.app.Model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PaypalUrlModel(
	val login_url: String? = null,
	val accountstatus: String? = null,
	val status: Int? = null
) : Parcelable
