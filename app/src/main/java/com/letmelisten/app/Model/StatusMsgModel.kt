package com.letmelisten.app.Model

data class StatusMsgModel(
    val message: String,
    val status: Int
)