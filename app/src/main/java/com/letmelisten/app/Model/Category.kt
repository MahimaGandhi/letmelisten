package com.letmelisten.app.Model

data class Category(
    val categoryId: String,
    val created: String,
    val description: String,
    val name: String,
    var isSelected: Boolean = false
)