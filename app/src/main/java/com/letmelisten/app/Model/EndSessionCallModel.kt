package com.letmelisten.app.Model

import com.google.gson.annotations.SerializedName

data class EndSessionCallModel(

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
)
