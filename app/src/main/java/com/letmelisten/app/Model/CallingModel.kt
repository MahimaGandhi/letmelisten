package com.letmelisten.app.Model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CallingModel(

	@field:SerializedName("sessionDetails")
	val sessionDetails: SessionDetails? = null,

	@field:SerializedName("advisorDetails")
	val advisorDetails: AdvisorDetails? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("userDetails")
	val userDetails: UserDetailss? = null,

	@field:SerializedName("status")
	val status: Int? = null
) : Parcelable

@Parcelize
data class AdvisorDetails(

	@field:SerializedName("lastName")
	val lastName: String? = null,

	@field:SerializedName("emailVerification")
	val emailVerification: String? = null,

	@field:SerializedName("document")
	val document: String? = null,

	@field:SerializedName("avaliableCall")
	val avaliableCall: String? = null,

	@field:SerializedName("paypalName")
	val paypalName: String? = null,

	@field:SerializedName("profileImage")
	val profileImage: String? = null,

	@field:SerializedName("paypalStreet1")
	val paypalStreet1: String? = null,

	@field:SerializedName("phoneNo")
	val phoneNo: String? = null,

	@field:SerializedName("password")
	val password: String? = null,

	@field:SerializedName("passwordChange")
	val passwordChange: String? = null,

	@field:SerializedName("paypalState")
	val paypalState: String? = null,

	@field:SerializedName("advisorDescription")
	val advisorDescription: String? = null,

	@field:SerializedName("fbToken")
	val fbToken: String? = null,

	@field:SerializedName("customerId")
	val customerId: String? = null,

	@field:SerializedName("advisorPrice")
	val advisorPrice: String? = null,

	@field:SerializedName("disabled")
	val disabled: String? = null,

	@field:SerializedName("loginAs")
	val loginAs: String? = null,

	@field:SerializedName("paypalID")
	val paypalID: String? = null,

	@field:SerializedName("email")
	val email: String? = null,

	@field:SerializedName("advisorPhoneno")
	val advisorPhoneno: String? = null,

	@field:SerializedName("paypalToken")
	val paypalToken: String? = null,

	@field:SerializedName("paypalMiddleName")
	val paypalMiddleName: String? = null,

	@field:SerializedName("passwordChangeCode")
	val passwordChangeCode: String? = null,

	@field:SerializedName("avaliableStatus")
	val avaliableStatus: String? = null,

	@field:SerializedName("created")
	val created: String? = null,

	@field:SerializedName("advisorReq")
	val advisorReq: String? = null,

	@field:SerializedName("appleToken")
	val appleToken: String? = null,

	@field:SerializedName("allowPush")
	val allowPush: String? = null,

	@field:SerializedName("advisorName")
	val advisorName: String? = null,

	@field:SerializedName("paypalEmail")
	val paypalEmail: String? = null,

	@field:SerializedName("accessToken")
	val accessToken: String? = null,

	@field:SerializedName("userId")
	val userId: String? = null,

	@field:SerializedName("advisorEmail")
	val advisorEmail: String? = null,

	@field:SerializedName("googleToken")
	val googleToken: String? = null,

	@field:SerializedName("verificationCode")
	val verificationCode: String? = null,

	@field:SerializedName("walletAmount")
	val walletAmount: String? = null,

	@field:SerializedName("firstName")
	val firstName: String? = null,

	@field:SerializedName("paypalCity")
	val paypalCity: String? = null,

	@field:SerializedName("paypalPostalCode")
	val paypalPostalCode: String? = null,

	@field:SerializedName("isAdvisor")
	val isAdvisor: String? = null,

	@field:SerializedName("advisorImage")
	val advisorImage: String? = null,

	@field:SerializedName("avaliableChat")
	val avaliableChat: String? = null,

	@field:SerializedName("paypalVerifiedAccount")
	val paypalVerifiedAccount: String? = null,

	@field:SerializedName("paypalCountry")
	val paypalCountry: String? = null,

	@field:SerializedName("categoryId")
	val categoryId: String? = null,

	@field:SerializedName("refreshToken")
	val refreshToken: String? = null
) : Parcelable

@Parcelize
data class UserDetailss(

	@field:SerializedName("lastName")
	val lastName: String? = null,

	@field:SerializedName("emailVerification")
	val emailVerification: String? = null,

	@field:SerializedName("document")
	val document: String? = null,

	@field:SerializedName("avaliableCall")
	val avaliableCall: String? = null,

	@field:SerializedName("paypalName")
	val paypalName: String? = null,

	@field:SerializedName("profileImage")
	val profileImage: String? = null,

	@field:SerializedName("paypalStreet1")
	val paypalStreet1: String? = null,

	@field:SerializedName("phoneNo")
	val phoneNo: String? = null,

	@field:SerializedName("password")
	val password: String? = null,

	@field:SerializedName("passwordChange")
	val passwordChange: String? = null,

	@field:SerializedName("paypalState")
	val paypalState: String? = null,

	@field:SerializedName("advisorDescription")
	val advisorDescription: String? = null,

	@field:SerializedName("fbToken")
	val fbToken: String? = null,

	@field:SerializedName("customerId")
	val customerId: String? = null,

	@field:SerializedName("advisorPrice")
	val advisorPrice: String? = null,

	@field:SerializedName("disabled")
	val disabled: String? = null,

	@field:SerializedName("loginAs")
	val loginAs: String? = null,

	@field:SerializedName("paypalID")
	val paypalID: String? = null,

	@field:SerializedName("email")
	val email: String? = null,

	@field:SerializedName("advisorPhoneno")
	val advisorPhoneno: String? = null,

	@field:SerializedName("paypalToken")
	val paypalToken: String? = null,

	@field:SerializedName("paypalMiddleName")
	val paypalMiddleName: String? = null,

	@field:SerializedName("passwordChangeCode")
	val passwordChangeCode: String? = null,

	@field:SerializedName("avaliableStatus")
	val avaliableStatus: String? = null,

	@field:SerializedName("created")
	val created: String? = null,

	@field:SerializedName("advisorReq")
	val advisorReq: String? = null,

	@field:SerializedName("appleToken")
	val appleToken: String? = null,

	@field:SerializedName("allowPush")
	val allowPush: String? = null,

	@field:SerializedName("advisorName")
	val advisorName: String? = null,

	@field:SerializedName("paypalEmail")
	val paypalEmail: String? = null,

	@field:SerializedName("accessToken")
	val accessToken: String? = null,

	@field:SerializedName("userId")
	val userId: String? = null,

	@field:SerializedName("advisorEmail")
	val advisorEmail: String? = null,

	@field:SerializedName("googleToken")
	val googleToken: String? = null,

	@field:SerializedName("verificationCode")
	val verificationCode: String? = null,

	@field:SerializedName("walletAmount")
	val walletAmount: String? = null,

	@field:SerializedName("firstName")
	val firstName: String? = null,

	@field:SerializedName("paypalCity")
	val paypalCity: String? = null,

	@field:SerializedName("paypalPostalCode")
	val paypalPostalCode: String? = null,

	@field:SerializedName("isAdvisor")
	val isAdvisor: String? = null,

	@field:SerializedName("advisorImage")
	val advisorImage: String? = null,

	@field:SerializedName("avaliableChat")
	val avaliableChat: String? = null,

	@field:SerializedName("paypalVerifiedAccount")
	val paypalVerifiedAccount: String? = null,

	@field:SerializedName("paypalCountry")
	val paypalCountry: String? = null,

	@field:SerializedName("categoryId")
	val categoryId: String? = null,

	@field:SerializedName("refreshToken")
	val refreshToken: String? = null
) : Parcelable

@Parcelize
data class SessionDetails(

	@field:SerializedName("perMinCost")
	val perMinCost: String? = null,

	@field:SerializedName("isBusy")
	val isBusy: String? = null,

	@field:SerializedName("advisorId")
	val advisorId: String? = null,

	@field:SerializedName("sessionStart")
	val sessionStart: String? = null,

	@field:SerializedName("totalTime")
	val totalTime: String? = null,

	@field:SerializedName("created")
	val created: String? = null,

	@field:SerializedName("sessionEnd")
	val sessionEnd: String? = null,

	@field:SerializedName("rating")
	val rating: String? = null,

	@field:SerializedName("sessionId")
	val sessionId: String? = null,

	@field:SerializedName("userJoin")
	val userJoin: String? = null,

	@field:SerializedName("userId")
	val userId: String? = null,

	@field:SerializedName("roomId")
	val roomId: String? = null,

	@field:SerializedName("token")
	val token: String? = null,

	@field:SerializedName("totalAmount")
	val totalAmount: String? = null,

	@field:SerializedName("timeAdded")
	val timeAdded: String? = null,

	@field:SerializedName("advisorJoin")
	val advisorJoin: String? = null,

	@field:SerializedName("appId")
	val appId: String? = null,

	@field:SerializedName("startTime")
	val startTime: String? = null,

	@field:SerializedName("channelName")
	val channelName: String? = null,

	@field:SerializedName("endTime")
	val endTime: String? = null,

	@field:SerializedName("status")
	val status: String? = null
) : Parcelable
