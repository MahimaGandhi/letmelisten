package com.letmelisten.app.Model

data class SessionCreateModel(
    val data: SesssionData? = null,
    val message: String? = null,
    val status: Int? = null
)

data class SesssionData(
    val perMinCost: String? = null,
    val isBusy: String? = null,
    val advisorId: String? = null,
    val totalTime: String? = null,
    val created: String? = null,
    val rating: String? = null,
    val sessionId: String? = null,
    val userId: String? = null,
    val roomId: String? = null,
    val totalAmount: String? = null,
    val timeAdded: String? = null,
    val startTime: String? = null,
    val endTime: String? = null,
    val status: String? = null
)

