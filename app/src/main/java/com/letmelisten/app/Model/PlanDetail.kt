package com.letmelisten.app.Model

data class PlanDetail(
    val created: String,
    val description: String,
    val minute: String,
    val planId: String,
    val price: String
)