package com.letmelisten.app.Model

data class FavUnfavModel(
    val isFav: String,
    val message: String,
    val status: Int
)