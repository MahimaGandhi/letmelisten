package com.letmelisten.app.Model

data class LoginModel(
    val `data`: Data,
    val message: String,
    val status: Int
)