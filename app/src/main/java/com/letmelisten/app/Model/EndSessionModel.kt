package com.letmelisten.app.Model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class EndSessionModel(
	val message: String? = null,
	val status: Int? = null
) : Parcelable
