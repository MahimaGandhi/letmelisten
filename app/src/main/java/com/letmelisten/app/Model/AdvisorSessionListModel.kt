package com.letmelisten.app.Model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class AdvisorSessionListModel(
	val lastPage: String? = null,
	val data: ArrayList<AdvisorList>,
	val message: String? = null,
	val status: Int? = null
) : Parcelable

@Parcelize
data class AdvisorList(
	val perMinCost: String? = null,
	val isBusy: String? = null,
	val advisorId: String? = null,
	val totalTime: String? = null,
	val created: String? = null,
	val rating: String? = null,
	val lastMessage: String? = null,
	val sessionId: String? = null,
	val profileImage: String? = null,
	val userId: String? = null,
	val roomId: String? = null,
	val name: String? = null,
	val totalAmount: String? = null,
	val timeAdded: String? = null,
	val msgTime: String? = null,
	val startTime: String? = null,
	val endTime: String? = null,
	val status: String? = null
) : Parcelable
