package com.letmelisten.app.Model

data class EditProfileModel(
    val `data`: EditProfileData,
    val message: String,
    val status: Int
)