package com.letmelisten.app.Model

data class SessionData(
    val advisorId: String,
    val created: String,
    val endTime: String,
    val isBusy: String,
    val perMinCost: String,
    val rating: String,
    val roomId: String,
    val sessionId: String,
    val startTime: String,
    val status: String,
    val timeAdded: String,
    val totalAmount: String,
    val totalTime: String,
    val userId: String
)