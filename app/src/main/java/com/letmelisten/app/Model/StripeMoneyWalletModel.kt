package com.letmelisten.app.Model

data class StripeMoneyWalletModel(
	val Amount: Amount,
	val message: String,
	val status: Int
)
