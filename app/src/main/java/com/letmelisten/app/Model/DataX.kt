package com.letmelisten.app.Model

data class DataX(
    val advisorDescription: String,
    val advisorPrice: String,
    val advisorReq: String,
    val allowPush: String,
    val sessionEnd: String,

    val appleToken: String,
    val title: String,
    val duration: String,
    val avaliableCall: String,
    val avaliableChat: String,
    val avaliableStatus: String,
    val categoryId: String,
    val created: String,
    val disabled: String,
    val email: String,
    val emailVerification: String,
    val fbToken: String,
    val firstName: String,
    val googleToken: String,
    val isAdvisor: String,
    val isFav: String,
    val lastName: String,
    val password: String,
    val passwordChange: String,
    val passwordChangeCode: String,
    val paypalEmail: String,
    val phoneNo: String,
    val profileImage: String,
    val rating: String,
    val userId: String,
    val verificationCode: String,
    val walletAmount: String
)