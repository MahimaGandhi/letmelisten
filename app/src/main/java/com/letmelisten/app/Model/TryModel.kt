package com.letmelisten.app.Model

import com.google.gson.annotations.SerializedName

data class DataItemm(

    @field:SerializedName("category_id")
    var categoryId: String? = null,

    @field:SerializedName("created")
    val created: String? = null,

    @field:SerializedName("name")
    var name: String? = null,

    @field:SerializedName("description")
    val description: String? = null
)