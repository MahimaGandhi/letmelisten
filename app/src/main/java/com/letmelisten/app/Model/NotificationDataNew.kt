package com.letmelisten.app.Model

data class NotificationDataNew(
    val created: String,
    val description: String,
    val detailId: String,
    val lastMessage: String,
    val msgTime: String,
    val name: String,
    val notificationId: String,
    val notificationReadStatus: String,
    val notificationType: String,
    val otherId: String,
    val profileImage: String,
    val roomId: String,
    val title: String,
    val userId: String,
    val duration: String
)