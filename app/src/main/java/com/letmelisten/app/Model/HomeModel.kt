package com.letmelisten.app.Model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class HomeModel(
	val lastPage: String? = null,
	val data: ArrayList<DataItemss?>? = null,
	val categories: ArrayList<CategoriesItems?>? = null,
	val message: String? = null,
	val status: Int? = null
) : Parcelable

@Parcelize
data class CategoriesItems(
	val category_id: String? = null,
	val created: String? = null,
	val name: String? = null,
	val description: String? = null,
	var isSelected: Boolean = false,
) : Parcelable

@Parcelize
data class DataItemss(
	val lastName: String? = null,
	val emailVerification: String? = null,
	val document: String? = null,
	val rating: String? = null,
	val avaliableCall: String? = null,
	val paypalName: String? = null,
	val profileImage: String? = null,
	val paypalStreet1: String? = null,
	val phoneNo: String? = null,
	val password: String? = null,
	val passwordChange: String? = null,
	val paypalState: String? = null,
	val advisorDescription: String? = null,
	val fbToken: String? = null,
	val customerId: String? = null,
	val advisorPrice: String? = null,
	val disabled: String? = null,
	val paypalID: String? = null,
	val email: String? = null,
	val advisorPhoneno: String? = null,
	val paypalToken: String? = null,
	val paypalMiddleName: String? = null,
	val passwordChangeCode: String? = null,
	val avaliableStatus: String? = null,
	val created: String? = null,
	val advisorReq: String? = null,
	val appleToken: String? = null,
	val allowPush: String? = null,
	val advisorName: String? = null,
	val paypalEmail: String? = null,
	val accessToken: String? = null,
	val userId: String? = null,
	var isFav: String? = null,
	val advisorEmail: String? = null,
	val googleToken: String? = null,
	val verificationCode: String? = null,
	val walletAmount: String? = null,
	val firstName: String? = null,
	val paypalCity: String? = null,
	val paypalPostalCode: String? = null,
	val isAdvisor: String? = null,
	val advisorImage: String? = null,
	val avaliableChat: String? = null,
	val paypalVerifiedAccount: String? = null,
	val paypalCountry: String? = null,
	val planDetails: List<PlanDetailsItems?>? = null,
	val categoryId: String? = null,
	val refreshToken: String? = null
) : Parcelable

@Parcelize
data class PlanDetailsItems(
	val price: String? = null,
	val created: String? = null,
	val description: String? = null,
	val planId: String? = null,
	val minute: String? = null
) : Parcelable
