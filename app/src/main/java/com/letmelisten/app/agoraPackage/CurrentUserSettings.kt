package com.letmelisten.app.agoraPackage

class CurrentUserSettings {
    var mEncryptionModeIndex = 0

    var mEncryptionKey: String? = null

    var mChannelName: String? = null

    fun CurrentUserSettings() {
        reset()
    }

    fun reset() {}
}