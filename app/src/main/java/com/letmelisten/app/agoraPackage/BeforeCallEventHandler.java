package com.letmelisten.app.agoraPackage;

import io.agora.rtc.IRtcEngineEventHandler;

public interface BeforeCallEventHandler extends AGEventHandler {
    void onLastmileQuality(int quality);
    void onLastmileProbeResult(IRtcEngineEventHandler.LastmileProbeResult result);
}
