package com.letmelisten.app.agoraPackage

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.Context

import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.ConnectivityManager
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.SystemClock
import android.text.TextUtils
import android.util.DisplayMetrics
import android.view.*
import android.view.ViewTreeObserver.OnGlobalLayoutListener
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.letmelisten.app.LetMeListenApplication
import com.letmelisten.app.Model.ConstantApp
import com.letmelisten.app.R
import com.letmelisten.app.utils.IS_LOGIN
import com.letmelisten.app.utils.LetMeListenPreferences
import com.letmelisten.app.utils.USER_ID
import io.agora.rtc.RtcEngine
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.io.ByteArrayOutputStream
import java.util.*
import java.util.regex.Pattern

abstract class VideoBaseActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val layout = findViewById<View>(Window.ID_ANDROID_CONTENT)
        val vto = layout.viewTreeObserver
        vto.addOnGlobalLayoutListener(object : OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    layout.viewTreeObserver.removeOnGlobalLayoutListener(this)
                } else {
                    layout.viewTreeObserver.removeGlobalOnLayoutListener(this)
                }
                initUIandEvent()
            }
        })
    }

    protected abstract fun initUIandEvent()
    protected abstract fun deInitUIandEvent()
    protected fun permissionGranted() {}
    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        Handler().postDelayed(Runnable {
            if (isFinishing) {
                return@Runnable
            }
            val checkPermissionResult = checkSelfPermissions()
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                // so far we do not use OnRequestPermissionsResultCallback
            }
        }, 500)
    }

    private fun checkSelfPermissions(): Boolean {
        return checkSelfPermission(
            Manifest.permission.RECORD_AUDIO,
            ConstantApp.PERMISSION_REQ_ID_RECORD_AUDIO
        ) &&
                checkSelfPermission(
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    ConstantApp.PERMISSION_REQ_ID_WRITE_EXTERNAL_STORAGE
                )
    }

    override fun onDestroy() {
        deInitUIandEvent()
        super.onDestroy()
    }

    fun closeIME(v: View) {
        val mgr = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
        mgr.hideSoftInputFromWindow(v.windowToken, 0) // 0 force close IME
        v.clearFocus()
    }

    open fun checkSelfPermission(permission: String, requestCode: Int): Boolean {
        log.debug("checkSelfPermission $permission $requestCode")
        if (ContextCompat.checkSelfPermission(
                this,
                permission
            )
            != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this, arrayOf(permission),
                requestCode
            )
            return false
        }
        if (Manifest.permission.CAMERA == permission) {
            permissionGranted()
        }
        return true
    }

    protected fun application(): LetMeListenApplication {
        return application as LetMeListenApplication
    }

    protected fun rtcEngine(): RtcEngine {
        return application().rtcEngine()!!
    }

    protected fun config(): EngineConfig {
        return application().config()!!
    }

    protected fun addEventHandler(handler: AGEventHandler?) {
        application().addEventHandler(handler)
    }

    protected fun removeEventHandler(handler: AGEventHandler?) {
        application().remoteEventHandler(handler)
    }

    protected fun vSettings(): CurrentUserSettings? {
        return application()?.userSettings()
    }

    open fun showLongToast(msg: String?) {
        runOnUiThread { Toast.makeText(applicationContext, msg, Toast.LENGTH_LONG).show() }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>, grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        log.debug(
            "onRequestPermissionsResult " + requestCode + " " + Arrays.toString(permissions) + " " + Arrays.toString(
                grantResults
            )
        )
        when (requestCode) {
            ConstantApp.PERMISSION_REQ_ID_RECORD_AUDIO -> {
                if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    checkSelfPermission(
                        Manifest.permission.CAMERA,
                        ConstantApp.PERMISSION_REQ_ID_WRITE_EXTERNAL_STORAGE
                    )
                } else {
                    finish()
                }
            }

            ConstantApp.PERMISSION_REQ_ID_WRITE_EXTERNAL_STORAGE -> {
                if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                } else {
                    finish()
                }
            }
        }
    }

    protected fun virtualKeyHeight(): Int {
        val hasPermanentMenuKey = ViewConfiguration.get(application).hasPermanentMenuKey()
        if (hasPermanentMenuKey) {
            return 0
        }

        // Also can use getResources().getIdentifier("navigation_bar_height", "dimen", "android");
        val metrics = DisplayMetrics()
        val display = windowManager.defaultDisplay
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            display.getRealMetrics(metrics)
        } else {
            display.getMetrics(metrics)
        }
        var fullHeight = metrics.heightPixels
        var fullWidth = metrics.widthPixels
        if (fullHeight < fullWidth) {
            fullHeight = fullHeight xor fullWidth
            fullWidth = fullWidth xor fullHeight
            fullHeight = fullHeight xor fullWidth
        }
        display.getMetrics(metrics)
        var newFullHeight = metrics.heightPixels
        var newFullWidth = metrics.widthPixels
        if (newFullHeight < newFullWidth) {
            newFullHeight = newFullHeight xor newFullWidth
            newFullWidth = newFullWidth xor newFullHeight
            newFullHeight = newFullHeight xor newFullWidth
        }
        var virtualKeyHeight = fullHeight - newFullHeight
        if (virtualKeyHeight > 0) {
            return virtualKeyHeight
        }
        virtualKeyHeight = fullWidth - newFullWidth
        return virtualKeyHeight
    }

    // status bar height
    protected val statusBarHeight: Int
        protected get() {
            // status bar height
            var statusBarHeight = 0
            val resourceId = resources.getIdentifier("status_bar_height", "dimen", "android")
            if (resourceId > 0) {
                statusBarHeight = resources.getDimensionPixelSize(resourceId)
            }
            if (statusBarHeight == 0) {
                log.error("Can not get height of status bar")
            }
            return statusBarHeight
        }

    // action bar height
    protected val actionBarHeight: Int
        protected get() {
            // action bar height
            var actionBarHeight = 0
            val styledAttributes =
                this.theme.obtainStyledAttributes(intArrayOf(R.attr.actionBarSize))
            actionBarHeight = styledAttributes.getDimension(0, 0f).toInt()
            styledAttributes.recycle()
            if (actionBarHeight == 0) {
                log.error("Can not get height of action bar")
            }
            return actionBarHeight
        }

    /**
     * Starts/Stops the local video preview
     *
     *
     * Before calling this method, you must:
     * Call the enableVideo method to enable the video.
     *
     * @param start Whether to start/stop the local preview
     * @param view  The SurfaceView in which to render the preview
     * @param uid   User ID.
     */


    /**
     * Allows a user to join a channel.
     *
     *
     * Users in the same channel can talk to each other, and multiple users in the same channel can start a group chat. Users with different App IDs cannot call each other.
     *
     *
     * You must call the leaveChannel method to exit the current call before joining another channel.
     *
     *
     * A successful joinChannel method call triggers the following callbacks:
     *
     *
     * The local client: onJoinChannelSuccess.
     * The remote client: onUserJoined, if the user joining the channel is in the Communication profile, or is a BROADCASTER in the Live Broadcast profile.
     *
     *
     * When the connection between the client and Agora's server is interrupted due to poor
     * network conditions, the SDK tries reconnecting to the server. When the local client
     * successfully rejoins the channel, the SDK triggers the onRejoinChannelSuccess callback
     * on the local client.
     *
     * @param channel The unique channel name for the AgoraRTC session in the string format.
     * @param uid     User ID.
     */
    fun joinChannel(channel: String, uid: Int) {
        // String accessToken = getApplicationContext().getString(R.string.agora_access_token);
//        if (TextUtils.equals(accessToken, "") || TextUtils.equals(accessToken, "006a57ec5cb20ee4873b58010930a487126IAAT5i2J9N46Ib2BkiE4dXEnFOFyvJrK5DeAtw3VaDbXUp5wcHYAAAAAEABwy0iQMTe6XgEAAQAuN7pe")) {
////            accessToken = null; // default, no token
//        }

        rtcEngine().joinChannel("", channel, "DEMO", uid)
        config().mChannel = channel
        log.debug("joinChannel $channel $uid")
    }

    /**
     * Allows a user to leave a channel.
     *
     *
     * After joining a channel, the user must call the leaveChannel method to end the call before
     * joining another channel. This method returns 0 if the user leaves the channel and releases
     * all resources related to the call. This method call is asynchronous, and the user has not
     * exited the channel when the method call returns. Once the user leaves the channel,
     * the SDK triggers the onLeaveChannel callback.
     *
     *
     * A successful leaveChannel method call triggers the following callbacks:
     *
     *
     * The local client: onLeaveChannel.
     * The remote client: onUserOffline, if the user leaving the channel is in the
     * Communication channel, or is a BROADCASTER in the Live Broadcast profile.
     *
     * @param channel Channel Name
     */
    fun leaveChannel(channel: String) {
        log.debug("leaveChannel $channel")
        config().mChannel = null
        rtcEngine().leaveChannel()
        config().reset()
    }

    /**
     * Enables image enhancement and sets the options.
     */
//    protected fun enablePreProcessor() {
//        if (Constant.BEAUTY_EFFECT_ENABLED) {
//            rtcEngine().setBeautyEffectOptions(true, Constant.BEAUTY_OPTIONS)
//        }
//    }
//
//    fun setBeautyEffectParameters(lightness: Float, smoothness: Float, redness: Float) {
//        Constant.BEAUTY_OPTIONS.lighteningLevel = lightness
//        Constant.BEAUTY_OPTIONS.smoothnessLevel = smoothness
//        Constant.BEAUTY_OPTIONS.rednessLevel = redness
//    }

    /**
     * Disables image enhancement.
     */
//    protected fun disablePreProcessor() {
//        // do not support null when setBeautyEffectOptions to false
//        rtcEngine().setBeautyEffectOptions(false, Constant.BEAUTY_OPTIONS)
//    }



    companion object {
        private val log = LoggerFactory.getLogger(VideoBaseActivity::class.java)
    }



    /*
   * Get Class Name
   * */
    var TAG = this@VideoBaseActivity.javaClass.simpleName
    private val log: Logger =
        LoggerFactory.getLogger(com.letmelisten.app.activities.BaseActivity::class.java)
    /*
    * Initialize Activity
    * */
    var mActivity: Activity = this@VideoBaseActivity
    private var mLastClickTab1: Long = 0

    /*
    * Initialize Other Classes Objects...
    * */
    var progressDialog: Dialog? = null

    /*
   * Is User Login
   * */
    fun isUserLogin() : Boolean{
        return LetMeListenPreferences().readBoolean(mActivity, IS_LOGIN, false)
    }






    /*
Transparent status bar
 */
    fun setStatusBar(mActivity: Activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            mActivity.window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            // edited here
            mActivity.window.statusBarColor = resources.getColor(R.color.colorWhite)
        }
    }

    /*
Clear editText focus
 */
    fun setEditTextFocused(mActivity: Activity, mEditText: EditText) {
        mEditText.setOnEditorActionListener(TextView.OnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                mEditText.clearFocus()
                val imm = v.context
                    .getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(v.windowToken, 0)
                return@OnEditorActionListener true
            }
            false
        })
    }

    /*
     *Hide/Close Keyboard forcefully
     *
     * */
    open fun hideCloseKeyboard(activity: Activity) {
        val imm = activity.getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
        //Find the currently focused view, so we can grab the correct window token from it.
        var view = activity.currentFocus
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = View(activity)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    open fun convertBitmapToByteArrayUncompressed(bitmap: Bitmap): ByteArray? {
        val stream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 40, stream)
        return stream.toByteArray()
    }

    open fun getAlphaNumericString(): String? {
        val n = 20

        // chose a Character random from this String
        val AlphaNumericString = ("ABCDEFGHIJKLMNOPQRSTUVWXYZ" /*+ "0123456789"*/
                + "abcdefghijklmnopqrstuvxyz")

        // create StringBuffer size of AlphaNumericString
        val sb = StringBuilder(n)
        for (i in 0 until n) {

            // generate a random number between
            // 0 to AlphaNumericString variable length
            val index = (AlphaNumericString.length
                    * Math.random()).toInt()

            // add Character one by one in end of sb
            sb.append(
                AlphaNumericString[index]
            )
        }
        return sb.toString()
    }

    /*Switch between fragments*/
    fun switchFragment(
        fragment: Fragment?,
        Tag: String?,
        addToStack: Boolean,
        bundle: Bundle?
    ) {
        val fragmentManager = supportFragmentManager
        if (fragment != null) {
            val fragmentTransaction =
                fragmentManager.beginTransaction()
            fragmentTransaction.replace(R.id.mainFL, fragment, Tag)
            if (addToStack) fragmentTransaction.addToBackStack(Tag)
            if (bundle != null) fragment.arguments = bundle
            fragmentTransaction.commit()

//            fragmentTransaction.remove(fragment)
            fragmentManager.executePendingTransactions()
//            fragmentManager.popBackStack()
        }
    }
//    /*Switch between fragments(advisor flow)*/
//    fun switchFragment2(
//        fragment: Fragment?,
//        Tag: String?,
//        addToStack: Boolean,
//        bundle: Bundle?
//    ) {
//        val fragmentManager = supportFragmentManager
//        if (fragment != null) {
//            val fragmentTransaction =
//                fragmentManager.beginTransaction()
//            fragmentTransaction.replace(R.id.nav_host_fragment2, fragment, Tag)
//            if (addToStack) fragmentTransaction.addToBackStack(Tag)
//            if (bundle != null) fragment.arguments = bundle
//            fragmentTransaction.commit()
//            fragmentManager.executePendingTransactions()
//        }
//    }

    /*
         * Validate Email Address
         * */
    fun isValidEmailId(email: String?): Boolean {
        return Pattern.compile(
            "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                    + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                    + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                    + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$"
        ).matcher(email).matches()
    }

    /*
         * Validate Phone Number
         * */
    fun isValidPhoneNumber(phNumber: String): Boolean {
        return Pattern.compile(
            "^\\s*(?:\\+?(\\d{1,3}))?[-. (]*(\\d{3})[-. )]*(\\d{3})[-. ]*(\\d{4})(?: *x(\\d+))?\\s*$"
        ).matcher(phNumber.toString()).matches()
    }

    /*
     *
     * Error Alert Dialog
     * */
    fun showAlertDialog(mActivity: Activity?, strMessage: String?) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_alert)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
        val btnDismiss = alertDialog.findViewById<TextView>(R.id.btnDismiss)
        txtMessageTV.text = strMessage
        btnDismiss.setOnClickListener { alertDialog.dismiss() }
        alertDialog.show()
    }

    /*
    *
    * Error Alert Dialog
    * */
    fun showFinishAlertDialog(mActivity: Activity?, strMessage: String?) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_alert)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
        val btnDismiss = alertDialog.findViewById<TextView>(R.id.btnDismiss)
        txtMessageTV.text = strMessage
        btnDismiss.setOnClickListener { alertDialog.dismiss()
            finish()}
        alertDialog.show()
    }

    open fun preventMultipleClick() {
        // Preventing multiple clicks, using threshold of 1 second
        if (SystemClock.elapsedRealtime() - mLastClickTab1 < 2000) {
            return
        }
        mLastClickTab1 = SystemClock.elapsedRealtime()
    }




    /*
     * Check Internet Connections
     * */
    fun isNetworkAvailable(mContext: Context): Boolean {
        val connectivityManager =
            mContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }


    /*
     * Toast Message
     * */
    fun showToast(mActivity: Activity?, strMessage: String?) {
        Toast.makeText(mActivity, strMessage, Toast.LENGTH_SHORT).show()
    }


    /*
     * Show Progress Dialog
     * */

    fun  showProgressDialog(mActivity: Activity?) {
        progressDialog = Dialog(mActivity!!)
        progressDialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        progressDialog!!.setContentView(R.layout.dialog_progress)
        Objects.requireNonNull(progressDialog!!.window)?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        progressDialog!!.setCanceledOnTouchOutside(false)
        progressDialog!!.setCancelable(false)
        if (progressDialog != null) progressDialog!!.show()
    }


    /*
     * Hide Progress Dialog
     * */
    fun dismissProgressDialog() {
        try {
            if (this.progressDialog != null && this.progressDialog!!.isShowing()) {
                this.progressDialog!!.dismiss()
            }
        } catch (e: IllegalArgumentException) {
            // Handle or log or ignore
        } catch (e: Exception) {
            // Handle or log or ignore
        } finally {
            this.progressDialog = null
        }
    }

    /*
      * Getting User ID
      * */
    fun getLoggedInUserID(): String {
        return LetMeListenPreferences().readString(mActivity, USER_ID, "")!!
    }


}
